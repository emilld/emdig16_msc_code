# Introduction
This repository contains the code used in my MSc thesis titled *Machine Learning Methods for Reliable Detection of Faults for Drones*, [1].

# Organization of the Repository
The repository is organized into three directories as follows:

- `cpp_fault_tolerant_control` is a `C++` project containing code for adaptive extended Kalman filter, fault-tolerant MPC, hexacopter dynamic model, echo-state network execution, etc. Used in [3].
- `python_esn_aekf` is a `Python` project containing code for adaptive extended Kalman filter, hexacopter dynamic model, echo-state network training and execution, etc. Used in [2], [3].
- `ros_docker` contains a definition of a dockerfile and a workspace with ROS nodes implementing the adaptive extended Kalman filter for the indoor flight experiments in [2].

The thesis manuscript is found in `emdig16_thesis.pdf`.

A video showing the indoor flight experiment in [2] can be seen [here](http://www.manoonpong.com/FaultDiagnosis/video1.mp4 "Super cool video!").

# Bibliography
[1]: Diget, E. L., "Machine Learning Methods for Reliable Detection of Faults for Drones", 2021, MSc Thesis.

[2]: Diget, E. L., Hasan, A. and Manoonpong, P., "Machine learning with echo state networks for automated fault diagnosis in small unmanned aircraft systems", Not published, 2021.

[3]: Diget, E. L., Hasan, A. and Manoonpong, P., "Fault-tolerant model predictive control in multirotor unmanned aerial vehicles”, Not published, 2021.
