from setuptools import find_packages, setup

setup(
    name='esn_imu',
    packages=find_packages(),
    version='0.1.0',
    description="Emil Lykke Diget's Master Thesis code for anomaly detection using Echo State Network",
    author='Emil Lykke Diget',
    license='MIT',
)