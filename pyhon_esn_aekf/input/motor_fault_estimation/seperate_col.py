#!/usr/bin/env python3 

import numpy as np
import pandas as pd

data_in = pd.read_csv("datafile_normal_prop_05.csv")

print(data_in.columns)

# data_out = np.asarray(data_in["actual_roll"])

# np.savetxt("roll_data.csv", data_out)

data_out = np.asarray(data_in["actual_pos_x"])
np.savetxt("pos_x_data.csv", data_out)

data_out = np.asarray(data_in["actual_pos_y"])
np.savetxt("pos_y_data.csv", data_out)

data_out = np.asarray(data_in["actual_pos_z"])
np.savetxt("pos_z_data.csv", data_out)

data_out = np.asarray(data_in["actual_roll"])
np.savetxt("roll_data.csv", data_out)

data_out = np.asarray(data_in["actual_pitch"])
np.savetxt("pitch_data.csv", data_out)

data_out = np.asarray(data_in["actual_yaw"])
np.savetxt("yaw_data.csv", data_out)

data_out = np.asarray(data_in["actual_ang_vel_x"])
np.savetxt("ang_vel_x_data.csv", data_out)

data_out = np.asarray(data_in["actual_ang_vel_y"])
np.savetxt("ang_vel_y_data.csv", data_out)

data_out = np.asarray(data_in["actual_ang_vel_z"])
np.savetxt("ang_vel_z_data.csv", data_out)