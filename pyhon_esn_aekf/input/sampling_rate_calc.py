#!/usr/bin/env python3

import numpy as np
# from transforms3d.euler import quat2euler
import matplotlib.pyplot as plt
# import copy
# import scipy.sparse as sparse
# import scipy.stats as stats

# data = np.loadtxt('log_0_2020-9-21-09-39-58_vehicle_attitude_0.csv', skiprows=1, delimiter=',')
# data = np.loadtxt('log_0_2020-10-14-11-43-50_vehicle_attitude_0.csv', skiprows=1, delimiter=',')
data = np.loadtxt('11_53_00_vehicle_attitude_0.csv', skiprows=1, delimiter=',')

data = data[500:data.shape[0] - 500, 0] # get the timespamp collumn

# Calculate the diff in the timestamp.
t = np.diff(data) * 1e-6 # it is given in microseconds; this converts it to seconds.

plt.figure()
plt.plot(t)

# mean
print(f"mean dt: {np.mean(t)} s, \tstd: {np.std(t)} s²")
print(f"frequency: {1 / np.mean(t)}")

plt.show()

# # Save as roll, pitch, yaw

# # data = data[1:]
# np.savetxt("imu_data_rpy.csv", data[:-1])
# np.savetxt("imu_data_roll.csv", data[:-1, 0])

# # Remove some of the data
# # find 70% point
# # print(np.shape(data))
# # print(np.size(data))
# # ponr = int(0.7 * np.shape(data)[0])
# # print(f'ponr, {ponr}')
# # data[ponr:ponr + 500, :] = 0
# # np.savetxt("imu_data_missing.csv", data)

# # Filter the data to get a target data set.
# def lowpass(x : np.ndarray, alpha):
#     y = np.zeros_like(x)
#     # y[0] = alpha * x[0]
#     y[0] = x[0]
#     for i in range(1, len(x), 1):
#         y[i] = alpha * x[i] + (1 - alpha) * y[i - 1]
#     return y

# # data_lp = lowpass(data, 1)
# # data_lp = copy.copy(data)

# # noiseRange = 0.01
# # rvs = stats.uniform(loc=-noiseRange, scale=2 * noiseRange).rvs
# # noise_mat = sparse.random(data_lp.shape[0], data_lp.shape[1], data_rvs=rvs)
# # data_lp += noise_mat.toarray()

# data_lp = data[1:]
# np.savetxt("imu_data_rpy_target.csv", data_lp)
# np.savetxt("imu_data_roll_target.csv", data_lp[:, 0])

# plt.figure()
# plt.plot(data[:, 0], label="original")
# plt.plot(data_lp, label="filtered")
# plt.legend()

# plt.show()
