#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

def throttle2thrust(pwm_in):
    # converts the throttle value from px4 pwm in from 1075 to 1950 to thrust [N]
    # first convert [1000; 2000] to [0; 1]
    rang = [1075, 1950]
    throttle = (pwm_in - rang[0]) / (rang[1] - rang[0])

    # Throttle values to thrust from datasheet
    #### 1 gram-force to newton = 0.00980665 newton
    # throttle [%]      Thrust [g]      Thrust [N]
    # 50                435             4.266
    # 55                527             5.168
    # 60                608             5.962
    # 65                702             6.884
    # 75                888             8.708
    # 85                1076            10.551
    # 100               1293            12.680
    data_points = [[0.50,  4.266],
                   [0.55,  5.168],
                   [0.60,  5.962],
                   [0.65,  6.884],
                   [0.75,  8.708],
                   [0.85, 10.551],
                   [1.00, 12.680]]

    # data_points = np.asarray(data_points)

    # plt.figure()
    # print(data_points[:, 0])
    # print(data_points[:, 1])
    # plt.plot(data_points[:, 0], data_points[:, 1], '*-')
    # plt.grid()

    # plt.show()

    ret_val = -1
    
    if throttle < data_points[0][0]: # to catch the first case if the throttle is below the first one.
        # ret_val = 0
        # Make a linear interpolation from the first point to (0, 0)
        ret_val = data_points[0][1] \
            + (throttle - data_points[0][0]) \
            * (data_points[0][1] - 0) \
            / (data_points[0][0] - 0)

    if throttle >= data_points[-1][0]: # if the throttle is larger than the last, cap it at the top value
        ret_val = data_points[-1][1]  # this should never be the case. never above 100%

    if ret_val < 0:
        for i in range(len(data_points) - 1):
            if throttle >= data_points[i][0] and throttle < data_points[i + 1][0]:
                # if throttle is between two values, make a linear interpolation
                # y = y0 + (x - x0) * (y1 - y0)/(x1 - x0)
                ret_val = data_points[i][1] \
                    + (throttle - data_points[i][0]) \
                    * (data_points[i + 1][1] - data_points[i][1]) \
                    / (data_points[i + 1][0] - data_points[i][0])

                break

    return ret_val
       



if __name__ == "__main__":
    # print(throttle2thrust(1700))
    rang = [1000, 2000]

    xvals = np.arange(rang[0], rang[1])
    print(xvals)

    # forces = np.apply_along_axis(throttle2thrust, 1, xvals)
    fv = np.vectorize(throttle2thrust, otypes=[float])
    forces = fv(xvals)
    print(forces)

    # original
    data_points = [[0,  0],
                   [0.1,  0.5],
                   [0.50,  4.266],
                   [0.55,  5.168],
                   [0.60,  5.962],
                   [0.65,  6.884],
                   [0.75,  8.708],
                   [0.85, 10.551],
                   [1.00, 12.680]]
    data_points = np.asarray(data_points)
    f = lambda x: rang[0] + (rang[1] - rang[0]) * x # linear transformation
        # y - y0 = (x - x0) * (y1 - y0) / (x1 - x0)
        # where: x0 = 0, x1 = 1, y0 = 1075, y1 = 1950
    fv = np.vectorize(f, otypes=[float])
    data_points[:, 0] = fv(data_points[:, 0])

    ############### Polynomial regression
    # y = X * beta
    y = data_points[:, 1].reshape((data_points.shape[0], 1))
    X = np.concatenate((
            np.ones((data_points.shape[0], 1)), # ones
            data_points[:, 0].reshape((data_points.shape[0], 1)), # x
            np.power(data_points[:, 0], 2).reshape((data_points.shape[0], 1)), # x^2
            np.power(data_points[:, 0], 3).reshape((data_points.shape[0], 1)) # x^3
    ), axis=1)
    print("y", y)
    print("X", X)

    beta, residuals, rank, s = np.linalg.lstsq(X, y)
    print("beta", beta)
    print("residuals", residuals)
    print("rank", rank)
    print("s", s)
    ###############


    plt.figure()

    # print(data_points[:, 0])
    # print(data_points[:, 1])
    plt.plot(data_points[:, 0], data_points[:, 1], '*-', label="data points")
    plt.grid()
    
    
    plt.plot(xvals, forces, "--", label="linear linterpolation between points")

    
    # Plot the polynomial regression
    f = lambda x: beta[0, 0] + beta[1, 0] * x + beta[2, 0] * x**2 + beta[3, 0] * x**3
    fv = np.vectorize(f, otypes=[float])
    poly_forces = fv(xvals)
    plt.plot(xvals, poly_forces, '-.', label=f"y = {beta[0, 0]} + {beta[1, 0]} * x + {beta[2, 0]} * x**2 + {beta[3, 0]} * x**3")

    plt.legend()

    plt.show()


    