#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.markers as markers
import matplotlib.pyplot as plt
import os
import copy


from esn_imu.model.echo_state_network import *
from esn_imu.noise.noise_testing import *

if __name__ == "__main__":
    np.random.seed(seed=42)

    esn = EchoStateNetwork(
        numberOfInputs = 1, 
        numberOfOutputs = 1,
        numberOfHiddenUnits = 40, 
        inputSparsity = 0.5,
        internalSparsity = 0.5,
        spectralRadius = 0.6, # To preserve echo state
        inputScaling = 0.6,
        # leakingRate = 0.3,
        leakingRate = 0.7,
        reservoirNeuronBias = 0.001,
        noiseRange = 0.0001
    ) 
    esn.reset()
    
    repetitions = 1
    factor = 100
    
    #
    data_orig = pd.read_csv("input/motor_fault_estimation/datafile_normal_prop_rpy_03.csv")[0:-150]
    # data_orig = pd.read_csv("input/motor_fault_estimation/mavros_imu_raw.csv")
    data_orig = data_orig.drop(data_orig.columns[0], axis=1) # no need for the time information
    keys = data_orig.columns.values
    print(keys)
    # for i in range(5):
    # data_orig = data_orig.append(data_orig)
    data = data_orig[:-1] / factor
    dataTarget = data_orig[1:] / factor

    print(data_orig.shape)
    split_point = int(0.5 * data.shape[0])
    print(f"########## split point {split_point}")

    print(data)

    plt.figure()
    plt.plot(data)
    plt.show()


    # idx = ["roll", "pitch", "yaw"]
    # idx = ["roll", "pitch"]
    idx = ["ang_vel_x", "ang_vel_y"]
    # idx = ["field.angular_velocity.x", "field.angular_velocity.y"]

    # for i in range(data_orig.shape[1] - 6):
    # for i in range(0):
    # for i in range(1):
    for key in idx:
        trainData = np.asarray(data[key])[:split_point]
        trainTarget = np.asarray(dataTarget[key])[:split_point]
        testData = np.asarray(data[key])[split_point:]
        testTarget = np.asarray(dataTarget[key])[split_point:]

        plt.figure()
        plt.plot(trainData, label="trainData")
        plt.plot(trainTarget, label="trainTarget")
        plt.legend()
        
        plt.figure()
        plt.plot(testData, label="testData")
        plt.plot(testTarget, label="testTarget")
        plt.legend()

        # if keys[i][:4] == "ang_": 
        #     trainData *= 2
        #     testData *= 2
        #     esn.leakingRate = 0.7
        # else:
        #     esn.leakingRate = 0.7

        print(trainData)
        print(trainTarget)

        print("###########################################")
        print(f"######## Training {key} #################")
        print("###########################################")

        # w_out = esn.train_RLS(trainData, trainTarget, auto_corr=1e-10, repetitions=repetitions, 
        w_out = esn.train_RLS(trainData, trainTarget, auto_corr=1e-7, repetitions=repetitions, 
                iterations=300, debug=True, learning_rate=0.99) 

        print("Trained w_out", esn.W_out)       


        print("###########################################")
        print("############## Predicting #################")
        print("###########################################")

        predictions = []

        for u in testData:
            predictions.append(esn.predict(u))

        predictions = np.asarray(predictions)
        predictions = np.reshape(predictions, (predictions.shape[0], esn.numberOfOutputs))
        print("predictions", predictions)

        nrmsd = esn.calculate_error(testTarget, predictions)
        print(f"nrmsd: {nrmsd}")

        err = np.abs(np.subtract(predictions, testTarget.reshape((testTarget.shape[0], esn.numberOfOutputs))))

        plt.figure()
        ax1 = plt.subplot(311)
        # plt.plot(testData, label="Input")#, marker=".")
        plt.plot(predictions[:,0], label="Predictions")#, marker=".")
        # plt.plot(allData, label="Input")#, marker=".")
        plt.plot(testData, label="Input")#, marker=".")
        plt.legend()
        plt.grid()

        plt.subplot(312, sharex = ax1, sharey = ax1)
        # plt.plot(testData, label="input")
        plt.plot(predictions[:,0], label="Predictions")#, marker=".")
        plt.plot(testTarget, label="Target")
        # plt.plot(allTarget, label="Target")#, marker=".")
        plt.legend()
        plt.grid()
        # plt.xlim((2000 - 499, 2000))
        
        plt.subplot(313, sharex = ax1)
        # plt.plot(testData, label="input")
        plt.plot(err, label="error")#, marker=".")

        plt.suptitle(f"{key}")
        print(testData.shape, predictions.shape )

        esn.save_files("save_files/sensor_fault_esns/", f"{key}_")

        plt.show()


    # print(data)
    # print(dataTarget)

    # plt.figure()
    # plt.plot(quat_w, label="Data")
    # plt.plot(quat_w_target, label="Data target")
    # plt.legend()
    plt.show()


    