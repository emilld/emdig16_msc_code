#!/usr/bin/env python3

import numpy as np
import copy
from esn_imu.motor_fault_estimation import compute_dynamics_quad
import matplotlib.pyplot as plt
from scipy.stats import norm
from mpl_toolkits.mplot3d import Axes3D
import time
np.random.seed(42)

class AEKF:
    def __init__(self, 
                 A : np.ndarray,
                 B : np.ndarray, 
                 # C : np.ndarray,
                 QF : np.ndarray, 
                 # RF : np.ndarray,
                 quad : compute_dynamics_quad.Quadcopter):
        self.A = A
        self.B = B
        # self.C = C
        self.QF = QF
        # self.RF = RF

        self.quad = quad

        self.Xhat = np.zeros((self.A.shape[1], 1))
        self.Pplus = np.eye(np.linalg.matrix_rank(self.A))
        self.thetahat = np.zeros((4, 1))
        self.thetahatM = np.zeros((4, 1))
        self.S = 0.001 * np.eye(4)
        self.UpsilonM = np.zeros((self.A.shape[1], 4))
        self.lambdaa = 0.98 

        self.FXhat, self.FUk = quad.compute_jacobians(self.Xhat, self.B, np.zeros((4, 1)), self.thetahat, 0)

        # self.first_time = True

    # def predict(self, Uk : np.ndarray, dt : float):
    #     self.FXhat, self.FUk = self.quad.compute_jacobians(self.Xhat, self.B, Uk, self.thetahat, dt)
    #     self.Pmin = self.FXhat @ self.Pplus @ self.FXhat.T + self.QF
    #     # self.Xhat = self.compute_dynamics(self.Xhat, Uk, self.thetahat, dt)

    def update(self, y : np.ndarray, C: np.ndarray, RF : np.ndarray, Uk : np.ndarray, dt : float, first_measurement : bool = False):
        # [FXhat, FUk] = compute_jacobians(Xhat, B, Uk, thetahat, dt);
        # if self.first_time:
        if first_measurement:
            # self.Xhat[:y.shape[0], :] = y 
            # self.Xhat[0:5] = y[0:5]
            # self.Xhat[[6, 8, 10], :] = y[6:]
            # print(y)
            # print(C)
            # print(np.linalg.pinv(C))
            # print(np.linalg.pinv(C) @ y)
            # self.Pplus *= 10 # Don't trust the initial value, as it's zero
            # self.Xhat = np.linalg.pinv(C) @ y
            newVals = np.linalg.pinv(C) @ y # insert the first value from measurement
            # print(newVals)
            # print(np.where(newVals > 0))
            # print(newVals != 0)
            self.Xhat[newVals.flatten != 0] = newVals
            # self.first_time = False

        # update psi in fault
        Psi_system = -self.FUk @ np.diag(Uk.reshape((np.max(Uk.shape),)))

        # Calculate the Kalman gain K(k+1) and error covariance matrix P(k+1 | k+1)
        self.FXhat, self.FUk = self.quad.compute_jacobians(self.Xhat, self.B, Uk, self.thetahat, dt)
        self.Pmin = self.FXhat @ self.Pplus @ self.FXhat.T + self.QF
        Sigma = C @ self.Pmin @ C.T + RF
        KF = self.Pmin @ C.T @ np.linalg.inv(Sigma)
        self.Pplus = (np.eye(np.linalg.matrix_rank(self.A)) - KF @ C) @ self.Pmin

        # Calculate the fault estimation Sigma(k+1)
        Upsilon = (np.eye(np.linalg.matrix_rank(self.A)) - KF @ C) @ self.FXhat @ self.UpsilonM + \
                  (np.eye(np.linalg.matrix_rank(self.A)) - KF @ C) @ Psi_system

        Omega = C @ self.FXhat @ self.UpsilonM + C @ Psi_system
        Lambdaa = np.linalg.inv(self.lambdaa * Sigma + Omega @ self.S @ Omega.T)
        Gamma = self.S @ Omega.T @ Lambdaa
        self.S = (1 / self.lambdaa) * self.S - \
                 (1 / self.lambdaa) * self.S @ Omega.T @ Lambdaa @ Omega @ self.S
        self.UpsilonM = copy.copy(Upsilon)

        # Predict
        # self.Xhat = self.quad.compute_dynamics(self.A, self.Xhat, self.B, Uk, self.thetahat, dt)
        self.Xhat = self.compute_dynamics(self.Xhat, Uk, self.thetahat, dt)

        # Error
        ytilde = np.subtract(
            y, # sensor feedback
            C @ self.Xhat # model estimation
        )

        self.thetahat = self.thetahatM + Gamma @ ytilde

        # Correction
        self.Xhat = self.Xhat + KF @ ytilde + Upsilon @ np.subtract(self.thetahat, self.thetahatM)

        self.thetahatM = copy.copy(self.thetahat)

    def compute_dynamics(self, X, Uk, gain_loss, dt):
        """
        Wrapper of quad function.
        """
        return self.quad.compute_dynamics(self.A, X, self.B, Uk, gain_loss, dt)

def main():
    # simulation setup
    simulation_time = 40
    dt = 0.05
    t = np.arange(dt, simulation_time, dt) # dt:dt:simulation_time;

    # Hexarotor parameters
    m = 1.5            # mass in kg
    L = 0.215          # arm length in m
    Jxx = 0.0347563    # moment of inertia around 'x' axis in kg�m^2
    Jyy = 0.0458929    # moment of inertia around 'y' axis in kg�m^2
    Jzz = 0.0977       # moment of inertia around 'z' axis in kg�m^2
    g = 9.81           # gravity
    # Real system initialization
    Uk = np.zeros((4, 1)) # np.array([0, 0, 0, 0]).T  # [F1, F2, F3, F4]'
    # X = np.array([zeros(10,1), 10, 0]).T # [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]'
    X = np.zeros((12, 1))
    # X[0] = np.pi/4
    X[10] = 10 # init z = 10

    gain_loss = np.zeros((4,1))  # fault

    # AEKF System description
    A = np.array(
       [[1, dt, 0, 0,  0, 0,  0, 0,  0, 0,  0, 0], 
        [0, 1,  0, 0,  0, 0,  0, 0,  0, 0,  0, 0],
        [0, 0,  1, dt, 0, 0,  0, 0,  0, 0,  0, 0],
        [0, 0,  0, 1,  0, 0,  0, 0,  0, 0,  0, 0],
        [0, 0,  0, 0,  1, dt, 0, 0,  0, 0,  0, 0],
        [0, 0,  0, 0,  0, 1,  0, 0,  0, 0,  0, 0],
        [0, 0,  0, 0,  0, 0,  1, dt, 0, 0,  0, 0],
        [0, 0,  0, 0,  0, 0,  0, 1,  0, 0,  0, 0],
        [0, 0,  0, 0,  0, 0,  0, 0,  1, dt, 0, 0],
        [0, 0,  0, 0,  0, 0,  0, 0,  0, 1,  0, 0],
        [0, 0,  0, 0,  0, 0,  0, 0,  0, 0,  1, dt],
        [0, 0,  0, 0,  0, 0,  0, 0,  0, 0,  0, 1]])

    b = 0.01

    c45 = np.cos(np.deg2rad(45))
    s45 = np.sin(np.deg2rad(45))
    B = np.array(
        [[1, 1, 1,  1],
         [-L*c45,  L*c45, L*c45, -L*c45],
         [L*s45, -L*s45, L*s45, -L*s45],
         [b, b, -b, -b]])
        
    # C = np.array(
    #     [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll
    #      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll rate
    #      [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch 
    #      [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch rate
    #      [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],  # yaw
    #      [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  # yaw rate
    #      [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],  # x
    #      [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],  # y
    #      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]) # z
    # Need to have different measurement matrices for each sensor:
    C_imu = np.array(
        [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll
         [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll rate
         [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch 
         [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch rate
         [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],  # yaw
         [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]]  # yaw rate
    )

    C_gps = np.array(
        [[0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],  # x
         [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],  # y
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]  # z
    )

    C = np.concatenate((C_imu, C_gps), axis=0)
    # print(C)
    # print(C.shape)

    QF = np.diag([0.1, 0.01, 0.1, 0.01, 0.1, 0.01, 0.5, 0.1, 0.5, 0.1, 0.1, 0.01]) # [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]
    # RF = np.diag([0.5, 0.1, 0.5, 0.1, 0.2, 0.1, 1, 1, 0.3])  # [phi, phidot, theta, thetadot, psi, psidot, x, y, z]'
    RF_imu = np.diag([0.5, 0.1, 0.5, 0.1, 0.2, 0.1])
    RF_gps = np.diag([1, 1, 0.3])

    RF = np.diag(np.concatenate((np.diag(RF_imu), np.diag(RF_gps))))
    # print(RF)

    quad = compute_dynamics_quad.Quadcopter(m, L, Jxx, Jyy, Jzz, g, b)
    # aekf = AEKF(A, B, QF, RF, quad)
    aekf = AEKF(A, B, QF, quad)

    # print(quad.compute_dynamics(A, X, B, Uk, gain_loss, dt)) 
    
    X_hist = []
    X_hat_hist = []
    y_hist = []
    thetahat_hist = []
    gain_loss_array = []
    Uk_hist = []

    first_measurement_imu = True
    first_measurement_gps = True

    before = time.time()
    # for i in range(1, int(simulation_time/dt)):
    for tt in t:
        # time = i * dt
        # to keep track of the simulation time
        # if np.mod(i, 1/dt) == 0:
        #    print(f"time {i * dt}")
        if np.mod(np.round(tt, 5), 1) == 0:
            print(f"time {np.round(tt, 0)}")
        
        # if i > 0 / dt:
        if tt > 0:
            Uk = m * g/4. * np.ones((4, 1))

        # if i > 5 / dt:
        if tt > 5:
            # print("now")
            # gain_loss = np.array([[0.1, 0.05, 0.2, 0.15]]).T
            gain_loss = np.array([[0.1, 0, 0, 0]]).T
            # gain_loss = np.array([[0.1, 0.1, 0.1, 0.1]]).T
            Uk = m * g/4. * np.ones((4, 1)) + np.divide(np.multiply(m * g/4. * np.ones((4, 1)), gain_loss), 1 - gain_loss) 

        # X = quad.compute_dynamics(A, X, B, Uk, gain_loss, dt)
        #<############################################################
        X = aekf.compute_dynamics(X, Uk, gain_loss, dt)
                
        # y = C @ X + dt * RF @ norm.ppf(np.random.rand(np.linalg.matrix_rank(C), 1)) # Making a measurements. In the final solution this would come from the sensors.
        y_imu = C_imu @ X + dt * RF_imu @ norm.ppf(np.random.rand(np.linalg.matrix_rank(C_imu), 1)) 
        y_gps = C_gps @ X + dt * RF_gps @ norm.ppf(np.random.rand(np.linalg.matrix_rank(C_gps), 1)) 
        y = np.concatenate((y_imu, y_gps), axis=0)
        # print(y)
        # It's also very probable that the IMU and the GPS doesn't operate at the same frequency, so you should be able to select which sensor data you are updating with.
        #############################################################>

        # aekf.predict(Uk, dt)
        aekf.update(y, C, RF, Uk, dt, first_measurement_imu)
        first_measurement_imu = False
        # aekf.update(y_imu, C_imu, RF_imu, Uk, dt, first_measurement_imu)
        # first_measurement_imu = False
        # aekf.update(y_gps, C_gps, RF_gps, Uk, dt, first_measurement_gps)
        # first_measurement_gps = False

        X_hist.append(X.reshape((X.shape[0],)))
        X_hat_hist.append(aekf.Xhat.reshape((aekf.Xhat.shape[0], )))
        thetahat_hist.append(aekf.thetahat.reshape(aekf.thetahat.shape[0],))
        gain_loss_array.append(gain_loss.reshape((gain_loss.shape[0],)))
        Uk_hist.append(Uk.reshape((Uk.shape[0],)))
        y_hist.append(y.reshape((y.shape[0],)))

        # print(quad.compute_jacobians(X, B, Uk, gain_loss, dt))

    after = time.time()
    print(f"Duration {after - before}")

    X_hist = np.asarray(X_hist)
    X_hat_hist = np.asarray(X_hat_hist)    
    thetahat_hist = np.asarray(thetahat_hist)
    gain_loss_array = np.asarray(gain_loss_array)
    y_hist = np.asarray(y_hist)

    # Figure showing the z value
    plt.figure()
    plt.subplot(311)
    plt.plot(t, X_hist[:, 6], '--', label = "Position true state")
    plt.plot(t, X_hat_hist[:, 6], '-', label = "Position AEKF")
    plt.plot(t, X_hist[:, 7], '--', label = "Velocity true state")
    plt.plot(t, X_hat_hist[:, 7], '-', label = "Velocity AEKF")
    plt.legend()
    plt.title('X')
    plt.grid()

    plt.subplot(312)
    plt.plot(t, X_hist[:, 8], '--', label = "Position true state")
    plt.plot(t, X_hat_hist[:, 8], '-', label = "Position AEKF")
    plt.plot(t, X_hist[:, 9], '--', label = "Velocity true state")
    plt.plot(t, X_hat_hist[:, 9], '-', label = "Velocity AEKF")
    plt.legend()
    plt.title('Y')
    plt.grid()

    plt.subplot(313)
    plt.plot(t, X_hist[:, 10], '--', label = "Position true state")
    plt.plot(t, X_hat_hist[:, 10], '-', label = "Position AEKF")
    plt.plot(t, X_hist[:, 11], '--', label = "Velocity true state")
    plt.plot(t, X_hat_hist[:, 11], '-', label = "Velocity AEKF")
    plt.legend()
    plt.title('Z')
    plt.grid()


    ## Figure showing the roll pitch yaw angles
    plt.figure()
    plt.subplot(311)
    plt.plot(t, X_hist[:, 0] * 180 / np.pi, '--', label="Angle true state")
    plt.plot(t, X_hat_hist[:, 0] * 180 / np.pi, label="Angle AEKF")
    plt.plot(t, X_hist[:, 1] * 180 / np.pi, '--', label="Angle rate true state")
    plt.plot(t, X_hat_hist[:, 1] * 180 / np.pi, label="Angle rate AEKF")
    plt.title("Roll")
    plt.legend()
    plt.ylabel("Angle [degrees]")
    plt.grid()
    
    plt.subplot(312)
    plt.plot(t, X_hist[:, 2] * 180 / np.pi, '--', label="Angle true state")
    plt.plot(t, X_hat_hist[:, 2] * 180 / np.pi, label="Angle AEKF")
    plt.plot(t, X_hist[:, 3] * 180 / np.pi, '--', label="Angle rate true state")
    plt.plot(t, X_hat_hist[:, 3] * 180 / np.pi, label="Angle rate AEKF")
    plt.title("Pitch")
    plt.legend()
    plt.ylabel("Angle [degrees]")
    plt.grid()
    
    plt.subplot(313)
    plt.plot(t, X_hist[:, 4] * 180 / np.pi, '--', label="Angle true state")
    plt.plot(t, X_hat_hist[:, 4] * 180 / np.pi, label="Angle AEKF")
    plt.plot(t, X_hist[:, 5] * 180 / np.pi, '--', label="Angle rate true state")
    plt.plot(t, X_hat_hist[:, 5] * 180 / np.pi, label="Angle rate AEKF")
    plt.title("Yaw")
    plt.legend()
    plt.xlabel("Times [s]")
    plt.ylabel("Angle [degrees]")
    plt.grid()

    ## Figure showing the estimated motor faults
    plt.figure()
    for i in range(4):
        plt.subplot(221 + i)
        plt.plot(t, thetahat_hist[:, i])
        plt.plot(t, gain_loss_array[:, i], 'k--')
        plt.title(f"Motor {i + 1}")
        plt.grid()

    ## Figure showing all states
    plt.figure()
    plt.plot(t, X_hist, label="State")
    plt.legend()
    plt.grid()

    ## Figure showing the commanded motor thrust
    plt.figure()
    plt.plot(t, Uk_hist, label="Motor thrust")
    plt.legend()

    # Figure, virtual force loss estimation
    plt.figure()
    print(B.shape, gain_loss_array.shape)
    print(B)
    print(gain_loss_array)
    print(thetahat_hist)
    V_loss_real = (B @ gain_loss_array.T).T
    V_loss_est = (B @ thetahat_hist.T).T

    print(V_loss_real)
    print(V_loss_est)

    names = ["Thrust", "Moment X", "Moment Y", "Moment Z"]    

    for i in range(4):
        plt.subplot(221 + i)
        plt.plot(t, V_loss_est[:, i], 'r-')
        plt.plot(t, V_loss_real[:, i], 'k--')
        plt.grid()
        plt.title(names[i])
   
    # figure showing the 3d position of the drone
    # print(X_hist[:, 10])
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter3D([0], [0], [10], label="Reference position")
    ax.plot3D(X_hist[:, 6], X_hist[:, 8], X_hist[:, 10], "--", label="Position true state")
    ax.plot3D(X_hat_hist[:, 6], X_hat_hist[:, 8], X_hat_hist[:, 10], "-", label="Position AEKF")
    # ax.plot(X_hist[:, 6], X_hist[:, 8], X_hist[:, 10], label="Position AEKF")
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
    ax.set_zlabel("z [m]")
    plt.legend()
    plt.show()

if __name__ == "__main__":
    main()