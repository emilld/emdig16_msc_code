#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

class Hexacopter:
    def __init__(self, m, L, Jxx, Jyy, Jzz, g, b):
        """
        m       is the mass of the UAV
        L       is the arm length
        Jxx     is the moment of inertia orund the 'x' axis
        Jyy     is the moment of inertia orund the 'y' axis
        Jzz     is the moment of inertia orund the 'z' axis
        g
        b       is the drag coefficient
        """
        self.m = m
        self.Jxx = Jxx
        self.Jyy = Jyy 
        self.Jzz = Jzz
        self.g = g
        self.L = L
        self.b = b
    
    def compute_dynamics(self, 
        A : np.ndarray, 
        X : np.ndarray, 
        B : np.ndarray, 
        Uf : np.ndarray, 
        gain_loss : np.ndarray, 
        dt : float):
        """
        A           is the dynamic model
        X           is the previous state
        B           is the motor matrix mixer
        Uf          is the control signal for each motor
        gain_loss   is the fault
        dt          is the time difference between iterations
        
        This function uses the following global variables:
            m       is the mass of the UAV
            Jxx     is the moment of inertia orund the 'x' axis
            Jyy     is the moment of inertia orund the 'y' axis
            Jzz     is the moment of inertia orund the 'z' axis
            g       is the gravity
        """
        # print(X)
        # Input vector reading
        phi     = X[0, 0]
        phid    = X[1, 0]
        theta   = X[2, 0]
        thetad  = X[3, 0]
        psi     = X[4, 0]
        psid    = X[5, 0]
        x       = X[6, 0]
        xd      = X[7, 0]
        y       = X[8, 0]
        yd      = X[9, 0]
        z       = X[10, 0]
        zd      = X[11, 0]

        # Virtual control input calculation
        # print(B)
        # print(np.diag(Uf.reshape((np.max(Uf.shape),))))

        Psi = -B @ np.diag(Uf.reshape((np.max(Uf.shape),)))
        # print(Psi)
        # return
        # print(Psi)
        Uv = B @ Uf + Psi @ gain_loss
        # print("Uf", Uf.shape)
        # print("Uv", Uv)
        T  = Uv[0, 0]
        Mx = Uv[1, 0]
        My = Uv[2, 0]
        Mz = Uv[3, 0]
        
        # Acceleration calculation
        ux = np.cos(phi) * np.cos(psi) * np.sin(theta) + np.sin(phi) * np.sin(psi)
        uy = np.cos(phi) * np.sin(theta) * np.sin(psi) - np.sin(phi) * np.cos(psi)
        uz = np.cos(phi) * np.cos(theta)

        # Rotational
        # print("thetad", thetad, "psid", psid, "Mx", Mx)
        phidd   = 1/self.Jxx * (thetad * psid * (self.Jyy - self.Jzz) + Mx)
        thetadd = 1/self.Jyy * (phid * psid   * (self.Jzz - self.Jxx) + My)
        psidd   = 1/self.Jzz * (phid * thetad * (self.Jxx - self.Jyy) + Mz)
        # Translational
        xdotd = 1/self.m * ux * T
        ydotd = 1/self.m * uy * T
        zdotd = -self.g + 1/self.m * uz * T

        # print("phid", phid)
        # print("phidd", phidd)
        # print(phi, phid + phidd * dt) 
        # Update the velocities in X with the new dynamics
        X = [phi, phid + phidd * dt,
             theta, thetad + thetadd * dt,
             psi, psid + psidd * dt,
             x, xd + xdotd * dt,
             y, yd + ydotd * dt,
             z, zd + zdotd * dt]

        # print(X)

        X = np.asarray(X).T
        
        # Update the positions in the X with the new velocities.
        Xout = A @ X
        #  saturate z value
        # print(X)
        if Xout[10] < 0:
            Xout[10] = 0
            Xout[11] = 0

        Xout = Xout.reshape((Xout.shape[0], 1))
        # print("Xout", Xout)
        return Xout

    def compute_jacobians(self, 
        X : np.ndarray,
        B : np.ndarray,
        Uf : np.ndarray,
        gain_loss : np.ndarray,
        dt : float):
        # compute_jacobians compute the Jacobian of the new state. The JAcobian is
        # calculated using the script jacobian_sym
        #   X           is the current state
        #   B           is the motor matrix mixer
        #   Uf          is the control signal for each motor
        #   gain_loss   is the fault
        #   dt          is the time difference between iterations
        # 
        #   This function uses the following global variables:
        #       m       is the mass of the UAV
        #       L       is the arm length of the UAV
        #       Jxx     is the moment of inertia orund the 'x' axis
        #       Jyy     is the moment of inertia orund the 'y' axis
        #       Jzz     is the moment of inertia orund the 'z' axis
        #       c60     is the cosine of 60
        #       g       is the gravity
        c60 = np.cos(np.deg2rad(60))
        s60 = np.sin(np.deg2rad(60))

        # Input vector reading
        phi     = X[0, 0]
        phid    = X[1, 0]
        theta   = X[2, 0]
        thetad  = X[3, 0]
        psi     = X[4, 0]
        psid    = X[5, 0]

        # Virtual control input calculation
        Psi = -B @ np.diag(Uf.reshape((np.max(Uf.shape),)))
        Uv = B @ Uf + Psi @ gain_loss
        T  = Uv[0, 0]

        # Compute jacobian
        JacX = np.array([
[                                                              1,                            dt,                                        0, (dt**2*psid*(self.Jyy - self.Jzz))/self.Jxx,                                              0, (dt**2*thetad*(self.Jyy - self.Jzz))/self.Jxx, 0,  0, 0,  0, 0,  0],
[                                                              0,                             1,                                        0,   (dt*psid*(self.Jyy - self.Jzz))/self.Jxx,                                                              0,   (dt*thetad*(self.Jyy - self.Jzz))/self.Jxx, 0,  0, 0,  0, 0,  0],
[                                                              0,  -(dt**2*psid*(self.Jxx - self.Jzz))/self.Jyy,                        1,                          dt,                                                              0,  -(dt**2*phid*(self.Jxx - self.Jzz))/self.Jyy, 0,  0, 0,  0, 0,  0],
[                                                              0,    -(dt*psid* (self.Jxx - self.Jzz))/self.Jyy,                        0,                           1,                                                              0,    -(dt*phid*(self.Jxx - self.Jzz))/self.Jyy, 0,  0, 0,  0, 0,  0],
[                                                              0, (dt**2*thetad*(self.Jxx - self.Jyy))/self.Jzz,                        0, (dt**2*phid*(self.Jxx - self.Jyy))/self.Jzz,                                              1,                            dt, 0,  0, 0,  0, 0,  0],
[                                                              0,   (dt*thetad* (self.Jxx - self.Jyy))/self.Jzz,                        0,   (dt*phid*(self.Jxx - self.Jyy))/self.Jzz,                                               0,                             1, 0,  0, 0,  0, 0,  0],
[  (T*dt**2*(np.cos(phi)*np.sin(psi) - np.cos(psi)*np.sin(phi)*np.sin(theta)))/self.m,        0, (T*dt**2*np.cos(phi)*np.cos(psi)*np.cos(theta))/self.m,            0, (T*dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,        0, 1, dt, 0,  0, 0,  0],
[    (T*dt*(np.cos(phi)*np.sin(psi) - np.cos(psi)*np.sin(phi)*np.sin(theta)))/self.m,         0,   (T*dt*np.cos(phi)*np.cos(psi)*np.cos(theta))/self.m,             0,   (T*dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,          0, 0,  1, 0,  0, 0,  0],
[ -(T*dt**2*(np.cos(phi)*np.cos(psi) + np.sin(phi)*np.sin(psi)*np.sin(theta)))/self.m,        0, (T*dt**2*np.cos(phi)*np.cos(theta)*np.sin(psi))/self.m,            0, (T*dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,         0, 0,  0, 1, dt, 0,  0],
[   -(T*dt*(np.cos(phi)*np.cos(psi) + np.sin(phi)*np.sin(psi)*np.sin(theta)))/self.m,         0,   (T*dt*np.cos(phi)*np.cos(theta)*np.sin(psi))/self.m,             0,   (T*dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,          0, 0,  0, 0,  1, 0,  0],
[                                -(T*dt**2*np.cos(theta)*np.sin(phi))/self.m,                 0,         -(T*dt**2*np.cos(phi)*np.sin(theta))/self.m,               0,                                                              0,                             0, 0,  0, 0,  0, 1, dt],
[                                  -(T*dt*np.cos(theta)*np.sin(phi))/self.m,                  0,           -(T*dt*np.cos(phi)*np.sin(theta))/self.m,                0,                                                              0,                             0, 0,  0, 0,  0, 0,  1]
        ])

#         JacU = np.array([
# [                                      -(self.L*c45*dt**2)/self.Jxx,                                       (self.L*c45*dt**2)/self.Jxx,                                       (self.L*c45*dt**2)/self.Jxx,                                            -(self.L*c45*dt**2)/self.Jxx],  
# [                                              -(self.L*c45*dt)/self.Jxx,                                               (self.L*c45*dt)/self.Jxx,                                               (self.L*c45*dt)/self.Jxx,                                              -(self.L*c45*dt)/self.Jxx], 
# [                                             (self.L*dt**2*s45)/self.Jyy,                                            -(self.L*dt**2*s45)/self.Jyy,                                             (self.L*dt**2*s45)/self.Jyy,                                            -(self.L*dt**2*s45)/self.Jyy], 
# [                                               (self.L*dt*s45)/self.Jyy,                                              -(self.L*dt*s45)/self.Jyy,                                               (self.L*dt*s45)/self.Jyy,                                              -(self.L*dt*s45)/self.Jyy], 
# [                                                 (self.b*dt**2)/self.Jzz,                                                 (self.b*dt**2)/self.Jzz,                                                -(self.b*dt**2)/self.Jzz,                                                -(self.b*dt**2)/self.Jzz], 
# [                                                   (self.b*dt)/self.Jzz,                                                   (self.b*dt)/self.Jzz,                                                  -(self.b*dt)/self.Jzz,                                                  -(self.b*dt)/self.Jzz], 
# [  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m], 
# [    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m], 
# [ -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m], 
# [   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m], 
# [                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m], 
# [                                   (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m]
#         ])

        JacU = np.array([ 
[                                                -(self.L*dt**2)/self.Jxx,                                                 (self.L*dt**2)/self.Jxx,                                             (self.L*c60*dt**2)/self.Jxx,                                            -(self.L*c60*dt**2)/self.Jxx,                                            -(self.L*c60*dt**2)/self.Jxx,                                             (self.L*c60*dt**2)/self.Jxx],
[                                                   -(self.L*dt)/self.Jxx,                                                   (self.L*dt)/self.Jxx,                                               (self.L*c60*dt)/self.Jxx,                                              -(self.L*c60*dt)/self.Jxx,                                              -(self.L*c60*dt)/self.Jxx,                                               (self.L*c60*dt)/self.Jxx],
[                                                             0,                                                            0,                                             (self.L*dt**2*s60)/self.Jyy,                                            -(self.L*dt**2*s60)/self.Jyy,                                             (self.L*dt**2*s60)/self.Jyy,                                            -(self.L*dt**2*s60)/self.Jyy],
[                                                             0,                                                            0,                                               (self.L*dt*s60)/self.Jyy,                                              -(self.L*dt*s60)/self.Jyy,                                               (self.L*dt*s60)/self.Jyy,                                              -(self.L*dt*s60)/self.Jyy],
[                                                 -(self.b*dt**2)/self.Jzz,                                                 (self.b*dt**2)/self.Jzz,                                                -(self.b*dt**2)/self.Jzz,                                                 (self.b*dt**2)/self.Jzz,                                                 (self.b*dt**2)/self.Jzz,                                                -(self.b*dt**2)/self.Jzz],
[                                                   -(self.b*dt)/self.Jzz,                                                   (self.b*dt)/self.Jzz,                                                  -(self.b*dt)/self.Jzz,                                                   (self.b*dt)/self.Jzz,                                                   (self.b*dt)/self.Jzz,                                                  -(self.b*dt)/self.Jzz],
[   (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,  (dt**2*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m],
[     (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m,    (dt*(np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)*np.sin(theta)))/self.m],
[  -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m, -(dt**2*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m],
[    -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m,   -(dt*(np.cos(psi)*np.sin(phi) - np.cos(phi)*np.sin(psi)*np.sin(theta)))/self.m],
[                                  (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m,                                 (dt**2*np.cos(phi)*np.cos(theta))/self.m],
[                                    (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m,                                   (dt*np.cos(phi)*np.cos(theta))/self.m]
        ])

        return JacX, JacU

    @staticmethod
    def get_model(dt, L):
        # AEKF System description
        A = np.array(
            [[1, dt,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0], 
             [0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0],
             [0,  0,  1, dt,  0,  0,  0,  0,  0,  0,  0, 0],
             [0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0, 0],
             [0,  0,  0,  0,  1, dt,  0,  0,  0,  0,  0, 0],
             [0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0, 0],
             [0,  0,  0,  0,  0,  0,  1, dt,  0,  0,  0, 0],
             [0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0, 0],
             [0,  0,  0,  0,  0,  0,  0,  0,  1, dt,  0, 0],
             [0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0, 0],
             [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1, dt],
             [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 1]])

        b = 0.01

        c60 = np.cos(np.deg2rad(60))
        s60 = np.sin(np.deg2rad(60))
        B = np.array(
            [[1, 1, 1,  1, 1, 1],
            [-L, L, L*c60,  -L*c60, -L*c60, L*c60],
            [0, 0, L*s60, -L*s60, L*s60, -L*s60],
            [-b, b, -b, b, b, -b]])  

        C = np.array(
            [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll
             [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll rate
             [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch 
             [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch rate
             [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],  # yaw
             [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  # yaw rate
             [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],  # x
             [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],  # y
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]) # z

        return A, b, B, C

def main():
    # simulation setup
    simulation_time = 10
    dt = 0.05
    t = np.arange(dt, simulation_time, dt) # dt:dt:simulation_time;

    # Hexarotor parameters, from the sdu_drone spec
    m = 1.5            # mass in kg
    L = 0.37           # arm length in m
    Jxx = 0.0762625    # moment of inertia around 'x' axis in kg�m^2
    Jyy = 0.0762625    # moment of inertia around 'y' axis in kg�m^2
    Jzz = 0.1369       # moment of inertia around 'z' axis in kg�m^2
    g = 9.81           # gravity
    # Real system initialization
    Uk = np.zeros((6, 1)) # np.array([0, 0, 0, 0, 0, 0]).T  # [F1, F2, F3, F4, F5, F6]'
    # X = np.array([zeros(10,1), 10, 0]).T # [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]'
    X = np.zeros((12, 1))
    X[10] = 10 # init z = 10

    gain_loss = np.zeros((6,1))  # fault

    # AEKF System description
    # A = np.array(
    #    [[1, dt, 0, 0,  0, 0,  0, 0,  0, 0,  0, 0], 
    #     [0, 1,  0, 0,  0, 0,  0, 0,  0, 0,  0, 0],
    #     [0, 0,  1, dt, 0, 0,  0, 0,  0, 0,  0, 0],
    #     [0, 0,  0, 1,  0, 0,  0, 0,  0, 0,  0, 0],
    #     [0, 0,  0, 0,  1, dt, 0, 0,  0, 0,  0, 0],
    #     [0, 0,  0, 0,  0, 1,  0, 0,  0, 0,  0, 0],
    #     [0, 0,  0, 0,  0, 0,  1, dt, 0, 0,  0, 0],
    #     [0, 0,  0, 0,  0, 0,  0, 1,  0, 0,  0, 0],
    #     [0, 0,  0, 0,  0, 0,  0, 0,  1, dt, 0, 0],
    #     [0, 0,  0, 0,  0, 0,  0, 0,  0, 1,  0, 0],
    #     [0, 0,  0, 0,  0, 0,  0, 0,  0, 0,  1, dt],
    #     [0, 0,  0, 0,  0, 0,  0, 0,  0, 0,  0, 1]])

    # b = 0.01

    # c45 = np.cos(np.deg2rad(45))
    # s45 = np.sin(np.deg2rad(45))
    # B = np.array(
    #     [[1, 1, 1,  1],
    #      [-L*c45,  L*c45, L*c45, -L*c45],
    #      [L*s45, -L*s45, L*s45, -L*s45],
    #      [b, b, -b, -b]])
        

    # C = np.array(
    #     [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll
    #      [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # roll rate
    #      [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch 
    #      [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  # pitch rate
    #      [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],  # yaw
    #      [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  # yaw rate
    #      [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],  # x
    #      [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],  # y
    #      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]) # z
    
    A, b, B, C = Hexacopter.get_model(dt, L)

    hexa = Hexacopter(m, L, Jxx, Jyy, Jzz, g, b)

    X[0] = 1
    X[1] = 0.1 
    X[2] = 0.5
    Uk[0, :] = 1
    Uk[1, :] = 2
    Uk[2, :] = 1
    Uk[3, :] = 2
    
    # jacX, jacU = quad.compute_jacobians(X, B, Uk, gain_loss, dt)
    xhat = X
    for i in range(6):
        xhat = hexa.compute_dynamics(A, xhat, B, Uk, gain_loss, dt)
        print("xhat = \n", xhat)

    # print("jacX = \n", jacX)
    # print("jacU = \n", jacU)

    # return 
    # print(quad.compute_dynamics(A, X, B, Uk, gain_loss, dt)) 
    
    X_hist = []

    for i in range(0, int(simulation_time/dt)):
        time = i * dt
        
        X = hexa.compute_dynamics(A, X, B, Uk, gain_loss, dt)
        X_hist.append(X)

        # print(quad.compute_jacobians(X, B, Uk, gain_loss, dt))

    X_hist = np.asarray(X_hist)

    plt.figure()
    plt.plot(X_hist[:, 10], label = "Z")
    plt.legend()
    plt.grid()
    
    plt.figure()
    plt.subplot(311)
    plt.plot(X_hist[:, 0], label="roll")
    plt.grid()
    
    plt.subplot(312)
    plt.plot(X_hist[:, 2], label="pitch")
    plt.grid()
    
    plt.subplot(313)
    plt.plot(X_hist[:, 4], label="yaw")
    plt.grid()

    plt.show()

    
if __name__ == "__main__":
    main()