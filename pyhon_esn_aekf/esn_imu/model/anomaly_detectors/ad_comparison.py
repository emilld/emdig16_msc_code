#!/usr/bin/env python3

"""
Script for comparing the anomaly detectors (AD) for the sensor measurements.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Position xy AD
import esn_imu.model.anomaly_detectors.setup_pos_xy as setup_pos_xy
# Position z AD
import esn_imu.model.anomaly_detectors.setup_pos_z as setup_pos_z
# Roll, pitch AD
import esn_imu.model.anomaly_detectors.setup_rp as setup_rp
# Yaw AD 
import esn_imu.model.anomaly_detectors.setup_y as setup_y
# Angular velocities, x, y, z, AD
import esn_imu.model.anomaly_detectors.setup_ang_vel_xy as setup_ang_vel_xy
import esn_imu.model.anomaly_detectors.setup_ang_vel_z as setup_ang_vel_z

from esn_imu.noise.noise_testing import inject_short_anomalies, inject_noise_anomalies

def main():
    # Load in all the setups including files.
    path = "save_files/sensor_fault_esns/"
    filename_w_in_res = f"{path}ang_vel_x_weights_in_res.txt" # these two are the same for all setups
    filename_w_res_res = f"{path}ang_vel_x_weights_res_res.txt"
    
    # Position x, y, z
    ad_pos_x = setup_pos_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}pos_x_weights_res_out.txt")
    ad_pos_y = setup_pos_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}pos_y_weights_res_out.txt")
    ad_pos_z = setup_pos_z.Setup(filename_w_res_res, filename_w_in_res,  filename_w_res_out=f"{path}pos_z_weights_res_out.txt")

    # Orientation roll, pitch, yaw
    ad_roll = setup_rp.Setup(filename_w_res_res, filename_w_in_res,  filename_w_res_out=f"{path}roll_weights_res_out.txt")
    ad_pitch = setup_rp.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}pitch_weights_res_out.txt")
    ad_yaw = setup_y.Setup(filename_w_res_res, filename_w_in_res,    filename_w_res_out=f"{path}yaw_weights_res_out.txt")

    # Angular velocities x, y, z
    ad_ang_vel_x = setup_ang_vel_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}ang_vel_x_weights_res_out.txt")
    ad_ang_vel_y = setup_ang_vel_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}ang_vel_y_weights_res_out.txt")
    ad_ang_vel_z = setup_ang_vel_z.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}ang_vel_z_weights_res_out.txt")

    # Load data in
    data = pd.read_csv("input/motor_fault_estimation/datafile_normal_prop_rpy_03.csv")[0:-150]
    data = data.drop(data.columns[0], axis=1) # no need for the time information
    dt = 0.1
    t = np.arange(0, dt * data.shape[0] - dt, dt)

    networks = [ad_pos_x, ad_pos_y, ad_pos_z, ad_roll, ad_pitch, ad_yaw, ad_ang_vel_x, ad_ang_vel_y, ad_ang_vel_z]
    names           = ["pos_x", "pos_y", "pos_z", "roll", "pitch", "yaw", "ang_vel_x", "ang_vel_y", "ang_vel_z"] # column keys
    shot_noise_size = [      1,       1,       1,      5,       5,     1,           7,           7,           7]
    # shot_noise_size = [      0,       0,       0,      0,       0,     0,           0,           0,           0]
    injected_anomalies = {}

    for name, xxx in zip(names, shot_noise_size):
        injected_anomalies[name] = inject_short_anomalies(data[name], xxx, 50)


    for name, network in zip(names, networks):
        aekf_input = []
        pred_err_filtered = []
        pred_err_old_filtered = []
        for u in data[name]:
            # print(setup1.get_update(u))
            # aekf_input.append(network.get_update(u))
            aekf_input.append(network.get_update(np.asarray(u)))
            # aekf_input[-1] = (aekf_input[-1][0], -aekf_input[-1][1], -aekf_input[-1][2])
            pred_err_filtered.append(network.anomaly_detector.dataPointsAvg[0])
            pred_err_old_filtered.append(network.anomaly_detector.dataPointsAvg[1])

        pred_err_filtered = np.asarray(pred_err_filtered)
        pred_err_old_filtered = np.asarray(pred_err_old_filtered)
        # print(aekf_input)
        aekf_input = np.asarray(aekf_input)
        print(aekf_input)
        
        fig = plt.figure()
        plt.title(name)
        plt.plot(t, data[name], '-', label="Input")
        plt.plot(t, aekf_input[:, 1], '--', label="Output")
        tmp = np.where(aekf_input[:, 0] > 0)[0]
        
        print(tmp)
        print(t)
        # if tmp.shape[0] > 0:
        #     rho = tmp[-1] / t[-1]
        anomaly_y_offset = np.ones_like(tmp) * np.mean(data[name]) + 2 * np.std(data[name])
        anomaly_y_offset_inject = np.ones_like(injected_anomalies[name]) * np.mean(data[name]) + 3 * np.std(data[name])

        print(t[tmp], "\n", anomaly_y_offset)
        print(t[injected_anomalies[name]], "\n", anomaly_y_offset_inject)

        print("Signal mean:", np.mean(data[name]))
        print("Signal std:", np.std(data[name]))

        # plt.plot(np.multiply(tmp, dt), np.zeros_like(tmp), "ro", fillstyle='none', label="Detected Anomalies")
        plt.plot(t[tmp], anomaly_y_offset, "ro", fillstyle='none', label="Detected Anomalies")

        # plt.plot(np.multiply(injected_anomalies, dt), np.zeros_like(injected_anomalies) + 0.05, "kx", label="Injected Anomalies")
        # plt.plot(np.multiply(injected_anomalies[name], dt), np.zeros_like(injected_anomalies[name]) + 0.05, "kx", label="Injected Anomalies")
        plt.plot(t[injected_anomalies[name]], anomaly_y_offset_inject, "kx", label="Injected Anomalies")

        # plt.ylabel("Angle [rad]")
        # plt.ylabel("Position $x$ [m]")
        plt.xlabel("Time [s]")
        # plt.title(name)

        # plt.legend()
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
           ncol=2, mode="expand", borderaxespad=0.)

        plt.grid()
        # plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
        # plt.savefig(f'./output/plots/comparison_{name}.pdf')

        # Plot of the prediction error
        fig = plt.figure()
        plt.title(name)
        plt.plot(t, aekf_input[:, 2], label="Prediction error")
        # plt.plot(t, pred_err_filtered, label="Prediction error filtered")
        # plt.plot(t, pred_err_old_filtered, label="Pred err filtered old")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
           ncol=2, mode="expand", borderaxespad=0.)
        plt.grid()
        # plt.ylabel("Error [rad]")
        plt.ylabel("Error [m]")
        plt.xlabel("Time [s]")
        # plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
        plt.ylim((-0.001, 0.1))

        plt.show()

    plt.show()

if __name__ == "__main__":
    main()