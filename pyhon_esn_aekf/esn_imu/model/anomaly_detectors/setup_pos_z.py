#!/usr/bin/env python3

import numpy as np
from esn_imu.model.echo_state_network import EchoStateNetwork
from esn_imu.noise.noise_testing import PeakRecognition

class Setup(EchoStateNetwork):
    def __init__(self, filename_w_res_res, filename_w_in_res, filename_w_res_out, rho1=0.02, rho2=0.00001, factor=100):
        """
        Class for Setup 2 of the ESN.
            ESN for anomaly detection with ESN output as input to the AEKF.
            On update it shall return:
                bool anomaly
                float signal
        """
        # It is assumed that before this is loaded a network is trained and saved in files.
        super(Setup, self).__init__(
            numberOfInputs = 1,
            numberOfOutputs = 1,  
            numberOfHiddenUnits = 40,
            inputSparsity = 0.5,
            internalSparsity = 0.5,
            spectralRadius = 0.6,
            inputScaling = 0.6,
            leakingRate = 0.7,
            reservoirNeuronBias = 0.001,
            noiseRange = 0.0001
        )
        self.anomaly_detector = PeakRecognition(rho1, rho2)
        self.last_prediction = None
        self.factor = factor

        super(Setup, self).load_from_file(filename_w_res_res, filename_w_in_res, filename_w_res_out)

    def get_update(self, u):
        if self.last_prediction is None:
            prediction_error = np.zeros((1, self.numberOfOutputs))
            # self.last_prediction = u
        else:
            prediction_error = np.abs(np.subtract(u, self.last_prediction))[0].reshape((1, self.numberOfOutputs))
        
        # tmp = self.last_prediction
        # Scale the input by a factor to get it in the range of the ESN.
        # Undo the operation on the output
        self.last_prediction = super(Setup, self).predict(u / self.factor) * self.factor
        anomaly = self.anomaly_detector.update(prediction_error)

        # return anomaly, tmp.flatten()[0], prediction_error.flatten()[0] # self.last_prediction.flatten()[0]
        return anomaly, self.last_prediction.flatten()[0], prediction_error.flatten()[0] # self.last_prediction.flatten()[0]
        

if __name__ == "__main__":
    s2 = Setup("save_files/weights_res_res.txt", "save_files/weights_in_res.txt", "save_files/weights_res_out.txt")
    print(s2)

    print(s2.get_update(np.array([5])))