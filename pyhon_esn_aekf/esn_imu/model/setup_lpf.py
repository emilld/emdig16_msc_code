#!/usr/bin/env python3
"""
Setup where a low pass filter is used instead of an Echo State Network to make anomaly detection.
"""

import numpy as np
import scipy

from esn_imu.model.echo_state_network import EchoStateNetwork
from esn_imu.noise.noise_testing import PeakRecognition

class SetupLPF():
    def __init__(self, N, N_inputs, kernel="mean", rho1=0.02, rho2=0.001):
        if isinstance(rho1, float):
            rho1 = rho1 * np.ones((N_inputs, 1))
        else:
            rho1 = np.asarray(rho1)
        
        if isinstance(rho2, float):
            rho2 = rho2 * np.ones((N_inputs, 1))
        else:
            rho2 = np.asarray(rho2)

        self.anomaly_detectors = [PeakRecognition(rho1[i], rho2[i]) for i in range(3)]
        self.last_prediction = None
        self.N = N
        # self.last_N_samples = [0 for i in range(N)]
        self.N_inputs = N_inputs
        self.last_N_samples = np.zeros((self.N_inputs, self.N))

        if kernel == "mean":
            self.kernel = np.ones(N) / N
        # else if kernel == ""
        

    def get_update(self, u):
        """
        Has to be called get_update() to be compatible with the other setups.
        """
        # if self.last_prediction is None:
        #     prediction_error = 0
        # else:
        #     prediction_error = u - self.last_prediction

        # add a measurement to end and remove the first
        # self.last_N_samples.append(u)
        # self.last_N_samples.pop(0)
        self.last_N_samples = np.append(self.last_N_samples, u.reshape(u.size, 1), axis=1)
        # print(self.last_N_samples)
        if self.last_N_samples.shape[1] == self.N + 1:
            self.last_N_samples = np.delete(self.last_N_samples, 0, axis=1)

        # print(self.last_N_samples)

        # return [], self.last_prediction, prediction_error

        # self.last_prediction = sum([self.kernel[i] * self.last_N_samples[i] for i in range(self.N)])
        # self.last_prediction = scipy.ndimage.convolve1d(self.last_N_samples, self.kernel, axis=1)
        
        self.last_prediction = np.mean(self.last_N_samples, axis=1)

        prediction_error = np.subtract(u, self.last_prediction).reshape((self.N_inputs, 1))

        # print(self.last_prediction)

        anomaly = []
        
        for i in range(3):
            anomaly.append(self.anomaly_detectors[i].update(prediction_error[i]))

        return anomaly, self.last_prediction.flatten(), prediction_error.flatten()

