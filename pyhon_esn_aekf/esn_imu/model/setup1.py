#!/usr/bin/env python3

import numpy as np
from esn_imu.model.echo_state_network import EchoStateNetwork
from esn_imu.noise.noise_testing import PeakRecognition

class Setup1(EchoStateNetwork):
    def __init__(self, filename_w_res_res, filename_w_in_res, filename_w_res_out, rho1=0.02, rho2=0.001):
        """
        Class for Setup 1 of the ESN.
            ESN for anomaly detection with sensor reading as input to the AEKF.
            On update it shall return:
                bool anomaly
                float signal
        """
        # It is assumed that before this is loaded a network is trained and saved in files.
        super(Setup1, self).__init__(
            numberOfInputs = 1,
            numberOfOutputs = 1,  
            numberOfHiddenUnits = 40,
            inputSparsity = 0.5,
            internalSparsity = 0.5,
            spectralRadius = 0.6,
            inputScaling = 0.6,
            leakingRate = 0.7,
            reservoirNeuronBias = 0.001,
            noiseRange = 0.0001
        )
        self.anomaly_detector = PeakRecognition(rho1, rho2)
        self.last_prediction = None

        super(Setup1, self).load_from_file(filename_w_res_res, filename_w_in_res, filename_w_res_out)

    def get_update(self, u):
        if self.last_prediction is None:
            prediction_error = np.zeros((1, self.numberOfOutputs))
        else:
            prediction_error = np.abs(np.subtract(u, self.last_prediction))[0].reshape((1, self.numberOfOutputs))

        self.last_prediction = super(Setup1, self).predict(u)
        anomaly = self.anomaly_detector.update(prediction_error)
        # print(prediction_error)
        return anomaly, u.flatten()[0], prediction_error.flatten()[0]


if __name__ == "__main__":
    s1 = Setup1("save_files/weights_res_res.txt", "save_files/weights_in_res.txt", "save_files/weights_res_out.txt")
    print(s1)

    print(s1.get_update(np.array([5])))