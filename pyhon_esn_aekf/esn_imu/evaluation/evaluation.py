#/usr/bin/env python3

# Path hack.
# import sys, os
# sys.path.insert(0, os.path.abspath('..'))

import numpy as np
from esn_imu.model.echo_state_network import *
import matplotlib.pyplot as plt
# import concurrent.futures
import multiprocessing
from itertools import product

def evaluation(dataInput : np.ndarray, dataTarget : np.ndarray,
               neurons, input_scaling, spectral_radius, leak_rate,
               split_point : float):
    """
    Function for evaluating the ESN with different parameters.
    """
    esn = EchoStateNetwork(
        numberOfInputs = 1, 
        numberOfOutputs = 1,
        numberOfHiddenUnits = neurons, 
        inputSparsity = 0.5,
        internalSparsity = 0.5,
        spectralRadius = spectral_radius, # To preserve echo state
        inputScaling = input_scaling,
        leakingRate = leak_rate,
        reservoirNeuronBias = 0.001,
        noiseRange = 0.0001
    )

    split_point_index = int(split_point * dataInput.shape[0])
    trainData = dataInput[:split_point_index]
    trainTarget = dataTarget[:split_point_index]
    testData = dataInput[split_point_index:]
    testTarget = dataTarget[split_point_index:]

    ## Train the network
    w_out = esn.train_RLS(trainData, trainTarget, auto_corr=1, repetitions=1,
            iterations=1500, debug=False, learning_rate=0.99)

    # Test the network 
    predictions = []
    for u in testData:
        predictions.append(esn.predict(u))

    # Cutoff the first 100 samples as they are not representative.
    cutoff = 100
    testTarget = testTarget[cutoff:]
    predictions = np.asarray(predictions)[cutoff:]
    predictions = predictions.reshape(predictions.shape[0], esn.numberOfOutputs)

    # Calculate the errror
    nrmsd = esn.calculate_error(testTarget, predictions)
    return nrmsd 

def evaluation_wrapper(n, input_scale, spectral_rad, leak_rate, iterations):
    nrmsd = 0
    for i in range(iterations):
        nrmsd += evaluation(
            dataInput=       dataInput, 
            dataTarget=      dataTarget, 
            neurons=         n, 
            input_scaling=   input_scale, 
            spectral_radius= spectral_rad, 
            leak_rate=       leak_rate, 
            split_point=     0.7
        )

    nrmsd /= float(iterations)
    print(f"{n}, {input_scale}, {spectral_rad}, {leak_rate}, {nrmsd}")

    with open("output/parameter_selection_results.csv", 'a') as f:
        print(f"{n}, {input_scale}, {spectral_rad}, {leak_rate}, {nrmsd}", file=f)
    
    return (n, input_scale, spectral_rad, leak_rate, nrmsd)


if __name__ == "__main__":
    """
    This function wil start x number of threads that will call
    evaluation to evaluate the effect of different parameters
    on the ESN performance.
    """

    # neurons = [10, 20]
    # input_scalings = [0.1, 0.2]
    # spectral_radius = [0.1, 0.2]
    # leaking_rates = [0.2, 0.3]
    neurons = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    input_scalings = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    spectral_radius = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0] 
    leaking_rates = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0] 
    iterations = [3]

    maxTime = len(neurons) * len(input_scalings) * len(spectral_radius) * len(leaking_rates) * len(iterations)

    # dataInput  = np.loadtxt("input/imu_data_rpy.csv")[:, 1]
    dataInput = np.loadtxt("input/MackeyGlass_t17.txt")[:-1]
    dataTarget = np.loadtxt("input/MackeyGlass_t17.txt")[1:]
    # dataTarget = np.loadtxt("input/imu_data_rpy_target.csv")[:, 1]

    print(f"neurons, input_scale, spectral_radius, leak_rate, nrmsd")
    with open("output/parameter_selection_results.csv", 'a') as f:
        print(f"neurons, input_scale, spectral_radius, leak_rate, nrmsd", file=f)
        

    # with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    # for a in range(len(neurons)):
    #     for b in range(len(input_scalings)):
    #         for c in range(len(spectral_radius)):
    #             for d in range(len(leaking_rates)):
                    # nrmsd = evaluation_wrapper(neurons[a], leaking_rates[b], spectral_radius[c], leaking_rates[d])

                    # print(f"The calculated NRMSD is {nrmsd}")

    expression_list = product(neurons, input_scalings, spectral_radius, leaking_rates, iterations)

    results = pool.starmap(evaluation_wrapper, expression_list) 
    
    print("processing finished")
    print(results)

    plt.show()
    print("Done")