#!/usr/bin/env python3

import numpy as np
from dataclasses import dataclass, field


@dataclass
class ConfusionMatrix:
    tp : int # True positives
    fp : int # False positives
    tn : int # True negatives
    fn : int # False negatives
    mcc : float = field(init=False)

    def __post_init__(self):
        self.mcc = self.get_mcc()

    # mcc : float = field(default_factory=get_mcc(tn, fp, fn, tp))
    def get_mcc(self):
        mcc = (self.tp * self.tn - self.fp * self.fn)
        denom = np.sqrt(
            (self.tp + self.fp) * 
            (self.tp + self.fn) * 
            (self.tn + self.fp) * 
            (self.tn + self.fn)
        )

        if denom == 0:
            denom = 1

        mcc /= denom

        return mcc

    
    

def get_confusion_matrix(true, prediction):
    K = len(np.unique(true)) # number of classes
    result = np.zeros((K, K))

    # print("true", true, true.shape)
    # print("prediction", prediction, prediction.shape)
    # print(result.shape)

    for i in range(len(true)):
        # print(true[i], prediction[i])
        result[int(true[i]), int(prediction[i])] += 1

    return result

