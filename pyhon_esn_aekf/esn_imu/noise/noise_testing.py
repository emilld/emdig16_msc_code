#/usr/bin/env python3
import numpy as np

def inject_short_anomalies(in_data, beta, period):
    in_data = np.asarray(in_data)
    data_size = in_data.shape[0]
    # Calculate std on the whole signal
    std = np.std(np.asarray(in_data), axis=0)
    # step_size = int(np.floor(data_size / no_of_faults))
    period = int(period)
    injected_anomalies = []
    for i in range(period, data_size, period):
        # print("beta:", beta, "std:", std)
        # print(i)
        # print(in_data[i])
        # print(in_data[i, :])
        in_data[i, :] += beta * std
        injected_anomalies.append(i)

    # print("inj", injected_anomalies)
    return in_data, injected_anomalies

def inject_noise_anomalies(in_data, start, w, beta):
    # in_data: Data to be altered.
    # start: The first index to be altered.
    # w: the length of the noise window.
    # beta: the amplification factor.
    
    # Calculate std for the period:
    std = np.std(in_data[start : start + w], 0)

    # print(std)
    noise_samples = np.random.normal(0, beta * std, w)
    print(noise_samples)

    in_data[start : start + w] += noise_samples
    return [i for i in range(start, start+w)] # indexes of anomalies

class PeakRecognition:
    # Three stages:
    #   1. Reduce the perturbations in the prediction error by applying a moving average on
    #       two consecutive data points. 
    #       d_avg(i) = 0.5 * (abs(d(i - 1)) + abs(d(i)))
    #   2.  Mark datapoints as anomalies by applying two different tresholds to two consecutive data points:
    #       d_avg(i) > rho1         # rho1; detect general location of anomaly
    #       d_avg(i - 1) > rho2     # only applied if the first is true. Ensure correct detection of starting point.
    #   3.  Removes extrked data points by assuming that single point anomalies will at most be marked
    #       as four data points.
    def __init__(self, rho1, rho2):
        self.rho1 = rho1
        self.rho2 = rho2
        # Lists for keeping the data and the filtered data.
        # 0 is the same as i, and 1 is i - 1.
        # The first element in the list is the newest.
        self.dataPoints = [0, 0]
        self.dataPointsAvg = [0, 0] # Filtered data points

        self.anomaly_history = [0, 0, 0, 0, 0]
        
    def update(self, datapoint):
        # Moving the data points
        datapoint = float(datapoint)
        self.dataPoints[1] = self.dataPoints[0]
        self.dataPoints[0] = datapoint
    
        self._stage1()
        retVal = self._stage2()
        retVal = self._stage3(retVal)

        # print(self.dataPoints)
        # print(self.dataPointsAvg)

        return retVal

    def _stage1(self):
        # Moving the saved average points.
        new_avg = 0.5 * (abs(self.dataPoints[1]) + abs(self.dataPoints[0]))
        self.dataPointsAvg[1] = self.dataPointsAvg[0]
        self.dataPointsAvg[0] = new_avg

    def _stage2(self):
        if self.dataPointsAvg[0] > self.rho1:
            if self.dataPointsAvg[1] > self.rho2:
                return True

        return False

    def _stage3(self, result):
        self.anomaly_history.insert(0, result)
        del self.anomaly_history[-1]

        # print(self.anomaly_history)

        if sum(self.anomaly_history) >= len(self.anomaly_history):
            # It has reached the limit of operation.
            self.anomaly_history = [0 for i in range(len(self.anomaly_history))]

        if sum(self.anomaly_history) > 1:
            return False

        return result

 