#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.markers as markers
import matplotlib.pyplot as plt
import os
import copy
import random

from esn_imu.model.echo_state_network import *
from esn_imu.noise.noise_testing import *

def train_network(keys, leakingRate, output_dir, prefix, scaling=1., noise=0.):
    esn = EchoStateNetwork(
        numberOfInputs = len(keys),
        numberOfOutputs = len(keys),
        numberOfHiddenUnits = 50,
        inputSparsity = 0.1,
        internalSparsity = 0.1,
        spectralRadius = 0.99, # To preserve echo state
        inputScaling = 0.4,
        feedbackScaling = 0.4,
        # leakingRate = 0.3,
        leakingRate = leakingRate,
        reservoirNeuronBias = 0.001,
        noiseRange = 0.0001
    )
    esn.reset()

    if noise == 0:
        noise = np.zeros((len(keys), 1))

    df = pd.read_csv("input/numsim_drone/hexa_aekf_mpc_sim.csv")
    df.columns = df.columns.str.lstrip()
    df = df.filter(regex=("state_measured*"))
    # data = df.loc[:, df.columns.isin(["state_measured0", "state_measured2", "state_measured4"])]
    # df = df[500:1500]
    # df = df[500:2500]
    data_orig = df.loc[:, keys] / scaling
    data = data_orig[:-1]
    data_target = data_orig[1:]
    print(data)

    plt.figure()
    plt.plot(data)
    plt.grid()
    plt.legend(data.columns)

    # state_measured0: roll
    # state_measured1: roll rate
    # state_measured2: pitch
    # state_measured3: pitch rate
    # state_measured4: yaw
    # state_measured5: yaw rate
    # state_measured6: x
    # state_measured7: y
    # state_measured8: z

    # Train network for roll, pitch, and yaw.
    # Split the data up into a training set and a test set
    split_point = int(0.8 * data.shape[0])
    train_data = np.asarray(data[keys][:split_point])
    train_target = np.asarray(data_target[keys][:split_point])
    test_data = np.asarray(data[keys][split_point:])
    test_target = np.asarray(data_target[keys][split_point:])

    plt.figure()
    plt.plot(train_data, label="trainData")
    plt.plot(train_target, label="trainTarget")
    plt.legend()

    plt.figure()
    plt.plot(test_data, label="testData")
    plt.plot(test_target, label="testTarget")
    plt.legend()


    print("###########################################")
    print(f"######## Training {keys} #################")
    print("###########################################")

    # w_out = esn.train(train_data, train_target, debug=True)
    repetitions = 2
    w_out = esn.train_RLS(train_data, train_target, auto_corr=1e-4, repetitions=repetitions,
            iterations=5000, debug=True, learning_rate=0.9999995)
    # w_out = esn.train_dropout(train_data, train_target,
    #                           L_pairs=4, err=0.001,
    #                           max_iter=5,
    #                           learning_rate=0.9999995,
    #                         #   learning_rate=0.99995,
    #                         #   auto_corr=1e-4,
    #                           auto_corr=1e-4,
    #                           debug=True,
    #                           dropout_prob=0.8)

    print("Trained w_out", esn.W_out)


    print("###########################################")
    print("############## Predicting #################")
    print("###########################################")

    predictions = []

    i = 0
    id_dropout = random.randint(0, len(keys) - 1)
    beta = 2
    std = np.std(test_data, axis=0)
    
    for u in test_data:
        i += 1
        # if i > 1200 and i < 1260:
        #     u[0] *= 0
        # u[0] *= 0
        # if i == 1200:
        #     u[0] = 0
        # if i > 1200 and i < 1300:
        #     u[0] = 0
        # if i % 50 == 0:
        #     idx = random.randint(0, len(keys) - 1)
        #     u[idx] += 5. * np.pi/180. # degree
        #     # u[idx] += beta * std[idx]

        if i > 1250 and i < 1270:
            # u[id_dropout] = 0
            # u[0] = 0            
            # u[0] += noise[0] / scaling[0]
            u[0] *= 2

        if i == 500:
            # u[0] += 5 * np.pi/180.
            u[0] += noise[0] / scaling[0]
        elif i == 1000:
            u[1] += noise[1] / scaling[1]
        elif i == 1500:
            u[2] += noise[2] / scaling[2]

        if i > 1550:
            u[0] = 0

        # if i > 900 and i < 1000:
        #     u[2] = 0
        # #     u[0] *= 10
        predictions.append(esn.predict(u).reshape(esn.numberOfOutputs,))

    # print(predictions)
    predictions = np.asarray(predictions)
    # print(predictions)

    nrmsd = esn.calculate_error(test_target, predictions)
    print(f"nrmsd: {nrmsd}")

    # err = np.abs(np.subtract(predictions[1:,:], test_target.reshape((test_target.shape[0], esn.numberOfOutputs))[:-1,:]))
    # err = np.abs(np.subtract(predictions[1:,:], test_data.reshape((test_data.shape[0], esn.numberOfOutputs))[:-1,:]))
    err = np.abs(np.subtract(predictions, test_data.reshape((test_data.shape[0], esn.numberOfOutputs))))
    # print(predictions)
    # print(test_data)

    # fig, ax = plt.subplots(311)
    # ax[0].plot(predictions, label="Predictions")
    # ax[0].plot(test_data, label="Input")
    # ax[0].legend()
    # ax[0].grid()

    fig, axs = plt.subplots(len(keys), 1, sharex=True)
    fig_err, axs_err = plt.subplots(len(keys), 1, sharex=True)

    if (len(keys) >= 1):
        axs[0].sharex(axs_err[0])

        for i in range(len(keys)):
            axs[i].plot(test_data[:, i], label=f"Input, {keys[i]}")
            axs[i].plot(test_target[:, i], label=f"Target, {keys[i]}")
            axs[i].plot(predictions[:, i], label=f"Predictions, {keys[i]}")
            axs[i].legend()
            axs[i].grid()

            axs_err[i].plot(err[:, i], label="Error")
            axs_err[i].legend()
            axs_err[i].grid()
    else:
        axs.plot(test_data[:, 0], label=f"Input, {keys[0]}")
        axs.plot(test_target[:, 0], label=f"Target, {keys[0]}")
        axs.plot(predictions[:, 0], label=f"Predictions, {keys[0]}")
        axs.legend()
        axs.grid()

        axs_err.plot(err[:, 0], label="Error")
        axs_err.legend()
        axs_err.grid()

    print(esn)
    esn.save_files(output_dir, prefix)


if __name__ == "__main__":
    np.random.seed(seed=42)

    # # roll, pitch, yaw
    # keys = ["state_measured0", "state_measured2", "state_measured4"]
    # train_network(keys, 0.7, "", scaling=[np.pi/2, np.pi/2, 2*np.pi], 
    #     noise=[5 * np.pi/180., 5 * np.pi/180., 5 * np.pi/180.])
    # plt.show()

    # # roll rate, pitch rate, yaw rate
    # keys = ["state_measured1", "state_measured3", "state_measured5"]
    # train_network(keys, 0.1, "", scaling=[np.pi, np.pi, np.pi],
    #     noise=[10 * np.pi/180., 10 * np.pi/180., 10 * np.pi/180.])
    # plt.show()

    # # x, y, z
    # keys = ["state_measured6", "state_measured7", "state_measured8"]
    # train_network(keys, 0.1, "", scaling=[1000, 1000, 1000],
    #     noise=[1., 1., 1.])
    # plt.show()

    # # roll, roll rate, y
    # keys = ["state_measured0", "state_measured1", "state_measured7"]
    # train_network(keys, 0.1, "", scaling=[np.pi, 2 * np.pi, 1000],
    #     noise=[5 * np.pi/180., 10 * np.pi/180., 1.])
    # plt.show()

    # roll, roll rate, pitch, pitch rate, yaw, yaw rate, x, y, z
    # keys = [f"state_measured{i}" for i in range(9)]
    # train_network(keys, 0.1, "", scaling=[np.pi/2, np.pi, np.pi/2, np.pi, 2*np.pi, np.pi, 1000, 1000, 1000],
    #     noise=[5 * np.pi/180., 10*np.pi/180., 5 * np.pi/180., 10*np.pi/180., 5 * np.pi/180., 10*np.pi/180., 1., 1., 1.])
    # # train_network(keys, "")
    # plt.show()

    # # roll
    # keys = ["state_measured0"]
    # train_network(keys, "", scaling=[1])
    # plt.show()

    # roll, roll rate, y
    np.random.seed(seed=42)
    keys = ["state_measured0", "state_measured1", "state_measured7"]
    train_network(keys, 0.1, "save_files/num_sims/", "roll_rollrate_y", 
        scaling=[np.pi/2, np.pi, 100],
        noise=[5 * np.pi/180., 10 * np.pi/180., 1.])
    # plt.show()

    # pitch, pitch rate, x
    np.random.seed(seed=42)
    keys = ["state_measured2", "state_measured3", "state_measured6"]
    train_network(keys, 0.1, "save_files/num_sims/", "pitch_pitchrate_x", 
        scaling=[np.pi/2, np.pi, 100],
        noise=[5 * np.pi/180., 10 * np.pi/180., 1.])
    # plt.show()

    # yaw, yaw rate, z
    np.random.seed(seed=42)
    keys = ["state_measured4", "state_measured5", "state_measured8"]
    train_network(keys, 0.1, "save_files/num_sims/", "yaw_yawrate_z", 
        scaling=[2*np.pi, np.pi, 100],
        noise=[5 * np.pi/180., 10 * np.pi/180., 1.])
    plt.show()

    # roll, roll rate, y, pitch, pitch rate, x
    # keys = ["state_measured0", "state_measured1", "state_measured7", "state_measured2", "state_measured3", "state_measured6"]
    # train_network(keys, "", scaling=[np.pi, np.pi, 1000, np.pi, np.pi, 1000])
    # plt.show()

    # # angles and rates
    # keys = ["state_measured0", "state_measured1", "state_measured2", "state_measured3", "state_measured4", "state_measured5"]
    # # train_network(keys, "", scaling=[np.pi, np.pi, np.pi, np.pi, np.pi, np.pi])
    # train_network(keys, "")
    # plt.show()