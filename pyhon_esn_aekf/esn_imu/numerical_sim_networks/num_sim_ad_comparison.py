#!/usr/bin/env python3

"""
Script for comparing the anomaly detectors (AD) for the sensor measurements.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import esn_imu.numerical_sim_networks.esn_setup as esn_setup

from esn_imu.noise.noise_testing import inject_short_anomalies, inject_noise_anomalies

def main():
    # Load in all the setups including files.
    path = "save_files/num_sims/"
    filename_w_in_res = f"{path}pitch_pitchrate_x_weights_in_res.txt" # these two are the same for all setups
    filename_w_res_res = f"{path}pitch_pitchrate_x_weights_res_res.txt"
    
    # Roll, roll rate, y
    ad_roll_rollrate_y = esn_setup.Setup(filename_w_res_res, 
        filename_w_in_res, 
        filename_w_res_out=f"{path}roll_rollrate_y_weights_res_out.txt",
        factor=[np.pi/2, np.pi, 100],
        rho1=[0.05, 0.1, 0.5], rho2=[0.001, 0.001, 0.001])
    
    # Pitch, pitch rate, x
    ad_pitch_pitchrate_x = esn_setup.Setup(filename_w_res_res, 
        filename_w_in_res, 
        filename_w_res_out=f"{path}pitch_pitchrate_x_weights_res_out.txt",
        factor=[np.pi/2, np.pi, 100],
        rho1=[0.1, 0.2, 0.5], rho2=[0.001, 0.001, 0.001])
    
    # Yaw, yaw rate, z
    ad_yaw_yawrate_z = esn_setup.Setup(filename_w_res_res, 
        filename_w_in_res, 
        filename_w_res_out=f"{path}yaw_yawrate_z_weights_res_out.txt",
        factor=[np.pi/2, np.pi, 100],
        rho1=[0.01, 0.01, 0.1], rho2=[0.0001, 0.0001, 0.0001])

    # Load data in
    df = pd.read_csv("input/numsim_drone/hexa_aekf_mpc_sim.csv")[1000:2000]
    df.columns = df.columns.str.lstrip()
    df = df.filter(regex=("state_measured*"))
    data = df

    dt = 0.01
    t = np.arange(0, dt * data.shape[0], dt)

    networks = [ad_roll_rollrate_y, ad_pitch_pitchrate_x, ad_yaw_yawrate_z]
    names           = [["state_measured0", "state_measured1", "state_measured7"],
                       ["state_measured2", "state_measured3", "state_measured6"],
                       ["state_measured4", "state_measured5", "state_measured8"]] # column keys
    shot_noise_size = [[5, 10, 1], [10, 10, 1], [20, 20, 1]]
    # shot_noise_size = [      0,       0,       0,      0,       0,     0,           0,           0,           0]
    injected_anomalies = {}
    
    print(data)
    print(names[0])
    print(data.loc[:, names[0]])

    for name, xxx in zip(names, shot_noise_size):
        # injected_anomalies[name] = inject_short_anomalies(data.loc[:, name], xxx, 50)
        print(data.loc[45:55, name])
        out_data, tmp = inject_short_anomalies(data.loc[:, name], xxx, 50)
        data.loc[:, name] = out_data
        injected_anomalies[tuple(name)] = tmp # anomalies, data
        print(out_data[45:55])

    i = 0
    for name, network in zip(names, networks):
        aekf_input = []
        pred_err_filtered = []
        pred_err_old_filtered = []
        # print(name)

        ttt = 0
        tmpdata = np.asarray(data.loc[:, name])
        print("tmpdata", tmpdata)
        for iii in range(tmpdata.shape[0]):
            ttt += dt
            if ttt > 2.1 and ttt <= 2.13:
                tmpdata[iii, 2] *= 0

            # if ttt > 4 and ttt <= 4.25:
            #     tmpdata[iii, 0] *= 0

        data.loc[:, name] = tmpdata

        for u in np.asarray(data.loc[:, name]):
            # ttt += dt

            # if ttt > 4 and ttt < 9:
            #     u[0] *= 10
                # print(ttt)
            # print(u)
            # print(setup1.get_update(u))
            # aekf_input.append(network.get_update(u))
            update_val = network.get_update(np.asarray(u))
            # print("update_val\n", update_val)
            aekf_input.append(update_val)
            # aekf_input[-1] = (aekf_input[-1][0], -aekf_input[-1][1], -aekf_input[-1][2])
            pred_err_filtered.append(network.anomaly_detectors[i].dataPointsAvg[0])
            pred_err_old_filtered.append(network.anomaly_detectors[i].dataPointsAvg[1])
        
        i += 1

        pred_err_filtered = np.asarray(pred_err_filtered)
        pred_err_old_filtered = np.asarray(pred_err_old_filtered)
        # print(aekf_input)
        aekf_input = np.asarray(aekf_input)
        print(aekf_input)
        
        fig, axs = plt.subplots(3, 1, sharex=True)
        fig_err, axs_err = plt.subplots(3, 1, sharex=True)
        # axs[0].title(name)
        # axs_err[0].title(name)

        for j in range(3):
            # print(data.loc[:, name[j]])
            # print(aekf_input[:, 1][:, j])
            # print(aekf_input[:, 0][:, j])

            axs[j].plot(t, data.loc[:, name[j]], '-', label="Input")
            axs[j].plot(t, aekf_input[:, 1][:, j], '--', label="Output")
            tmp = np.where(aekf_input[:, 0][:, j] > 0)[0]
            
            # print(aekf_input)
            # print(tmp)
            # # print(t)
            # # # if tmp.shape[0] > 0:
            # # #     rho = tmp[-1] / t[-1]
            anomaly_y_offset = np.ones_like(tmp) * np.mean(np.asarray(data.loc[:, name[j]])) + \
                1 * np.std(np.asarray(data.loc[:, name[j]]))
            anomaly_y_offset_inject = np.ones_like(injected_anomalies[tuple(name)]) * np.mean(np.asarray(data.loc[:, name[j]])) + \
                2 * np.std(np.asarray(data.loc[:, name[j]]))

            # print(t[tmp], "\n", anomaly_y_offset)
            # print(t[injected_anomalies[name]], "\n", anomaly_y_offset_inject)

            # print("Signal mean:", np.mean(data[name]))
            # print("Signal std:", np.std(data[name]))

            # # plt.plot(np.multiply(tmp, dt), np.zeros_like(tmp), "ro", fillstyle='none', label="Detected Anomalies")
            axs[j].plot(t[tmp], anomaly_y_offset, "ro", fillstyle='none', label="Detected Anomalies")

            # # plt.plot(np.multiply(injected_anomalies, dt), np.zeros_like(injected_anomalies) + 0.05, "kx", label="Injected Anomalies")
            # # plt.plot(np.multiply(injected_anomalies[name], dt), np.zeros_like(injected_anomalies[name]) + 0.05, "kx", label="Injected Anomalies")
            axs[j].plot(t[injected_anomalies[tuple(name)]], anomaly_y_offset_inject, "kx", label="Injected Anomalies")

            # # plt.ylabel("Angle [rad]")
            # # plt.ylabel("Position $x$ [m]")
            # plt.xlabel("Time [s]")
            # # plt.title(name)

            # # plt.legend()
            axs[j].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
            ncol=2, mode="expand", borderaxespad=0.)

            axs[j].grid()
            # plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
            # plt.savefig(f'./output/plots/comparison_{name}.pdf')

            # Plot of the prediction error
            # fig = plt.figure()
            # plt.title(name)
            axs_err[j].plot(t, aekf_input[:, 2][:, j], label="Prediction error")
            # print(pred_err_filtered)
            # axs_err[j].plot(t, pred_err_filtered[:, j], label="Prediction error filtered")
            # plt.plot(t, pred_err_old_filtered, label="Pred err filtered old")
            axs_err[j].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
               ncol=2, mode="expand", borderaxespad=0.)
            axs_err[j].grid()
            # plt.ylabel("Error [rad]")
            axs_err[j].set_ylabel("Error [m]")
            axs_err[j].set_xlabel("Time [s]")
            # plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
            # axs_err[j].set_ylim((-0.001, 0.1))

        plt.show()

    plt.show()

if __name__ == "__main__":
    main()