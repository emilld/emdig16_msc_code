#!/usr/bin/env python3

"""
Script for making experiment 4.
The idea is to apply faults of a specific amplitude and frequency, and
count the confusion matrix parameters.

    True positive  (TP): Predicted an anomaly and there was one.
    False positive (FP): Predicted an anomaly when there wasn't one.
    True negative  (TN): Didn't predict anomaly when there wasn't one.
    False negative (FN): Didn't predict anomaly when there was one. Misses one.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import copy

# import esn_imu.numerical_sim_networks.esn_setup as esn_setup
import esn_imu.model.setup_lpf as lpf_setup
from esn_imu.noise.noise_testing import inject_short_anomalies, inject_noise_anomalies
from esn_imu.noise.confusion_matrix import get_confusion_matrix, ConfusionMatrix

CUTOFF = 100


class Experiment:
    def __init__(self):
        # Load in all the setups including files.
        path = "save_files/num_sims/"
        filename_w_in_res = f"{path}pitch_pitchrate_x_weights_in_res.txt" # these two are the same for all setups
        filename_w_res_res = f"{path}pitch_pitchrate_x_weights_res_res.txt"
        
        # Roll, roll rate, y
        # self.ad_roll_rollrate_y = esn_setup.Setup(filename_w_res_res, 
        #     filename_w_in_res, 
        #     filename_w_res_out=f"{path}roll_rollrate_y_weights_res_out.txt",
        #     factor=[np.pi/2, np.pi, 100],
        #     rho1=[0.02, 0.015, 0.2], rho2=[0.001, 0.001, 0.001])
        self.ad_roll_rollrate_y = lpf_setup.SetupLPF(10, 3, rho1=[0.02, 0.015, 0.2], rho2=[0.001, 0.001, 0.001])

        self.col_names = ["state_measured0", "state_measured1", "state_measured7"]

         # Load data in
        df = pd.read_csv("input/numsim_drone/hexa_aekf_mpc_sim.csv")[1000:2000]
        df.columns = df.columns.str.lstrip()
        df = df.filter(regex=("state_measured*"))
        self.data = df

        self.dt = 0.01
        self.t = np.arange(0, self.dt * self.data.shape[0], self.dt)


    def run_trial(self, period, beta):
        # period: number of samples between spikes.
        # beta: size of spikes relative to signal std.

        shot_noise_size = [1, 1, 1] # times the signal std
        shot_noise_size = [shot_noise_size[i] * beta for i in range(len(shot_noise_size))]

        injected_anomalies = {}
        # # for name, noise in zip(self.col_names, shot_noise_size):
        out_data, tmp = inject_short_anomalies(self.data.loc[:, self.col_names], shot_noise_size, period)
        _data = copy.deepcopy(self.data)
        _data.loc[:, self.col_names] = out_data
        # tmp = []
        injected_anomalies[tuple(self.col_names)] = tmp # anomalies, data

        # try it out
        aekf_input = []
        pred_err_filtered = []
        pred_err_old_filtered = []

        for u in np.asarray(_data.loc[:, self.col_names]):
            # ttt += dt

            # if ttt > 4 and ttt < 9:
            #     u[0] *= 10
                # print(ttt)
            # print(u)
            # print(setup1.get_update(u))
            # aekf_input.append(network.get_update(u))
            update_val = self.ad_roll_rollrate_y.get_update(np.asarray(u))
            # print("update_val\n", update_val)
            aekf_input.append(update_val)
            # aekf_input[-1] = (aekf_input[-1][0], -aekf_input[-1][1], -aekf_input[-1][2])
            pred_err_filtered.append([self.ad_roll_rollrate_y.anomaly_detectors[i].dataPointsAvg[0] for i in range(3)])
            pred_err_old_filtered.append([self.ad_roll_rollrate_y.anomaly_detectors[i].dataPointsAvg[1] for i in range(3)])

        pred_err_filtered = np.asarray(pred_err_filtered)
        pred_err_old_filtered = np.asarray(pred_err_old_filtered)
        # print(aekf_input)

        # print(pred_err_filtered)

        aekf_input = np.asarray(aekf_input)
        # print(aekf_input)
            
        fig, axs = plt.subplots(3, 1, sharex=True)
        fig_err, axs_err = plt.subplots(3, 1)
        # axs[0].title(name)
        # axs_err[0].title(name)

        all_conf_mats = []

        for j in range(3):
            # print(data.loc[:, name[j]])
            # print(aekf_input[:, 1][:, j])
            # print(aekf_input[:, 0][:, j])
            axs_err[j].sharex(axs[j])

            axs[j].plot(self.t, _data.loc[:, self.col_names[j]], '-', label="Input")
            axs[j].plot(self.t, aekf_input[:, 1][:, j], '--', label="Output")
            tmp = np.where(aekf_input[:, 0][:, j] > 0)[0]

            # print(tmp)
            # print(aekf_input[:, 0][:, j])
            
            # print(aekf_input)
            # print(tmp)
            # # print(t)
            # # # if tmp.shape[0] > 0:
            # # #     rho = tmp[-1] / t[-1]
            anomaly_y_offset = np.ones_like(tmp) * np.mean(np.asarray(_data.loc[:, self.col_names[j]])) + \
                1 * np.std(np.asarray(_data.loc[:, self.col_names[j]]))
            anomaly_y_offset_inject = np.ones_like(injected_anomalies[tuple(self.col_names)]) * np.mean(np.asarray(_data.loc[:, self.col_names[j]])) + \
                2 * np.std(np.asarray(_data.loc[:, self.col_names[j]]))

            # print(t[tmp], "\n", anomaly_y_offset)
            # print(t[injected_anomalies[name]], "\n", anomaly_y_offset_inject)
            
            # confusion_matrix
            # print(injected_anomalies[tuple(self.col_names)])
            ground_truth = np.zeros_like(_data.loc[:, self.col_names[j]])
            ground_truth[injected_anomalies[tuple(self.col_names)]] = 1

            # print("ground truth", ground_truth)

            predicted = aekf_input[:, 0][:, j]

            # print("Signal mean:", np.mean(data[name]))
            # print("Signal std:", np.std(data[name]))

            confusion_matrix = get_confusion_matrix(ground_truth[CUTOFF:], predicted[CUTOFF:])
            print("confusion matrix")
            print(confusion_matrix)

            confmat = ConfusionMatrix(confusion_matrix[1, 1], confusion_matrix[0,1], 
                confusion_matrix[0, 0], confusion_matrix[1, 0])

            all_conf_mats.append(confmat)

            print(f"tn {confmat.tn}, fp {confmat.fp}, fn {confmat.fn}, tp {confmat.tp}, mcc {confmat.mcc}")

            # # plt.plot(np.multiply(tmp, dt), np.zeros_like(tmp), "ro", fillstyle='none', label="Detected Anomalies")
            axs[j].plot(self.t[tmp], anomaly_y_offset, "ro", fillstyle='none', label="Detected Anomalies")

            # # plt.plot(np.multiply(injected_anomalies, dt), np.zeros_like(injected_anomalies) + 0.05, "kx", label="Injected Anomalies")
            # # plt.plot(np.multiply(injected_anomalies[name], dt), np.zeros_like(injected_anomalies[name]) + 0.05, "kx", label="Injected Anomalies")
            axs[j].plot(self.t[injected_anomalies[tuple(self.col_names)]], anomaly_y_offset_inject, "kx", label="Injected Anomalies")
            

            # # plt.ylabel("Angle [rad]")
            # # plt.ylabel("Position $x$ [m]")
            # plt.xlabel("Time [s]")
            # # plt.title(name)

            # # plt.legend()
            axs[j].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
            ncol=2, mode="expand", borderaxespad=0.)

            axs[j].grid()
            # plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
            # plt.savefig(f'./output/plots/comparison_{name}.pdf')

            # Plot of the prediction error
            # fig = plt.figure()
            # plt.title(name)
            axs_err[j].plot(self.t, aekf_input[:, 2][:, j], label="Prediction error")
            # print(pred_err_filtered)
            axs_err[j].plot(self.t, pred_err_filtered[:, j], label="Prediction error filtered")
            axs_err[j].plot(self.t, pred_err_old_filtered[:, j], label="Prediction err filtered old")

            axs_err[j].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
                ncol=2, mode="expand", borderaxespad=0.)
            axs_err[j].grid()
            # plt.ylabel("Error [rad]")
            axs_err[j].set_ylabel("Error [m]")
            axs_err[j].set_xlabel("Time [s]")
            # plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
            # axs_err[j].set_ylim((-0.001, 0.1))

        # plt.show()
        fig.savefig(f"output/esn_noise_spikes_test/plots_lpf/beta-{beta}_period-{period}_signals.pdf")
        fig_err.savefig(f"output/esn_noise_spikes_test/plots_lpf/beta-{beta}_period-{period}_errors.pdf")
        plt.close()

        return all_conf_mats

def main(): 
    periods = np.arange(10, 110, 10)
    amplitudes = np.arange(1, 11, 1)
    # periods = np.arange(10, 20, 10)
    # amplitudes = np.arange(1, 2, 1)

    outfile = "output/esn_noise_spikes_test/confusion_matrices_lpf.csv"
    with open(outfile, "w") as f:
        f.write("period,amplitude,tn1,fp1,fn1,tp1,tn2,fp2,fn2,tp2,tn3,fp3,fn3,tp3\n")

    for p in periods:
        for a in amplitudes:
            exp = Experiment()
            conf_mats = exp.run_trial(period=p, beta=a) # returns three confusion matrices
            print(conf_mats)
            
            with open(outfile, "a") as f:
                f.write(f"{p},{a},{','.join([f'{conf_mats[i].tn},{conf_mats[i].fp},{conf_mats[i].fn},{conf_mats[i].tp}' for i in range(3)])}\n")

if __name__ == "__main__":
    main()