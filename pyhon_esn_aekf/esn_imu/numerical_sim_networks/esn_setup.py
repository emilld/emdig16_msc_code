#!/usr/bin/env python3

import numpy as np
from esn_imu.model.echo_state_network import EchoStateNetwork
from esn_imu.noise.noise_testing import PeakRecognition

class Setup(EchoStateNetwork):
    def __init__(self, filename_w_res_res, filename_w_in_res, filename_w_res_out, rho1=0.15, rho2=0.00005, factor=1):
        """
        Class for yaw anomaly detector the ESN.
            ESN for anomaly detection with ESN output as input to the AEKF.
            On update it shall return:
                bool anomaly
                float signal
        """
        # It is assumed that before this is loaded a network is trained and saved in files.
        super(Setup, self).__init__(
            numberOfInputs = 3,
            numberOfOutputs = 3,  
            numberOfHiddenUnits = 50,
            inputSparsity = 0.1,
            internalSparsity = 0.1,
            spectralRadius = 0.99,
            inputScaling = 0.4,
            feedbackScaling = 0.4,
            leakingRate = 0.1,
            reservoirNeuronBias = 0.001,
            noiseRange = 0.0001
        )
        print(isinstance(rho1, float))

        if isinstance(rho1, float):
            rho1 = rho1 * np.ones((self.numberOfInputs, 1))
        else:
            rho1 = np.asarray(rho1)
        
        if isinstance(rho2, float):
            rho2 = rho2 * np.ones((self.numberOfInputs, 1))
        else:
            rho2 = np.asarray(rho2)

        print(rho1, rho2)
        self.anomaly_detectors = [PeakRecognition(rho1[i], rho2[i]) for i in range(3)]
        self.last_prediction = np.zeros((3, 1))

        if factor == 1:
            self.factor = np.ones((3, 1))
        else:
            self.factor = np.asarray(factor).reshape((3, 1))

        super(Setup, self).load_from_file(filename_w_res_res, filename_w_in_res, filename_w_res_out)

    def get_update(self, u):
        # print(u)
        u = np.array(u)# .reshape((3, 1))
        u = u.reshape((3, 1))
        # print(u)

        # if self.last_prediction is None:
        #     prediction_error = np.zeros((self.numberOfOutputs, 1))
        #     # self.last_prediction = u
        # else:
        #     # prediction_error = np.abs(np.subtract(u, self.last_prediction))[0].reshape((self.numberOfOutputs, 1))
        #     # prediction_error = np.abs(np.subtract(u, self.last_prediction)).reshape((self.numberOfOutputs, 1))
        #   prediction_error = np.subtract(u, self.last_prediction).reshape((self.numberOfOutputs, 1))    
        
        # tmp = self.last_prediction
        # Scale the input by a factor to get it in the range of the ESN.
        # Undo the operation on the output

        # print("u/factor", u / self.factor)
        self.last_prediction = super(Setup, self).predict(u / self.factor) * self.factor
        
        prediction_error = np.subtract(u, self.last_prediction).reshape((self.numberOfOutputs, 1))

        anomaly = []
        for i in range(3):
            # print(prediction_error[i])
            anomaly.append(self.anomaly_detectors[i].update(prediction_error[i]))

        # return anomaly, tmp.flatten()[0], prediction_error.flatten()[0] # self.last_prediction.flatten()[0]
        return anomaly, self.last_prediction.flatten(), prediction_error.flatten() # self.last_prediction.flatten()[0]
        

if __name__ == "__main__":
    s2 = Setup("save_files/num_sims/pitch_pitchrate_x_weights_res_res.txt", 
               "save_files/num_sims/pitch_pitchrate_x_weights_in_res.txt", 
               "save_files/num_sims/pitch_pitchrate_x_weights_res_out.txt")
    print(s2)

    print(s2.get_update(np.array([5, 1, 2])))