#!/usr/bin/env python3

"""
Script for conducting a noise experiment of the ESN.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import esn_imu.numerical_sim_networks.esn_setup as esn_setup

from esn_imu.noise.noise_testing import inject_short_anomalies, inject_noise_anomalies

# #define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
# #define PBWIDTH 60

# void printProgress(double current, double all) {
#     double percentage = current / all;
#     int val = (int) (percentage * 100);
#     int lpad = (int) (percentage * PBWIDTH);
#     int rpad = PBWIDTH - lpad;
#     printf("\r%3d%% [%.*s%*s] %1.2f / %1.2f", val, lpad, PBSTR, rpad, "", current, all);
#     fflush(stdout);
# }

PBSTR = "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
PBWIDTH = 60

def printProgress(current, all):
    percentage = float(current)/float(all)
    value = int(percentage * 100)
    lpad = int(percentage * PBWIDTH)
    rpad = int(PBWIDTH - lpad)
    # print(f"\r{value:03d}% [{PBSTR[:lpad]}{" ":<rpad}]", end=" ")
    # print(f"\r{value:03d}% [{PBSTR[:lpad]}{" ":<int(rpad)}]", end=" ")
    # print(" " * rpad, "test")
    space = " " * rpad
    print(f"\r{value:3d}% [{PBSTR[:lpad]}{space}] {current:1.2f} / {all:1.2f}", end=" ")

# Load in all the setups including files.
path = "save_files/num_sims/"
filename_w_in_res = f"{path}pitch_pitchrate_x_weights_in_res.txt" # these two are the same for all setups
filename_w_res_res = f"{path}pitch_pitchrate_x_weights_res_res.txt"

# Roll, roll rate, y
ad_roll_rollrate_y = esn_setup.Setup(filename_w_res_res, 
    filename_w_in_res, 
    filename_w_res_out=f"{path}roll_rollrate_y_weights_res_out.txt",
    factor=[np.pi/2, np.pi, 100],
    rho1=[0.05, 0.1, 0.5], rho2=[0.001, 0.001, 0.001])

# Load data in
df = pd.read_csv("input/numsim_drone/hexa_aekf_mpc_sim.csv")[1000:2000]
df.columns = df.columns.str.lstrip()
df = df.filter(regex=("state_measured*"))
data = df

dt = 0.01
t = np.arange(0, dt * data.shape[0], dt)

networks = [ad_roll_rollrate_y]
names    = [["state_measured0", "state_measured1", "state_measured7"]] # column keys

step = 0.1
dt = 0.01
# noise_stds = np.arange(0, 0.2 + step, step)
# noise_factor = np.arange(0, 10 + step, step)
noise_factor = np.arange(0.00, 5.01, 0.01)
# measurement_std = dt * np.array([0.5, 0.1, 1.])

trials = 5

output_folder = "output/esn_noise_test/data/"

for name, network in zip(names, networks):
    max_progress = trials * len(noise_factor)

    for j, std_level in enumerate(noise_factor):
        # print("std_level", std_level)
        # print("j", j)

        for i in range(trials):
            # input = np.asarray(data.loc[:, name])[:-1]
            original_input = np.asarray(data.loc[:, name])[:-1]
            input = []
            target = np.asarray(data.loc[:, name])[1:]
            predictions = []

            data_range = np.array([np.ptp(target[:, i]) for i in range(target.shape[1])])
            # print("data_range", data_range)

            for u in original_input:
                # The noise should be scaled with the network factor from esn.network    
                rand_noise = np.random.normal(loc=0.0, scale=1.0, size=(3,))
                # print(rand_noise)
                # print(network.factor)
                # rand_noise = np.multiply(rand_noise, network.factor.reshape((3,)))
                # print(rand_noise, std_level, data_range)
                # print(std_level * data_range)

                rand_noise = np.multiply(rand_noise, std_level * data_range)
                # print(rand_noise)
                u += rand_noise
                update_val = network.get_update(np.asarray(u))
                # print("update_val\n", update_val)
                # esn_output.append(update_val)
                input.append(u)
                predictions.append(update_val[1])
            
            input = np.asarray(input)
            predictions = np.asarray(predictions)

            # print(esn_output)
            printProgress(trials * j + i, max_progress)

            # with open(output_folder + f"trial-{i:2d}_noise-{std_level:1.2f}.csv", "w") as f:
            with open(output_folder + f"noise-{std_level:1.3f}_trial-{i:2d}.csv", "w") as f:
                # Output the input, the predictions, and the target
                # for u, y, d in zip(input, predictions, target):
                f.write("input1,input2,input3,prediction1,prediction2,prediction3,target1,target2,target3\n")

                for k in range(input.shape[0]):
                    f.write(
                        f"{','.join(np.char.mod('%f', input[k, :]))}," +
                        f"{','.join(np.char.mod('%f', predictions[k, :]))}," +
                        f"{','.join(np.char.mod('%f', target[k, :]))}\n"
                    )



