#!/usr/bin/env python3 
import numpy as np
from esn_imu.motor_fault_estimation import compute_dynamics_hexa

L = 0.37
dt = 0.01 # Not needed

A, b, B, C = compute_dynamics_hexa.Hexacopter.get_model(dt, L)

print(B)

# ub = 8 # N
ub = 21.812
lb = 0

# Max throttle
motor_forces = np.asarray([ub, ub, ub, ub, ub, ub])
virtual_forces = B @ motor_forces
print("max T:", virtual_forces[0])

# Min throttle
motor_forces = np.asarray([lb, lb, lb, lb, lb, lb])
virtual_forces = B @ motor_forces
print("min T:", virtual_forces[0])

# Max Mx
motor_forces = np.ones((6,)) * lb
motor_forces[[1, 2, 5]] = ub
virtual_forces = B @ motor_forces
print("max Mx:", virtual_forces[1])

# Min Mx
motor_forces = np.ones((6,)) * lb
motor_forces[[0, 3, 4]] = ub
virtual_forces = B @ motor_forces
print("min Mx:", virtual_forces[1])

# Max My
motor_forces = np.ones((6,)) * lb
motor_forces[[2, 4]] = ub
virtual_forces = B @ motor_forces
print("max My:", virtual_forces[2])

# Min My
motor_forces = np.ones((6,)) * lb
motor_forces[[3, 5]] = ub
virtual_forces = B @ motor_forces
print("min My:", virtual_forces[2])

# Max Mz
motor_forces = np.ones((6,)) * lb
motor_forces[[1, 3, 4]] = ub
virtual_forces = B @ motor_forces
print("max Mz:", virtual_forces[3])

# Min Mz
motor_forces = np.ones((6,)) * lb
motor_forces[[0, 2, 5]] = ub
virtual_forces = B @ motor_forces
print("min Mz:", virtual_forces[3])