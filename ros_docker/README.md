# Readme for the simulation environment used in my master thesis.
## Docker container with ROS, PX4 and Gazebo for the simulation of drones.

This repository should be cloned to the computer.

Download the PX4 firmware repo version and select version v1.11.0.
    
    cd docker-workspace
    git clone https://github.com/PX4/Firmware.git
    cd Firmware
    git checkout tags/v1.11.0
    git submodule update --init --recursive

It can be benificial to add your user to the docker group such that sudo is not needed to run docker commands.

    sudo groupadd docker
    sudo usermod -aG docker $USER

and reboot the computer.

Build the Docker image in the root directory of this git repo using new Docker buildkit by setting the env variable `DOCKER_BUILDKIT=1`:
    
    DOCKER_BUILDKIT=1 docker build --tag master_px4_gazebo \
    --build-arg USER_ID=$(id -u) \
    --build-arg GROUP_ID=$(id -g) .

In main terminal run:
    
    ./run-docker-image.sh

If other terminals are needed run the following to open a bash shell:

    docker exec -it master_px4_gazebo-tester bash

Now build the PX4 firmware inside the Docker image according to the guide.

    cd /workspace/Firmware
    DONT_RUN=1 make px4_sitl_default gazebo

Build the ROS workspace in the Docker image:

    cd /workspace/ros_ws
    catkin build

Now test the system to see if it works.
In one terminal run:
    
    beit-ws
    roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"

And in another run:

    beit-ws
    roslaunch eit_playground posix.launch vehicle:=sdu_drone


## Command to display local position

    rosrun rqt_plot rqt_plot /anomaly_test/lal_position_out/pose/pose/position/x /anomaly_test/local_position_out/pose/pose/position/y /anomaly_test/local_position_out/pose/pose/position/z


Compare prediction signal and anomaly signal

    rosrun rqt_plot rqt_plot /anomaly_test/esn_err /anomaly_test/anomaly

Compare esn prediction and real signal

    rosrun rqt_plot rqt_plot /anomaly_test/local_position_out/pose/pose/position/x /mavros/local_position/pose/pose/position/x 