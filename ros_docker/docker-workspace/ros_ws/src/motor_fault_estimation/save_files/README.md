In this directory the weights for the Echo State Networks used for the Anomaly detection are saved.
`weights_in_res.txt` and `weights_res_res.txt` are shared between all networks.

### Disclaimer: 
As per today (9/2 2021) all of the `weights_res_out_{sensor_val}.txt` are the same.
At a later stage these should be trained for their respective sensor data.
