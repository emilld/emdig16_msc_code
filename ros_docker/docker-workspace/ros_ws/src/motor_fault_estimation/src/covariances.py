import numpy as np
import copy

cov_pos_default = [0.01, 0.01, 0.01] # std for x, y, z
# cov_pos_anomaly = [10000 * x for x in cov_pos_default] # [10000 for i in range(3)] # big std for x, y, z
# cov_pos_anomaly = copy.copy(cov_pos_default) # std for x, y, z
# cov_pos_anomaly[0] = 1e100
cov_pos_anomaly = [cov_pos_default[i]*1e20 for i in range(3)]
# cov_pos_anomaly = [0, 0, 0]

# cov_imu_default = [0.5,    0.1,   0.5,      0.1, 0.2,    0.1]
cov_imu_default = [0.05,    0.5,   0.05,    0.5, 0.05,    0.5]
                # [phi, phidot, theta, thetadot, psi, psidot]
# cov_imu_anomaly = [100 for i in range(6)]
# cov_imu_anomaly = copy.copy(cov_imu_default)
cov_imu_anomaly = [cov_imu_default[i]*1e20 for i in range(6)]