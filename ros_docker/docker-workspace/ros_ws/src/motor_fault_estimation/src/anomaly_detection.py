#!/usr/bin/env python3

import rospy
import numpy as np

from std_msgs.msg import Float64, Header
from motor_fault_estimation.msg import Float64Anomaly

from esn_imu.model.setup2 import Setup2 as Setup


class AnomalyDetection():
    def __init__(self):
        rospy.init_node('anomaly_detection')

        # subscribers
        self.sub_value_in = rospy.Subscriber('~value_in', Float64, self.cb_value_in)
        
        # publisher
        self.pub_value_out = rospy.Publisher('~value_out', Float64Anomaly, queue_size=1)

        # parameters
        filename_w_res_res = rospy.get_param("~filename_w_res_res", "motor_fault_estimation/save_files/weights_res_res.txt")
        filename_w_in_res  = rospy.get_param("~filename_w_in_res",  "motor_fault_estimation/save_files/weights_in_res.txt")
        filename_w_res_out = rospy.get_param("~filename_w_res_out", "motor_fault_estimation/save_files/weights_res_out.txt")
        rho1               = rospy.get_param("~rho1", 0.02)
        rho2               = rospy.get_param("~rho1", 0.001)
        
        self.network = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out, rho1, rho2)

        rospy.spin()


    def cb_value_in(self, msg):
        # rospy.loginfo(msg)

        # self.pub_value_out.publish(Float64Anomaly(data=msg.data, anomaly=True))
        # Value to anomaly network
        res = self.network.get_update(np.asarray(msg.data)) # outputs in format (anomaly, data)
        # rospy.loginfo(res)

        h = Header()
        h.stamp = rospy.Time.now()
        self.pub_value_out.publish(Float64Anomaly(header=h, data=res[1], anomaly=res[0]))


if __name__ == "__main__":
    node = AnomalyDetection()