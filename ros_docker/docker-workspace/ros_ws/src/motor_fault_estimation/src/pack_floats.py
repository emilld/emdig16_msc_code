#!/usr/bin/env python3

import rospy
import message_filters
import numpy as np

from geometry_msgs.msg import PoseWithCovarianceStamped, PoseWithCovariance, Pose, Point
from sensor_msgs.msg import Imu
from std_msgs.msg import Float64, Header
from motor_fault_estimation.msg import Float64Anomaly

import covariances

class Pack:
    def __init__(self):
        rospy.init_node("value_packer")

        # subcribers
        self.sub_pose_x = message_filters.Subscriber('~pose/x', Float64Anomaly)
        self.sub_pose_y = message_filters.Subscriber('~pose/y', Float64Anomaly)
        self.sub_pose_z = message_filters.Subscriber('~pose/z', Float64Anomaly)
        
        ts_pose = message_filters.ApproximateTimeSynchronizer([self.sub_pose_x, self.sub_pose_y, self.sub_pose_z], 10, 0.1)
        ts_pose.registerCallback(self.cb_pose)

        self.sub_orien_x = message_filters.Subscriber('~orientation/x', Float64Anomaly)
        self.sub_orien_y = message_filters.Subscriber('~orientation/y', Float64Anomaly)
        self.sub_orien_z = message_filters.Subscriber('~orientation/z', Float64Anomaly)
        self.sub_orien_w = message_filters.Subscriber('~orientation/w', Float64Anomaly)

        self.sub_ang_vel_x = message_filters.Subscriber('~ang_vel/x', Float64Anomaly)
        self.sub_ang_vel_y = message_filters.Subscriber('~ang_vel/y', Float64Anomaly)
        self.sub_ang_vel_z = message_filters.Subscriber('~ang_vel/z', Float64Anomaly)

        ts_imu = message_filters.ApproximateTimeSynchronizer(
            [
                self.sub_orien_x,
                self.sub_orien_y,
                self.sub_orien_z,
                self.sub_orien_w,
                self.sub_ang_vel_x, 
                self.sub_ang_vel_y, 
                self.sub_ang_vel_z
            ], 10, 0.1)
        ts_imu.registerCallback(self.cb_imu)

        # Publishers
        self.pub_pose = rospy.Publisher('~pose_out', PoseWithCovarianceStamped, queue_size=1)
        self.pub_imu  = rospy.Publisher('~imu_out', Imu, queue_size=1)

        rospy.spin()

    def cb_pose(self, x, y, z):
        # rospy.logerr(f"{x.data}, {y.data}, {z.data}")
        # rospy.logerr(f"{x.anomaly}, {y.anomaly}, {z.anomaly}")
        std_vec = [0, 0, 0]
        data = [x, y, z]
        for i in range(3):
            if data[i].anomaly:
                std_vec[i] = covariances.cov_pos_anomaly[i]
            else:
                std_vec[i] = covariances.cov_pos_default[i]

        out = PoseWithCovarianceStamped()
        out.header = Header()
        out.header.stamp = rospy.Time.now()
        # out.pose = PoseWithCovariance(
        #     pose = Point(x=x, y=y, z=z),
        #     covariance = np.diag(std_vec + [0, 0, 0]).flatten()
        # )
        out.pose.pose.position.x = x.data
        out.pose.pose.position.y = y.data
        out.pose.pose.position.z = z.data

        out.pose.pose.orientation.x = 0 # orientations are not used
        out.pose.pose.orientation.y = 0
        out.pose.pose.orientation.z = 0
        out.pose.pose.orientation.w = 0

        # rospy.logerr(std_vec)
        # rospy.logerr(np.diag(std_vec + [0, 0, 0]).flatten())
        out.pose.covariance = np.diag(std_vec + [0, 0, 0]).flatten()

        # rospy.logerr(out)
        
        self.pub_pose.publish(out)
    
    def cb_imu(self, x, y, z, w, dx, dy, dz):
        out = Imu()
        out.header = Header()
        out.header.stamp = rospy.Time.now()

        out.orientation.x = x.data
        out.orientation.y = y.data
        out.orientation.z = z.data
        out.orientation.w = x.data

        out.angular_velocity.x = dx.data
        out.angular_velocity.y = dy.data
        out.angular_velocity.z = dz.data

        # Covariances
        # Orientations are as quaternion, so I won't do the translation to rpy.
        # for this reason if there is an anomaly on one reading, there is on everyone.
        if np.any([x.anomaly, y.anomaly, z.anomaly, w.anomaly]):
            out.orientation_covariance = np.diag(
                [covariances.cov_imu_default[i] for i in [0, 2, 4]]
            ).flatten()
        else:
            out.orientation_covariance = np.diag(
                [covariances.cov_imu_anomaly[i] for i in [0, 2, 4]]
            ).flatten()


        std_vec = [0, 0, 0]
        ix = [1, 3, 5]
        data = [dx, dy, dz]
        for i in range(3):
            if data[i].anomaly:
                std_vec[i] = covariances.cov_imu_anomaly[ix[i]]
            else:
                std_vec[i] = covariances.cov_imu_default[ix[i]]

        out.angular_velocity_covariance = np.diag(
            std_vec
        ).flatten()

        # rospy.logerr(out)
        self.pub_imu.publish(out)

def main():
    node = Pack()

if __name__ == "__main__":
    main()