#!/usr/bin/env python3

import rospy
import numpy as np

from std_msgs.msg import Float64, Header
from motor_fault_estimation.msg import Float64Anomaly
from geometry_msgs.msg import PoseStamped, TwistStamped, PoseWithCovarianceStamped, PoseWithCovariance
from mavros_msgs.msg import RCOut
from sensor_msgs.msg import Imu

# from esn_imu.model.setup2 import Setup2 as Setup
# from esn_imu.model.setup1 import Setup1 as Setup 
# Setup1 outputs the original value and if it's an anomaly.
# Setup2 outputs a filtered value and if it's an anomaly.

# Position xy AD
import esn_imu.model.anomaly_detectors.setup_pos_xy as setup_pos_xy
# Position z AD
import esn_imu.model.anomaly_detectors.setup_pos_z as setup_pos_z
# Roll, pitch AD
import esn_imu.model.anomaly_detectors.setup_rp as setup_rp
# Yaw AD 
import esn_imu.model.anomaly_detectors.setup_y as setup_y
# Angular velocities, x, y, z, AD
import esn_imu.model.anomaly_detectors.setup_ang_vel_xy as setup_ang_vel_xy
import esn_imu.model.anomaly_detectors.setup_ang_vel_z as setup_ang_vel_z


from transforms3d.euler import quat2euler, euler2quat

import copy

import covariances

class AnomalyDetection():
    def __init__(self):
        rospy.init_node('anomaly_detection')

        # moving averages for the rc out data which is very noisy
        self.rc_filter_N = rospy.get_param("~rc_filter_N", 5)
        self.rc_filter_N_inv = 1 / self.rc_filter_N
        self.rc_out_mean = np.zeros((1, 6))

        # The networks are trained on data sampled at 10Hz.
        # The networks should only be updated at this rate, thus I need to save the incoming messages.
        self.last_position_msg = None
        self.last_imu_msg = None
        self.previous_position_msg = None
        self.previous_imu_msg = None
        dt = 0.1 # 10 Hz
        self.iii = 0

        # subscribers
        self.sub_local_position = rospy.Subscriber('~local_position_in', PoseStamped, self.cb_local_pos)
        self.sub_imu            = rospy.Subscriber('~imu_in', Imu, self.cb_imu)
        self.sub_rc_channels    = rospy.Subscriber('~rcout_in', RCOut, self.cb_rc)
        
        # publishers
        self.pub_local_position = rospy.Publisher('~local_position_out', PoseWithCovarianceStamped, queue_size=1)
        self.pub_imu            = rospy.Publisher('~imu_out', Imu, queue_size=1)
        self.pub_rc_channels    = rospy.Publisher('~rcout_out', RCOut, queue_size=1)

        # Debug publisher for the anomaly detector error signal
        self.pub_esn_err        = rospy.Publisher('~esn_err', Float64, queue_size=1)
        self.pub_anomaly        = rospy.Publisher('~anomaly', Float64, queue_size=1)
        # parameters
        # The weights 
        #   from input to reservoir and
        #   from reservoir to reservoir
        # are the same for all networks.
        filename_w_res_res = rospy.get_param("~filename_w_res_res", "motor_fault_estimation/save_files/weights_res_res.txt")
        filename_w_in_res  = rospy.get_param("~filename_w_in_res",  "motor_fault_estimation/save_files/weights_in_res.txt")
        
        filename_w_res_out_pose_x = rospy.get_param("~filename_w_res_out_pose_x", "motor_fault_estimation/save_files/weights_res_out_pose_x.txt")
        filename_w_res_out_pose_y = rospy.get_param("~filename_w_res_out_pose_y", "motor_fault_estimation/save_files/weights_res_out_pose_y.txt")
        filename_w_res_out_pose_z = rospy.get_param("~filename_w_res_out_pose_z", "motor_fault_estimation/save_files/weights_res_out_pose_z.txt")
        filename_w_res_out_orientation_x = rospy.get_param("~filename_w_res_out_orientation_x", "motor_fault_estimation/save_files/weights_res_out_orientation_x.txt")
        filename_w_res_out_orientation_y = rospy.get_param("~filename_w_res_out_orientation_y", "motor_fault_estimation/save_files/weights_res_out_orientation_y.txt")
        filename_w_res_out_orientation_z = rospy.get_param("~filename_w_res_out_orientation_z", "motor_fault_estimation/save_files/weights_res_out_orientation_z.txt")
        filename_w_res_out_ang_vel_x = rospy.get_param("~filename_w_res_out_ang_vel_x", "motor_fault_estimation/save_files/weights_res_out_ang_vel_x.txt")
        filename_w_res_out_ang_vel_y = rospy.get_param("~filename_w_res_out_ang_vel_y", "motor_fault_estimation/save_files/weights_res_out_ang_vel_y.txt")
        filename_w_res_out_ang_vel_z = rospy.get_param("~filename_w_res_out_ang_vel_z", "motor_fault_estimation/save_files/weights_res_out_ang_vel_z.txt")

        # rho1               = rospy.get_param("~rho1", 0.02)
        # rho2               = rospy.get_param("~rho1", 0.001)
        
        # Define networks for all sensors
        # self.network_pose_x = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_pose_x, rho1, rho2)
        self.network_pose_x = setup_pos_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_pose_x, rho1=0.1)
        # self.network_pose_y = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_pose_y, rho1, rho2)
        self.network_pose_y = setup_pos_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_pose_y, rho1=0.1)
        # self.network_pose_z = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_pose_z, rho1, rho2)
        self.network_pose_z = setup_pos_z.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_pose_z, rho1=0.1, rho2=0.00005, factor=100)

        # self.network_orientation_x = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_orientation_x, rho1, rho2)
        self.network_orientation_x = setup_rp.Setup(filename_w_res_res, filename_w_in_res,  filename_w_res_out_orientation_x, rho1=0.2)
        # self.network_orientation_y = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_orientation_y, rho1, rho2)
        self.network_orientation_y = setup_rp.Setup(filename_w_res_res, filename_w_in_res,  filename_w_res_out_orientation_y, rho1=0.2)
        # self.network_orientation_z = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_orientation_z, rho1, rho2)
        self.network_orientation_z = setup_y.Setup(filename_w_res_res, filename_w_in_res,  filename_w_res_out_orientation_z, rho1=0.2)

        # self.network_ang_vel_x = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_ang_vel_x, rho1, rho2)
        self.network_ang_vel_x = setup_ang_vel_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_ang_vel_x, rho1=1)
        # self.network_ang_vel_y = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_ang_vel_y, rho1, rho2)
        self.network_ang_vel_y = setup_ang_vel_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_ang_vel_y, rho1=1)
        # self.network_ang_vel_z = Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_ang_vel_z, rho1, rho2)
        self.network_ang_vel_z = setup_ang_vel_z.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out_ang_vel_z, rho1=1)


        rospy.Timer(rospy.Duration(dt), self.timer_loop)

        rospy.spin()


    # def cb_value_in(self, msg):
    #     # rospy.loginfo(msg)

    #     # self.pub_value_out.publish(Float64Anomaly(data=msg.data, anomaly=True))
    #     # Value to anomaly network
    #     res = self.network.get_update(np.asarray(msg.data)) # outputs in format (anomaly, data)
    #     # rospy.loginfo(res)

    #     h = Header()
    #     h.stamp = rospy.Time.now()
    #     self.pub_value_out.publish(Float64Anomaly(header=h, data=res[1], anomaly=res[0]))
    def timer_loop(self, timer_event):
        if self.last_imu_msg is None \
            or self.last_position_msg is None \
            or self.previous_position_msg is None \
            or self.previous_imu_msg is None:
            # rospy.logerr("stuck")
            return 
        
        self.iii += 1
        # rospy.logerr(self.iii)
        # if self.iii == 200: # inject anomaly each second
        if self.iii > 10:
            self.last_position_msg.pose.position.x += 0.5
            # self.previous_position_msg.
            self.last_position_msg.pose.position.y += 0.5
            self.last_position_msg.pose.position.z += 0.5
            # self.pub_anomaly.publish(0.3)
            # err = True
        # else:
            # self.pub_anomaly.publish(0)
            # err = False

        # Updates the local position networks to find possible anomalies.
        res_x = self.network_pose_x.get_update(np.asarray(self.last_position_msg.pose.position.x)) # outputs in format (anomaly [true/false], data)
        res_y = self.network_pose_y.get_update(np.asarray(self.last_position_msg.pose.position.y))
        res_z = self.network_pose_z.get_update(np.asarray(self.last_position_msg.pose.position.z))
        
        # rospy.logerr(res_x)

        # self.pub_esn_err.publish(res_z[2]) # the last position is the pred err
        # self.pub_anomaly.publish(float(res_z[0]) * 0.5 + 0.5)
        # self.pub_anomaly.publish()
        # rospy.logerr(res_z)

        data = [res_x, res_y, res_z]
        std_out = copy.copy(covariances.cov_pos_default)
        # rospy.logerr(std_out)

        for i in range(3):
            if data[i][0]:
            # if err:
                std_out[i] = covariances.cov_pos_anomaly[i]

        # rospy.logerr(std_out)

        out = PoseWithCovarianceStamped()
        # out.header = self.last_position_msg.header
        # # out.header = self.previous_position_msg.header
        out.header = Header(
            stamp=rospy.Time.now()
        )
        out.pose = PoseWithCovariance(
            pose = self.last_position_msg.pose,
            # pose = self.previous_position_msg.pose,
            covariance = np.diag(std_out + [0, 0, 0]).flatten()
        )

        # out.pose.pose.position.x = self.last_position_msg.pose.position.x
        # out.pose.pose.position.y = self.last_position_msg.pose.position.y
        # out.pose.pose.position.z = self.last_position_msg.pose.position.z

        # out.pose.pose.position.x = res_x[1]
        # out.pose.pose.position.y = res_y[1]
        # out.pose.pose.position.z = res_z[1]
        # out = copy.copy(self.last_position_msg)
        # out.pose.covariance = np.diag(std_out + [0, 0, 0]).flatten()

        # rospy.loginfo(out)
        self.previous_position_msg = self.last_position_msg
        self.pub_local_position.publish(out)

        # Updates the imu networks to find possible anomalies.
        # Convert quaternion to rpy
        quat_w = float(self.last_imu_msg.orientation.w)
        quat_x = float(self.last_imu_msg.orientation.x)
        quat_y = float(self.last_imu_msg.orientation.y)
        quat_z = float(self.last_imu_msg.orientation.z)
        
        # print([quat_w, quat_x, quat_y, quat_z])
        angles = list(quat2euler([quat_w, quat_x, quat_y, quat_z]))

        if self.iii > 10:
            angles[0] += np.deg2rad(50)
            angles[1] += np.deg2rad(50)
            angles[2] += np.deg2rad(50)
            self.last_imu_msg.angular_velocity.x += np.deg2rad(200)
            # self.previous_position_msg.
            self.last_imu_msg.angular_velocity.y += np.deg2rad(200)
            self.last_imu_msg.angular_velocity.z += np.deg2rad(200)
            self.iii = 0

        res_orientation_x = self.network_orientation_x.get_update(np.asarray(angles[0]))  # outputs in format (anomaly [true/false], data)
        res_orientation_y = self.network_orientation_y.get_update(np.asarray(angles[1]))
        res_orientation_z = self.network_orientation_z.get_update(np.asarray(angles[2]))
        res_ang_vel_x = self.network_ang_vel_x.get_update(np.asarray(self.last_imu_msg.angular_velocity.x))
        res_ang_vel_y = self.network_ang_vel_y.get_update(np.asarray(self.last_imu_msg.angular_velocity.y))
        res_ang_vel_z = self.network_ang_vel_z.get_update(np.asarray(self.last_imu_msg.angular_velocity.z))

        self.pub_esn_err.publish(res_ang_vel_z[2]) # the last position is the pred err
        self.pub_anomaly.publish(float(res_ang_vel_z[0]) * 0.5 + 0.5)

        out = copy.copy(self.last_imu_msg)
        out.header = Header(
            stamp=rospy.Time.now()
        )

        if np.any([res_orientation_x[0], res_orientation_y[0], res_orientation_z[0]]):
            out.orientation_covariance = np.diag(
                [covariances.cov_imu_default[i] for i in [0, 2, 4]]
            ).flatten()
        else:
            out.orientation_covariance = np.diag(
                [covariances.cov_imu_anomaly[i] for i in [0, 2, 4]]
            ).flatten()

        ix = [1, 3, 5]
        std_vec = [covariances.cov_imu_default[i] for i in ix]
        data = [res_ang_vel_x, res_ang_vel_y, res_ang_vel_z]
        for i in range(3):
            if data[i][0]:
                std_vec[i] = covariances.cov_imu_anomaly[ix[i]]

        out.angular_velocity_covariance = np.diag(
            std_vec
        ).flatten()

        # quat = euler2quat(res_orientation_x[1], res_orientation_y[1], res_orientation_z[1])

        # out.orientation.x = quat[1]
        # out.orientation.y = quat[2]
        # out.orientation.z = quat[3]
        # out.orientation.w = quat[0]
        # out.angular_velocity.x = res_ang_vel_x[1]
        # out.angular_velocity.y = res_ang_vel_y[1]
        # out.angular_velocity.z = res_ang_vel_z[1]

        # rospy.logerr(out)       

        self.pub_imu.publish(out)
        self.previous_imu_msg = self.last_imu_msg


    def cb_local_pos(self, msg):
        # self.previous_position_msg = self.last_position_msg
        if self.previous_position_msg is None:
            self.previous_position_msg = self.last_position_msg
        self.last_position_msg = msg

        # self.iii += 1
        # # rospy.logerr(self.iii)
        # # if self.iii == 200: # inject anomaly each second
        # if self.iii > 100:
        #     self.last_position_msg.pose.position.x += 0.05
        #     # self.last_position_msg.pose.position.y += 0.5
        #     # self.last_position_msg.pose.position.z += 0.5
        #     self.iii = 0
        # # Updates the local position networks to find possible anomalies.
        # res_x = self.network_pose_x.get_update(np.asarray(msg.pose.position.x)) # outputs in format (anomaly [true/false], data)
        # res_y = self.network_pose_y.get_update(np.asarray(msg.pose.position.y))
        # res_z = self.network_pose_z.get_update(np.asarray(msg.pose.position.z))

        # data = [res_x, res_y, res_z]
        # std_out = covariances.cov_pos_default
        # for i in range(3):
        #     if data[i][0]:
        #         std_out[i] = covariances.cov_pos_anomaly[i]

        # out = PoseWithCovarianceStamped()
        # out.header = msg.header
        # out.pose = PoseWithCovariance(
        #     pose = msg.pose,
        #     covariance = np.diag(std_out + [0, 0, 0]).flatten()
        # )

        # out.pose.pose.position.x = res_x[1]
        # out.pose.pose.position.y = res_y[1]
        # out.pose.pose.position.z = res_z[1]

        # # rospy.loginfo(out)
        # self.pub_local_position.publish(out)
        # # pass

    def cb_imu(self, msg):
        # self.previous_imu_msg = self.last_imu_msg
        if self.previous_imu_msg is None:
            self.previous_imu_msg = self.last_imu_msg
        self.last_imu_msg = msg
        # # Updates the imu networks to find possible anomalies.
        # # Convert quaternion to rpy
        # quat_w = float(msg.orientation.w)
        # quat_x = float(msg.orientation.x)
        # quat_y = float(msg.orientation.y)
        # quat_z = float(msg.orientation.z)
        
        # # print([quat_w, quat_x, quat_y, quat_z])
        # angles = quat2euler([quat_w, quat_x, quat_y, quat_z])

        # res_orientation_x = self.network_orientation_x.get_update(np.asarray(angles[0]))  # outputs in format (anomaly [true/false], data)
        # res_orientation_y = self.network_orientation_y.get_update(np.asarray(angles[1]))
        # res_orientation_z = self.network_orientation_z.get_update(np.asarray(angles[2]))
        # res_ang_vel_x = self.network_ang_vel_x.get_update(np.asarray(msg.angular_velocity.x))
        # res_ang_vel_y = self.network_ang_vel_y.get_update(np.asarray(msg.angular_velocity.y))
        # res_ang_vel_z = self.network_ang_vel_z.get_update(np.asarray(msg.angular_velocity.z))

        # out = msg

        # if np.any([res_orientation_x[0], res_orientation_y[0], res_orientation_z[0]]):
        #     out.orientation_covariance = np.diag(
        #         [covariances.cov_imu_default[i] for i in [0, 2, 4]]
        #     ).flatten()
        # else:
        #     out.orientation_covariance = np.diag(
        #         [covariances.cov_imu_anomaly[i] for i in [0, 2, 4]]
        #     ).flatten()

        # ix = [1, 3, 5]
        # std_vec = [covariances.cov_imu_default[i] for i in ix]
        # data = [res_ang_vel_x, res_ang_vel_y, res_ang_vel_z]
        # for i in range(3):
        #     if data[i][0]:
        #         std_vec[i] = covariances.cov_imu_anomaly[ix[i]]

        # out.angular_velocity_covariance = np.diag(
        #     std_vec
        # ).flatten()

        # quat = euler2quat(res_orientation_x[1], res_orientation_y[1], res_orientation_z[1])

        # out.orientation.x = quat[1]
        # out.orientation.y = quat[2]
        # out.orientation.z = quat[3]
        # out.orientation.w = quat[0]
        # out.angular_velocity.x = res_ang_vel_x[1]
        # out.angular_velocity.y = res_ang_vel_y[1]
        # out.angular_velocity.z = res_ang_vel_z[1]

        # # rospy.logerr(out)       

        # self.pub_imu.publish(out)

    def cb_rc(self, msg):
        # tmp = list(msg.channels)
        # tmp[0] = 2000
        # tmp[1] = 2000
        # msg.channels = tmp
        self.rc_out_mean = self.rc_out_mean + self.rc_filter_N_inv * (msg.channels[:6] - self.rc_out_mean)
        tmp = np.ndarray.tolist(np.rint(self.rc_out_mean))
        chans = list(msg.channels)
        # rospy.loginfo(tmp)
        for i in range(6):
            chans[i] = int(tmp[0][i])
        msg.channels = chans
        # rospy.loginfo(type(msg.channels[0]))
        # rospy.loginfo(msg)
        self.pub_rc_channels.publish(msg)


if __name__ == "__main__":
    node = AnomalyDetection()