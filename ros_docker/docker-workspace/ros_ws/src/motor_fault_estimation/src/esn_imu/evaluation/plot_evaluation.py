#!/usr/bin/env python3
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.rc('pgf', texsystem='pdflatex')

# df = pd.read_csv("output/parameter_selection_results.csv")
df = pd.read_csv("output/parameter_selection_results_backup.csv").sort_values(by=['neurons'])
df.columns = df.columns.str.strip()
print(df.columns)

## Figure 1
# This figure will plot the error in relation with number of neurons
df_subset = df[
                np.logical_and(
                    df.input_scale == 0.5, 
                    np.logical_and(
                        df.leak_rate == 0.5, 
                        df.spectral_radius == 0.5
                    )
                )
            ].sort_values(by=['neurons'])
print(df_subset)

# plt.plot(df_subset.neurons, df_subset.nrmsd)
tmp = [df.nrmsd[df.neurons == x] for x in df.neurons.unique()]

fig1 = plt.figure()
ax = fig1.add_subplot(111)
ax.boxplot(tmp)
ax.set_xticklabels([str(x) for x in df.neurons.unique()])
ax.set_xlabel("Neurons")
ax.set_ylabel("NRMSD")
ax.set_title("Boxplot of all trials")
plt.savefig('output/plots/fig_boxplot.pgf')

fig2 = plt.figure()
ax = fig2.add_subplot(111)
ax.plot(df.neurons.unique(), [np.mean(x) for x in tmp])
ax.set_title("Mean of all trials")
plt.savefig('output/plots/fig_mean_err.pgf')

# fig3 = plt.figure()
# ax = fig3.add_subplot(111)
# ax.plot(df_subset.neurons, df_subset.nrmsd)

## Figure 4
# Three plots showing:
#   131: leaking rate, input scaling
#   132: leaking rate, spectral radius
#   133: input scaling, spectral radius
chosen_neurons = 40

fig4 = plt.figure(figsize=(14,5))
ax = fig4.add_subplot(131, projection='3d')
tmp = df[np.logical_and(df.neurons == chosen_neurons, df.spectral_radius == 0.6)]
ax.plot_trisurf(tmp.leak_rate, 
                tmp.input_scale,
                tmp.nrmsd,
                cmap=plt.cm.viridis)

ax.set_xlabel("Leak rate")
ax.set_ylabel("Input scale")
ax.set_zlabel("NRMSD")
plt.title("Spectral radius = 0.6")

ax = fig4.add_subplot(132, projection='3d')
tmp = df[np.logical_and(df.neurons == chosen_neurons, df.input_scale == 0.1)]
ax.plot_trisurf(tmp.leak_rate, 
                tmp.spectral_radius,
                tmp.nrmsd,
                cmap=plt.cm.viridis)

ax.set_xlabel("Leak rate")
ax.set_ylabel("Spectral radius")
ax.set_zlabel("NRMSD")
plt.title("Input scale = 0.1")

ax = fig4.add_subplot(133, projection='3d')
tmp = df[np.logical_and(df.neurons == chosen_neurons, df.leak_rate == 0.7)]
ax.plot_trisurf(tmp.spectral_radius,
                tmp.input_scale,
                tmp.nrmsd,
                cmap=plt.cm.viridis)

ax.set_xlabel("Spectral radius")
ax.set_ylabel("Input scaling")
ax.set_zlabel("NRMSD")
plt.title("Leak rate = 0.7")

plt.tight_layout(w_pad=5)
plt.suptitle(f"{chosen_neurons} neurons")
plt.savefig('output/plots/fig_different_params.pgf')

plt.show()