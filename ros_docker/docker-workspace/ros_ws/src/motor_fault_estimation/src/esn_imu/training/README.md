Scripts for training the 10 ESNs needed for the Motor Fault Estimation implementation in ROS.
Data is saved in `input/motor_fault_estimation/datafile_normal_prop_03.csv`.
The following sensor readings should be used:

* quaternion x, y, z, w
* angular rates x, y, z
* position x, y, z
