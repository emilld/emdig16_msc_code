#!/usr/bin/env python3

import numpy as np
from esn_imu.model.echo_state_network import EchoStateNetwork
from esn_imu.noise.noise_testing import PeakRecognition

class Setup3(EchoStateNetwork):
    def __init__(self, filename_w_res_res, filename_w_in_res, filename_w_res_out, rho1=0.02, rho2=0.001):
        """
        Class for Setup 3 of the ESN.
            ESN for filtering of sensor data.
            Measurement uncertaintry of AEKF left untouched, therefor anomaly is always False.
            On update it shall return:
                bool anomaly
                float signal
        """
        # It is assumed that before this is loaded a network is trained and saved in files.
        super(Setup3, self).__init__(
            numberOfInputs = 1,
            numberOfOutputs = 1,  
            numberOfHiddenUnits = 40,
            inputSparsity = 0.5,
            internalSparsity = 0.5,
            spectralRadius = 0.6,
            inputScaling = 0.6,
            leakingRate = 0.7,
            reservoirNeuronBias = 0.001,
            noiseRange = 0.0001
        )
        # self.anomaly_detector = PeakRecognition(0.02, 0.001)
        # self.last_prediction_error = None
        self.anomaly_detector = PeakRecognition(rho1, rho2)

        super(Setup3, self).load_from_file(filename_w_res_res, filename_w_in_res, filename_w_res_out)

    def get_update(self, u):
        # if self.last_prediction_error is None:
        #     prediction_error = np.zeros((1, self.numberOfOutputs))
        # else:
        #     prediction_error = np.abs(np.subtract(u, last_prediction_error))[0]
        # last_prediction_error = prediction_error
        val = super(Setup3, self).predict(u)
        # anomaly = self.anomaly_detector.update(prediction_error)
        anomaly = False
        prediction_error = 0
        return anomaly, val, prediction_error
 
        
if __name__ == "__main__":
    s3 = Setup3("save_files/weights_res_res.txt", "save_files/weights_in_res.txt", "save_files/weights_res_out.txt")
    print(s3)

    print(s3.get_update(np.array([5])))