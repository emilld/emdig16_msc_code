#!/usr/bin/env python3
"""
Setup where a low pass filter is used instead of an Echo State Network to make anomaly detection.
"""

import numpy as np
from esn_imu.model.echo_state_network import EchoStateNetwork
from esn_imu.noise.noise_testing import PeakRecognition

class SetupLPF():
    def __init__(self, N, kernel="mean", rho1=0.02, rho2=0.001):
        self.anomaly_detector = PeakRecognition(rho1, rho2)
        self.last_prediction = None
        self.N = N
        self.last_N_samples = [0 for i in range(N)]

        if kernel == "mean":
            self.kernel = np.ones(N) / N
        # else if kernel == ""

    def get_update(self, u):
        """
        Has to be called get_update() to be compatible with the other setups.
        """
        if self.last_prediction is None:
            prediction_error = 0
        else:
            prediction_error = u - self.last_prediction

        # add a measurement to end and remove the first
        self.last_N_samples.append(u)
        self.last_N_samples.pop(0)

        self.last_prediction = sum([self.kernel[i] * self.last_N_samples[i] for i in range(self.N)])

        anomaly = self.anomaly_detector.update(prediction_error)

        return anomaly, self.last_prediction.flatten()[0], prediction_error

