#!/usr/bin/env python3

import scipy.sparse as sparse 
import scipy.stats as stats 
import numpy as np 
import matplotlib.pyplot as plt


class EchoStateNetwork:
    # Class for creating an Echo State Network
    def __init__(self, 
                 numberOfInputs,        # Nu
                 numberOfOutputs,       # Ny
                 numberOfHiddenUnits,   # Nx
                 inputSparsity,         # 
                 internalSparsity,      # 
                 spectralRadius,        #
                 inputScaling,          # 
                 leakingRate,            
                 reservoirNeuronBias,
                 noiseRange    # Add some noise to the output
                 ): 
        self.numberOfInputs = numberOfInputs
        self.numberOfOutputs = numberOfOutputs
        self.numberOfHiddenUnits = numberOfHiddenUnits
        self.leakingRate = leakingRate
        self.noiseRange = noiseRange
        self.reservoirNeuronBias = reservoirNeuronBias
        self.internalSparsity = internalSparsity
        self.inputSparsity = inputSparsity
        self.inputScaling = inputScaling
        self.spectralRadius = spectralRadius
        self.reset()
    
    def __str__(self):
        return (
            "Description of Echo State Network \n" +
            "\n" + 
            f"numberOfInputs {self.numberOfInputs}\n" + 
            f"numberOfOutputs {self.numberOfOutputs}\n" +
            f"numberOfHiddenUnits {self.numberOfHiddenUnits}\n" +
            f"leakingRate {self.leakingRate}\n" +
            f"noiseRange {self.noiseRange}\n" +
            f"reservoirNeuronBias {self.reservoirNeuronBias}\n" +
            f"internalSparsity {self.internalSparsity}\n" +
            f"inputSparsity {self.inputSparsity}\n" +
            f"inputScaling {self.inputScaling}\n" +
            f"spectralRadius {self.spectralRadius}"
        )
        
    def reset(self):        
        # Create the matrices
        # self.W_in = self.generate_weights((numberOfHiddenUnits, 1 + numberOfInputs), inputScaling, inputSparsity)
        self.W_in = self.generate_weights((self.numberOfHiddenUnits, self.numberOfInputs), self.inputScaling, self.inputSparsity) # Without bias node and feed forward.
        self.W_reservoir = self.generate_weights((self.numberOfHiddenUnits, self.numberOfHiddenUnits), 1, self.internalSparsity)
        self.W_reservoir = self.normalize_reservoir(self.W_reservoir, self.spectralRadius)
        # self.W_out = np.zeros((numberOfOutputs, 1 + numberOfInputs + numberOfHiddenUnits))
        self.W_out = np.zeros((self.numberOfOutputs, self.numberOfHiddenUnits)) # Without bias node and feed forward.

        self.x = np.zeros((self.numberOfHiddenUnits, 1))
        # print("W_in\n", self.W_in)
        # print("W_reservoir\n", self.W_reservoir)


    def load_from_file(self, filename_w_res_res, filename_w_in_res, filename_w_res_out):
        self.W_in = np.loadtxt(filename_w_in_res, ndmin=2)
        # print(f"W_in shape", self.W_in.shape)
        self.W_reservoir = np.loadtxt(filename_w_res_res, ndmin=2)
        self.W_out = np.loadtxt(filename_w_res_out, ndmin=2)

    @staticmethod
    def normalize_data(data):
        """
        Normalize data based on max min.
        """
        # return (np.subtract(data, np.min(data, axis=0)))/(np.subtract(np.max(data, axis=0), np.min(data, axis=0)))
        # Normalize the data to go from the interval [-pi; pi] to [-1; 1]
        return np.subtract(data, -np.pi) / np.subtract(np.pi, -np.pi)

    def train(self, trainData : np.ndarray, targetData : np.ndarray, beta=0, debug=False):
        # Ridge regression
        # states = np.zeros((1 + self.numberOfInputs + self.numberOfHiddenUnits, trainData.shape[0]))
        states = np.zeros((self.numberOfHiddenUnits, trainData.shape[0]))

        t = 0
        for u in trainData:
            if type(u) == np.ndarray:
                u = u[:self.numberOfInputs]
                # print("u", u)
            tmp = self.update(u)
            
            # states[:, t] = np.vstack((1, u, tmp))[:, 0]
            # states[:, t] = np.vstack((1, u.reshape((u.size, 1)), tmp))[:, 0]
            states[:, t] = tmp[:, 0]
            t += 1
        
        # tmp = np.asarray(states)
        # tmp = np.reshape(tmp, (tmp.shape[0], tmp.shape[1]))

        if debug:
            plt.figure()
            for i in range(10):
                plt.plot(states[i, :])
            plt.title("10 first activations")

        # Compute linear readout weights W_out from the reservoir using linear regression, minimizing the MSE between y(n) and y(target).
        # Ridge Regression
        targetData = np.reshape(targetData, (self.numberOfOutputs, targetData.shape[0]))
        
        self.W_out = targetData @ states.T
        tmp = states @ states.T
        tmp1 = tmp + beta * np.eye(states.shape[0])
        self.W_out = self.W_out @ np.linalg.inv(tmp1)
            
        if debug:
            plt.figure()
            # plt.bar(np.arange(1 + self.numberOfInputs + self.numberOfHiddenUnits), self.W_out[0].T)
            plt.bar(np.arange(self.numberOfHiddenUnits), self.W_out[0].T)
            plt.title("Trained weights")

        return self.W_out

    def train_RLS(self, trainData : np.ndarray, targetData : np.ndarray, repetitions = 1, iterations=2000, learning_rate = 0.99, auto_corr = 1e-4, debug=False):
        if debug:
            print("################################################")
            print("########## Training using RLS ##################")
            print("################################################")
        
        self.W_out = np.zeros((self.numberOfOutputs, self.numberOfHiddenUnits)) # Without bias node and feed forward.
        self.x = np.zeros((self.numberOfHiddenUnits, 1))
        P_mat = np.eye(self.numberOfHiddenUnits) / auto_corr
        # print(P_mat)
        # return
        if debug:
            states = np.zeros((self.numberOfHiddenUnits, trainData.shape[0]))

        if debug:
            debug_weights = []

        for kkk in range(repetitions):
            t = 0
            if debug: 
                print(f"Repetition {kkk}")
            i = 0
            for u, d in zip(trainData, targetData):
                i += 1
                if i == iterations:
                    break

                y = self.predict(u) # Update the reservoir activation and get a new output.
                e = d - y # Calculate error between desired and output
                e = np.reshape(np.asarray(e), (self.numberOfOutputs, 1))
                scale = (learning_rate + self.x.T @ P_mat @ self.x)
                if debug: 
                    print("scale", scale)
                K = P_mat @ self.x / scale
                # print(K.shape)
                # print(e.shape)
                P_mat = (P_mat - K @ self.x.T @ P_mat) / learning_rate
                # print(P_mat.shape)
                self.W_out = self.W_out + (K * e).T

                if debug:
                    debug_weights.append(self.W_out[0, :10])
                    states[:, t] = self.x[:, 0] 
                    t += 1
                
            # print(self.W_out.shape, (K * e).T.shape)

        # print(self.W_out)
        # print("Weight out shape", self.W_out.shape)

        if debug:
            print(debug_weights)
            
            plt.figure()
            # plt.bar(np.arange(1 + self.numberOfInputs + self.numberOfHiddenUnits), self.W_out[0].T)
            plt.bar(np.arange(self.numberOfHiddenUnits), self.W_out[0].T)
            plt.title("Trained weights")

            plt.figure()
            plt.title("History for the first 10 weights")
            debug_weights = np.asarray(debug_weights)
            plt.plot(debug_weights)
        
            plt.figure()
            for i in range(10):
                plt.plot(states[i, :])
            plt.title("10 first activations")


        return self.W_out


    def update(self, input):
        # k = np.vstack((
        #         1, input.reshape((input.size, 1))
        #     ))
        # print(k)
        # print(k.shape)
        # print(self.W_in.shape)
        rvs = stats.uniform(loc=-self.noiseRange, scale=2 * self.noiseRange).rvs
        noise_mat = sparse.random(self.numberOfHiddenUnits, self.numberOfOutputs, data_rvs=rvs)
        # print(noise_mat.toarray().shape)
        k = np.asarray(input).reshape((input.size, 1))

        # print(self.W_in)
        # print(self.W_in.shape, k.shape)
        tmp1 = self.W_in @ k
        # print(tmp1)
        tmp2 = self.W_reservoir @ self.x
        # print(tmp2)

        tmp = np.tanh(tmp1 + tmp2 + self.reservoirNeuronBias)
        # tmp = tmp1 + tmp2 + self.reservoirNeuronBias
        self.x = (1 - self.leakingRate) * self.x + self.leakingRate * tmp # + noise_mat.toarray()
        # print(self.x.shape) 
        return self.x

    def predict(self, input):
        self.update(input)
        
        # return self.W_out @ np.vstack((1, input.reshape((input.size, 1)), self.x))
        return self.W_out @ self.x

    @staticmethod
    def generate_weights(size : tuple, weightRange, sparsity):
        # size: tuple of the size (rows, cols)
        # weightRange: range of the uniform distribution 
        # sparsity: Number from 0.-1. describing probability of connection being zero.
        rvs = stats.uniform(loc=-weightRange, scale=2 * weightRange).rvs
        W = sparse.random(size[0], size[1], density=sparsity, data_rvs=rvs)
        # print(W)
        return W.toarray()

    @staticmethod
    def normalize_reservoir(W, spectralRadius):
        # Calculate current spectral radius by finding the largest eigenvalue:
        sr = np.real(np.max(np.linalg.eigvals(W)))
        # print(type(sr))
        W = W / sr
        return W * spectralRadius

    @staticmethod
    def calculate_error(targets, estimates):
        # Calculates the NRMSD
        #   Normalized Root Mean Square Error
        estimates = estimates.reshape(targets.shape)

        err = np.subtract(targets, estimates)
        # print("targets.shape", targets.shape)
        # print("estimates.shape", estimates.shape)
        # print("err shape", err.shape)
        # print("err\n", err)
        # mean square error
        mse = np.mean(err * err)
        # print("mse", mse)
        # Normalized Root Mean Square Error
        # nrmse = np.sqrt(mse / (len(targets) * np.var(targets)))
        # nrmse = np.sqrt(mse) / (np.max(targets) - np.min(targets))
        nrmse = np.sqrt(mse) / np.std(targets)
        # print(targets.shape[0], np.var(targets), targets.shape[0] * np.var(targets))
        # print(nrmse)
        # print("mse", mse)
        # print("var of targets", np.var(targets))
        return nrmse

    def save_files(self, folder, prefix=""):
        np.savetxt(folder + f"{prefix}weights_res_res.txt", self.W_reservoir)
        np.savetxt(folder + f"{prefix}weights_res_out.txt", self.W_out)
        np.savetxt(folder + f"{prefix}weights_in_res.txt", self.W_in)     
        

if __name__ == "__main__":
    esn = EchoStateNetwork(
        numberOfInputs = 1, 
        numberOfOutputs = 1,
        numberOfHiddenUnits = 50,
        inputSparsity = 0.5,
        internalSparsity = 0.5,
        spectralRadius = 0.9, # To preserve echo state
        inputScaling = 0.1,
        leakingRate = 0.3
    ) 

    