#!/usr/bin/evn python3

"""
name: simulation.py
author: Emil Lykke Diget, emil@lykke-diget.dk
date: 1/12 2020

Script for simulating drone flying in the same spot with simulated IMU data and
AEKF (Adaptive Extended Kalman Filter) motor fault estimation.
There will be introduced shot noise during the flight, which will be detected
using anomaly detection via an ESN (Echo State Network).
When an anomaly is detected the measurement uncertainty is changed to a failure
mode where the uncertainty is very high.

Firstly an ESN is only used on one of the angle measurements.
Ideally you would keep the same input->reservoir and reservoir->reservoir weights
and change the reservoir->output weight vector depending on what measurement it's
used on: roll, roll rate, pitch rate, yaw rate, x, y and z. 
"""

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import time
from scipy.stats import norm
from mpl_toolkits.mplot3d import Axes3D
import argparse
import sys
import copy

# from esn_imu.model.setup1 import Setup1 as esn_network
# from esn_imu.model.setup2 import Setup2 as esn_network
# from esn_imu.model.setup3 import Setup3 as esn_network

# Position xy AD
import esn_imu.model.anomaly_detectors.setup_pos_xy as setup_pos_xy
# Position z AD
import esn_imu.model.anomaly_detectors.setup_pos_z as setup_pos_z
# Roll, pitch AD
import esn_imu.model.anomaly_detectors.setup_rp as setup_rp
# Yaw AD 
import esn_imu.model.anomaly_detectors.setup_y as setup_y
# Angular velocities, x, y, z, AD
import esn_imu.model.anomaly_detectors.setup_ang_vel_xy as setup_ang_vel_xy
import esn_imu.model.anomaly_detectors.setup_ang_vel_z as setup_ang_vel_z
# look in esn_imu.model.anomaly_detectors.ad_comparison to see how to set them up

from esn_imu.motor_fault_estimation import aekf_hexa, compute_dynamics_hexa
from esn_imu.noise import noise_testing

# plt.rc('pgf', texsystem='pdflatex')

# plt.rc('font', family='serif', serif='Times')
# plt.rc('text', usetex=True)
# plt.rc('xtick', labelsize=8)
# plt.rc('ytick', labelsize=8)
# plt.rc('axes', labelsize=8)
# plt.rc('legend', fontsize=8)

# mpl.rcParams['lines.markersize'] = 4

DETECT_ANOMALIES = True

def main(args):
    # Initialization of the anomaly detection.
    # anomaly_detection = esn_network(args.weights_res_res_file, 
    #                                 args.weights_in_res_file, 
    #                                 args.weights_res_out_file,
    #                                 rho1 = 0.02, # slight tuning needed for setup of system.
    #                                 rho2 = 0.001) # depending on frequency of signal.
    # Load in all the setups including files.
    path = "save_files/sensor_fault_esns/"
    filename_w_in_res = f"{path}ang_vel_x_weights_in_res.txt" # these two are the same for all setups
    filename_w_res_res = f"{path}ang_vel_x_weights_res_res.txt"
    
    # Position x, y, z
    ad_pos_x = setup_pos_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}pos_x_weights_res_out.txt")
    ad_pos_y = setup_pos_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}pos_y_weights_res_out.txt")
    ad_pos_z = setup_pos_z.Setup(filename_w_res_res, filename_w_in_res,  filename_w_res_out=f"{path}pos_z_weights_res_out.txt", factor=1000)

    # Orientation roll, pitch, yaw
    ad_roll = setup_rp.Setup(filename_w_res_res, filename_w_in_res,  filename_w_res_out=f"{path}roll_weights_res_out.txt")
    ad_pitch = setup_rp.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}pitch_weights_res_out.txt")
    ad_yaw = setup_y.Setup(filename_w_res_res, filename_w_in_res,    filename_w_res_out=f"{path}yaw_weights_res_out.txt")

    # Angular velocities x, y, z
    ad_ang_vel_x = setup_ang_vel_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}ang_vel_x_weights_res_out.txt")#, rho1=0.04, rho2=0)
    ad_ang_vel_y = setup_ang_vel_xy.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}ang_vel_y_weights_res_out.txt")
    ad_ang_vel_z = setup_ang_vel_z.Setup(filename_w_res_res, filename_w_in_res, filename_w_res_out=f"{path}ang_vel_z_weights_res_out.txt")


    # simulation setup
    simulation_time = 40
    dt = 0.1
    t = np.arange(dt, simulation_time, dt) # dt:dt:simulation_time;

    # Hexarotor parameters
    m = 1.5            # mass in kg
    L = 0.268           # arm length in m
    h = 0.05 # Assume that the inertia of the drone equals that of a 
             # 5 cm disc with the same diameter and weight.
    Jxx = 1/12. * m * (3 * L**2 + h**2)
    Jyy = 1/12. * m * (3 * L**2 + h**2)
    Jzz = 0.5 * m * L**2
    print(Jxx, Jyy, Jzz)
    # Jxx = 0.0762625    # moment of inertia around 'x' axis in kg�m^2
    # Jyy = 0.0762625    # moment of inertia around 'y' axis in kg�m^2
    # Jzz = 0.1369       # moment of inertia around 'z' axis in kg�m^2
    g = 9.81           # gravity
    # Real system initialization
    Uk = np.zeros((6, 1)) # np.array([0, 0, 0, 0]).T  # [F1, F2, F3, F4]'
    # X = np.array([zeros(10,1), 10, 0]).T # [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]'
    X = np.zeros((12, 1))
    # X[0] = np.pi/4
    X[10] = 10 # init z = 10

    gain_loss = np.zeros((6,1))  # fault

    # AEKF System description
    A, b, B, C = compute_dynamics_hexa.Hexacopter.get_model(dt, L)

    QF = np.diag([0.1, 0.01, 0.1, 0.01, 0.1, 0.01, 0.5, 0.1, 0.5, 0.1, 0.1, 0.01]) # [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]
    QF *= 0.01
    # RF = np.diag([0.5, 0.1, 0.5, 0.1, 0.2, 0.1, 1, 1, 0.3])  # [phi, phidot, theta, thetadot, psi, psidot, x, y, z]'
    # RF_imu = np.diag([0.5, 0.1, 0.5, 0.1, 0.2, 0.1])
    # RF_imu = np.diag([0.00414412,    0.414412,   0.00196935, 1.678089, 0.00206063, 0.206063])
    RF_imu = np.diag([0.005,    0.05,   0.005,    0.05, 0.005,    0.05])
    # RF_gps = np.diag([1, 1, 0.3])
    # RF_gps = np.diag([0.01607431, 0.00826116, 0.0045306])
    RF_gps = np.diag([0.0001, 0.0001, 0.0001])

    RF = np.diag(np.concatenate((np.diag(RF_imu), np.diag(RF_gps))))
    RF_orig = copy.copy(RF)
    # print(RF)

    hexa = compute_dynamics_hexa.Hexacopter(m, L, Jxx, Jyy, Jzz, g, b)
    # aekf = AEKF(A, B, QF, RF, quad)
    motor_fault_estimator = aekf_hexa.AEKF(A, B, QF, hexa)

    # print(quad.compute_dynamics(A, X, B, Uk, gain_loss, dt)) 
    
    X_hist = []
    X_hat_hist = []
    y_hist = []
    thetahat_hist = []
    gain_loss_array = []
    Uk_hist = []

    first_measurement_imu = True
    first_measurement_gps = True

    before = time.time()

    aekf_signal_plot = {
        "x" : [],
        "y" : [],
        "z" : [],
        "roll" : [],
        "pitch": [],
        "yaw"  : [],
        "ang_vel_x" : [],
        "ang_vel_y" : [],
        "ang_vel_z" : []
    }

    # for i in range(1, int(simulation_time/dt)):
    for tt in t:
        # time = i * dt
        # to keep track of the simulation time
        # if np.mod(i, 1/dt) == 0:
        #    print(f"time {i * dt}")
        if np.mod(np.round(tt, 5), 1) == 0:
            print(f"time {np.round(tt, 0)}")
        
        # if i > 0 / dt:
        if tt > 0:
            Uk = m * g/6. * np.ones((6, 1))

        # if i > 5 / dt:
        if tt > 5:
            # print("now")
            # gain_loss = np.array([[0.1, 0.05, 0.2, 0.15]]).T
            # gain_loss = np.array([[0.1, 0, 0, 0]]).T
            gain_loss = np.array([[0.1, 0.1, 0.1, 0.1, 0.1, 0.1]]).T
            # gain_loss = np.array([[0.1, 0.05, 0.2, 0.15, 0.1, 0.07]]).T
            # gain_loss = np.array([[0.1, 0, 0, 0, 0, 0]]).T 
            # gain_loss = np.zeros((6,1))
            Uk = m * g/6. * np.ones((6, 1)) + np.divide(np.multiply(m * g/6. * np.ones((6, 1)), gain_loss), 1 - gain_loss) 


        # X = quad.compute_dynamics(A, X, B, Uk, gain_loss, dt)
        #<############################################################
        X = motor_fault_estimator.compute_dynamics(X, Uk, gain_loss, dt)

        RF[:] = RF_orig[:]
        y = C @ X + dt * RF @ norm.ppf(np.random.rand(np.linalg.matrix_rank(C), 1)) # Making a measurements. In the final solution this would come from the sensors.
        # y_imu = C_imu @ X + dt * RF_imu @ norm.ppf(np.random.rand(np.linalg.matrix_rank(C_imu), 1)) 
        # y_gps = C_gps @ X + dt * RF_gps @ norm.ppf(np.random.rand(np.linalg.matrix_rank(C_gps), 1)) 
        # y = np.concatenate((y_imu, y_gps), axis=0)
        # print(y)
        
        if np.round(tt, 5) % 2 == 0:
            # y[[0, 2, 4]] += np.deg2rad(40)
            # y[[1, 3, 5]] += np.deg2rad(10)
            y[0] += np.deg2rad(10) # error in roll
            y[1] += np.deg2rad(100)  # error in roll rate, ang_vel_x
            y[2] += np.deg2rad(10) # error in pitch
            y[3] += np.deg2rad(100) # error in pitch rate, ang_vel_y
            y[4] += np.deg2rad(10) # error in yaw
            y[5] += np.deg2rad(100) # error in yae rate, ang_vel_z
            y[6] += 0.1 # error in pos x
            y[7] += 0.1 # error in pos y
            y[8] += 0.1 # error in pos z
            # y[1] += np.deg2rad(10)
            # y[0] += np.deg2rad(100)
            # RF *= 1000
            print(tt, "injected")
        
        # Anomaly detection
        aekf_signal_plot["x"].append(ad_pos_x.get_update(y[6]))
        aekf_signal_plot["y"].append(ad_pos_y.get_update(y[7]))
        aekf_signal_plot["z"].append(ad_pos_z.get_update(y[8]))
        aekf_signal_plot["roll"].append(ad_roll.get_update(y[0]))
        aekf_signal_plot["pitch"].append(ad_pitch.get_update(y[2]))
        aekf_signal_plot["yaw"].append(ad_yaw.get_update(y[4]))
        aekf_signal_plot["ang_vel_x"].append(ad_ang_vel_x.get_update(y[1]))
        aekf_signal_plot["ang_vel_y"].append(ad_ang_vel_y.get_update(y[3]))
        aekf_signal_plot["ang_vel_z"].append(ad_ang_vel_z.get_update(y[5]))

        if DETECT_ANOMALIES:
            if aekf_signal_plot["x"][-1][0]:
                print(tt, "anomaly")
                # An anomaly is detected.
                # Change Rf to have a very large value.
                RF[6, 6] = 1e6 # RF_orig[6, 6] * 1e6
                # RF[1, 1] = RF_orig[1, 1] * 1000
            else:
                RF[6, 6] = RF_orig[6, 6]

            if aekf_signal_plot["y"][-1][0]:
                RF[7, 7] = 1e6 # RF_orig[7, 7] * 1e6
            else:
                RF[7, 7] = RF_orig[7, 7]

            if aekf_signal_plot["z"][-1][0]:
                RF[8, 8] = 1e6 # RF_orig[8, 8] * 1e6
            else:
                RF[8, 8] = RF_orig[8, 8]

            if aekf_signal_plot["roll"][-1][0]:
                RF[0, 0] = 1e6 # RF_orig[8, 8] * 1e6
            else:
                RF[0, 0] = RF_orig[0, 0]

            if aekf_signal_plot["pitch"][-1][0]:
                RF[2, 2] = 1e6 # RF_orig[8, 8] * 1e6
            else:
                RF[2, 2] = RF_orig[2, 2]

            if aekf_signal_plot["yaw"][-1][0]:
                RF[4, 4] = 1e6 # RF_orig[8, 8] * 1e6
            else:
                RF[4, 4] = RF_orig[4, 4]

            if aekf_signal_plot["ang_vel_x"][-1][0]:
                RF[1, 1] = 1e6 # RF_orig[8, 8] * 1e6
            else:
                RF[1, 1] = RF_orig[1, 1]

            if aekf_signal_plot["ang_vel_y"][-1][0]:
                RF[3, 3] = 1e6 # RF_orig[8, 8] * 1e6
            else:
                RF[3, 3] = RF_orig[3, 3]

            if aekf_signal_plot["ang_vel_z"][-1][0]:
                RF[5, 5] = 1e6 # RF_orig[8, 8] * 1e6
            else:
                RF[5, 5] = RF_orig[5, 5]

        y_hist.append(copy.copy(y.reshape((y.shape[0],))))
        # y[0] = aekf_signal_plot[-1][1]
        # print(aekf_signal_plot[-1])

        # It's also very probable that the IMU and the GPS doesn't operate at the same frequency, so you should be able to select which sensor data you are updating with.
        #############################################################>

        # aekf.predict(Uk, dt)
        motor_fault_estimator.update(y, C, RF, Uk, dt, first_measurement_imu)
        first_measurement_imu = False
        # aekf.update(y_imu, C_imu, RF_imu, Uk, dt, first_measurement_imu)
        # first_measurement_imu = False
        # aekf.update(y_gps, C_gps, RF_gps, Uk, dt, first_measurement_gps)
        # first_measurement_gps = Fals

        X_hist.append(X.reshape((X.shape[0],)))
        X_hat_hist.append(motor_fault_estimator.Xhat.reshape((motor_fault_estimator.Xhat.shape[0], )))
        thetahat_hist.append(motor_fault_estimator.thetahat.reshape(motor_fault_estimator.thetahat.shape[0],))
        gain_loss_array.append(gain_loss.reshape((gain_loss.shape[0],)))
        Uk_hist.append(Uk.reshape((Uk.shape[0],)))
        # y_hist.append(y.reshape((y.shape[0],)))

        # print(quad.compute_jacobians(X, B, Uk, gain_loss, dt))

    after = time.time()
    print(f"Duration {after - before}")

    X_hist = np.asarray(X_hist)
    X_hat_hist = np.asarray(X_hat_hist)    
    thetahat_hist = np.asarray(thetahat_hist)
    gain_loss_array = np.asarray(gain_loss_array)
    y_hist = np.asarray(y_hist)

    # Figure showing the z value
    plt.figure()
    plt.subplot(311)
    plt.plot(t, X_hist[:, 6], '--', label = "Position true state")
    plt.plot(t, X_hat_hist[:, 6], '-', label = "Position AEKF")
    plt.plot(t, X_hist[:, 7], '--', label = "Velocity true state")
    plt.plot(t, X_hat_hist[:, 7], '-', label = "Velocity AEKF")
    plt.legend()
    plt.title('X')
    plt.grid()

    plt.subplot(312)
    plt.plot(t, X_hist[:, 8], '--', label = "Position true state")
    plt.plot(t, X_hat_hist[:, 8], '-', label = "Position AEKF")
    plt.plot(t, X_hist[:, 9], '--', label = "Velocity true state")
    plt.plot(t, X_hat_hist[:, 9], '-', label = "Velocity AEKF")
    plt.legend()
    plt.title('Y')
    plt.grid()

    plt.subplot(313)
    plt.plot(t, X_hist[:, 10], '--', label = "Position true state")
    plt.plot(t, X_hat_hist[:, 10], '-', label = "Position AEKF")
    plt.plot(t, X_hist[:, 11], '--', label = "Velocity true state")
    plt.plot(t, X_hat_hist[:, 11], '-', label = "Velocity AEKF")
    plt.legend()
    plt.title('Z')
    plt.grid()


    ## Figure showing the roll pitch yaw angles
    plt.figure()
    plt.subplot(311)
    plt.plot(t, X_hist[:, 0] * 180 / np.pi, '--', label="Angle true state")
    plt.plot(t, X_hat_hist[:, 0] * 180 / np.pi, label="Angle AEKF")
    plt.plot(t, X_hist[:, 1] * 180 / np.pi, '--', label="Angle rate true state")
    plt.plot(t, X_hat_hist[:, 1] * 180 / np.pi, label="Angle rate AEKF")
    plt.title("Roll")
    plt.legend()
    plt.ylabel("Angle [degrees]")
    plt.grid()
    
    plt.subplot(312)
    plt.plot(t, X_hist[:, 2] * 180 / np.pi, '--', label="Angle true state")
    plt.plot(t, X_hat_hist[:, 2] * 180 / np.pi, label="Angle AEKF")
    plt.plot(t, X_hist[:, 3] * 180 / np.pi, '--', label="Angle rate true state")
    plt.plot(t, X_hat_hist[:, 3] * 180 / np.pi, label="Angle rate AEKF")
    plt.title("Pitch")
    plt.legend()
    plt.ylabel("Angle [degrees]")
    plt.grid()
    
    plt.subplot(313)
    plt.plot(t, X_hist[:, 4] * 180 / np.pi, '--', label="Angle true state")
    plt.plot(t, X_hat_hist[:, 4] * 180 / np.pi, label="Angle AEKF")
    plt.plot(t, X_hist[:, 5] * 180 / np.pi, '--', label="Angle rate true state")
    plt.plot(t, X_hat_hist[:, 5] * 180 / np.pi, label="Angle rate AEKF")
    plt.title("Yaw")
    plt.legend()
    plt.xlabel("Times [s]")
    plt.ylabel("Angle [degrees]")
    plt.grid()

    ## Figure showing the estimated motor faults
    width = 3.4
    height = width / 1.618
    fig = plt.figure()
    fig.subplots_adjust(left=.15, bottom=.17, right=.97, top=.97, hspace=0.45, wspace=0.45)
    fig.set_size_inches(w=width, h=height*2)
    for i in range(6):
        plt.subplot(321 + i)
        plt.plot(t, thetahat_hist[:, i], label="Estimated")
        plt.plot(t, gain_loss_array[:, i], 'k--', label="True")
        # plt.title(f"Motor {i + 1}")
        plt.xlabel("Time [s]")
        plt.ylabel(f"Motor {i + 1} Fault")
        plt.grid()

    # plt.savefig(f"output/plots/sim_hexa/no_anomalies_fault_estimate.pdf")

    ## Figure showing the estimated motor faults
    plt.figure()
    for i in range(6):
        plt.subplot(611 + i)
        plt.plot(t, thetahat_hist[:, i])
        plt.plot(t, gain_loss_array[:, i], 'k--')
        # plt.title(f"Motor {i + 1}")
        plt.grid()

    fig = plt.figure()
    fig.subplots_adjust(left=.15, bottom=.17, right=.97, top=.97)
    fig.set_size_inches(w=width, h=height*1.5)
    for i in range(2):
        plt.subplot(2, 1, i + 1)
        plt.plot(t, thetahat_hist[:, 3 * i    ], label=f"Motor {3 * i + 1}")
        plt.plot(t, thetahat_hist[:, 3 * i + 1], label=f"Motor {3 * i + 2}")
        plt.plot(t, thetahat_hist[:, 3 * i + 2], label=f"Motor {3 * i + 3}")
        # plt.axhline(y=line_height, color='r', linestyle='-.', label="20\%")

        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1),
            ncol=3, fancybox=False, shadow=False)
        plt.grid()
        plt.ylabel("Fault")
        plt.ylim((-0.2, 1.2))

    plt.xlabel("Time [s]")

    ## Figure showing all states
    plt.figure()
    plt.plot(t, X_hist, label="State")
    plt.legend()
    plt.grid()

    ## Figure showing the commanded motor thrust
    plt.figure()
    plt.plot(t, Uk_hist, label="Motor thrust")
    plt.legend()

    # Figure, virtual force loss estimation
    plt.figure()
    print(B.shape, gain_loss_array.shape)
    print(B)
    print(gain_loss_array)
    print(thetahat_hist)
    V_loss_real = (B @ gain_loss_array.T).T
    V_loss_est = (B @ thetahat_hist.T).T

    print(V_loss_real)
    print(V_loss_est)

    names = ["Thrust", "Moment X", "Moment Y", "Moment Z"]    

    for i in range(4):
        plt.subplot(221 + i)
        plt.plot(t, V_loss_est[:, i], 'r-')
        plt.plot(t, V_loss_real[:, i], 'k--')
        plt.grid()
        plt.title(names[i])
   
    # figure showing the 3d position of the drone
    # print(X_hist[:, 10])
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter3D([0], [0], [10], label="Reference position")
    ax.plot3D(X_hist[:, 6], X_hist[:, 8], X_hist[:, 10], "--", label="Position true state")
    ax.plot3D(X_hat_hist[:, 6], X_hat_hist[:, 8], X_hat_hist[:, 10], "-", label="Position AEKF")
    # ax.plot(X_hist[:, 6], X_hist[:, 8], X_hist[:, 10], label="Position AEKF")
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
    ax.set_zlabel("z [m]")
    plt.legend()

    #### 
    for key in aekf_signal_plot.keys():
        aekf_signal_plot[key] = np.asarray(aekf_signal_plot[key])
        # value = np.asarray(value)
    print(aekf_signal_plot)

    fig, axs = plt.subplots(3, 1, sharex=True)
    fig.suptitle("Anomaly detections on position sensor input")
    
    names = ["x", "y", "z"]
    for i, ax in enumerate(axs):
        ax.plot(y_hist[:, 6 + i], label="Input")
        print(aekf_signal_plot[names[i]])
        ax.plot(aekf_signal_plot[names[i]][:, 1], label=f"Signal, pos ${names[i]}$")
        
        tmp = np.where(aekf_signal_plot[names[i]][:, 0] > 0)[0]
        anomaly_offset = np.ones_like(tmp) * np.mean(y_hist[:, 6 + i]) + 2 * np.std(y_hist[:, 6 + i])
        # anomaly_offset = np.ones_like(tmp) * np.mean(y_hist[:, 6 + i]) + 2 * np.std(y_hist[:, 6 + i])

        ax.plot(tmp, anomaly_offset, "ro", fillstyle='none', label="Detected Anomalies")
        # plt.plot(injected_anomalies, np.zeros_like(injected_anomalies) + 0.05, "kx", label="Injected Anomalies")
        # ax.title("Anomaly detection on pos x")
        ax.legend()
        ax.grid()


    fig, axs = plt.subplots(3, 1, sharex=True)
    fig.suptitle("Anomaly detections on rpy input")
    
    names = ["roll", "pitch", "yaw"]
    for i, ax in enumerate(axs):
        ax.plot(y_hist[:, 2*i], label="Input")
        print(aekf_signal_plot[names[i]])
        ax.plot(aekf_signal_plot[names[i]][:, 1], label=f"Signal, {names[i]}")
        
        tmp = np.where(aekf_signal_plot[names[i]][:, 0] > 0)[0]
        anomaly_offset = np.ones_like(tmp) * np.mean(y_hist[:, 2*i]) + 2 * np.std(y_hist[:, 2*i])
        # anomaly_offset = np.ones_like(tmp) * np.mean(y_hist[:, 6 + i]) + 2 * np.std(y_hist[:, 6 + i])

        ax.plot(tmp, anomaly_offset, "ro", fillstyle='none', label="Detected Anomalies")
        # plt.plot(injected_anomalies, np.zeros_like(injected_anomalies) + 0.05, "kx", label="Injected Anomalies")
        # ax.title("Anomaly detection on pos x")
        ax.legend()
        ax.grid()


    fig, axs = plt.subplots(3, 1, sharex=True)
    fig.suptitle("Anomaly detections on angular velocities input")
    
    names = ["ang_vel_x", "ang_vel_y", "ang_vel_z"]#, "pitch", "yaw"]
    for i, ax in enumerate(axs):
        ax.plot(y_hist[:, 1 + 2*i], label="Input")
        print(aekf_signal_plot[names[i]])
        ax.plot(aekf_signal_plot[names[i]][:, 1], label=f"Signal, {names[i]}")
        
        tmp = np.where(aekf_signal_plot[names[i]][:, 0] > 0)[0]
        anomaly_offset = np.ones_like(tmp) * np.mean(y_hist[:, i + 2*i]) + 2 * np.std(y_hist[:, i + 2*i])
        # anomaly_offset = np.ones_like(tmp) * np.mean(y_hist[:, 6 + i]) + 2 * np.std(y_hist[:, 6 + i])

        ax.plot(tmp, anomaly_offset, "ro", fillstyle='none', label="Detected Anomalies")
        # plt.plot(injected_anomalies, np.zeros_like(injected_anomalies) + 0.05, "kx", label="Injected Anomalies")
        # ax.title("Anomaly detection on pos x")
        ax.legend()
        ax.grid()

    plt.show()


if __name__ == "__main__":
    # parser = argparse.ArgumentParser(description="Script for integrating a simulation of the AEKF motor fault estimation with the ESN anomaly detection..")
    # parser.add_argument("weights_in_res_file", type=str, help="path of data matrix for weights from input to reservoir")
    # parser.add_argument("weights_res_res_file", type=str, help="path of data matrix for weights from reservoir to reservoir")
    # parser.add_argument("weights_res_out_file", type=str, help="path of data matrix for weights from reservoir to output")
    # # parser.add_argument("input_data", type=str, help="path of input test data")
    # args = parser.parse_args()
    args = []
    main(args)