#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

# from esn_imu.model.setup1 import Setup1
# from esn_imu.model.setup2 import Setup2
# from esn_imu.model.setup3 import Setup3
from esn_imu.model.setup1 import Setup1
from esn_imu.model.setup2 import Setup2
from esn_imu.model.setup3 import Setup3
from esn_imu.model.setup_lpf import SetupLPF

from esn_imu.noise.noise_testing import inject_short_anomalies, inject_noise_anomalies

import argparse
import sys

# import matplotlib as mpl
# mpl.use("pgf")
# mpl.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     'pgf.rcfonts': False,
# })
plt.rc('pgf', texsystem='pdflatex')

plt.rc('font', family='serif', serif='Times')
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=8)
plt.rc('ytick', labelsize=8)
plt.rc('axes', labelsize=8)
plt.rc('legend', fontsize=8)


def main(args):
    print(args.weights_in_res_file)
    print(args.weights_res_res_file)
    print(args.weights_res_out_file)
    print(args.input_data)

    # Some fancy shit where the same input data file is run on each of the networks and the result is plotted.
    # rho1 = 0.01
    # rho2 = 0.0001

    rho1 = 0.01
    rho2 = 0.0001

    setup1 = Setup1(args.weights_res_res_file, args.weights_in_res_file, args.weights_res_out_file, rho1=rho1, rho2=rho2)
    setup2 = Setup2(args.weights_res_res_file, args.weights_in_res_file, args.weights_res_out_file, rho1=rho1, rho2=rho2)
    setup3 = Setup3(args.weights_res_res_file, args.weights_in_res_file, args.weights_res_out_file, rho1=rho1, rho2=rho2)
    setupLPF = SetupLPF(N=3, rho1=rho1, rho2=rho2)

    # networks = [setup1, setup2, setup3, setupLPF]
    # names = ["setup1", "setup2", "setup3", "setup_lpf"]
    networks = [setup2]
    names = ["setup2"]

    data = np.loadtxt(args.input_data)[:2500]# [:, 1]
    dt = 0.1# 220632968116578 # calculated from the data
    t = np.arange(0, dt * data.shape[0] - dt, dt)

    width = 3.4
    height = width / 1.618

    fig = plt.figure()
    fig.subplots_adjust(left=.15, bottom=.17, right=.97, top=.97)
    fig.set_size_inches(w=width, h=height)
    plt.plot(t, data, label="Input")
    plt.ylabel("Angle [rad]")
    plt.xlabel("Time [s]")
    plt.grid()
    plt.legend()
    
    plt.savefig('./output/plots/raw_data.pdf')

    # Add some anomalies
    # injected_anomalies = inject_short_anomalies(data, 1, 420)
    injected_anomalies = inject_short_anomalies(data, 5, 50)
    crop_point_beginning = 2 # remove first 5 and last 5 seconds
    crop_point_end = 60 # remove first 5 and last 5 seconds

    for name, network in zip(names, networks):
        aekf_input = []
        pred_err_filtered = []
        pred_err_old_filtered = []
        for u in data:
            # print(setup1.get_update(u))
            # aekf_input.append(network.get_update(u))
            aekf_input.append(network.get_update(u))
            # aekf_input[-1] = (aekf_input[-1][0], -aekf_input[-1][1], -aekf_input[-1][2])
            pred_err_filtered.append(network.anomaly_detector.dataPointsAvg[0])
            pred_err_old_filtered.append(network.anomaly_detector.dataPointsAvg[1])

        pred_err_filtered = np.asarray(pred_err_filtered)
        pred_err_old_filtered = np.asarray(pred_err_old_filtered)
        # print(aekf_input)
        aekf_input = np.asarray(aekf_input)
        print(aekf_input)
        
        fig = plt.figure()
        fig.subplots_adjust(left=.18, bottom=.17, right=.97, top=.80)
        fig.set_size_inches(w=width, h=height)
        plt.plot(t, data, '-', label="Input")
        plt.plot(t, aekf_input[:, 1], '--', label="Output")
        plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
        tmp = np.where(aekf_input[:, 0] > 0)[0]
        
        print(tmp)
        print(t)
        # if tmp.shape[0] > 0:
        #     rho = tmp[-1] / t[-1]

        plt.plot(np.multiply(tmp, dt), np.zeros_like(tmp), "ro", fillstyle='none', label="Detected Anomalies")

        plt.plot(np.multiply(injected_anomalies, dt), np.zeros_like(injected_anomalies) + 0.05, "kx", label="Injected Anomalies")

        # plt.ylabel("Angle [rad]")
        plt.ylabel("Position $x$ [m]")
        plt.xlabel("Time [s]")
        # plt.title(name)

        # plt.legend()
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
           ncol=2, mode="expand", borderaxespad=0.)

        plt.grid()
        plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
        plt.savefig(f'./output/plots/comparison_{name}.pdf')

        # Plot of the prediction error
        fig = plt.figure()
        fig.subplots_adjust(left=.18, bottom=.17, right=.97, top=.80)
        fig.set_size_inches(w=width, h=height)
        plt.plot(t, aekf_input[:, 2], label="Prediction error")
        # plt.plot(t, pred_err_filtered, label="Prediction error filtered")
        # plt.plot(t, pred_err_old_filtered, label="Pred err filtered old")
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
           ncol=2, mode="expand", borderaxespad=0.)
        plt.grid()
        # plt.ylabel("Error [rad]")
        plt.ylabel("Error [m]")
        plt.xlabel("Time [s]")
        plt.xlim((crop_point_beginning, t[-1] - crop_point_end))
        plt.ylim((-0.001, 0.1))
        plt.savefig(f'./output/plots/comparison_pred_err_{name}.pdf')


        # Figure with input,output and anomalies and the prediction err underneath.

        # plt.show()

    plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script for testing three different setups of the ESN.")
    parser.add_argument("weights_in_res_file", type=str, help="path of data matrix for weights from input to reservoir")
    parser.add_argument("weights_res_res_file", type=str, help="path of data matrix for weights from reservoir to reservoir")
    parser.add_argument("weights_res_out_file", type=str, help="path of data matrix for weights from reservoir to output")
    parser.add_argument("input_data", type=str, help="path of input test data")
    args = parser.parse_args()
    main(args)
    