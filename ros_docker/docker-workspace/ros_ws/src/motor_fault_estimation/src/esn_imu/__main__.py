#!/usr/bin/env python3
import numpy as np
import matplotlib.markers as markers
import matplotlib.pyplot as plt
import os
import copy

from .model.echo_state_network import *
from .noise.noise_testing import *

if __name__ == "__main__":
    np.random.seed(seed=42)

    esn = EchoStateNetwork(
        numberOfInputs = 1, 
        numberOfOutputs = 1,
        numberOfHiddenUnits = 40, 
        inputSparsity = 0.5,
        internalSparsity = 0.5,
        spectralRadius = 0.6, # To preserve echo state
        inputScaling = 0.6,
        # leakingRate = 0.3,
        leakingRate = 0.7,
        reservoirNeuronBias = 0.001,
        noiseRange = 0.0001
    ) 

    repetitions = 1

    print(os.getcwd())

    # data = np.loadtxt("input/imu_data.csv")[:, 0]
    # data = np.hstack((data, data, data))
    # print(data.shape)
    # print(data)

    # data = np.loadtxt("input/MackeyGlass_t17.txt")
    
    # dataTarget = copy.copy(data)

    # folder = "input/Data1_Atlantic_salmon_at_juvenile_stage/"
    # folder = "input/Data2_Atlantic_salmon_at_post_smolt_stage/"
    # folder = "input/Data3_Atlantic_salmon_at_adult_stage/"
    # folder = "input/Data4_Rainbow_trout_at_adult_stage/"
    
    # data = 0.1 * np.sin(np.arange(1, 100, 0.01))
    # data = np.loadtxt("input/Data1_Atlantic_salmon_at_juvenile_stage/Input123_499Data.txt")# [:, 0]
    # data = np.loadtxt(folder + "Input123_499Data.txt")# [:, 0]
    # data = np.vstack((data, data, data, data, data))
    # data = esn.normalize_data(data)
    # dataTarget = 0.1 * np.cos(np.arange(1, 100, 0.01))
    # dataTarget = np.loadtxt("input/Data1_Atlantic_salmon_at_juvenile_stage/Target_499Data.txt")
    # dataTarget = np.loadtxt(folder + "Target_499Data.txt")
    # dataTarget = np.hstack((dataTarget, dataTarget, dataTarget, dataTarget, dataTarget))
    
    fish = 0
    
    if fish:
        folder = "input/Data1_Atlantic_salmon_at_juvenile_stage/"
        data = np.loadtxt(folder + "Input123_2000Data.txt")# [:, 0]
        dataTarget = np.loadtxt(folder + "Target_2000Data.txt")
        esn.numberOfInputs = 3
        esn.numberOfOutputs = 1
        esn.reset()
    else: # imu
        data = np.loadtxt("input/imu_data_rpy.csv")[:, 1]
        # data = 2 * esn.normalize_data(data) - 1
        # data = data.reshape((data.shape[0], 1))
        # firstCol = np.ones((data.shape[0], 1))
        # data = np.loadtxt("input/MackeyGlass_t17.txt")[:-1]
        # dataTarget = copy.copy(data)
        # data = np.hstack((firstCol, data))
       
        dataTarget = np.loadtxt("input/imu_data_rpy_target.csv")[:, 1]
        # dataTarget = 2 * esn.normalize_data(dataTarget) - 1
        # dataTarget = np.loadtxt("input/MackeyGlass_t17.txt")[1:]

        esn.numberOfInputs = 1
        esn.numberOfOutputs = 1
        esn.reset()

    split_point = int(0.8 * data.shape[0])
    trainData = data[:split_point]
    trainTarget = dataTarget[:split_point]
    testData = data[split_point:]
    testTarget = dataTarget[split_point:]
    
    # Train network or load it from files
    loadFromFile = 0
    if loadFromFile:
        esn.load_from_file("save_files/weights_res_res.txt", "save_files/weights_in_res.txt", "save_files/weights_res_out.txt")
        # esn.load_from_file("save_files/koh/weights_res_res.txt", "save_files/koh/weights_in_res.txt", "save_files/koh/weights_res_out.txt")
        # esn.W_reservoir = esn.W_reservoir.T
    else:
        # w_out = esn.train(trainData, trainTarget, beta=0.001, debug=True) 
        w_out = esn.train_RLS(trainData, trainTarget, auto_corr=1, repetitions=repetitions, 
                iterations=1500, debug=True, learning_rate=0.99) 

    print("Trained w_out", esn.W_out)
    
    predictions = []
    # testData[250:260] = 0
    # testData[2000:2001] = 0
    # testData[2000:2001] += 0.01
    # injected_anomalies = inject_short_anomalies(testData, 1, 510)

    injected_anomalies = [] 
    injected_anomalies += inject_short_anomalies(testData, 0.5, 110)   
    # injected_anomalies += inject_noise_anomalies(testData, 0, testData.shape[0], 1)
    injected_anomalies += inject_noise_anomalies(testData, 500, 50, 1)
    
    # if esn.numberOfInputs > 1:
    #     allData = np.vstack((trainData, testData))
    # else:
    #     allData = np.hstack((trainData, testData))
    # allTarget = np.hstack((trainTarget, testTarget))

    # allData[2000] = 0
    #allData[2000:2002, 0] = 0
    # allData[300:305, :] = 0

    print("###########################################")
    print("############## Predicting #################")
    print("###########################################")
    # for u in allData:
    pred_err = []
    peaks = []
    pred_err_filtered = []
    pred_err_old_filtered = []
    pr = PeakRecognition(0.02, 0.001)
    for u in testData:
        # print(u)
        if len(predictions) > 0:
            pred_err.append(np.abs(np.subtract(u, predictions[-1]))[0])
        else:
            pred_err.append(np.zeros((1, esn.numberOfOutputs)))

        predictions.append(esn.predict(u))

        peaks.append(pr.update(pred_err[-1])) # Gives a boolean list, 1 for peak, 0 for nothing.
        pred_err_filtered.append(pr.dataPointsAvg[0])
        pred_err_old_filtered.append(pr.dataPointsAvg[1])
        # print("Prediction err", u - predictions[-1])

    pred_err = np.asarray(pred_err)
    print("pred_err\n", pred_err)
    print("pred_err.shape", pred_err.shape)

    cutoff = 100
    testData = testData[cutoff:]
    testTarget = testTarget[cutoff:]
    predictions = np.asarray(predictions)[cutoff:] # don't use the first 100 points
    predictions = np.reshape(predictions, (predictions.shape[0], esn.numberOfOutputs))
    pred_err = pred_err[cutoff:]
    peaks = peaks[cutoff:]
    pred_err_filtered = np.asarray(pred_err_filtered)
    pred_err_filtered = pred_err_filtered[cutoff:]
    pred_err_old_filtered = np.asarray(pred_err_old_filtered)
    pred_err_old_filtered = pred_err_old_filtered[cutoff:]
    injected_anomalies = [x - cutoff for x in injected_anomalies]
    
    print(predictions.shape)
    print(testTarget.shape)
    err = np.abs(np.subtract(predictions, testTarget.reshape((testTarget.shape[0], esn.numberOfOutputs))))
    # err = np.subtract(predictions, allTarget.reshape((allTarget.shape[0], esn.numberOfOutputs)))**2
    # err = np.subtract(predictions, testTarget.reshape((testTarget.shape[0], esn.numberOfOutputs)))**2
    print(err.shape)

    nrmsd = esn.calculate_error(testTarget, predictions)
    print(f"The calculated NRMSD is {nrmsd}")

    plt.figure()
    ax1 = plt.subplot(311)
    # plt.plot(testData, label="Input")#, marker=".")
    plt.plot(predictions[:,0], label="Predictions")#, marker=".")
    # plt.plot(allData, label="Input")#, marker=".")
    plt.plot(testData, label="Input")#, marker=".")
    plt.legend()
    plt.grid()

    plt.subplot(312, sharex = ax1, sharey = ax1)
    # plt.plot(testData, label="input")
    plt.plot(predictions[:,0], label="Predictions")#, marker=".")
    plt.plot(testTarget, label="Target")
    # plt.plot(allTarget, label="Target")#, marker=".")
    plt.legend()
    plt.grid()
    # plt.xlim((2000 - 499, 2000))
    
    plt.subplot(313, sharex = ax1)
    # plt.plot(testData, label="input")
    plt.plot(err, label="error")#, marker=".")
    print(testData.shape, predictions.shape )

    # pred_err = np.subtract(testData.reshape((testData.shape[0], esn.numberOfInputs)), predictions)
    # print(pred_err)
    plt.plot(pred_err, label="Prediction err")
    # plt.plot([x * 0.1 - 0.02 for x in peaks], "ro", fillstyle="none", label="Detected Anomalies")
    # print("peaks\n", peaks)
    tmp = np.where(np.asarray(peaks, dtype=np.float) > 0)[0]
    # print(np.where(np.asarray(peaks, dtype=np.float) > 0))
    print(tmp)
    plt.plot(tmp, 0.08 * np.ones(tmp.shape), "ro", fillstyle="none", label="Detected Anomalies")

    plt.plot(pred_err_filtered, label="Filtered pred err, i")
    plt.plot(pred_err_old_filtered, label="Filtered pred err, i - 1")

    plt.plot(injected_anomalies, [0.09 for i in range(len(injected_anomalies))], "bx", label="Injected Anomalies")


    plt.ylim((0, 0.1))
    plt.legend()
    plt.grid()
    # plt.xlim((2000 - 499, 2000))

    plt.figure()
    plt.plot()
    plt.plot(testData, label="Input")#, marker=".")
    plt.plot(testTarget, label="Target")
    plt.legend()
    plt.grid()

    esn.save_files("save_files/")
    
    print(testData.reshape(predictions.shape).shape, predictions.shape)
    out_data = np.hstack((testData.reshape(predictions.shape), predictions))
    np.savetxt("output/data_predictions.csv", out_data)

    plt.show()
