#!/usr/bin/env python3

import rospy
import numpy as np

from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import Imu
from std_msgs.msg import Float64

class Unpack:
    """
    Node to unpack values to floats, 
    such that they can be run trough anomaly detection
    """
    def __init__(self):
        rospy.init_node("value_unpacker")
        # subscribers
        self.sub_pose = rospy.Subscriber('~pose_in', PoseStamped, self.cb_pose)
        self.sub_imu  = rospy.Subscriber('~imu_in', Imu, self.cb_imu)

        # publishers
        self.pub_pose_x = rospy.Publisher('~pose/x', Float64, queue_size=1)
        self.pub_pose_y = rospy.Publisher('~pose/y', Float64, queue_size=1)
        self.pub_pose_z = rospy.Publisher('~pose/z', Float64, queue_size=1)
        self.pub_orien_x = rospy.Publisher('~orientation/x', Float64, queue_size=1)
        self.pub_orien_y = rospy.Publisher('~orientation/y', Float64, queue_size=1)
        self.pub_orien_z = rospy.Publisher('~orientation/z', Float64, queue_size=1)
        self.pub_orien_w = rospy.Publisher('~orientation/w', Float64, queue_size=1)
        self.pub_ang_vel_x = rospy.Publisher('~ang_vel/x', Float64, queue_size=1)
        self.pub_ang_vel_y = rospy.Publisher('~ang_vel/y', Float64, queue_size=1)
        self.pub_ang_vel_z = rospy.Publisher('~ang_vel/z', Float64, queue_size=1)

        rospy.spin()

    def cb_pose(self, msg):
        # Unpack the pose and publish it
        self.pub_pose_x.publish(msg.pose.position.x)
        self.pub_pose_y.publish(msg.pose.position.y)
        self.pub_pose_z.publish(msg.pose.position.z)

    def cb_imu(self, msg):
        # Unpack the orientation and publish it
        self.pub_orien_x.publish(msg.orientation.x)
        self.pub_orien_y.publish(msg.orientation.y)
        self.pub_orien_z.publish(msg.orientation.z)
        self.pub_orien_w.publish(msg.orientation.w)

        # Unpack the angular velocity and publish it
        self.pub_ang_vel_x.publish(msg.angular_velocity.x)
        self.pub_ang_vel_y.publish(msg.angular_velocity.y)
        self.pub_ang_vel_z.publish(msg.angular_velocity.z)


def main():
    node = Unpack()

if __name__ == "__main__":
    main()