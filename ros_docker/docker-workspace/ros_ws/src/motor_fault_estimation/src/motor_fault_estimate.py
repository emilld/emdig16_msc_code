#!/usr/bin/env python3

import rospy
import numpy as np

from geometry_msgs.msg import PoseStamped, TwistStamped, PoseWithCovarianceStamped
from mavros_msgs.msg import RCOut
from sensor_msgs.msg import Imu
from std_msgs.msg import Float64MultiArray, MultiArrayLayout, MultiArrayDimension

from transforms3d.euler import quat2euler

from esn_imu.motor_fault_estimation import aekf_hexa, compute_dynamics_hexa

class MotorFaultEstimate():
    def __init__(self):
        rospy.init_node('motor_fault_estimate')

        # subscribers
        self.sub_local_position = rospy.Subscriber('~local_position', PoseWithCovarianceStamped, self.cb_local_pos)
        self.sub_local_rate     = rospy.Subscriber('~imu', Imu, self.cb_imu)
        self.sub_rc_channels    = rospy.Subscriber('~rcout', RCOut, self.cb_rc)

        # publishers
        self.pub_state_estimate         = rospy.Publisher('~state_estimate', Float64MultiArray, queue_size=1)
        self.pub_motor_fault_estimate   = rospy.Publisher('~motor_fault_estimate', Float64MultiArray, queue_size=1)
        self.pub_motor_thrust           = rospy.Publisher('~motor_thrust', Float64MultiArray, queue_size=1)
        
        # Num to thrust function
        # poly_coeffs = [-9.13948477e-09, 4.24788753e-05, -5.41307079e-02, 2.09764844e+01]
        poly_coeffs = rospy.get_param("~poly_coeffs", [-9.13948477e-09, 4.24788753e-05, -5.41307079e-02, 2.09764844e+01])
        self._raw_num_to_thrust = np.poly1d(poly_coeffs)

        # variables for the estimation
        self.Uk = np.zeros((6, 1))
        self.Uk_raw = np.zeros((6, 1))

        # state vector
        self.X = np.zeros((12, 1))
        #  [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]'

        # measurement vector
        self.y = np.zeros((9, 1)) 
        # [phi, phidot, theta, thetadot, psi, psidot, x, y, z]
        self.measurement_ready = False

        # gain loss vector
        self.gain_loss = np.zeros((6,1))  # fault

        self.QF = np.diag([0.1,   0.01,   0.1,     0.01, 0.1,   0.01, 0.5,  0.1, 0.5,  0.1, 0.1, 0.01]) 
        self.QF *= 1
        # self.QF = np.diag([0.001,   0.0001,   0.001,     0.0001, 0.001,   0.0001, 0.005,  0.001, 0.005,  0.001, 0.001, 0.0001]) 
                        # [phi, phidot, theta, thetadot, psi, psidot,   x, xdot,   y, ydot,   z, zdot]
        # self.RF = np.diag([0.5,    0.1,   0.5,      0.1, 0.2,    0.1, 1, 1, 0.3])  
        self.RF_imu = np.array([1, 1, 1, 1, 1, 1])
        self.RF_pos = np.array([1, 1, 1])
        # self.RF = np.diag([0.00414412,    0.00414412,   0.00196935, 0.01678089, 0.00206063, 0.00206063, 0.01607431, 0.00826116, 0.0045306])  
                        # [phi, phidot, theta, thetadot, psi, psidot, x, y,   z]'

        # Hexacopter parameters
        self.m = rospy.get_param("~mass", 1.5)        # mass in kg
        self.L = rospy.get_param("~arm_length", 0.37) # arm length in m
        self.Jxx = rospy.get_param("~Jxx", 0.0762625) # moment of inertia around 'x' axis in kg�m^2
        self.Jyy = rospy.get_param("~Jyy", 0.0762625) # moment of inertia around 'y' axis in kg�m^2
        self.Jzz = rospy.get_param("~Jzz", 0.1369)    # moment of inertia around 'z' axis in kg�m^2
        self.g = rospy.get_param("~g", 9.81)          # gravity
        self.lambdaa = rospy.get_param("~lambdaa", 0.98) # learning rate of fault estimation

        # Directory for output file
        self.output_file = rospy.get_param("~outfile", None)
        #
        if self.output_file is not None:
            with open(self.output_file, 'w') as f:
                # f.write("time,roll,pitch,yaw,ang_vel_x,ang_vel_y,ang_vel_z,pos_x,pos_y,pos_z,rc_out_0,rc_out_1,rc_out_2,rc_out_3,rc_out_4,rc_out_5\n")
                f.write(
                    "time,"
                    + "estimated_pos_x,estimated_pos_y,estimated_pos_z,"
                    + "actual_pos_x,actual_pos_y,actual_pos_z,"
                    
                    + "estimated_roll,estimated_pitch,estimated_yaw,"
                    + "actual_roll,actual_pitch,actual_yaw,"

                    + "estimated_ang_vel_x,estimated_ang_vel_y,estimated_ang_vel_z,"
                    + "actual_ang_vel_x,actual_ang_vel_y,actual_ang_vel_z,"
                    
                    + "rc_out_0,rc_out_1,rc_out_2,rc_out_3,rc_out_4,rc_out_5,"
                    + "mfe_0,mfe_1,mfe_2,mfe_3,mfe_4,mfe_5"
                    + "\n"
                )

        self.b = 0.01
        self.dt = 0.1
        self.hexa = compute_dynamics_hexa.Hexacopter(self.m, self.L, self.Jxx, self.Jyy, self.Jzz, self.g, self.b)
        # AEKF System description
        self.A, self.b, self.B, self.C = compute_dynamics_hexa.Hexacopter.get_model(self.dt, self.L)

        # aekf = AEKF(A, B, QF, RF, quad)
        self.motor_fault_estimator = aekf_hexa.AEKF(self.A, self.B, self.QF, self.hexa, 0.001, self.lambdaa)

        # Update state estimate in 10Hz.
        rospy.sleep(10) # sleep 10s before doing anything
        rospy.Timer(rospy.Duration(self.dt), self.timer_loop)

        self.first_measurement = True

        rospy.spin()

    def cb_imu(self, msg):
        # print(msg)
        # Convert orientation from quartenion to rpy
        quat_w = float(msg.orientation.w)
        quat_x = float(msg.orientation.x)
        quat_y = float(msg.orientation.y)
        quat_z = float(msg.orientation.z)
        
        # print([quat_w, quat_x, quat_y, quat_z])
        angles = quat2euler([quat_w, quat_x, quat_y, quat_z])
        self.y[0] = angles[0]
        self.y[2] = angles[1]
        self.y[4] = angles[2]

        # angular velocities
        self.y[1] = msg.angular_velocity.x
        self.y[3] = msg.angular_velocity.y
        self.y[5] = msg.angular_velocity.z

        self.measurement_ready = True

        cov_orient = np.diag(np.asarray(msg.orientation_covariance).reshape((3, 3)))
        # rospy.logerr(cov_orient)
        cov_angular = np.diag(np.asarray(msg.angular_velocity_covariance).reshape((3, 3)))

        self.RF_imu = np.asarray([
            cov_orient[0],
            cov_angular[0],
            cov_orient[1],
            cov_angular[1],
            cov_orient[2],
            cov_angular[2],
        ])

        # print(self.y)

    def cb_local_pos(self, msg):
        # print(msg)
        # pass
        self.y[6] = msg.pose.pose.position.x
        self.y[7] = msg.pose.pose.position.y
        self.y[8] = msg.pose.pose.position.z
        # print(self.y)

        self.measurement_ready = True

        self.RF_pos = np.diag(np.asarray(msg.pose.covariance).reshape((6, 6)))[:3]
        # rospy.logerr(f"self.RF_pos: {self.RF_pos}")

    def cb_rc(self, msg):
        # print(msg)
        # print("motor 1", msg.channels[0])
        channels = np.asarray([msg.channels[:6]]).T
        # channels[0] = channels[2] # such that it doesn't know that propellor is not working # not working
        self.Uk_raw = channels
        self.Uk = self.num_to_thrust(channels)
        # print(self.Uk)
        # rospy.loginfo(f"     Motor thrust    : {list(self.Uk)}")
        # rospy.loginfo(f"     Motor thrust raw: {list(self.Uk_raw)}")
        self.measurement_ready = True

    def num_to_thrust(self, pwm_in):
        mask = pwm_in < 1000 # Gets a boolean mask six long on if one of them is less than 1000
        out = self._raw_num_to_thrust(pwm_in)
        out[mask] = 0
        return out
        # if pwm_in < 1000:
        #     return 0
        # elif pwm_in >= 2000:
        #     return self._raw_num_to_thrust(2000)
        # else:
        #     return self._raw_num_to_thrust(pwm_in)
    # @staticmethod
    # def num_to_thrust(pwm_in):
    #     # converts the throttle value from px4 pwm in from 1075 to 1950 to thrust [N]
    #     # first convert [1000; 2000] to [0; 1]
    #     rang = [1075, 1950]
    #     throttles = (pwm_in - rang[0]) / (rang[1] - rang[0])

    #     # Throttle values to thrust from datasheet
    #     #### 1 gram-force to newton = 0.00980665 newton
    #     # throttle [%]      Thrust [g]      Thrust [N]
    #     # 50                435             4.266
    #     # 55                527             5.168
    #     # 60                608             5.962
    #     # 65                702             6.884
    #     # 75                888             8.708
    #     # 85                1076            10.551
    #     # 100               1293            12.680
    #     data_points = [[0.50,  4.266],
    #                    [0.55,  5.168],
    #                    [0.60,  5.962],
    #                    [0.65,  6.884],
    #                    [0.75,  8.708],
    #                    [0.85, 10.551],
    #                    [1.00, 12.680]]
        
    #     out_throttles = np.zeros_like(throttles)

    #     for i in range(throttles.shape[0]):
    #         rospy.loginfo(i)
    #         throttle = throttles[i, 0]
    #         rospy.loginfo(throttle)

    #         ret_val = -1

    #         if throttle < 0: 
    #             ret_val = 0

    #         if throttle < data_points[0][0] and throttle >= 0: # to catch the first case if the throttle is below the first one.
    #             # ret_val = 0
    #             # Make a linear interpolation from the first point to (0, 0)
    #             ret_val = data_points[0][1] \
    #                 + (throttle - data_points[0][0]) \
    #                 * (data_points[0][1] - 0) \
    #                 / (data_points[0][0] - 0)

    #         if throttle >= data_points[-1][0] and throttle < 1: # if the throttle is larger than the last, cap it at the top value
    #             ret_val = data_points[-1][1]  # this should never be the case. never above 100%

            # if throttle >= 1:
            #     ret_val = data_points[-1][1]

    #         if ret_val < 0:
    #             for i in range(len(data_points) - 1):
    #                 if throttle >= data_points[i][0] and throttle < data_points[i + 1][0]:
    #                     # if throttle is between two values, make a linear interpolation
    #                     # y = y0 + (x - x0) * (y1 - y0)/(x1 - x0)
    #                     ret_val = data_points[i][1] \
    #                         + (throttle - data_points[i][0]) \
    #                         * (data_points[i + 1][1] - data_points[i][1]) \
    #                         / (data_points[i + 1][0] - data_points[i][0])

    #                     break

    #         out_throttles[i, 0] = ret_val

    #     return out_throttles

    def timer_loop(self, timer_event):
        # print(dir(timer_event))
        # print(timer_event.last_real)
        # print(timer_event.current_real)
        # a = timer_event.last_real
        # b = timer_event.current_real
        # if a is None: return # skip first iteration

        if not self.measurement_ready:
            return

        self.measurement_ready = False

        # dt = b - a
        # dt = dt.secs + dt.nsecs * 1e-9 # Convert to seconds
        # dt = timer_event.last_real - timer_event.current_real
        # print(dt.secs + dt.nsecs * 1e-9)
        # rospy.loginfo(dt)

        # AEKF System description
        self.A, self.b, self.B, self.C = compute_dynamics_hexa.Hexacopter.get_model(self.dt, self.L)

        # aekf = AEKF(A, B, QF, RF, quad)
        # self.motor_fault_estimator = aekf_hexa.AEKF(self.A, self.B, self.QF, self.hexa)

        # make RF matrix
        # rospy.logerr(np.concatenate((self.RF_imu, self.RF_pos)))
        RF = np.diag(np.concatenate((self.RF_imu, self.RF_pos)))
        # rospy.logerr(np.diag(RF))
        # rospy.logerr(RF)

        # Now to the Motor Fault Estimation
        self.motor_fault_estimator.update(self.y, self.C, RF, self.Uk, self.dt, self.first_measurement)
        self.first_measurement = False

        self.X = self.motor_fault_estimator.Xhat
        self.gain_loss = self.motor_fault_estimator.thetahat

        # rospy.loginfo(self.X)
        # rospy.loginfo(self.gain_loss)
        # self.print_xyz()
        # self.print_motor_fault_estimate()
        # self.print_motor_thrust()

        # rospy.logerr("before publishing")
        self.publish_state()
        self.publish_motor_fault_estimate()
        self.publish_motor_thrust()

        self.write_sensor_data_to_csv()
        # rospy.logerr("after publishing")

    def print_xyz(self):
        rospy.loginfo(f"x: {self.X[6, 0]}, y: {self.X[8, 0]}, z: {self.X[10, 0]}")

    def print_motor_fault_estimate(self):
        rospy.loginfo(f"Fault estimate: {list(self.gain_loss)}")

    def print_motor_thrust(self):
        rospy.loginfo(f"Loop Motor thrust    : {list(self.Uk)}")
        rospy.loginfo(f"Loop Motor thrust raw: {list(self.Uk_raw)}")

    def publish_state(self):
        msg = Float64MultiArray()
        msg.data = self.X
        msg.layout = MultiArrayLayout(
            dim = [MultiArrayDimension(
                label = "StateEstimate",
                size = self.X.shape[0],
                stride = 1
            )],
            data_offset = 0
        )
        self.pub_state_estimate.publish(msg)

    def publish_motor_fault_estimate(self):
        msg = Float64MultiArray()
        msg.data = self.gain_loss
        msg.layout = MultiArrayLayout(
            dim = [MultiArrayDimension(
                label = "MotorFaultEstimate",
                size = self.gain_loss.shape[0],
                stride = 1
            )],
            data_offset = 0
        )
        # rospy.loginfo("publish motor fault")
        self.pub_motor_fault_estimate.publish(msg)
        # rospy.loginfo("publish motor fault")

    def publish_motor_thrust(self):
        msg = Float64MultiArray()
        msg.data = self.Uk
        msg.layout = MultiArrayLayout(
            dim = [MultiArrayDimension(
                label = "MotorThrust",
                size = self.Uk.shape[0],
                stride = 1
            )],
            data_offset = 0
        )
        self.pub_motor_thrust.publish(msg)

    def write_sensor_data_to_csv(self):
        # function to write sensor data to csv
        if self.output_file is None:
            return

        with open(self.output_file, 'a') as f:
            tmp = ",".join([str(x[0]) for x in self.Uk_raw])
            # f.write(f"{rospy.get_time()},{self.y[0,0]},{self.y[2,0]},{self.y[4,0]},{self.y[1,0]},{self.y[3,0]},{self.y[5,0]},{self.y[6,0]},{self.y[7,0]},{self.y[8,0]},{tmp}\n")
            # f.write(
            #         "time,"
            #         + "estimated_pos_x,estimated_pos_y,estimated_pos_z,"
            #         + "actual_pos_x,actual_pos_y,actual_pos_z,"
                    
            #         + "estimated_roll,estimated_pitch,estimated_yaw,"
            #         + "actual_roll,actual_pitch,actual_yaw,"

            #         + "estimated_ang_vel_x,estimated_ang_vel_y,estimated_ang_vel_z,"
            #         + "actual_ang_vel_x,actual_ang_vel_y,actual_ang_vel_z,"
                    
            #         + "rc_out_0,rc_out_1,rc_out_2,rc_out_3,rc_out_4,rc_out_5,"
            #         + "mfe_0,mfe_1,mfe_2,mfe_3,mfe_4,mfe_5"
            #         + "\n"
            #     )
            f.write(
                f"{rospy.get_time()},"
                + f"{self.X[6,0]},{self.X[8,0]},{self.X[10,0]}," # Estimated pos
                + f"{self.y[6,0]},{self.y[7,0]},{self.y[8,0]}," # Actual pos

                + f"{self.X[0,0]},{self.X[2,0]},{self.X[4,0]}," # Estimated rpy
                + f"{self.y[0,0]},{self.y[2,0]},{self.y[4,0]}," # Actual rpy

                + f"{self.X[1,0]},{self.X[3,0]},{self.X[5,0]}," # Estimated angular velocity
                + f"{self.y[1,0]},{self.y[3,0]},{self.y[5,0]}," # Actual angular velocity
                
                + ",".join([str(x[0]) for x in self.Uk]) + "," # RC out values
                + ",".join([str(x[0]) for x in self.gain_loss]) +"\n" # gain loss values
            )


def main():
    mfe = MotorFaultEstimate()

if __name__ == '__main__':
    main()