#!/usr/bin/env python3

import rospy
import numpy as np

from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped, PoseWithCovariance
from sensor_msgs.msg import Imu

import covariances

class CovarianceReplacer:
    def __init__(self):
        rospy.init_node('covariance_replacer')

        # subscribers
        self.sub_pose = rospy.Subscriber('~pose_in', PoseStamped, self.cb_pose)
        self.sub_imu  = rospy.Subscriber('~imu_in', Imu, self.cb_imu)

        # publishers
        self.pub_pose = rospy.Publisher('~pose_out', PoseWithCovarianceStamped, queue_size=1)
        self.pub_imu  = rospy.Publisher('~imu_out', Imu, queue_size=1)

        rospy.spin()

    def cb_pose(self, msg):
        out = PoseWithCovarianceStamped()
        out.header = msg.header
        out.pose = PoseWithCovariance(
            pose = msg.pose,
            covariance = np.diag(covariances.cov_pos_default + [0, 0, 0]).flatten()
        )
        # rospy.loginfo(out)
        self.pub_pose.publish(out)

    def cb_imu(self, msg):
        out = msg
        out.orientation_covariance = np.diag(
                [covariances.cov_imu_default[i] for i in [0, 2, 4]]
            ).flatten()
        out.angular_velocity_covariance = np.diag(
                [covariances.cov_imu_default[i] for i in [1, 3, 5]]
            ).flatten()
        self.pub_imu.publish(out)

        rospy.loginfo(out)

if __name__ == "__main__":
    cv = CovarianceReplacer()