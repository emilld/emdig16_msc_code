#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import subprocess

# matplotlib.use('Agg',warn=False, force=True)
matplotlib.use('Agg')
plt.rcParams['animation.ffmpeg_path'] = '/usr/bin/ffmpeg'

filename = "broken_prop_01_clipped"
# filename = "normal_with_weight_01"
# filename = "normal_prop_05"
# data = pd.read_csv("datafile_normal_prop_05.csv")
# data = pd.read_csv("datafile_broken_prop_01.csv")
# data = pd.read_csv("datafile_normal_with_weight_01.csv")
data = pd.read_csv(f"datafile_{filename}.csv")

print(data.columns)

t = data['time'] - data['time'][0]
t = np.asarray(t)
print(t)

start_frame = 0
length = 200

color = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown']
# Video of the motor fault estimation
fig, axs = plt.subplots(3, 1, sharex=True, dpi=150)
fig.set_size_inches(w=5, h=8)
canvas_width, canvas_height = fig.canvas.get_width_height()

plt.xlabel("Time [s]")
axs[0].set_ylabel("Position $z$")
axs[1].set_ylabel("Force [N]")
axs[2].set_ylabel("Fault")

axs[0].grid()
axs[1].grid()
axs[2].grid()

# plt.xlim((0, 80))
plt.xlim((start_frame/10, (start_frame + length)/10))
axs[0].set_ylim((0, 1.5))
axs[1].set_ylim((0, 5))
axs[2].set_ylim((-0.2, 1.2))

def buildmeachart(i = int):
    # https://towardsdatascience.com/learn-how-to-create-animated-graphs-in-python-fce780421afe
    # Drone height plot
    p = axs[0].plot(t[:i], data[f"actual_pos_z"][:i])
    p[0].set_color(color[0])

    # Motor force plot
    axs[1].legend([f"Motor {x + 1} thrust" for x in range(6)],  loc="upper center", bbox_to_anchor=(0.5, 1), ncol=3)
    for k in range(6):
        p = axs[1].plot(t[:i], data[f"rc_out_{k}"][:i])
        p[0].set_color(color[k])

    # Motor fault plot
    axs[2].legend([f"Motor {x + 1}" for x in range(6)], loc="upper center", bbox_to_anchor=(0.5, 1), ncol=3)
    
    for k in range(6):
        # print(data[f"mfe_{k}"][:i])
        p = axs[2].plot(t[:i], data[f"mfe_{k}"][:i], '-')
        p[0].set_color(color[k])
        # plt.plot(t[:i], data[f"mfe_{k}"][:i])

frames = np.shape(data)[0]
# animator = ani.FuncAnimation(fig, buildmeachart, frames=frames, interval = 100)

# # mywriter = ani.writers['ffmpeg'](fps=30)
# mywriter = ani.writers['ffmpeg'](fps=10)
# animator.save(f"animations/{filename}.mp4", writer=mywriter, dpi=100)
# animator.save(f"animations/{filename}.mp4", dpi=200)

# plt.show()

# Open an ffmpeg process
outf = f"animations/graph_{filename}.mp4"
cmdstring = ('ffmpeg', 
    '-y', '-r', '10', # overwrite, 30fps
    '-s', '%dx%d' % (canvas_width, canvas_height), # size of image string
    '-pix_fmt', 'argb', # format
    '-f', 'rawvideo',  '-i', '-', # tell ffmpeg to expect raw video from the pipe
    '-c:v', 'libx264', outf) # output encoding
    # '-vcodec', 'mpeg4', outf) # output encoding
p = subprocess.Popen(cmdstring, stdin=subprocess.PIPE)

for frame in range(length):
    buildmeachart(frame + start_frame)
    fig.canvas.draw()

    string = fig.canvas.tostring_argb()

    # write to pipe 
    p.stdin.write(string)

p.communicate()