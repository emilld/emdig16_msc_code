#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from cycler import cycler

plt.rc('pgf', texsystem='pdflatex')

plt.rc('font', family='serif', serif='Times')
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=8)
plt.rc('ytick', labelsize=8)
plt.rc('axes', labelsize=8)
plt.rc('legend', fontsize=8)

monochrome = (cycler('color', ['k']) * cycler('marker', ['', '*']) *
              cycler('linestyle', ['-', '--', ':', '-.']))
            #   cycler('linestyle', ['-', '--', ':', '-.', '.']))
plt.rc('axes', prop_cycle=monochrome)

mpl.rcParams['lines.markersize'] = 4

width = 3.4
height = width / 1.618

# filename = "broken_prop_01"
# filename = "normal_with_weight_01"
# filename = "broken_prop_01_clipped"
# filename = "normal_with_weight_01_clipped"
filename = "normal_prop_03_clipped"
# line_height = 0.2
line_height = False
# data = pd.read_csv("datafile_normal_prop_05.csv")
# data = pd.read_csv("datafile_broken_prop_01.csv")
# data = pd.read_csv("datafile_normal_with_weight_01.csv")
data = pd.read_csv(f"datafile_{filename}.csv")
time_limit = [0, 90]

print(data.columns)

t = data['time'] - data['time'][0]
t = np.asarray(t)
print(t)

# Estimated x, y, z and the real thing
fig = plt.figure()
fig.subplots_adjust(left=.15, bottom=.17, right=.97, top=.97)
fig.set_size_inches(w=width, h=height*2)

name = ['x', 'y', 'z']
lims = [(-0.2, 0.2), (-0.2, 0.2), (0, 2)]

for i in range(len(name)):
    plt.subplot(3, 1, i + 1)
    plt.plot(t, data[f"estimated_pos_{name[i]}"], label=f"Estimated ${name[i]}$")
    plt.plot(t, data[f"actual_pos_{name[i]}"], label=f"Actual ${name[i]}$")
    # plt.legend()
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1),
          ncol=3, fancybox=False, shadow=False)
    plt.ylabel(f"Position ${name[i]}$ [m]")
    plt.grid()
    plt.ylim(lims[i])
    plt.xlim(time_limit)

plt.xlabel("Time [s]")

plt.savefig(f"plots/{filename}_position.pdf")

# Motor thrust
fig = plt.figure()
fig.subplots_adjust(left=.15, bottom=.17, right=.97, top=.97)
fig.set_size_inches(w=width, h=height*2)
for i in range(2):
    plt.subplot(2, 1, i + 1)
    plt.plot(t, data[f"rc_out_{3 * i + i}"], label=f"Motor {3 * i + 1}")
    plt.plot(t, data[f"rc_out_{3 * i + 1}"], label=f"Motor {3 * i + 2}")
    plt.plot(t, data[f"rc_out_{3 * i + 2}"], label=f"Motor {3 * i + 3}")
    
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1),
        ncol=3, fancybox=False, shadow=False)
    plt.ylabel("Force [N]")
    plt.ylim((0, 5))
    plt.xlim(time_limit)
    plt.grid()

plt.xlabel("Time [s]")


plt.savefig(f"plots/{filename}_motor_thrust.pdf")

# Motor fault estimate
fig = plt.figure()
fig.subplots_adjust(left=.15, bottom=.17, right=.97, top=.97)
fig.set_size_inches(w=width, h=height*1.5)
for i in range(2):
    plt.subplot(2, 1, i + 1)
    plt.plot(t, data[f"mfe_{3 * i}"], label=f"Motor {3 * i + 1}")
    plt.plot(t, data[f"mfe_{3 * i + 1}"], label=f"Motor {3 * i + 2}")
    plt.plot(t, data[f"mfe_{3 * i + 2}"], label=f"Motor {3 * i + 3}")
    if line_height:
        plt.axhline(y=line_height, color='r', linestyle='-.', label=f"{line_height * 100}\%")

    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1),
        ncol=3, fancybox=False, shadow=False)
    plt.grid()
    plt.ylabel("Fault")
    plt.ylim((-0.2, 1.2))
    plt.xlim(time_limit)

plt.xlabel("Time [s]")

plt.savefig(f"plots/{filename}_fault_estimate.pdf")

plt.show()