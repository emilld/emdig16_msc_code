#include <iostream>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>

#include "../include/bike.hpp"
#include "../include/mpc.hpp"
#include "../include/aekf.hpp"
#include "../include/hexacopter.hpp"


int main()
{
    double m = 1.5,         // mass in kg
           L = 0.37,        // arm length in m
           Jxx = 0.0762625, // moment of inertia around 'x' axis in kg�m^2
           Jyy = 0.0762625, // moment of inertia around 'y' axis in kg�m^2
           Jzz = 0.1369,    // moment of inertia around 'z' axis in kg�m^2
           g = 9.81,        // gravity
           b = 0.01,
           dt = 0.01;

    Hexacopter hexa(m, L, Jxx, Jyy, Jzz, g, b, dt);

    // Initialize state and input vector.
    Eigen::VectorXd state(hexa.Nx); // 12
    Eigen::VectorXd input_vec(hexa.Nu); // 6

    state << 0.2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
    input_vec << 3, 3, 3, 3, 3, 3;

    Eigen::MatrixXd Fxhat(12, 12), Fu(12, 6);

    // Some simulation
    std::cout << "t, phi, phid, theta, thetad, psi, psid, x, xd, y, yd, z, zd" << std::endl;
    
    for (double t = 0; t < 10; t += dt)
    {
        std::cout << t << ", ";
        for (size_t i = 0; i < hexa.Nx; i++)
        {
            if (i == hexa.Nx - 1)
                std::cout << state(i);
            else
                std::cout << state(i) << ", ";
        }

        std::cout << std::endl;
        
        state = hexa.compute_dynamics(state, input_vec, dt);

        // // Test that we can get FxHat.
        // Fxhat = hexa.get_Fxhat(state, input_vec, dt);
        // std::cout << "Fxhat = \n" << Fxhat << std::endl;

        // // Test that we can get Fu.
        // Fu = hexa.get_Fu(state, input_vec, dt);
        // std::cout << "Fu = \n" << Fu << std::endl;
    }

    return 0;
}