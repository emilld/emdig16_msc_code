#include <iostream>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>

#include "../include/bike.hpp"
#include "../include/mpc.hpp"
#include "../include/aekf.hpp"

#include "nlopt.hpp"

int main() {
    std::cout << "Running AEKF test main!" << std::endl;
    
    Bike bike(1, 4, 2);
    std::cout << bike.Nu << " " << bike.Nx << std::endl;
    
    double ss = 0.001;
    double lambdaa = 0.99;
    AEKF aekf(bike, ss, lambdaa);

    Eigen::VectorXd Qf(bike.Nx), Rf(bike.get_C().rows());
    Qf << 0.01, 0.01, 0.01, 0.01; // System noise
    Rf << 0.04, 0.04, 0.04; // Measurement noise
    // Rf *= 1000;

    Eigen::MatrixXd QQf = Qf.asDiagonal();
    Eigen::MatrixXd RRf = Rf.asDiagonal();

    aekf.set_Q(QQf);
    aekf.set_R(RRf);

    std::cout << "Q = \n" << aekf.get_Q() << std::endl;
    std::cout << "R = \n" << aekf.get_R() << std::endl;

    // Initialize state and input vector.
    Eigen::VectorXd state(4, 1);
    Eigen::VectorXd input_vec(2, 1);

    state(0, 0) = 0; // pos x
    state(1, 0) = 0; // pos y
    state(2, 0) = 0; // M_PI / 3; // yaw 
    state(3, 0) = 0; //1; // velocity

    input_vec(0, 0) = M_PI/4; // M_PI / 4; // steering wheel
    input_vec(1, 0) = 1; // 2; // acceleration

    // Gain loss vector.
    Eigen::MatrixXd C = bike.get_C();
    Eigen::VectorXd gain_loss(bike.Nu);

    // Example simulation with non-complete measurement.
    Eigen::VectorXd y(C.rows()), randNoiseVec(C.rows());
    double dt = 0.01;

    std::ofstream outfile;
    outfile.open("../output/aekf_sim.csv");
    // outfile << "time, state_actual1, state_actual2, state_actual3, <, state_estimated, fault_actual, fault_estimated" << std::endl;
    outfile << "time, ";

    for (size_t i = 0; i < bike.Nx; i++)
        outfile << "state_actual" << i << ", ";
    for (size_t i = 0; i < y.rows(); i++)
        outfile << "state_measured" << i << ", ";
    for (size_t i = 0; i < bike.Nx; i++)
        outfile << "state_estimated" << i << ", ";
        for (size_t i = 0; i < bike.Nx; i++)
        outfile << "state_reference" << i << ", ";
    for (size_t i = 0; i < bike.Nu; i++)
        outfile << "fault_actual" << i << ", ";
    for (size_t i = 0; i < bike.Nu; i++)
        outfile << "fault_estimated" << i << ", ";
    for (size_t i = 0; i < bike.Nu; i++)
        outfile << "input" << i << ", ";
    
    outfile << std::endl;


    for (double t = 0; t < 100; t += dt) 
    {
        if (t > 10)
            gain_loss << 0.9, 0.9;
        // if (t > 40)
        //     gain_loss << 0.3, 0.5;

        state = bike.compute_dynamics(state, input_vec, gain_loss, dt);

        // Take a measurement
        // y = C*X + RF*dt*randn(rank(C),1); // Matlab code
        // y = C * state + RRf * dt * randNoiseVec.Random();
        // std::cout << C.rows() << ", " << C.cols() << std::endl;
        // std::cout << state.rows() << ", " << state.cols() << std::endl;
        // std::cout << randNoiseVec.Random(C.rows()) << std::endl;
        y = C * state + dt * RRf * randNoiseVec.Random(C.rows());

        // std::cout << "y = \n" << y << std::endl;

        aekf.update(y, input_vec, dt);

        // std::cout << "[" << t << "],\t" <<
        //     state.transpose() << ",\t" <<
        //     aekf.get_state_estimate().transpose() << ",\t" <<
        //     gain_loss.transpose() << ",\t" <<
        //     aekf.get_fault_estimate().transpose() << ",\t" <<
        // std::endl;
        
        outfile << t << ", ";

        for (size_t i = 0; i < bike.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << state(i) << ", ";
        for (size_t i = 0; i < y.rows(); i++)
            outfile << std::fixed << std::setprecision(4) << y(i) << ", ";
        for (size_t i = 0; i < bike.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_state_estimate()(i) << ", ";
        for (size_t i = 0; i < bike.Nx; i++)
            outfile << 0 << ", ";
        for (size_t i = 0; i < bike.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << gain_loss(i) << ", ";
        for (size_t i = 0; i < bike.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_fault_estimate()(i) << i << ", ";
        for (size_t i = 0; i < bike.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << input_vec(i) << i << ", ";
        
        outfile << std::endl;
    }

    outfile.close();

    return 0;
}