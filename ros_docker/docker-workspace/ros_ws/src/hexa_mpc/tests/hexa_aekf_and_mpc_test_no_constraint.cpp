#include <iostream>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>

#include "../include/hexa_mpc/hexacopter.hpp"
#include "../include/hexa_mpc/mpc2.hpp"
#include "../include/hexa_mpc/aekf.hpp"

#include "nlopt.hpp"

int main() {
    std::cout << "Running AEKF and MPC on a hexacopter model test main!" << std::endl;
    
    // Making a bike
    // Bike bike(1, 4, 2);
    double m = 1.5,         // mass in kg
        L = 0.37,        // arm length in m
        Jxx = 0.0762625, // moment of inertia around 'x' axis in kg�m^2
        Jyy = 0.0762625, // moment of inertia around 'y' axis in kg�m^2
        Jzz = 0.1369,    // moment of inertia around 'z' axis in kg�m^2
        g = 9.81,        // gravity
        b = 0.01,
        dt = 0.01,
        dt_mpc = 0.01, 
        // dt_mpc = 0.01,
        alpha = 1; /* for lpf on inputs in MPC*/;

    Hexacopter hexa(m, L, Jxx, Jyy, Jzz, g, b, dt);
    std::cout << hexa.Nu << " " << hexa.Nx << std::endl;
    
    // Making the state and fault estimator.
    double ss = 0.001;
    double lambdaa = 0.995;
    // double lambdaa = 0.97;
    AEKF aekf(hexa, ss, lambdaa);

    Eigen::VectorXd Qf(hexa.Nx), Rf(hexa.get_C().rows());
    Qf << 0.1, 0.01, 0.1, 0.01, 0.1, 0.01, 0.5, 0.1, 0.5, 0.1, 0.1, 0.01; // System noise, [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]
    Rf << 0.5, 0.1, 0.5, 0.1, 0.2, 0.1, 1, 1, 0.3; // Measurement noise, [phi, phidot, theta, thetadot, psi, psidot, x, y, z]'
    // Rf *= 1000;

    Eigen::MatrixXd QQf = Qf.asDiagonal();
    Eigen::MatrixXd RRf = Rf.asDiagonal();

    aekf.set_Q(QQf);
    aekf.set_R(RRf);

    std::cout << "Q = \n" << aekf.get_Q() << std::endl;
    std::cout << "R = \n" << aekf.get_R() << std::endl;

    // Making the MPC.
    // int N = 20; // Horizon
    // int N = ceil(1 / dt);
    int N_control = 2;
    int N_prediction = 50;

    ModelPredictiveControl2 mpc(hexa, N_prediction, N_control);
    Eigen::VectorXd q(hexa.Nx), r(hexa.Nu);
    // q <<   /*pos x*/ 1, /*pos y*/ 1,  /*yaw*/ 0.05, /*velocity*/ 100;
    // r <<   10,          10;
    q <<   100, 100, 100, 100, 200, 100, 200, 100, 200, 100, 200, 100;
    // // q *= 10;
    r <<   0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
    // r <<   0.01, 0.01, 0.01, 0.01, 0.01, 0.01;
    // q << 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1;
    // r <<   0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
    mpc.create_G_matrix(q, r);

    std::cout << "Setting bounds." << std::endl;

    // Set bounds
    Eigen::VectorXd lb(hexa.Nu), ub(hexa.Nu);
    lb <<  0, 0, 0, 0, 0, 0;
    // lb <<  1, 1, 1, 1, 1, 1;
    // lb *= 2.5;
    ub <<  8, 8, 8, 8, 8, 8;
    // lb << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -0.7, -2;
    // ub <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  0.7,  2;

    mpc.set_bounds(lb, ub);

    // Eigen::VectorXd lb_states(hexa.Nx), ub_states(hexa.Nx);
    // lb_states << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, 0.1;
    // ub_states <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL, 4.9;

    // bike.set_sim_state_bound(lb_states, ub_states);
    mpc.print_bounds();

    // Set reference:
    Eigen::VectorXd z_ref(hexa.Nx);
    // z_ref << 0, 0, 0, 0, M_PI_2, 0, 3, 0, -2, 0, 10, 0;
    z_ref << 0, 0, 0, 0, M_PI_2, 0, 10, 0, 10, 0, 10, 0;
    // z_ref << 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 10, 0;
    
    mpc.set_Z_ref(z_ref);

    // Initialize state and input vector.
    Eigen::VectorXd state(hexa.Nx);
    state.setZero();
    Eigen::VectorXd input_vec(hexa.Nu);
    input_vec.setZero();
    // state(10) = 20;
    // input_vec << 4, 4.2, 4, 4, 4, 4.1;
    // input_vec << 0, 0, 0, 0, 0, 0;
    // input_vec = (ub - lb) / 2;

    // Gain loss vector.
    Eigen::VectorXd gain_loss(hexa.Nu);
    gain_loss.setZero();
    
    Eigen::MatrixXd C = hexa.get_C();

    // Example simulation with non-complete measurement.
    Eigen::VectorXd y(C.rows()), randNoiseVec(C.rows());
    y.setZero();
    randNoiseVec.setZero();
    // double dt = 0.01, dt_mpc = 0.1, alpha = 0.5; /* for lpf on inputs in MPC*/;


    std::ofstream outfile;
    outfile.open("../output/hexa_aekf_mpc_sim.csv");
    // outfile << "time, state_actual1, state_actual2, state_actual3, <, state_estimated, fault_actual, fault_estimated" << std::endl;
    outfile << "time, ";

    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << "state_actual" << i << ", ";
    for (size_t i = 0; i < y.rows(); i++)
        outfile << "state_measured" << i << ", ";
    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << "state_estimated" << i << ", ";
    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << "state_reference" << i << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << "fault_actual" << i << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << "fault_estimated" << i << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << "input" << i << ", ";
    
    outfile << std::endl;

    // y = C * state + dt * RRf * randNoiseVec.Random(C.rows());
    // aekf.update(y, input_vec, dt);
    
    outfile << 0 << ", ";

    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << std::fixed << std::setprecision(4) << state(i) << ", ";
    for (size_t i = 0; i < y.rows(); i++)
        outfile << std::fixed << std::setprecision(4) << y(i) << ", ";
    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << std::fixed << std::setprecision(4) << aekf.get_state_estimate()(i) << ", ";
    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << std::fixed << std::setprecision(4) << z_ref(i) << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << std::fixed << std::setprecision(4) << gain_loss(i) << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << std::fixed << std::setprecision(4) << aekf.get_fault_estimate()(i) << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << std::fixed << std::setprecision(4) << input_vec(i) << ", ";
    
    outfile << std::endl;

    std::chrono::high_resolution_clock::time_point start, end;

    mpc.use_state_constraint();

    for (double t = 0; t < 40; t += dt) 
    // for (double t = 0; t < dt; t += dt) 
    {
        // std::cout << t << std::endl;

        if (t > 5)
            gain_loss << 0.1, 0.05, 0.2, 0.15, 0.1, 0.07;
        //     gain_loss << 0.5, 0, 0, 0, 0, 0;
        // if (t > 10)
        //     gain_loss << 0.7, 0, 0.4, 0.1, 0.2, 0; 
        
        // if (t > 10)
        //     gain_loss << 0.1, 0.05, 0.2, 0.15, 0.1, 0.07;

        // if (t > 50)
        //     gain_loss << 0.15, 0.2;
        // state = bike.compute_dynamics(state, input_vec, gain_loss, dt);

        // Take a measurement
        // y = C*X + RF*dt*randn(rank(C),1); // Matlab code
        // y = C * state + RRf * dt * randNoiseVec.Random();
        // std::cout << C.rows() << ", " << C.cols() << std::endl;
        // std::cout << state.rows() << ", " << state.cols() << std::endl;
        // std::cout << randNoiseVec.Random(C.rows()) << std::endl;
        
        state = hexa.compute_dynamics(state, input_vec, gain_loss, dt); // The "real thing".
        y = C * state + dt * RRf * randNoiseVec.Random(C.rows());

        // std::cout << "y = \n" << y << std::endl;

        aekf.update(y, input_vec, dt);
        
        mpc.set_Z_ref(z_ref);

        std::cout << t << " " << z_ref.transpose() << std::flush;

        start = std::chrono::high_resolution_clock::now();
        input_vec = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
        // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc, true);
        // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, gain_loss_fake, dt_mpc, alpha);
        end = std::chrono::high_resolution_clock::now();
        double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();

        // input_vec << 4, 4, 4, 4, 4, 4;

        std::cout << ", elapsed: " << time_elapsed << " ms" << std::endl;

        // state = hexa.compute_dynamics(state, input_vec, gain_loss, dt); // The "real thing".

        // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, dt_mpc);
        // input_vec = mpc.update(state, input_vec, dt_mpc);

        // std::cout << "[" << t << "],\t" <<
        //     state.transpose() << ",\t" <<
        //     aekf.get_state_estimate().transpose() << ",\t" <<
        //     gain_loss.transpose() << ",\t" <<
        //     aekf.get_fault_estimate().transpose() << ",\t" <<
        // std::endl;

        outfile << t + dt << ", ";

        for (size_t i = 0; i < hexa.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << state(i) << ", ";
        for (size_t i = 0; i < y.rows(); i++)
            outfile << std::fixed << std::setprecision(4) << y(i) << ", ";
        for (size_t i = 0; i < hexa.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_state_estimate()(i) << ", ";
        for (size_t i = 0; i < hexa.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << z_ref(i) << ", ";
        for (size_t i = 0; i < hexa.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << gain_loss(i) << ", ";
        for (size_t i = 0; i < hexa.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_fault_estimate()(i) << ", ";
        for (size_t i = 0; i < hexa.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << input_vec(i) << ", ";
        
        outfile << std::endl;
    }

    outfile.close();

    return 0;
}
