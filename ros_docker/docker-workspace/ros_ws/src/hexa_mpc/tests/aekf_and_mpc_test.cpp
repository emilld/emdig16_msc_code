#include <iostream>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>

#include "../include/bike.hpp"
#include "../include/mpc.hpp"
#include "../include/aekf.hpp"

#include "nlopt.hpp"

int main() {
    std::cout << "Running AEKF and MPC test main!" << std::endl;
    
    // Making a bike
    Bike bike(1, 4, 2);
    std::cout << bike.Nu << " " << bike.Nx << std::endl;
    
    // Making the state and fault estimator.
    double ss = 0.001;
    double lambdaa = 0.995;
    AEKF aekf(bike, ss, lambdaa);

    Eigen::VectorXd Qf(bike.Nx), Rf(bike.get_C().rows());
    Qf << 0.01, 0.01, 0.01, 0.01; // System noise
    Rf << 0.4, 0.4, 0.4; // Measurement noise
    // Rf *= 1000;

    Eigen::MatrixXd QQf = Qf.asDiagonal();
    Eigen::MatrixXd RRf = Rf.asDiagonal();

    aekf.set_Q(QQf);
    aekf.set_R(RRf);

    std::cout << "Q = \n" << aekf.get_Q() << std::endl;
    std::cout << "R = \n" << aekf.get_R() << std::endl;

    // Making the MPC.
    int N = 10; // Horizon
    ModelPredictiveControl mpc(bike, N);
    Eigen::VectorXd q(bike.Nx), r(bike.Nu);
    // q <<   /*pos x*/ 1, /*pos y*/ 1,  /*yaw*/ 0.05, /*velocity*/ 100;
    // r <<   10,          10;
    q <<   /*pos x*/ 20,  /*pos y*/ 50,  /*yaw*/ 0.1, /*velocity*/ 5;
    r <<   0.1,          0.1;
    mpc.create_G_matrix(q, r);

    std::cout << "G = \n" << mpc.get_G() << std::endl;

    // Set bounds
    Eigen::VectorXd lb(bike.Nx + bike.Nu), ub(bike.Nx + bike.Nu);
    lb << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL,  0, -0.7, -2;
    ub <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  5,  0.7,  2;
    // lb << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -0.7, -2;
    // ub <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  0.7,  2;

    mpc.set_bounds(lb, ub);

    Eigen::VectorXd lb_states(bike.Nx), ub_states(bike.Nx);
    lb_states << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, 0.1;
    ub_states <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL, 4.9;

    bike.set_sim_state_bound(lb_states, ub_states);
    mpc.print_bounds();

    // Set reference:
    Eigen::VectorXd z_ref(bike.Nx);
    z_ref << 10, 10, 0, 1;

    mpc.set_Z_ref(z_ref);

    // Initialize state and input vector.
    Eigen::VectorXd state(4);
    Eigen::VectorXd input_vec(2);

    state(0) = 0; // pos x
    state(1) = 0; // pos y
    state(2) = 0; // M_PI / 3; // yaw 
    state(3) = 1; //1; // velocity

    input_vec(0) = 0; // M_PI / 4; // steering wheel
    input_vec(1) = 0; // 2; // acceleration

    // Gain loss vector.
    Eigen::MatrixXd C = bike.get_C();
    Eigen::VectorXd gain_loss(bike.Nu), gain_loss_fake(bike.Nu);
    gain_loss.setZero();

    // Example simulation with non-complete measurement.
    Eigen::VectorXd y(C.rows()), randNoiseVec(C.rows());
    double dt = 0.01, dt_mpc = 0.1, alpha = 0.5; /* for lpf on inputs in MPC*/;

    // Waypoint array
    Eigen::MatrixXd waypoints(8, 4);
    waypoints <<   0,     0,            0,    4,
                   0,   -20,      -M_PI_2,    4,
                  20,   -20,            0,    4,
                  20,   -40,      -M_PI_2,    4,
                 -40,   -40,        -M_PI,    4,
                 -40,   -20,    -3*M_PI_2,    4,
                 -20,   -20,      -2*M_PI,    4,
                 -20,     0,    -3*M_PI_2,    4;

    std::cout << waypoints << std::endl;
    int id_waypoint = 0;

    std::ofstream outfile;
    outfile.open("../output/aekf_sim.csv");
    // outfile << "time, state_actual1, state_actual2, state_actual3, <, state_estimated, fault_actual, fault_estimated" << std::endl;
    outfile << "time, ";

    for (size_t i = 0; i < bike.Nx; i++)
        outfile << "state_actual" << i << ", ";
    for (size_t i = 0; i < y.rows(); i++)
        outfile << "state_measured" << i << ", ";
    for (size_t i = 0; i < bike.Nx; i++)
        outfile << "state_estimated" << i << ", ";
    for (size_t i = 0; i < bike.Nx; i++)
        outfile << "state_reference" << i << ", ";
    for (size_t i = 0; i < bike.Nu; i++)
        outfile << "fault_actual" << i << ", ";
    for (size_t i = 0; i < bike.Nu; i++)
        outfile << "fault_estimated" << i << ", ";
    for (size_t i = 0; i < bike.Nu; i++)
        outfile << "input" << i << ", ";
    
    outfile << std::endl;

    std::chrono::high_resolution_clock::time_point start, end;

    for (double t = 0; t < 100; t += dt) 
    {
        // std::cout << t << std::endl;

        if (t > 50)
            gain_loss << 0.48, 0.52;
        // if (t > 50)
        //     gain_loss << 0.15, 0.2;
        // state = bike.compute_dynamics(state, input_vec, gain_loss, dt);

        // Take a measurement
        // y = C*X + RF*dt*randn(rank(C),1); // Matlab code
        // y = C * state + RRf * dt * randNoiseVec.Random();
        // std::cout << C.rows() << ", " << C.cols() << std::endl;
        // std::cout << state.rows() << ", " << state.cols() << std::endl;
        // std::cout << randNoiseVec.Random(C.rows()) << std::endl;
        y = C * state + dt * RRf * randNoiseVec.Random(C.rows());

        // std::cout << "y = \n" << y << std::endl;

        aekf.update(y, input_vec, dt);

        // Control law.
        // Set reference.
        // z_ref << 5 * cos(0.1 * t), 5 * sin(0.1 * t), 0, 1;
        // z_ref << -t, 2*t, 0, 1;
        // z_ref << t, 5 * sin(0.1 * t), 0, 1;
        // double yaw = 0.1 * t % M_PI_2 + M_PI/2;
        // double yaw = fmod(0.1 * t  + M_PI/2, 2 * M_PI);
        // double yaw = 0.05 * t  + M_PI_2;
        // z_ref << 20 * cos(0.05 * t), 20 * sin(0.05 * t), yaw, 1;
        // z_ref << t, t, 0, 1;
        // z_ref << 100, 100, M_PI, 0;
        // z_ref << 100, 100, M_PI, 0;

        // // Check if we are at the waypoint by calculating the error.
        // Eigen::VectorXd err_vec = z_ref(Eigen::seqN(0, 2)) - state(Eigen::seqN(0, 2), 0);
        
        // if (err_vec.norm() < 5 * *bike.get_L()) // err smaller than 1 m, break.
        //     if (++id_waypoint >= waypoints.rows())
        //         id_waypoint = 0;
            
        // z_ref << waypoints(id_waypoint, Eigen::all);
        // z_ref << waypoints(id_waypoint, Eigen::all).transpose();
        // z_ref << t, 5 * sin(0.1 * t), 0, 4;

        double x_ref = t,
               y_ref = 6 * cos(2 * M_PI * 0.05 * t) - 6,
               yaw_ref = -M_PI_2 * sin(2 * M_PI * 0.05 * t),
            //    yaw_ref = 0,
            //    vel_ref = 1;
               vel_ref = abs(0.5 * sin(2 * M_PI * 0.05 * t)) + 1;

        z_ref << x_ref,
                 y_ref,
                //  atan2(y_ref, x_ref),
                 yaw_ref,
                 vel_ref;

        // z_ref << 10, 10, 0, 2;
        // z_ref << 20 * cos(0.05 * t), 20 * sin(0.05 * t), 0, 1;
        // std::cout << t << " " << z_ref.transpose() << std::endl;
        mpc.set_Z_ref(z_ref);

        // double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();
        // double time_elapsed = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
        // // std::cout << "loop took " << time_elapsed << " miliseconds to execute\n";
        // std::cout << "loop took " << time_elapsed << " microseconds to execute\n";
        // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
        std::cout << t << " " << z_ref.transpose() << std::flush;

        start = std::chrono::high_resolution_clock::now();
        input_vec = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc, alpha);
        // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, gain_loss_fake, dt_mpc, alpha);
        end = std::chrono::high_resolution_clock::now();
        double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();

        std::cout << ", elapsed: " << time_elapsed << " ms" << std::endl;
        // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, dt_mpc);
        // input_vec = mpc.update(state, input_vec, dt_mpc);

        // std::cout << "[" << t << "],\t" <<
        //     state.transpose() << ",\t" <<
        //     aekf.get_state_estimate().transpose() << ",\t" <<
        //     gain_loss.transpose() << ",\t" <<
        //     aekf.get_fault_estimate().transpose() << ",\t" <<
        // std::endl;
        
        state = bike.compute_dynamics(state, input_vec, gain_loss, dt); // The "real thing".

        outfile << t << ", ";

        for (size_t i = 0; i < bike.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << state(i) << ", ";
        for (size_t i = 0; i < y.rows(); i++)
            outfile << std::fixed << std::setprecision(4) << y(i) << ", ";
        for (size_t i = 0; i < bike.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_state_estimate()(i) << ", ";
        for (size_t i = 0; i < bike.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << z_ref(i) << ", ";
        for (size_t i = 0; i < bike.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << gain_loss(i) << ", ";
        for (size_t i = 0; i < bike.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_fault_estimate()(i) << ", ";
        for (size_t i = 0; i < bike.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << input_vec(i) << ", ";
        
        outfile << std::endl;
    }

    outfile.close();

    return 0;
}