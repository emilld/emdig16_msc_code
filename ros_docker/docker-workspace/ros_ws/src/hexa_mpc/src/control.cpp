/**
 * @file control.cpp
 * @brief Implementation of an MPC as a ROS node, written with MAVROS version 0.19.x, PX4 Pro Flight
 * 
 * Stack and node tested in Gazebo SITL
 */

// ROS includes
#include <ros/ros.h>

#include <tf/tf.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <mavros_msgs/RCOut.h>
#include <mavros_msgs/OverrideRCIn.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>

#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/ActuatorControl.h>

// MPC and model includes
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <vector>

#include "../include/hexa_mpc/hexacopter.hpp"
#include "../include/hexa_mpc/mpc2.hpp"
#include "../include/hexa_mpc/aekf.hpp"

#include "nlopt.hpp"

// Defines
#define ROLL_RATE_CHANNEL   0
#define PITCH_RATE_CHANNEL  1
#define YAW_RATE_CHANNEL    2
#define THROTTLE_CHANNEL    3

// Global variables
ros::Publisher pub_state_estimate,
               pub_measurement_vec,
               pub_motor_fault_estimate,
               pub_motor_thrust,
               pub_actuator_control;               

ros::Subscriber sub_local_position,
                sub_local_rate,
                sub_rc_channels;

Eigen::VectorXd state_reference;

// Input vector
Eigen::VectorXd input_vec,
                measurement, // roll, roll_rate, pitch, pitch_rate, yaw, yaw_rate, x, y, z
                pwm_vector;

// parameters
double m,         // mass in kg
        L,        // arm length in m
        Jxx, // moment of inertia around 'x' axis in kg�m^2
        Jyy, // moment of inertia around 'y' axis in kg�m^2
        Jzz,    // moment of inertia around 'z' axis in kg�m^2
        g,        // gravity
        b,
        dt,
        dt_mpc,
        alpha, /* for lpf on inputs in MPC*/
        ss, // init covariance for aekf
        lambdaa; // speed of convergence of fault estimate

int N_control; // Event horizon.
int N_prediction;

std::vector<double> aekf_Q_diag, // System covariance matrix diagonal.
                    aekf_R_diag, // Measurement covariance matrix diagonal.
                    mpc_Q_diag, // State penalty.
                    mpc_R_diag; // Input penalty.

// Eigen::DiagonalMatrix<double, Eigen::Dynamic> aekf_Q,
//                                               aekf_R,
//                                               mpc_Q,
//                                               mpc_R;
Eigen::MatrixXd aekf_Q,
                aekf_R;
Eigen::VectorXd mpc_Q,
                mpc_R;

std::vector<double> mpc_lb, // lower bound
                    mpc_ub; // upper bound
Eigen::VectorXd mpc_lb_eig;
Eigen::VectorXd mpc_ub_eig;

Eigen::VectorXd RF_imu, RF_pos;

ros::Timer timerControlLoop;

std::chrono::high_resolution_clock::time_point start, end;

Eigen::VectorXd state_est, fault_est;

std::vector<double> poly_coeffs;

// Limits for virtual forces normalization.
std::vector<double> throttle_lim(2),
                    mx_lim(2),
                    my_lim(2),
                    mz_lim(2);

mavros_msgs::ActuatorControl newest_actuator_control;

bool mpc_control = false;
//////////////////////////////////////////////

mavros_msgs::State current_state;
void state_cb(const mavros_msgs::State::ConstPtr& msg){
    current_state = *msg;
}

void cb_local_pos(const geometry_msgs::PoseWithCovarianceStamped & msg)
{
    // ROS_INFO("%f", msg.pose.pose.position.x);
    measurement(6) = msg.pose.pose.position.x;
    measurement(7) = msg.pose.pose.position.y;
    measurement(8) = msg.pose.pose.position.z;

    RF_pos << msg.pose.covariance[0],
              msg.pose.covariance[7],
              msg.pose.covariance[14];
}

struct EulerAngles {
    double roll, pitch, yaw;
};

// EulerAngles ToEulerAngles(Eigen::Quaterniond q) {
//     EulerAngles angles;

//     // roll (x-axis rotation)
//     double sinr_cosp = 2 * (q.w() * q.x() + q.y() * q.z());
//     double cosr_cosp = 1 - 2 * (q.x() * q.x() + q.y() * q.y());
//     angles.roll = std::atan2(sinr_cosp, cosr_cosp);

//     // pitch (y-axis rotation)
//     double sinp = 2 * (q.w() * q.y() - q.z() * q.x());
//     if (std::abs(sinp) >= 1)
//         angles.pitch = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
//     else
//         angles.pitch = std::asin(sinp);

//     // yaw (z-axis rotation)
//     double siny_cosp = 2 * (q.w() * q.z() + q.x() * q.y());
//     double cosy_cosp = 1 - 2 * (q.y() * q.y() + q.z() * q.z());
//     angles.yaw = std::atan2(siny_cosp, cosy_cosp);

//     return angles;
// }

void cb_imu(const sensor_msgs::Imu & msg)
{
    // ROS_INFO("%f", msg.orientation.w);
    // Eigen::Quaterniond q(msg.orientation.w, msg.orientation.x, msg.orientation.y, msg.orientation.z);
    // Eigen::Vector3d euler_angles = q.normalized().toRotationMatrix().eulerAngles(0, 1, 2);
    // auto euler_angles = ToEulerAngles(q.normalized());

    tf::Quaternion q(
        msg.orientation.x,
        msg.orientation.y,
        msg.orientation.z,
        msg.orientation.w
    );
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    // Save angles
    measurement(0) = roll;
    measurement(2) = pitch;
    measurement(4) = yaw;

    // Save angular velocities
    measurement(1) = msg.angular_velocity.x;
    measurement(3) = msg.angular_velocity.y;
    measurement(5) = msg.angular_velocity.z;

    // Save covariances
    RF_imu << msg.orientation_covariance[0],
              msg.angular_velocity_covariance[0],
              msg.orientation_covariance[4],
              msg.angular_velocity_covariance[4],
              msg.orientation_covariance[8],
              msg.angular_velocity_covariance[8];
}

double num_to_thrust(double pwm_in)
{
    // clamping
    if (pwm_in < 1000)
    {
        pwm_in = 1000;
    }
    else if (pwm_in > 2000)
    {
        pwm_in = 2000;
    }
    
    double sum = 0;
    for (size_t i = 0; i < poly_coeffs.size(); i++)
    {
        // sum += pow(poly_coeffs.at(poly_coeffs.size() - i - 1), i) * pwm_in;
        sum += poly_coeffs.at(poly_coeffs.size() - i - 1) * pow(pwm_in, i);
    }
    return sum;
}

void cb_rc(const mavros_msgs::RCOut & msg)
{
    // ROS_INFO("%d", msg.channels[0]);
    // Eigen::VectorXd channels(6);
    // for (size_t i = 0; i < channels.rows(); i++)
    // {
    //     channels(i) = msg.channels[i];
    // }

    for (size_t i = 0; i < pwm_vector.rows(); i++)
    {
        // pwm_vector(i) = num_to_thrust(channels(i));
        pwm_vector(i) = num_to_thrust(msg.channels[i]);
    }

    ROS_INFO("pwm: %d %d %d %d %d %d", msg.channels[0], msg.channels[1], msg.channels[2], msg.channels[3], msg.channels[4], msg.channels[5]);
    ROS_INFO("[N]: %f %f %f %f %f %f", pwm_vector(0), pwm_vector(1), pwm_vector(2), pwm_vector(3), pwm_vector(4), pwm_vector(5));
}

Eigen::VectorXd clamp_pwm_vec(const Eigen::VectorXd & vec, Eigen::VectorXd & lb, Eigen::VectorXd & ub)
{
    Eigen::VectorXd out(vec.rows());
    for (size_t i = 0; i < vec.rows(); i++)
    {
        if (vec(i) <= lb(i))
            out(i) = lb(i);
        else if (vec(i) >= ub(i))
            out(i) = ub(i);
        else
            out(i) = vec(i);
    }
    return out;
}

void timer_loop(
    Plant & p, 
    AEKF & aekf, 
    ModelPredictiveControl2 & mpc)
{
    // ROS_INFO("time");
    // std::cout << "Nu: " << p.Nu << " Nx: " << p.Nx << std::endl;

    std::cout << measurement.transpose() << std::endl;
    // std::cout << input_vec.transpose() << std::endl;
    // std::cout << dt << std::endl;

    // update RF
    Eigen::VectorXd RF_combined(RF_imu.size() + RF_pos.size());
    RF_combined << RF_imu, RF_pos;
    aekf_R = RF_combined.asDiagonal();
    // aekf_R.diagonal() << RF_imu, RF_pos;
    aekf.set_R(aekf_R);
    // std::cout << aekf_R << std::endl;
    // std::cout << aekf.get_R() << std::endl;
    // aekf.set_R(RF_combined.asDiagonal());
    // aekf.update(measurement, input_vec, dt);
    aekf.update(measurement, pwm_vector, dt);

    // std::cout << state_reference.transpose() << std::endl;
    mpc.set_Z_ref(state_reference);

    // std::cout << aekf.get_state_estimate().transpose() << std::endl;
    // std::cout << input_vec.transpose() << std::endl;
    // std::cout << aekf.get_fault_estimate().transpose() << std::endl;
    // std::cout << dt_mpc << std::endl;

    start = std::chrono::high_resolution_clock::now();
    
    // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc, alpha);
    Eigen::VectorXd tmp = clamp_pwm_vec(pwm_vector, mpc_lb_eig, mpc_ub_eig);
    std::cout << tmp.transpose() << std::endl;
    input_vec = mpc.update(aekf.get_state_estimate(), tmp, aekf.get_fault_estimate(), dt_mpc, alpha);
    // Eigen::VectorXd fake_gain_loss(6);
    // fake_gain_loss.setZero();
    // input_vec = mpc.update(aekf.get_state_estimate(), tmp, fake_gain_loss, dt_mpc, alpha);

    // mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
    // auto tmp = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
    // std::cout << mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc) << std::endl;
    end = std::chrono::high_resolution_clock::now();
    double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();

    std::cout << input_vec.transpose() <<  ", elapsed: " << time_elapsed << " ms" << std::endl;
}

double linear_scale(double rmin, double rmax, double tmin, double tmax, double x)
{
    /* 
     * https://stats.stackexchange.com/questions/281162/scale-a-number-between-a-range/281164
     * rmin denote the minimum of the range of your measurement.
     * rmax denote the maximum of the range of your measurement.
     * tmin denote the minimum of the range of desired target scaling.
     * tmax denote the maximum of the range of desired target scaling.
     * x is the measurement
     */
    double out = ((x - rmin) / (rmax - rmin)) * (tmax - tmin) + tmin;
    if (out < tmin)
        out = tmin;
    else if (out > tmax)
        out = tmax;
    return out;
}

void publish_motor_thrust()
{
    std_msgs::Float64MultiArray msg;
    msg.data.resize(input_vec.rows()); 
    // Eigen::Map<Eigen::VectorXd>(msg.data, input_vec.rows()) = input_vec;
    for (size_t i = 0; i < input_vec.rows(); i++)
        msg.data[i] = input_vec(i);
    pub_motor_thrust.publish(msg);
}

void calc_actuator_control(Eigen::MatrixXd mmm)
{
    // mavros_msgs::ActuatorControl msg;
    // newest_actuator_control.header.stamp = ros::Time::now();
    newest_actuator_control.group_mix = 0;

    Eigen::VectorXd virtual_forces = mmm * input_vec;
    // virtual_forces = 0.5 * virtual_forces;
    // newest_actuator_control.controls[THROTTLE_CHANNEL] =   linear_scale(throttle_lim[0], throttle_lim[1], 0, 1, virtual_forces[0]);
    // newest_actuator_control.controls[ROLL_RATE_CHANNEL] =  linear_scale(mx_lim[0], mx_lim[1], -1, 1, virtual_forces[1]);
    // newest_actuator_control.controls[PITCH_RATE_CHANNEL] = linear_scale(my_lim[0], my_lim[1], -1, 1, virtual_forces[2]);
    // newest_actuator_control.controls[YAW_RATE_CHANNEL] =   linear_scale(mz_lim[0], mz_lim[1], -1, 1, virtual_forces[3]);
    
    // newest_actuator_control.controls[THROTTLE_CHANNEL] =   0.5 * linear_scale(throttle_lim[0], throttle_lim[1], 0, 1, virtual_forces[0]);
    // newest_actuator_control.controls[ROLL_RATE_CHANNEL] =  0.1 * linear_scale(mx_lim[0], mx_lim[1], -1, 1, virtual_forces[1]);
    // newest_actuator_control.controls[PITCH_RATE_CHANNEL] = 0.1 * linear_scale(my_lim[0], my_lim[1], -1, 1, virtual_forces[2]);
    // newest_actuator_control.controls[YAW_RATE_CHANNEL] =   0.1 * linear_scale(mz_lim[0], mz_lim[1], -1, 1, virtual_forces[3]);

    newest_actuator_control.controls[THROTTLE_CHANNEL] =   linear_scale(throttle_lim[0], throttle_lim[1], 0, 1, virtual_forces[0]);
    newest_actuator_control.controls[ROLL_RATE_CHANNEL] =  linear_scale(mx_lim[0], mx_lim[1], -1, 1, virtual_forces[1]);
    newest_actuator_control.controls[PITCH_RATE_CHANNEL] = linear_scale(my_lim[0], my_lim[1], -1, 1, virtual_forces[2]);
    newest_actuator_control.controls[YAW_RATE_CHANNEL] =   linear_scale(mz_lim[0], mz_lim[1], -1, 1, virtual_forces[3]);

    // newest_actuator_control.controls[THROTTLE_CHANNEL] =  virtual_forces[0];
    // newest_actuator_control.controls[ROLL_RATE_CHANNEL] =  virtual_forces[1];
    // newest_actuator_control.controls[PITCH_RATE_CHANNEL] = virtual_forces[2];
    // newest_actuator_control.controls[YAW_RATE_CHANNEL] =   virtual_forces[3];
}

void timer_loop_actuator_control(const ros::TimerEvent&)
{
    if (mpc_control)
    {
        newest_actuator_control.header.stamp = ros::Time::now();
        pub_actuator_control.publish(newest_actuator_control);
        ROS_INFO("In timer!");
    }
}

void publish_motor_fault_estimate(Eigen::VectorXd data)
{
    std_msgs::Float64MultiArray msg;
    msg.data.resize(data.rows());

    for (size_t i = 0; i < data.rows(); i++)
        msg.data[i] = data(i);

    pub_motor_fault_estimate.publish(msg);
}

void publish_state_estimate(Eigen::VectorXd data)
{
    std_msgs::Float64MultiArray msg;
    msg.data.resize(data.rows());

    for (size_t i = 0; i < data.rows(); i++)
        msg.data[i] = data(i);

    pub_state_estimate.publish(msg);
}

void publish_measurement_vec(Eigen::VectorXd data)
{
    std_msgs::Float64MultiArray msg;
    msg.data.resize(data.rows());

    for (size_t i = 0; i < data.rows(); i++)
        msg.data[i] = data(i);

    pub_measurement_vec.publish(msg);
}

int main(int argc, char **argv) 
{
    ros::init(argc, argv, "mpc");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");

    // subscribers
    sub_local_position = pnh.subscribe("local_position", 10, cb_local_pos);
    sub_local_rate = pnh.subscribe("imu", 10, cb_imu);
    sub_rc_channels = pnh.subscribe("rcout", 10, cb_rc);

    ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>
            ("mavros/state", 10, state_cb);

    // publishers
    pub_state_estimate = pnh.advertise<std_msgs::Float64MultiArray>("state_estimate", 1);
    pub_measurement_vec = pnh.advertise<std_msgs::Float64MultiArray>("measurement_vec", 1);
    pub_motor_fault_estimate = pnh.advertise<std_msgs::Float64MultiArray>("motor_fault_estimate", 1);
    pub_motor_thrust = pnh.advertise<std_msgs::Float64MultiArray>("motor_thrust", 100);
    // pub_rcoverride = pnh.advertise<mavros_msgs::OverrideRCIn>("rc_override", 1);
    pub_actuator_control = nh.advertise<mavros_msgs::ActuatorControl>("/mavros/actuator_control", 100);
    ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>
            ("mavros/setpoint_position/local", 10);

    // services
    ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>
            ("mavros/cmd/arming");
    ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
            ("mavros/set_mode");


    // std_msgs::Float64MultiArray msg;
    // while (1)
    // {
    //     pub_state_estimate.publish(msg);
    //     pub_motor_fault_estimate.publish(msg);
    //     pub_motor_thrust.publish(msg);
    // }

    pnh.param("mass", m, 1.5);
    pnh.param("arm_length", L, 0.37);
    pnh.param("Jxx", Jxx, 0.0762625);
    pnh.param("Jyy", Jyy, 0.0762625);
    pnh.param("Jzz", Jzz, 0.1369);
    pnh.param("g", g, 9.81);
    pnh.param("b", b, 0.01);
    pnh.param("dt", dt, 0.01);
    pnh.param("dt_mpc", dt_mpc, 0.01);
    pnh.param("N_control", N_control, 2);
    pnh.param("N_prediction", N_prediction, 50);
    pnh.param("alpha", alpha, 1.0);
    pnh.param("ss", ss, 0.001);
    pnh.param("lambdaa", lambdaa, 0.995);

    pnh.param("aekf_Q_diag", aekf_Q_diag, std::vector<double>{0.1, 0.01, 0.1, 0.01, 0.1, 0.01, 0.5, 0.1, 0.5, 0.1, 0.1, 0.01});
    pnh.param("aekf_R_diag", aekf_R_diag, std::vector<double>{0.5, 0.1, 0.5, 0.1, 0.2, 0.1, 1, 1, 0.3});
    pnh.param("mpc_Q_diag", mpc_Q_diag, std::vector<double>{100, 100, 100, 100, 200, 100, 200, 100, 200, 100, 200, 100});
    pnh.param("mpc_R_diag", mpc_R_diag, std::vector<double>{0.1, 0.1, 0.1, 0.1, 0.1, 0.1});
    pnh.param("mpc_lb", mpc_lb, std::vector<double>{0., 0., 0., 0., 0., 0.});
    pnh.param("mpc_ub", mpc_ub, std::vector<double>{8., 8., 8., 8., 8., 8.});

    pnh.param("throttle_lim", throttle_lim, std::vector<double>{0.0, 48.0});
    pnh.param("mx_lim", mx_lim, std::vector<double>{-5.920000000000001, 5.920000000000001});
    pnh.param("my_lim", my_lim, std::vector<double>{-5.126870390403877, 5.126870390403877});
    pnh.param("mz_lim", mz_lim, std::vector<double>{-0.24, 0.24});
    
    pnh.param("poly_coeffs", poly_coeffs, std::vector<double>{-9.13948477e-09, 4.24788753e-05, -5.41307079e-02, 2.09764844e+01});

    // init_hexa();
    Hexacopter hexa(m, L, Jxx, Jyy, Jzz, g, b, dt);
    input_vec = Eigen::VectorXd(hexa.Nu);
    input_vec.setZero();

    Eigen::MatrixXd C = hexa.get_C();
    measurement = Eigen::VectorXd(C.rows());
    measurement.setZero();
    // measurement << 0, 0, 0, 0, 0, 0, 1, 2, 3;
    
    // init_aekf();
    RF_imu = Eigen::VectorXd(6);
    RF_pos = Eigen::VectorXd(3);
    RF_imu << 1, 1, 1, 1, 1, 1;
    RF_pos << 1, 1, 1;

    AEKF aekf(hexa, ss, lambdaa);
    std::cout << "Nu: " << hexa.Nu << " Nx: " << hexa.Nx << std::endl;

    // Eigen::MatrixXd tmp = Eigen::Map<Eigen::VectorXd>(aekf_Q_diag.data(), aekf_Q_diag.size()).asDiagonal();
    // std::cout << tmp << std::endl;
    aekf_Q = Eigen::Map<Eigen::VectorXd>(aekf_Q_diag.data(), aekf_Q_diag.size()).asDiagonal();
    // std::cout << aekf_Q.diagonal().transpose() << std::endl;
    aekf_R = Eigen::Map<Eigen::VectorXd>(aekf_R_diag.data(), aekf_R_diag.size()).asDiagonal();

    aekf.set_Q(aekf_Q);
    aekf.set_R(aekf_R);

    std::cout << "Q = \n" << aekf.get_Q() << std::endl;
    std::cout << "R = \n" << aekf.get_R() << std::endl;

    state_reference = Eigen::VectorXd(hexa.Nx); // reference for x, y, z
    state_reference.setZero();
    // state_reference
    state_reference(4) = 0;
    state_reference(6) = 0;
    state_reference(8) = 0;
    state_reference(10) = 10;
    // init_mpc();

    ModelPredictiveControl2 mpc(hexa, N_prediction, N_control);

    mpc_Q = Eigen::Map<Eigen::VectorXd>(mpc_Q_diag.data(), mpc_Q_diag.size());
    mpc_R = Eigen::Map<Eigen::VectorXd>(mpc_R_diag.data(), mpc_R_diag.size());

    std::cout << mpc_Q.transpose() << std::endl;
    std::cout << mpc_R.transpose() << std::endl;

    mpc.create_G_matrix(mpc_Q, mpc_R);
    
    // std::cout << "after" << std::endl;
    // for (auto x : mpc_lb)
    //     std::cout << x << ",";
    // std::cout << std::endl;

    mpc_lb_eig = Eigen::Map<Eigen::VectorXd>(mpc_lb.data(), mpc_lb.size());
    mpc_ub_eig = Eigen::Map<Eigen::VectorXd>(mpc_ub.data(), mpc_ub.size());

    mpc.set_bounds(mpc_lb_eig, mpc_ub_eig);

    mpc.print_bounds();

    mpc.set_Z_ref(state_reference);

    mpc.use_state_constraint();

    state_est = Eigen::VectorXd(hexa.Nx);
    fault_est = Eigen::VectorXd(hexa.Nu);
    state_est.setZero();
    fault_est.setZero();

    pwm_vector = Eigen::VectorXd(6);
    
    for (int i = 0; i < 6; i++)
    {
        pwm_vector(i) = mpc_lb_eig(i);
        input_vec(i) = mpc_lb_eig(i);
    }

    // timer_loop(hexa, aekf, mpc, measurement, input_vec);
    // ROS_INFO("Init done.");

    // // Create timer callback
    // ROS_INFO("dt: %f", dt);
    // timerControlLoop =
    //     nh.createTimer(ros::Duration(dt),
    //                     std::bind(timer_loop, hexa, aekf, mpc, measurement, input_vec));
    ros::Rate rate(1/dt);
    // ros::Rate actuator_control_rate(500);
    ros::Rate actuator_control_rate(1/dt);

    // ros::Duration(10.0).sleep();
        // wait for FCU connection
    while(ros::ok() && !current_state.connected){
        ROS_INFO("Waiting for connection");
        ros::spinOnce();
        rate.sleep();
    }

    // // mavros_msgs::ActuatorControl msg;
    newest_actuator_control.group_mix = 0;
    
    for (size_t i = 0; i < 8; i++)
        newest_actuator_control.controls[i] = 0;
    newest_actuator_control.controls[THROTTLE_CHANNEL] = 0.1;

    ros::Timer actuator_control_timer = pnh.createTimer(actuator_control_rate.expectedCycleTime(), timer_loop_actuator_control);

    geometry_msgs::PoseStamped pose;
    pose.pose.position.x = 0;
    pose.pose.position.y = 0;
    pose.pose.position.z = 0;

    //send a few setpoints before starting
    for(int i = 100; ros::ok() && i > 0; --i){
        // pub_actuator_control.publish(msg);
        // local_pos_pub.publish(pose);
        ros::spinOnce();
        // rate.sleep();
        actuator_control_rate.sleep();
    }

   // OFFBOARD mode
    mavros_msgs::SetMode offb_set_mode;
    offb_set_mode.request.custom_mode = "OFFBOARD";

    // AUTO.LAND mode
    mavros_msgs::SetMode land_set_mode;
    land_set_mode.request.custom_mode = "AUTO.LAND";

    mavros_msgs::CommandBool arm_cmd;
    arm_cmd.request.value = true;

    bool armed = false;
    bool reach_first_point = true;

    ros::Time last_request = ros::Time::now();
    ros::Time ts_start = ros::Time::now();

    while(ros::ok())
    {   
       if( current_state.mode != "OFFBOARD" &&
            (ros::Time::now() - last_request > ros::Duration(5.0))){
            ROS_INFO("Trying to enable Offboard...");
            if( set_mode_client.call(offb_set_mode) &&
                offb_set_mode.response.mode_sent){
                ROS_INFO(">> Offboard enabled");
            }
            last_request = ros::Time::now();
        } else {
            if( !current_state.armed &&
                (ros::Time::now() - last_request > ros::Duration(5.0))){
                ROS_INFO("Trying to arm...");
                if( arming_client.call(arm_cmd) &&
                    arm_cmd.response.success)
                {
                    ROS_INFO(">> Vehicle armed");
                    armed = true;
                }
                else
                {
                    armed = false;
                }
                last_request = ros::Time::now();
            }
        }

        mpc_control = true;

        // if ((abs(measurement[8] - pose.pose.position.z) < 0.2) && reach_first_point)
        // {
        //     mpc_control = true;
        //     reach_first_point = false;
        // }
        // else
        //     local_pos_pub.publish(pose);
        // // mpc_control = true;
        // local_pos_pub.publish(pose);

        // if( current_state.mode != "LAND" &&
        //     (ros::Time::now() - ts_start > ros::Duration(30.0))){
        //     ROS_INFO("Trying to enable AUTO.LAND...");
        //     if( set_mode_client.call(land_set_mode) &&
        //         land_set_mode.response.mode_sent){
        //         ROS_INFO("AUTO.LAND enabled");
        //         break;
        //     }
        // }

        ROS_INFO("Arm status %d, MPC control %d", armed, mpc_control);
        
        // calc_actuator_control(hexa.get_B());

        if (armed && mpc_control)
        // if (armed)
        {
            timer_loop(hexa, aekf, mpc);

            // output input_vec on motor thrust topic
            publish_motor_thrust();
            calc_actuator_control(hexa.get_B());
            publish_motor_fault_estimate(aekf.get_fault_estimate());
            publish_state_estimate(aekf.get_state_estimate());
            publish_measurement_vec(measurement);
        }
        // else
        // {
        //     mavros_msgs::OverrideRCIn msg;
        //     msg.channels[THROTTLE_CHANNEL] =   1000;
        //     msg.channels[ROLL_RATE_CHANNEL] =  1500;
        //     msg.channels[PITCH_RATE_CHANNEL] = 1500;
        //     msg.channels[YAW_RATE_CHANNEL] =   1500;
        //     msg.channels[4] = 1500;
        //     msg.channels[5] = 1500;
        //     msg.channels[6] = 1500;
        //     msg.channels[7] = 1500;
        //     pub_rcoverride.publish(msg);
        // }

        ros::spinOnce();
        rate.sleep();
    }

    ROS_INFO("Done");
    return 0;
}
