#include "../include/hexa_mpc/bike.hpp"
#include <cmath>
#include <iostream>

Bike::Bike(double anL, int Nx, int Nu)
    : Plant(Nx, Nu)
{
    L = anL;
    Linv = 1/L;
    lb = Eigen::VectorXd(Nx);
    ub = Eigen::VectorXd(Nx);

    for(size_t i = 0; i < Nx; i++)
    {
        lb(i) = -HUGE_VAL;
        ub(i) =  HUGE_VAL;
    }
}

Eigen::VectorXd Bike::compute_dynamics(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const double & dt) 
{
    // double pos_x = state(0, 0), pos_y = state(1, 0), yaw = state(2, 0), vel = state(3, 0);
    // double steering_angle = input_vec(0, 0), acc = input_vec(1, 0);
    Eigen::VectorXd out(4);

    out(0, 0) = state(0, 0) + state(3, 0) * cos(state(2, 0)) * dt;
    out(1, 0) = state(1, 0) + state(3, 0) * sin(state(2, 0)) * dt;
    out(2, 0) = state(2, 0) + Linv * state(3, 0) * input_vec(0, 0) * dt;
    out(3, 0) = state(3, 0) + input_vec(1, 0) * dt;

    for (size_t i = 0; i < out.rows(); i++) 
    {
        if (out(i, 0) > ub(i))
        {
            out(i, 0) = ub(i);
        }
        else if (out(i, 0) < lb(i)) 
        {
            out(i, 0) = lb(i);
        }
    }

    return out;
}

Eigen::VectorXd Bike::compute_dynamics(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const Eigen::VectorXd & gain_loss,
    const double & dt)
{
    Eigen::MatrixXd I(gain_loss.rows(), gain_loss.rows());
    I.setIdentity();

    Eigen::MatrixXd gain_loss_diag(gain_loss.rows(), gain_loss.rows());
    gain_loss_diag = gain_loss.asDiagonal();

    Eigen::MatrixXd input_vec_altered = (I - gain_loss_diag) * input_vec;
    // std::cout << input_vec_altered.transpose() << std::endl;
    return compute_dynamics(state, input_vec_altered, dt);
}

Eigen::MatrixXd Bike::get_Fxhat(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const double & dt) 
{
    // double pos_x = state(0, 0), pos_y = state(1, 0), yaw = state(2, 0), vel = state(3, 0);
    // double steering_angle = input_vec(0, 0), acc = input_vec(1, 0);
    Eigen::MatrixXd out = Eigen::MatrixXd::Identity(4, 4);

    out(0, 2) = -state(3, 0) * sin(state(2, 0)) * dt;
    out(0, 3) =                cos(state(2, 0)) * dt;
    out(1, 2) =  state(3, 0) * cos(state(2, 0)) * dt;
    out(1, 3) =                sin(state(2, 0)) * dt;
    out(2, 3) =          dt * input_vec(0, 0) * Linv;

    return out;
}

Eigen::MatrixXd Bike::get_Fu(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const double & dt) 
{
    // double pos_x = state(0, 0), pos_y = state(1, 0), yaw = state(2, 0), vel = state(3, 0);
    // double steering_angle = input_vec(0, 0), acc = input_vec(1, 0);

    Eigen::MatrixXd out(4, 2);

    out(2, 0) = dt * state(3, 0) * Linv;    
    out(3, 1) = dt;

    return out;
}

Eigen::MatrixXd Bike::get_C() 
{
    Eigen::MatrixXd out(3, 4);
    
    out(0, 0) = 1; // pos x
    out(1, 1) = 1; // pos y
    out(2, 3) = 1; // velocity

    return out;
}

void Bike::set_sim_state_bound(
    const Eigen::VectorXd & alb,
    const Eigen::VectorXd & aub) 
{
    lb = alb;
    ub = aub;    
}

Bike::~Bike() 
{
    
}

