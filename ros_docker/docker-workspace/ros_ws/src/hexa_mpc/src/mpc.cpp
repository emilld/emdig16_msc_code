#include "../include/hexa_mpc/mpc.hpp"
#include <nlopt.hpp>
#include <chrono>

ModelPredictiveControl::ModelPredictiveControl(Plant & p, int anN)
{
    // std::cout << "what" << std::endl
    plant = &p;
    N = anN;

    // Optimizer init.
    // opt = nlopt::opt(nlopt::LD_SLSQP, N * (plant->Nx + plant->Nu));
    opt = nlopt::opt(nlopt::LD_AUGLAG, N * (plant->Nx + plant->Nu));
    opt.set_min_objective(objective_function, &obj_data);
    obj_data.G = Eigen::MatrixXd((plant->Nx + plant->Nu) * N, (plant->Nx + plant->Nu) * N);
    obj_data.z_ref = Eigen::VectorXd((plant->Nx + plant->Nu) * N);

    // std::vector<double> tol_constraint(plant->Nx * N, 1e-4);
    std::vector<double> tol_constraint(plant->Nx * N, 1e-1);
    opt.add_equality_mconstraint(model_constraint, &constraint_data, tol_constraint);

    opt.set_xtol_abs(1e-4);
    opt.set_ftol_abs(1e-6);
    opt.set_maxeval(1e4);

    input_vec_filtered = Eigen::VectorXd(plant->Nu);
    input_vec_filtered.setZero();
}

Eigen::VectorXd ModelPredictiveControl::update(
    const Eigen::MatrixXd & state,
    const Eigen::MatrixXd & input_vec,
    const double & dt)
{
    get_Aeq_beq(constraint_data.Aeq, constraint_data.beq, state, input_vec, dt);
    constraint_data.Aeq_sparse = constraint_data.Aeq.sparseView();
    constraint_data.beq_sparse = constraint_data.beq.sparseView();

    std::vector<double> z_init(N * (plant->Nx + plant->Nu));
    // Insert state as the initial guess for the optimizer.
    for (size_t i = 0; i < z_init.size(); i++)
    {
        if (i < plant->Nx)
            z_init.at(i) = state(i, 0);
        else if (i >= plant->Nx && i < (plant->Nx + plant->Nu))
            z_init.at(i) = input_vec(i - plant->Nx);
        else
            z_init.at(i) = 0;
    }

    double minf;
    // std::chrono::high_resolution_clock::time_point start, end;

    try
    {
        // std::cout << "Before optimizer" << std::endl;
        // start = std::chrono::high_resolution_clock::now();
        opt.optimize(z_init, minf);
        // end = std::chrono::high_resolution_clock::now();

        // double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();
        // double time_elapsed = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
        // // std::cout << "loop took " << time_elapsed << " miliseconds to execute\n";
        // std::cout << "loop took " << time_elapsed << " microseconds to execute\n";

        // std::cout << "after" << std::endl;

        // Eigen::VectorXd tmp = Eigen::Map<Eigen::VectorXd>(z_init.data(), z_init.size());
        // std::cout << "result = \n" << tmp << std::endl;
        // std::cout << "minimum value = \n" << minf << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "nlopt failed: " << e.what() << std::endl;
    }

    Eigen::VectorXd out(plant->Nu);

    for (size_t i = 0; i < plant->Nu; i++)
    {
        out(i) = z_init[N * plant->Nx + i];
    }

    return out;
}

Eigen::VectorXd ModelPredictiveControl::update(
    const Eigen::MatrixXd & state,
    const Eigen::MatrixXd & input_vec,
    const Eigen::MatrixXd & gain_loss,
    const double & dt)
{
    // std::cout << "gain_loss = \n" << gain_loss << std::endl;

    get_Aeq_beq(constraint_data.Aeq, constraint_data.beq, state, input_vec, gain_loss, dt);
    constraint_data.Aeq_sparse = constraint_data.Aeq.sparseView();
    constraint_data.beq_sparse = constraint_data.beq.sparseView();

    std::vector<double> z_init(N * (plant->Nx + plant->Nu));
    // Insert state as the initial guess for the optimizer.
    for (size_t i = 0; i < z_init.size(); i++)
    {
        if (i < plant->Nx)
            z_init.at(i) = state(i, 0);
        else if (i >= plant->Nx && i < (plant->Nx + plant->Nu))
            z_init.at(i) = input_vec(i - plant->Nx);
        else
            z_init.at(i) = 0;
    }

    double minf;
    // std::chrono::high_resolution_clock::time_point start, end;

    try
    {
        // std::cout << "Before optimizer" << std::endl;
        // start = std::chrono::high_resolution_clock::now();
        opt.optimize(z_init, minf);
        // end = std::chrono::high_resolution_clock::now();

        // double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();
        // double time_elapsed = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
        // // std::cout << "loop took " << time_elapsed << " miliseconds to execute\n";
        // std::cout << "loop took " << time_elapsed << " microseconds to execute\n";

        // std::cout << "after" << std::endl;

        // Eigen::VectorXd tmp = Eigen::Map<Eigen::VectorXd>(z_init.data(), z_init.size());
        // std::cout << "result = \n" << tmp << std::endl;
        // std::cout << "minimum value = \n" << minf << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "nlopt failed: " << e.what() << std::endl;
    }

    Eigen::VectorXd out(plant->Nu);

    for (size_t i = 0; i < plant->Nu; i++)
    {
        out(i) = z_init[N * plant->Nx + i];
    }

    return out;
}

Eigen::VectorXd ModelPredictiveControl::update(
    const Eigen::MatrixXd & state,
    const Eigen::MatrixXd & input_vec,
    const Eigen::MatrixXd & gain_loss,
    const double & dt,
    const double & alpha)
{
    Eigen::VectorXd raw_input_vec = update(state, input_vec, gain_loss, dt);

    // Append to our filtered data.   
    input_vec_filtered = alpha * raw_input_vec + (1 - alpha) * input_vec_filtered;
    return input_vec_filtered;
}

void ModelPredictiveControl::create_G_matrix(
    Eigen::VectorXd & q, // penalties on states, length = Nx
    Eigen::VectorXd & r) // penalties on inputs, length = Nu
{
    assert(q.rows() == plant->Nx);
    assert(r.rows() == plant->Nu);

    // Eigen::MatrixXd G((plant->Nx + plant->Nu) * N, (plant->Nx + plant->Nu) * N);
    obj_data.G = Eigen::MatrixXd((plant->Nx + plant->Nu) * N, (plant->Nx + plant->Nu) * N);
    for (size_t i = 0; i < N; i++)
    {
        obj_data.G(Eigen::seqN(i * plant->Nx, plant->Nx),
                   Eigen::seqN(i * plant->Nx, plant->Nx)) = q.asDiagonal();

        obj_data.G(Eigen::seqN(plant->Nx * N + i * plant->Nu, plant->Nu),
                   Eigen::seqN(plant->Nx * N + i * plant->Nu, plant->Nu)) = r.asDiagonal();
    }
    obj_data.G_sparse = obj_data.G.sparseView();
}

void ModelPredictiveControl::set_Z_ref(const Eigen::VectorXd & z_ref)
{
    // Set reference for the states
    assert(z_ref.rows() == plant->Nx);

    for (size_t i = 0; i < N; i++) {
        // std::cout << i << std::endl;
        for (size_t j = 0; j < plant->Nx; j++)
        {
            obj_data.z_ref(plant->Nx * i + j)   = z_ref(j);
        }

        for (size_t k = 0; k < plant->Nu; k++)
        {
            obj_data.z_ref(N * plant->Nx + k * plant->Nu) = 0;
        }
    }
    // std::cout << obj_data.z_ref << std::endl;
    obj_data.z_ref_sparse = obj_data.z_ref.sparseView();
}

void ModelPredictiveControl::set_bounds(
    const Eigen::VectorXd & lb,
    const Eigen::VectorXd & ub)
{
    assert(lb.rows() == ub.rows());
    assert(lb.rows() == plant->Nu + plant->Nx);

    // Make lower and upper bounds
    std::vector<double> lb_long((plant->Nx + plant->Nu) * N), ub_long((plant->Nx + plant->Nu) * N);

    for (size_t i = 0; i < N; i++)
    { // State bounds
        for (size_t nnx = 0; nnx < plant->Nx; nnx++)
        {
            lb_long.at(i * plant->Nx + nnx)     = lb(nnx); // lower bound, states
            ub_long.at(i * plant->Nx + nnx)     = ub(nnx); // upper bound, states
        }
        for (size_t nnu = 0; nnu < plant->Nu; nnu++)
        {
            lb_long.at(N * plant->Nx + i * plant->Nu + nnu) = lb(plant->Nx + nnu); // lower bound, input
            ub_long.at(N * plant->Nx + i * plant->Nu + nnu) = ub(plant->Nx + nnu); // upper bound, input
        }
    }

    // for (auto x : lb_long)
    //     std::cout << x << " ";
    // std::cout << std::endl;

    // std::cout << "before setting bounds" << std::endl;
    opt.set_lower_bounds(lb_long);
    opt.set_upper_bounds(ub_long);
    // std::cout << "after setting bounds" << std::endl;
}

void ModelPredictiveControl::print_bounds()
{
    std::vector<double> lb = opt.get_lower_bounds();
    for (auto x : lb)
        std::cout << x << " ";
    std::cout << std::endl;

    std::vector<double> ub = opt.get_upper_bounds();
    for (auto x : ub)
        std::cout << x << " ";
    std::cout << std::endl;
}

void ModelPredictiveControl::get_Aeq_beq(
    Eigen::MatrixXd & Aeq, //////////////////////////
    Eigen::MatrixXd & beq, //////////////////////////
    const Eigen::MatrixXd & state,
    const Eigen::MatrixXd & input_vec,
    const double & dt)
{
    Eigen::MatrixXd A = plant->get_Fxhat(state, input_vec, dt);
    Eigen::MatrixXd B = plant->get_Fu(state, input_vec, dt);

    // Create the following matrix:
    // Aeq1 =
    //   I 0 ...   0
    //  -A I ...   0
    //   . ...  I  0
    //   0 ... -A  I
    // It has size N*A.shape[0] x N*A.shape[0]

    Aeq = Eigen::MatrixXd::Identity(N * A.rows(), N * A.rows() + N * B.cols());

    for (size_t i = 0; i < N; i++)
    { // Go through all "blocks" of the matrix.
        // Aeq1(Eigen::seqN(i + N, N), Eigen::seqN(i + N, N)) = A;
        if (i > 0)
            Aeq(Eigen::seqN(i * A.rows(), A.rows()),
                Eigen::seqN((i - 1) * A.rows(), A.rows())) = -A;
        // Eigen::MatrixXd tmp = Aeq1(Eigen::seqN(i * A.rows(), A.rows()), Eigen::seqN((i - 1) * A.rows(), A.rows()));
        // std::cout << "tmp \n" << tmp << std::endl;
        Aeq(Eigen::seqN(i * B.rows(), B.rows()),
            Eigen::seqN(N * A.rows() + i * B.cols(), B.cols())) = -B;
    }

    beq = Eigen::MatrixXd::Zero(Aeq.rows(), 1);
    beq(Eigen::seqN(0, A.rows()), Eigen::all) = A * state;
}

void ModelPredictiveControl::get_Aeq_beq(
    Eigen::MatrixXd & Aeq, //////////////////////////
    Eigen::MatrixXd & beq, //////////////////////////
    const Eigen::MatrixXd & state,
    const Eigen::MatrixXd & input_vec,
    const Eigen::MatrixXd & gain_loss,
    const double & dt)
{
    Eigen::MatrixXd A = plant->get_Fxhat(state, input_vec, gain_loss, dt);
    Eigen::MatrixXd B = plant->get_Fu(state, input_vec, gain_loss, dt);

    // Create the following matrix:
    // Aeq1 =
    //   I 0 ...   0
    //  -A I ...   0
    //   . ...  I  0
    //   0 ... -A  I
    // It has size N*A.shape[0] x N*A.shape[0]

    Aeq = Eigen::MatrixXd::Identity(N * A.rows(), N * A.rows() + N * B.cols());
    // Eigen::MatrixXd I(input_vec.rows(), input_vec.rows());
    // I.setIdentity();

    // // std::cout << -B * (I - I * gain_loss) << std::endl;
    // Eigen::MatrixXd gain_loss_diag = gain_loss.asDiagonal();
    // B = B * (I - gain_loss_diag);

    for (size_t i = 0; i < N; i++)
    { // Go through all "blocks" of the matrix.
        // Aeq1(Eigen::seqN(i + N, N), Eigen::seqN(i + N, N)) = A;
        if (i > 0)
            Aeq(Eigen::seqN(i * A.rows(), A.rows()),
                Eigen::seqN((i - 1) * A.rows(), A.rows())) = -A;
        // Eigen::MatrixXd tmp = Aeq1(Eigen::seqN(i * A.rows(), A.rows()), Eigen::seqN((i - 1) * A.rows(), A.rows()));
        // std::cout << "tmp \n" << tmp << std::endl;
        Aeq(Eigen::seqN(i * B.rows(), B.rows()),
            Eigen::seqN(N * A.rows() + i * B.cols(), B.cols())) = -B;
    }

    beq = Eigen::MatrixXd::Zero(Aeq.rows(), 1);
    beq(Eigen::seqN(0, A.rows()), Eigen::all) = A * state;
}

double ModelPredictiveControl::objective_function(
    const std::vector<double> & x,
    std::vector<double> & grad,
    void * my_func_data)
{
    /*
     * Quadratic objective function.
     * f(z) = 0.5 * z.T * G * z
     */
    Eigen::VectorXd z = Eigen::VectorXd::Map(x.data(), x.size());
    objective_params * params = reinterpret_cast<objective_params*>(my_func_data);
    // Eigen::MatrixXd G = params->G;
    // Eigen::VectorXd z_ref = params->z_ref;

    Eigen::VectorXd zzz = z - params->z_ref_sparse;
    Eigen::VectorXd jac = zzz.transpose() * params->G_sparse;

    if (!grad.empty())
    {
        // for (size_t i = 0; i < x.size(); i++)
        // {
        //     grad.at(i) = jac(i, 0);
        // }
        Eigen::Map<Eigen::VectorXd>(grad.data(), jac.rows(), jac.cols()) = jac;
    }

    // return 0.5 * zzz.transpose() * G * zzz;
    double out = 0.5 * jac.dot(zzz);
    // double out = zzz.transpose() * G * zzz;
    return out;
}

void ModelPredictiveControl::model_constraint(
    unsigned m, // size of result
    double * result, // Where output values should be stored.
    unsigned n, // Size of x
    const double * x, // The parameters to be optimized
    double * grad, // Output array for gradient.
    void * f_data) // Extra data, in this case, our linear model Aeq and beq.
{
    //n is the length of x, m is the length of result
    // The equality constraint is on the form h(x) = 0.
    // Ours look like this: Aeq * z = beq  =>  h(x) = Aeq * z - beq

    // Pass weight matrix G and z_ref in my_func_data
    Eigen::VectorXd z = Eigen::VectorXd::Map(&x[0], n);
    // objective_params & params = (objective_params &) my_func_data;
    model_constraint_data * params = reinterpret_cast<model_constraint_data*>(f_data);
    // Eigen::MatrixXd Aeq = params->Aeq;
    // Eigen::VectorXd beq = params->beq;

    // Eigen::VectorXd h = Aeq * z - beq;
    Eigen::VectorXd h = params->Aeq_sparse * z - params->beq_sparse;

    // Find a better way of copying data.
    for (size_t i = 0; i < m; i++)
    {
        result[i] = h(i);
    }

    if (grad)
    {
        // // Find a better way of copying data.
        // for (size_t i = 0; i < m; i++)
        // {
        //     for (size_t j = 0; j < n; j++)
        //     {
        //         grad[i * n + j] = Aeq(i, j);
        //     }
        // }
        // https://stackoverflow.com/questions/8443102/convert-eigen-matrix-to-c-array/29865019
        Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(grad, params->Aeq.rows(), params->Aeq.cols()) = params->Aeq;

        // std::cout << "jac=\n";
        // for(size_t i = 0; i < Aeq.rows() * Aeq.cols(); i++)
        // {o
        //     std::cout << grad[i] << ", ";
        // }
        // std::cout << std::endl;
    }

    return;
}

ModelPredictiveControl::~ModelPredictiveControl()
{

}
