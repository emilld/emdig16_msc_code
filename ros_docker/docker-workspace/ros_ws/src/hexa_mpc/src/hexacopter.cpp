#include "../include/hexa_mpc/hexacopter.hpp"
#include <iostream>

#ifndef M_PI
#define M_PI 3.141592
#endif
#define RAD2DEG 360.0/M_PI/2.0
#define DEG2RAD M_PI/360.0*2.0
// Above lines from
//      https://c-for-dummies.com/blog/?p=4139
// I switched them around, thought it made more sense.
// Ex: angle [rad] = angle[deg] * DEG2RAD

Hexacopter::Hexacopter(
    double & m,
    double & L, 
    double & Jxx, 
    double & Jyy, 
    double & Jzz, 
    double & g, 
    double & b,
    double & dt)
        : Plant(12, 6), m(m), L(L), Jxx(Jxx), Jyy(Jyy), Jzz(Jzz), g(g), b(b)
{
    A = Eigen::MatrixXd(Nx, Nx); // Integration matrix.
    A.setIdentity();
    for (size_t i = 0; i < Nx - 1; i += 2)
    {
        A(i, i + 1) = dt;
    }

    std::cout << A << std::endl;

    c60 = cos(60 * DEG2RAD);
    s60 = sin(60 * DEG2RAD);
    
    B = Eigen::MatrixXd(4, 6);
    B << 1, 1, 1, 1, 1, 1,
        -L, L, L*c60,  -L*c60, -L*c60, L*c60,
        0, 0, L*s60, -L*s60, L*s60, -L*s60,
        -b, b, -b, b, b, -b;  

    std::cout << B << std::endl;

    C = Eigen::MatrixXd(9, 12);
    C.block<7, 7>(0, 0).setIdentity();
    C(7,  8) = 1;
    C(8, 10) = 1;

    std::cout << C << std::endl;

    // Precalculating inverted values.
    mInv = 1 / m;
    JxxInv = 1 / Jxx;
    JyyInv = 1 / Jyy;
    JzzInv = 1 / Jzz;

    std::cout << "Jxx = " << Jxx << ", JxxInv = " << JxxInv << std::endl; 
    std::cout << "Jyy = " << Jyy << ", JyyInv = " << JyyInv << std::endl; 
    std::cout << "Jzz = " << Jzz << ", JzzInv = " << JzzInv << std::endl; 
}


Eigen::VectorXd Hexacopter::compute_dynamics(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const double & dt) 
{
    // Empty gain loss.
    Eigen::VectorXd gain_loss(6);
    gain_loss.setZero();

    return compute_dynamics(state, input_vec, gain_loss, dt);
}

Eigen::VectorXd Hexacopter::compute_dynamics(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const Eigen::VectorXd & gain_loss,
    const double & dt) 
{
    // Read the current state into variables for easier computation.
    double phi      = state(0),
           phid     = state(1),
           theta    = state(2),
           thetad   = state(3),
           psi      = state(4),
           psid     = state(5),
           x        = state(6),
           xd       = state(7),
           y        = state(8),
           yd       = state(9),
           z        = state(10),
           zd       = state(11);   

    // std::cout << "state =\n" << state.transpose() << std::endl;

    // std::cout << "states: " << phi << ", " << phid << ", " << theta << ", " <<
    //     thetad << ", " << psi << ", " << psid << ", " << x << ", " << xd << ", " <<
    //     y << ", " << yd << ", " << z << ", " << zd << std::endl;

    // std::cout << phi << std::endl;
    Eigen::MatrixXd gain_loss_diag = gain_loss.asDiagonal();
    Eigen::MatrixXd I(input_vec.rows(), input_vec.rows());
    I.setIdentity();

    // Calulate virtual force vector.
    Eigen::VectorXd Uv = B * (I - gain_loss_diag) * input_vec;
    // std::cout << "Uv=\n" << Uv << "\nUf=\n" << input_vec << std::endl;

    double T  = Uv(0),
           Mx = Uv(1),
           My = Uv(2),
           Mz = Uv(3);

    // Calculate the accelerations.
    double ux = cos(phi) * cos(psi)   * sin(theta) + sin(phi) * sin(psi);
    double uy = cos(phi) * sin(theta) * sin(psi)   - sin(phi) * cos(psi);
    double uz = cos(phi) * cos(theta);

    // Rotational accelerations.
    double phidd   = JxxInv * (thetad * psid   * (Jyy - Jzz) + Mx);
    double thetadd = JyyInv * (phid   * psid   * (Jzz - Jxx) + My);
    double psidd   = JzzInv * (phid   * thetad * (Jxx - Jzz) + Mz);

    // Translational accelerations.
    double xdotd = mInv * ux * T;
    double ydotd = mInv * uy * T;
    double zdotd = mInv * uz * T - g;

    // std::cout << "xdotd= " << xdotd << ", ydotd= " << ydotd << ", zdotd= " << zdotd << std::endl;

    Eigen::VectorXd Xout(12, 1);
    Xout << phi,    phid    + phidd * dt,
            theta,  thetad  + thetadd * dt,
            psi,    psid    + psidd * dt,
            x, xd + xdotd * dt,
            y, yd + ydotd * dt,
            z, zd + zdotd * dt;

    // std::cout << "A=\n" << A << std::endl;
    // std::cout << "B=\n" << B << std::endl;
    // std::cout << "before=\n" << Xout << std::endl;

    // Update the positions in the X with the new velocities using the A matrix.
    Xout = A * Xout;

    // std::cout << "after=\n" << Xout << std::endl;

    // Saturate z value
    if (Xout(10) < 0)
    {
        Xout(10) = 0;
        Xout(11) = 0;
    }

    return Xout;
}

Eigen::MatrixXd Hexacopter::get_Fxhat(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const double & dt) 
{
    // Empty gain loss.
    Eigen::VectorXd gain_loss(6);
    gain_loss.setZero();

    return get_Fxhat(state, input_vec, gain_loss, dt);
}

Eigen::MatrixXd Hexacopter::get_Fxhat(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const Eigen::VectorXd & gain_loss, // Fault on actuator
    const double & dt)
{
    // Jacobian from this was computed analytically in matlab.

    // Read the current state into variables for easier computation.
    double phi      = state(0),
           phid     = state(1),
           theta    = state(2),
           thetad   = state(3),
           psi      = state(4),
           psid     = state(5);

    Eigen::MatrixXd gain_loss_diag = gain_loss.asDiagonal();
    Eigen::MatrixXd I(input_vec.rows(), input_vec.rows());
    I.setIdentity();

    // Calulate virtual force vector.
    Eigen::VectorXd Uv = B * (I - gain_loss_diag) * input_vec;

    double T = Uv(0);

    Eigen::MatrixXd Fxhat(12, 12);
    double dt22 = dt * dt;

    // std::cout << 
    //     dt << ", " <<
    //     phid << ", " <<
    //     Jxx << ", " <<
    //     Jzz << ", " <<
    //     JyyInv << ", " <<
    //     -(dt*phid*(Jxx - Jzz))*JyyInv << std::endl;

    Fxhat <<
                                                              1,                               dt,                                          0, (dt22*psid*(Jyy - Jzz)) * JxxInv,                                                                0, (dt22*thetad*(Jyy - Jzz))*JxxInv, 0,  0, 0,  0, 0,  0,
                                                              0,                                1,                                          0,   (dt*psid*(Jyy - Jzz)) * JxxInv,                                                                0,   (dt*thetad*(Jyy - Jzz))*JxxInv, 0,  0, 0,  0, 0,  0,
                                                              0,  -(dt22*psid*(Jxx - Jzz))*JyyInv,                                          1,                               dt,                                                                0,  -(dt22*phid*(Jxx - Jzz))*JyyInv, 0,  0, 0,  0, 0,  0,
                                                              0,    -(dt*psid*(Jxx - Jzz))*JyyInv,                                          0,                                1,                                                                0,    -(dt*phid*(Jxx - Jzz))*JyyInv, 0,  0, 0,  0, 0,  0,
                                                              0, (dt22*thetad*(Jxx - Jzz))*JzzInv,                                          0, (dt22*phid*(Jxx - Jzz)) * JzzInv,                                                                1,                               dt, 0,  0, 0,  0, 0,  0,
                                                              0,   (dt*thetad*(Jxx - Jzz))*JzzInv,                                          0,   (dt*phid*(Jxx - Jzz)) * JzzInv,                                                                0,                                1, 0,  0, 0,  0, 0,  0,
  (T*dt22*(cos(phi)*sin(psi) - cos(psi)*sin(phi)*sin(theta)))*mInv,                             0, (T*dt22*cos(phi)*cos(psi)*cos(theta))*mInv,                                0, (T*dt22*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,                                0, 1, dt, 0,  0, 0,  0,
    (T*dt*(cos(phi)*sin(psi) - cos(psi)*sin(phi)*sin(theta)))*mInv,                             0,   (T*dt*cos(phi)*cos(psi)*cos(theta))*mInv,                                0,   (T*dt*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,                                0, 0,  1, 0,  0, 0,  0,
 -(T*dt22*(cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta)))*mInv,                             0, (T*dt22*cos(phi)*cos(theta)*sin(psi))*mInv,                                0, (T*dt22*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,                                0, 0,  0, 1, dt, 0,  0,
   -(T*dt*(cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta)))*mInv,                             0,   (T*dt*cos(phi)*cos(theta)*sin(psi))*mInv,                                0,   (T*dt*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,                                0, 0,  0, 0,  1, 0,  0,
                                -(T*dt22*cos(theta)*sin(phi))*mInv,                             0,         -(T*dt22*cos(phi)*sin(theta))*mInv,                                0,                                                                0,                                0, 0,  0, 0,  0, 1, dt,
                                  -(T*dt*cos(theta)*sin(phi))*mInv,                             0,           -(T*dt*cos(phi)*sin(theta))*mInv,                                0,                                                                0,                                0, 0,  0, 0,  0, 0,  1;

    return Fxhat;                                  
}

Eigen::MatrixXd Hexacopter::get_Fu(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const double & dt) 
{
    // Empty gain loss.
    Eigen::VectorXd gain_loss(6);
    gain_loss.setZero();

    return get_Fu(state, input_vec, gain_loss, dt);
}

Eigen::MatrixXd Hexacopter::get_Fu(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const Eigen::VectorXd & gain_loss,
    const double & dt) 
{
    // Jacobian from this was computed analytically in matlab.

    // Read the current state into variables for easier computation.
    double phi      = state(0),
           phid     = state(1),
           theta    = state(2),
           thetad   = state(3),
           psi      = state(4),
           psid     = state(5);

    Eigen::MatrixXd gain_loss_diag = gain_loss.asDiagonal(); 
    Eigen::MatrixXd I(input_vec.rows(), input_vec.rows());
    I.setIdentity();

    // Calulate virtual force vector.
    Eigen::VectorXd Uv = B * (I - gain_loss_diag) * input_vec;

    double T = Uv(0);

    Eigen::MatrixXd Fu(12, 6);
    double dt22 = dt * dt;

    Fu << 
                                                    -(L*dt22)*JxxInv,                                                 (L*dt22)*JxxInv,                                             (L*c60*dt22)*JxxInv,                                            -(L*c60*dt22)*JxxInv,                                            -(L*c60*dt22)*JxxInv,                                             (L*c60*dt22)*JxxInv,
                                                   -(L*dt)*JxxInv,                                                   (L*dt)*JxxInv,                                               (L*c60*dt)*JxxInv,                                              -(L*c60*dt)*JxxInv,                                              -(L*c60*dt)*JxxInv,                                               (L*c60*dt)*JxxInv,
                                                             0,                                                            0,                                             (L*dt22*s60)*JyyInv,                                            -(L*dt22*s60)*JyyInv,                                             (L*dt22*s60)*JyyInv,                                            -(L*dt22*s60)*JyyInv,
                                                             0,                                                            0,                                               (L*dt*s60)*JyyInv,                                              -(L*dt*s60)*JyyInv,                                               (L*dt*s60)*JyyInv,                                              -(L*dt*s60)*JyyInv,
                                                 -(b*dt22)*JzzInv,                                                 (b*dt22)*JzzInv,                                                -(b*dt22)*JzzInv,                                                 (b*dt22)*JzzInv,                                                 (b*dt22)*JzzInv,                                                -(b*dt22)*JzzInv,
                                                   -(b*dt)*JzzInv,                                                   (b*dt)*JzzInv,                                                  -(b*dt)*JzzInv,                                                   (b*dt)*JzzInv,                                                   (b*dt)*JzzInv,                                                  -(b*dt)*JzzInv,
   (dt22*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,  (dt22*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,  (dt22*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,  (dt22*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,  (dt22*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,  (dt22*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,
     (dt*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,    (dt*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,    (dt*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,    (dt*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,    (dt*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,    (dt*(sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta)))*mInv,
  -(dt22*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv, -(dt22*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv, -(dt22*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv, -(dt22*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv, -(dt22*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv, -(dt22*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,
    -(dt*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,   -(dt*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,   -(dt*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,   -(dt*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,   -(dt*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,   -(dt*(cos(psi)*sin(phi) - cos(phi)*sin(psi)*sin(theta)))*mInv,
                                  (dt22*cos(phi)*cos(theta))*mInv,                                 (dt22*cos(phi)*cos(theta))*mInv,                                 (dt22*cos(phi)*cos(theta))*mInv,                                 (dt22*cos(phi)*cos(theta))*mInv,                                 (dt22*cos(phi)*cos(theta))*mInv,                                 (dt22*cos(phi)*cos(theta))*mInv,
                                    (dt*cos(phi)*cos(theta))*mInv,                                   (dt*cos(phi)*cos(theta))*mInv,                                   (dt*cos(phi)*cos(theta))*mInv,                                   (dt*cos(phi)*cos(theta))*mInv,                                   (dt*cos(phi)*cos(theta))*mInv,                                   (dt*cos(phi)*cos(theta))*mInv;

    return Fu;
}

Eigen::MatrixXd Hexacopter::get_C() 
{
    return C;   
}

Eigen::VectorXd Hexacopter::get_state_constraint(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const Eigen::VectorXd & gain_loss, // Fault on actuator
    const double & dt
)
{ // Inequality constraint.

    // Eigen::VectorXd new_state = compute_dynamics(state, input_vec(Eigen::seqN(0, Nu)), gain_loss, dt);
    Eigen::VectorXd new_state = get_Fxhat(state, input_vec, gain_loss, dt) * state + get_Fu(state, input_vec, gain_loss, dt) * input_vec;
    new_state = get_Fxhat(new_state, input_vec, gain_loss, dt) * new_state + get_Fu(new_state, input_vec, gain_loss, dt) * input_vec;
    // Eigen::VectorXd new_state = state;
    Eigen::VectorXd constraints(Nx);
    constraints.setZero();
    
    constraints(0)  = abs(new_state(0)) - 50 * DEG2RAD; // Roll
    constraints(2)  = abs(new_state(2)) - 50 * DEG2RAD; // Pitch
    constraints(7)  = abs(new_state(7)) - 4; // max x speed 4 m/s
    constraints(9)  = abs(new_state(9)) - 4; // max y speed 4 m/s
    constraints(11) = abs(new_state(11)) - 2; // max z speed 2 m/s

    // std::cout << "constraints" << std::endl;
    // std::cout << constraints.transpose() << std::endl;
    // std::cout << state.transpose() << std::endl;
    // std::cout << new_state.transpose() << std::endl;

    return constraints; 
}

// TODO: Make a function that calculates the gradient of the state constraint.
Eigen::MatrixXd Hexacopter::get_state_constraint_jac(
    const Eigen::VectorXd & state, 
    const Eigen::VectorXd & input_vec,
    const Eigen::VectorXd & gain_loss, // Fault on actuator
    const double & dt
)
{
    Eigen::MatrixXd jac(Nx, Nu);

    double h = 1e-8,
        hinv = 1e8;

    Eigen::VectorXd f0 = get_state_constraint(state, input_vec, gain_loss, dt);
    Eigen::VectorXd fd;

    Eigen::VectorXd input_vec_tmp;

    for (size_t i; i < jac.cols(); i++)
    {
        input_vec_tmp = input_vec;
        input_vec_tmp(i) += h;
        fd = get_state_constraint(state, input_vec_tmp, gain_loss, dt);
        // std::cout << "fd\n" << fd << std::endl;
        jac(Eigen::all, i) = hinv * (fd - f0);
        // std::cout << jac(Eigen::all, i).transpose() << std::endl;
    }

    // std::cout << "jac\n" << jac << std::endl;

    // Eigen::MatrixXd Fu = get_Fu(state, input_vec, gain_loss, dt);
    // Eigen::MatrixXd jac(Nx, Nu);
    // jac(0, Eigen::all) = Fu(0, Eigen::all);
    // jac(2, Eigen::all) = Fu(2, Eigen::all);
    // jac(7, Eigen::all) = Fu(7, Eigen::all);
    // jac(9, Eigen::all) = Fu(9, Eigen::all);
    // jac(11, Eigen::all) = Fu(11, Eigen::all);

    return jac;
}

void Hexacopter::set_sim_state_bound(
    const Eigen::VectorXd & lb,
    const Eigen::VectorXd & ub) 
{
    // TODO: Implement this.
}

Hexacopter::~Hexacopter() 
{
    
}
 
