/**
 * @file control.cpp
 * @brief Implementation of an MPC as a ROS node, written with MAVROS version 0.19.x, PX4 Pro Flight
 * 
 * Stack and node tested in Gazebo SITL
 */

// ROS includes
#include <ros/ros.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <mavros_msgs/RCOut.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>

#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>

// MPC and model includes
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <vector>

#include "../include/mpc_hexa/hexacopter.hpp"
#include "../include/mpc_hexa/mpc2.hpp"
#include "../include/mpc_hexa/aekf.hpp"

#include "nlopt.hpp"


class Controller
{
    private:
        ros::NodeHandle nh;
        ros::NodeHandle pnh;

        ros::Publisher pub_state_estimate,
                       pub_motor_fault_estimate,
                       pub_motor_thrust;
                       // Todo: Add publisher for the stick commands for drone.
        
        ros::Subscriber sub_local_position,
                        sub_local_rate,
                        sub_rc_channels;

        // inline static Hexacopter hexa;
        // inline static AEKF aekf;
        // inline static ModelPredictiveControl2 mpc;
        


        Eigen::VectorXd state_reference;

        // Input vector
        Eigen::VectorXd input_vec,
                        measurement; // roll, roll_rate, pitch, pitch_rate, yaw, yaw_rate, x, y, z

        // parameters
        double m,         // mass in kg
               L,        // arm length in m
               Jxx, // moment of inertia around 'x' axis in kg�m^2
               Jyy, // moment of inertia around 'y' axis in kg�m^2
               Jzz,    // moment of inertia around 'z' axis in kg�m^2
               g,        // gravity
               b,
               dt,
               dt_mpc,
               alpha, /* for lpf on inputs in MPC*/
               ss, // init covariance for aekf
               lambdaa; // speed of convergence of fault estimate

        int N; // Event horizon.

        std::vector<double> aekf_Q_diag, // System covariance matrix diagonal.
                            aekf_R_diag, // Measurement covariance matrix diagonal.
                            mpc_Q_diag, // State penalty.
                            mpc_R_diag; // Input penalty.

        // Eigen::DiagonalMatrix<double, Eigen::Dynamic> aekf_Q,
        //                                               aekf_R,
        //                                               mpc_Q,
        //                                               mpc_R;
        Eigen::MatrixXd aekf_Q,
                        aekf_R;
        Eigen::VectorXd mpc_Q,
                        mpc_R;

        std::vector<double> mpc_lb, // lower bound
                            mpc_ub; // upper bound
        Eigen::VectorXd mpc_lb_eig;
        Eigen::VectorXd mpc_ub_eig;

        ros::Timer timerControlLoop;

        std::chrono::high_resolution_clock::time_point start, end;

        Eigen::VectorXd state_est, fault_est;


    public:
        Hexacopter _hexa;
        AEKF _aekf;
        ModelPredictiveControl2 _mpc;

        Controller(ros::NodeHandle & a_nh, ros::NodeHandle & a_pnh)
        {
            nh = a_nh;
            pnh = a_pnh;

            // subscribers
            sub_local_position = pnh.subscribe("local_position", 1, &Controller::cb_local_pos, this);
            sub_local_rate = pnh.subscribe("imu", 1, &Controller::cb_imu, this);
            sub_rc_channels = pnh.subscribe("rcout", 1, &Controller::cb_rc, this);

            // publishers
            pub_state_estimate = pnh.advertise<std_msgs::Float64MultiArray>("state_estimate", 1);
            pub_motor_fault_estimate = pnh.advertise<std_msgs::Float64MultiArray>("motor_fault_estimate", 1);
            pub_motor_thrust = pnh.advertise<std_msgs::Float64MultiArray>("motor_thrust", 1);

            // std_msgs::Float64MultiArray msg;
            // while (1)
            // {
            //     pub_state_estimate.publish(msg);
            //     pub_motor_fault_estimate.publish(msg);
            //     pub_motor_thrust.publish(msg);
            // }

            pnh.param("m", m, 1.5);
            pnh.param("L", L, 0.37);
            pnh.param("Jxx", Jxx, 0.0762625);
            pnh.param("Jyy", Jyy, 0.0762625);
            pnh.param("Jzz", Jzz, 0.1369);
            pnh.param("g", g, 9.81);
            pnh.param("b", b, 0.01);
            pnh.param("dt", dt, 0.01);
            pnh.param("dt_mpc", dt_mpc, 0.05);
            pnh.param("N", N, 10);
            pnh.param("alpha", alpha, 1.0);
            pnh.param("ss", ss, 0.001);
            pnh.param("lambdaa", lambdaa, 0.995);

            pnh.param("aekf_Q_diag", aekf_Q_diag, std::vector<double>{0.1, 0.01, 0.1, 0.01, 0.1, 0.01, 0.5, 0.1, 0.5, 0.1, 0.1, 0.01});
            pnh.param("aekf_R_diag", aekf_R_diag, std::vector<double>{0.5, 0.1, 0.5, 0.1, 0.2, 0.1, 1, 1, 0.3});
            pnh.param("mpc_Q_diag", mpc_Q_diag, std::vector<double>{100, 100, 100, 100, 200, 100, 200, 100, 200, 100, 200, 100});
            pnh.param("mpc_R_diag", mpc_R_diag, std::vector<double>{0.1, 0.1, 0.1, 0.1, 0.1, 0.1});
            pnh.param("mpc_lb", mpc_lb, std::vector<double>{0., 0., 0., 0., 0., 0.});
            pnh.param("mpc_ub", mpc_ub, std::vector<double>{8., 8., 8., 8., 8., 8.});

            // init_hexa();
            Hexacopter hexa(m, L, Jxx, Jyy, Jzz, g, b, dt);
            input_vec = Eigen::VectorXd(hexa.Nu);
            input_vec.setZero();

            Eigen::MatrixXd C = hexa.get_C();
            measurement = Eigen::VectorXd(C.rows());
            measurement.setZero();
            // measurement << 0, 0, 0, 0, 0, 0, 1, 2, 3;
            
            // init_aekf();
            AEKF aekf(hexa, ss, lambdaa);
            std::cout << "Nu: " << hexa.Nu << " Nx: " << hexa.Nx << std::endl;

            // Eigen::MatrixXd tmp = Eigen::Map<Eigen::VectorXd>(aekf_Q_diag.data(), aekf_Q_diag.size()).asDiagonal();
            // std::cout << tmp << std::endl;
            aekf_Q = Eigen::Map<Eigen::VectorXd>(aekf_Q_diag.data(), aekf_Q_diag.size()).asDiagonal();
            // std::cout << aekf_Q.diagonal().transpose() << std::endl;
            aekf_R = Eigen::Map<Eigen::VectorXd>(aekf_R_diag.data(), aekf_R_diag.size()).asDiagonal();

            aekf.set_Q(aekf_Q);
            aekf.set_R(aekf_R);

            std::cout << "Q = \n" << aekf.get_Q() << std::endl;
            std::cout << "R = \n" << aekf.get_R() << std::endl;

            state_reference = Eigen::VectorXd(hexa.Nx); // reference for x, y, z
            state_reference.setZero();
            // state_reference
            state_reference(6) = 1;
            state_reference(8) = -2;
            state_reference(10) = 10;
            // init_mpc();

            ModelPredictiveControl2 mpc(hexa, N);

            mpc_Q = Eigen::Map<Eigen::VectorXd>(mpc_Q_diag.data(), mpc_Q_diag.size());
            mpc_R = Eigen::Map<Eigen::VectorXd>(mpc_R_diag.data(), mpc_R_diag.size());

            std::cout << mpc_Q.transpose() << std::endl;
            std::cout << mpc_R.transpose() << std::endl;

            mpc.create_G_matrix(mpc_Q, mpc_R);
            
            // std::cout << "after" << std::endl;
            // for (auto x : mpc_lb)
            //     std::cout << x << ",";
            // std::cout << std::endl;

            mpc_lb_eig = Eigen::Map<Eigen::VectorXd>(mpc_lb.data(), mpc_lb.size());
            mpc_ub_eig = Eigen::Map<Eigen::VectorXd>(mpc_ub.data(), mpc_ub.size());

            mpc.set_bounds(mpc_lb_eig, mpc_ub_eig);

            mpc.print_bounds();

            mpc.set_Z_ref(state_reference);

            mpc.use_state_constraint();

            state_est = Eigen::VectorXd(hexa.Nx);
            fault_est = Eigen::VectorXd(hexa.Nu);
            state_est.setZero();
            fault_est.setZero();

            _hexa = hexa;
            _aekf = aekf;
            _mpc = mpc;

            ROS_INFO("Init done.");

            // Create timer callback
            ROS_INFO("dt: %f", dt);
            timerControlLoop =
                nh.createTimer(ros::Duration(dt),
                                std::bind(&Controller::timer_loop, this));
                        

            // std::cout << "Nu: " << hexa.Nu << " Nx: " << hexa.Nx << std::endl;
            
            // std::cout << measurement.transpose() << std::endl;
            // std::cout << input_vec.transpose() << std::endl;
            // std::cout << dt << std::endl;

            // aekf.update(measurement, input_vec, dt);

            // std::cout << state_reference.transpose() << std::endl;
            // mpc.set_Z_ref(state_reference);

            // std::cout << aekf.get_state_estimate().transpose() << std::endl;
            // std::cout << input_vec.transpose() << std::endl;
            // std::cout << aekf.get_fault_estimate().transpose() << std::endl;
            // std::cout << dt_mpc << std::endl;

            // start = std::chrono::high_resolution_clock::now();

            // state_est = aekf.get_state_estimate();
            // fault_est = aekf.get_fault_estimate();
            // std::cout << state_est.transpose() << std::endl;
            // std::cout << fault_est.transpose() << std::endl;

            // input_vec = mpc.update(state_est, input_vec, fault_est, dt_mpc, alpha);
            // // mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
            // // auto tmp = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
            // // std::cout << mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc) << std::endl;
            // end = std::chrono::high_resolution_clock::now();
            // double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();

            // std::cout << input_vec.transpose() <<  ", elapsed: " << time_elapsed << " ms" << std::endl;
        };

        ~Controller(){};

        // void init_hexa()
        // {
        //     std::cout << "Nu: " << hexa.Nu << " Nx: " << hexa.Nx << std::endl;
        // }

        // void init_aekf()
        // {
           
        // }

        // void init_mpc()
        // {
        //     mpc = ModelPredictiveControl2(hexa, N);

        //     mpc_Q = Eigen::Map<Eigen::VectorXd>(mpc_Q_diag.data(), mpc_Q_diag.size());
        //     mpc_R = Eigen::Map<Eigen::VectorXd>(mpc_R_diag.data(), mpc_R_diag.size());

        //     std::cout << mpc_Q.transpose() << std::endl;
        //     std::cout << mpc_R.transpose() << std::endl;

        //     mpc.create_G_matrix(mpc_Q, mpc_R);
            
        //     // std::cout << "after" << std::endl;
        //     // for (auto x : mpc_lb)
        //     //     std::cout << x << ",";
        //     // std::cout << std::endl;

        //     mpc_lb_eig = Eigen::Map<Eigen::VectorXd>(mpc_lb.data(), mpc_lb.size());
        //     mpc_ub_eig = Eigen::Map<Eigen::VectorXd>(mpc_ub.data(), mpc_ub.size());

        //     mpc.set_bounds(mpc_lb_eig, mpc_ub_eig);

        //     mpc.print_bounds();

        //     mpc.set_Z_ref(state_reference);

        //     mpc.use_state_constraint();
        // }

        void cb_local_pos(const geometry_msgs::PoseWithCovarianceStamped & msg)
        {
            ROS_INFO("%f", msg.pose.pose.position.x);
        }

        void cb_imu(const sensor_msgs::Imu & msg)
        {
            ROS_INFO("%f", msg.orientation.w);
        }

        void cb_rc(const mavros_msgs::RCOut & msg)
        {
            ROS_INFO("%d", msg.channels[0]);
        }

        void timer_loop()
        {
            // ROS_INFO("time");
            std::cout << "Nu: " << _hexa.Nu << " Nx: " << _hexa.Nx << std::endl;

            std::cout << measurement.transpose() << std::endl;
            std::cout << input_vec.transpose() << std::endl;
            std::cout << dt << std::endl;

            _aekf.update(measurement, input_vec, dt);

            std::cout << state_reference.transpose() << std::endl;
            _mpc.set_Z_ref(state_reference);

            std::cout << _aekf.get_state_estimate().transpose() << std::endl;
            std::cout << input_vec.transpose() << std::endl;
            std::cout << _aekf.get_fault_estimate().transpose() << std::endl;
            std::cout << dt_mpc << std::endl;

            start = std::chrono::high_resolution_clock::now();

            state_est = _aekf.get_state_estimate();
            fault_est = _aekf.get_fault_estimate();

            // input_vec = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
            _mpc.update(_aekf.get_state_estimate(), input_vec, _aekf.get_fault_estimate(), dt_mpc);
            // auto tmp = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc);
            // std::cout << mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt_mpc) << std::endl;
            end = std::chrono::high_resolution_clock::now();
            double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();

            std::cout << ", elapsed: " << time_elapsed << " ms" << std::endl;
        }
};

int main(int argc, char **argv) 
{
    ros::init(argc, argv, "mpc");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");
    Controller cn = Controller(nh, pnh);
    ros::spin();
}