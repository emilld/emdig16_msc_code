#ifndef __PLANT_H__
#define __PLANT_H__

/*
* Top class that other classes that defines a plant
* should inherit from.
*/

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>

class Plant
{ 
public:
    Plant(){};

    Plant(int _Nx, int _Nu) 
    {
        Nx = _Nx;
        Nu = _Nu;
    };

    virtual Eigen::VectorXd compute_dynamics(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    ) {};

    virtual Eigen::VectorXd compute_dynamics(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    ) {};

    virtual Eigen::MatrixXd get_Fxhat(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    ) {};

    virtual Eigen::MatrixXd get_Fu(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    ) {};

    virtual Eigen::MatrixXd get_Fxhat(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    ) {};

    virtual Eigen::MatrixXd get_Fu(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    ) {};

    virtual Eigen::VectorXd get_state_constraint(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    ) {};

    virtual Eigen::MatrixXd get_state_constraint_jac(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    ) {};

    virtual Eigen::MatrixXd get_C() {};

    ~Plant()
    {

    };
    int Nx, Nu;

private:
    
};




#endif // __PLANT_H__