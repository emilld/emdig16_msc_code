#ifndef __MPC2_H__
#define __MPC2_H__

#include <iostream>
#include <vector>
#include <thread>

#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <nlopt.hpp>

#include "plant.hpp"

typedef struct {
    Eigen::DiagonalMatrix<double, Eigen::Dynamic> * Q_weights; // Matrix of weights, q and r
    Eigen::DiagonalMatrix<double, Eigen::Dynamic> * R_weights; // 
    Eigen::VectorXd * z_ref; // Vector of references for the states
    Eigen::VectorXd * init_state;
    Eigen::MatrixXd * Fxhat; // Linearized model matrices.
    Eigen::MatrixXd * Fu;
    int horizon_prediction;
    int horizon_control;
    Plant * plant;
    double dt;
    Eigen::VectorXd * gain_loss;
} objective_params;

typedef struct {
    Eigen::VectorXd * state;
    Eigen::VectorXd * gain_loss;
    Plant * plant;
    double dt;
    int horizon_prediction;
    int horizon_control;
} model_constraint_data;

class ModelPredictiveControl2
{
public:
    // ModelPredictiveControl() {};
    ModelPredictiveControl2(){};
    ModelPredictiveControl2(Plant & p, int _prediction_horizon, int _control_horizon);
    ~ModelPredictiveControl2();

    Eigen::VectorXd update( // Returns the optimal next command for the system.
        const Eigen::MatrixXd & state,
        const Eigen::MatrixXd & input_vec,
        const Eigen::MatrixXd & gain_loss,
        const double & dt // "Distance in time" between each N simulated points in controller.
    );

    Eigen::VectorXd update( // Returns the optimal next command for the system.
        const Eigen::MatrixXd & state,
        const Eigen::MatrixXd & input_vec,
        const Eigen::MatrixXd & gain_loss,
        const double & dt, // "Distance in time" between each N simulated points in controller.
        const double & alpha // Filter coefficient.
    );

    void create_G_matrix(
        Eigen::VectorXd & q, // penalties on states, length = Nx
        Eigen::VectorXd & r  // penalties on inputs, length = Nu
    );

    void set_Z_ref(
        Eigen::VectorXd & z_ref
    );  // Set the reference vector. 
        // [x1, ..., xi]

    void set_bounds(
        const Eigen::VectorXd & lb,
        const Eigen::VectorXd & ub
    ); // Set the upper and lower bounds of the variables, [u0, ..., uN-1].

    void use_state_constraint(); // Uses the state constraint defined by the dynamic model.

    void print_bounds();

private:
    Plant * plant;
    // int N;
    int horizon_prediction;
    int horizon_control;
    nlopt::opt opt;
    Eigen::VectorXd input_vec_filtered;
    std::vector<double> tol_constraint;
    bool state_constraint;
    
    objective_params obj_data;
    model_constraint_data constraint_data;

    static double objective_function(
        const std::vector<double> & x, 
        std::vector<double> & grad, 
        void * my_func_data
    );

    static double inner_objective_function(
        const std::vector<double> & x, 
        void * my_func_data
    );

    // static void naive_jac_thread(
    //     const std::vector<double> & x, 
    //     std::vector<double> & grad, 
    //     void * my_func_data, 
    //     int i,
    //     double f0,
    //     double h,
    //     double hinv
    // );

    static void model_constraint(
        unsigned m, // size of result
        double * result, // Where output values should be stored.
        unsigned n, // Size of x
        const double * x, // The parameters to be optimized
        double * grad, // Output array for gradient.
        void * f_data // Extra data
    );
};
#endif // __MPC2_H__