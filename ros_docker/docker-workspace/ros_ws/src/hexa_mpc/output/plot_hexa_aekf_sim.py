#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# filename = "aekf_sim_without_fault_estimation.csv"
# filename = "aekf_sim_with_fault_estimation.csv"
filename = "hexa_aekf_mpc_sim.csv"

data = pd.read_csv(filename, nrows=1)
cols = data.columns.tolist()
cols_to_use = cols[:len(cols) - 1]
data = pd.read_csv(filename, usecols=cols_to_use, dtype="float64")
data.columns = data.columns.str.strip()

state_names = [ 'phi',
                'phid',
                'theta',
                'thetad',
                'psi',
                'psid',
                'x',
                'xd',
                'y',
                'yd',
                'z',
                'zd',
               ]

fig, ax = plt.subplots(12, 1)
for i in range(12):
    ax[i].plot(data["time"], data[f"state_actual{i}"], label=state_names[i])
    ax[i].plot(data["time"], data[f"state_estimated{i}"], '--', label="Est." + state_names[i])
    ax[i].plot(data["time"], data[f"state_reference{i}"], '--', label="Ref." + state_names[i])

# ax[0].plot(data["time"], data[f"state_measured{0}"], ':', label="Measured." + state_names[0])
# ax[1].plot(data["time"], data[f"state_measured{1}"], ':', label="Measured." + state_names[1])
# ax[3].plot(data["time"], data[f"state_measured{2}"], ':', label="Measured." + state_names[3])

for a in ax:
    a.legend()
    a.grid()

# Figure of position
# from the top
plt.figure()
plt.subplot(311)
plt.plot(data[f"state_actual{6}"], data[f"state_actual{8}"], label="Actual position")
plt.plot(data[f"state_estimated{6}"], data[f"state_estimated{8}"], '--', label="Estimated position")
plt.plot(data[f"state_reference{6}"], data[f"state_reference{8}"], '*--', label="Reference position")
plt.plot(data[f"state_measured{6}"], data[f"state_measured{7}"], '--', label="Measured position")
plt.legend()
plt.grid()
plt.axis("equal")
plt.xlabel("x")
plt.ylabel("y")

# from the side, x-z
plt.subplot(312)
plt.plot(data[f"state_actual{6}"], data[f"state_actual{10}"], label="Actual position")
plt.plot(data[f"state_estimated{6}"], data[f"state_estimated{10}"], '--', label="Estimated position")
plt.plot(data[f"state_reference{6}"], data[f"state_reference{10}"], '*--', label="Reference position")
plt.plot(data[f"state_measured{6}"], data[f"state_measured{8}"], '--', label="Measured position")
plt.legend()
plt.grid()
plt.axis("equal")
plt.xlabel("x")
plt.ylabel("z")

# from the side, y-z
plt.subplot(313)
plt.plot(data[f"state_actual{8}"], data[f"state_actual{10}"], label="Actual position")
plt.plot(data[f"state_estimated{8}"], data[f"state_estimated{10}"], '--', label="Estimated position")
plt.plot(data[f"state_reference{8}"], data[f"state_reference{10}"], '*--', label="Reference position")
plt.plot(data[f"state_measured{7}"], data[f"state_measured{8}"], '--', label="Measured position")
plt.legend()
plt.grid()
plt.axis("equal")
plt.xlabel("y")
plt.ylabel("z")

# 3d plot of position
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(data[f"state_actual{6}"], data[f"state_actual{8}"], data[f"state_actual{10}"], label="Actual position")
ax.plot(data[f"state_estimated{6}"], data[f"state_estimated{8}"], data[f"state_estimated{10}"], '--', label="Estimated position")
ax.plot(data[f"state_reference{6}"], data[f"state_reference{8}"], data[f"state_reference{10}"], '*--', label="Reference position")
ax.plot(data[f"state_measured{6}"], data[f"state_measured{7}"], data[f"state_measured{8}"], '--', label="Measured position")
# ax.set_box_aspect([1,1,1])
# Make axes limits 
xyzlim = np.array([ax.get_xlim3d(),ax.get_ylim3d(),ax.get_zlim3d()]).T
XYZlim = np.asarray([min(xyzlim[0]),max(xyzlim[1])]) * 1.4
ax.set_xlim3d(XYZlim) 
ax.set_ylim3d(XYZlim)
ax.set_zlim3d(XYZlim * 3 / 4)
# ax.set_aspect('equal')

ax.legend()

fig, ax = plt.subplots(3, 1)
ax[0].plot(data[f"state_actual{6}"], label="$x$")
ax[0].plot(data[f"state_actual{7}"], label="$\dot\{x\}$")
ax[1].plot(data[f"state_actual{8}"], label="$y$")
ax[1].plot(data[f"state_actual{9}"], label="$\dot\{y\}$")
ax[2].plot(data[f"state_actual{10}"], label="$z$")
ax[2].plot(data[f"state_actual{11}"], label="$\dot\{z\}$")

for a in ax:
    a.legend()

# fault_names = ["Steering fault", "Throttle fault"]
# plt.figure()
# for i in range(2):
#     plt.plot(data["time"], data[f"fault_actual{i}"], label=fault_names[i])
#     plt.plot(data["time"], data[f"fault_estimated{i}"], label= "Est." + fault_names[i])

# x = data[f"fault_estimated{0}"]
# print(x)
# print(np.round(x, 5))

# print(data[f"fault_estimated{0}"].to_numpy(dtype=float))

fault_names = [f'motor {i + 1}' for i in range(6)]

# Estimated faults.
fig, ax = plt.subplots(3, 2)
ax = ax.flatten()
for i in range(6):
    ax[i].plot(data["time"], data[f"fault_actual{i}"], label=fault_names[i])
    ax[i].plot(data["time"], data[f"fault_estimated{i}"], label= "Est." + fault_names[i])
    ax[i].legend()


# Plot showing all the actuator values.
plt.figure()
for i in range(6):
    plt.plot(data["time"], data[f"input{i}"], label=f"Motor {i+1}")
plt.legend()
plt.grid()

plt.show()