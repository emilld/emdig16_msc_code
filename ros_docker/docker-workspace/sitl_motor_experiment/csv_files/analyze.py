#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from num2tex import num2tex

data = {} # using lift as the key and mean and std as value
kgf = 9.80665 # N, one kgf

for file in os.listdir("."):
    if file.endswith(".csv"):
        print(os.path.join(".", file))

        tmp = pd.read_csv(os.path.join(".", file), sep=",")
        # print(tmp)

        propellor_data = tmp[[f"field.channels{i}" for i in range(6)]]
        # print(propellor_data)

        propellor_data = propellor_data.to_numpy().flatten()
        # print(propellor_data)

        mean = np.mean(propellor_data)
        std = np.std(propellor_data)

        # exstract weight from filename
        # first four letters is the mass in grams
        mass = float(file[:4]) / 1000.

        # Divide by six to get how much one motor lifts
        mass /= 6.
        lift = mass * kgf

        print(f"lift: {lift}N, mean +- std: {mean} +- {std}")
        data[lift] = [mean, std]


# Fit third order polynomial to data
lifts = np.asarray(list(data.keys()))
mean_PWM = np.asarray(list(data.values()))[:, 0]

coeff1 = np.polyfit(mean_PWM, lifts, 3)
t = np.arange(1000, 2000, 0.1)
p1 = np.poly1d(coeff1)

print("coefficients:", coeff1)

# plot mean and std and mass
print(data)

plt.figure()

print(lifts)
print(mean_PWM)

plt.plot(mean_PWM, lifts, '+', label="Gazebo motor")
plt.plot(t, p1(t), '--', label=f"$ y = {num2tex(coeff1[3], precision=3, format_spec='+', exp_format='times')} {num2tex(coeff1[2], precision=3, format_spec='+', exp_format='times')}x + {num2tex(coeff1[1], precision=3, format_spec='+', exp_format='times')}x^2 + {num2tex(coeff1[0], precision=3, format_spec='+', exp_format='times')}x^3 $")

plt.xlim([1000, 2000])
# plt.ylim([0, 8])

plt.grid()

plt.xlabel("ESC signal ($\mu$s)")
plt.ylabel("Thrust (N)")

plt.legend()



plt.show()