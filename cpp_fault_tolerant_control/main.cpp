#include <cmath>
#include <iostream>
#include <fstream>
#include <chrono>

#include "include/bike.hpp"
#include "include/mpc.hpp"

#include "nlopt.hpp"


using namespace std;

int main() {
    cout << "Running Main!" << endl;

    int N = 5;
    // nlopt::opt opt(nlopt::LD_SLSQP, 2);
    // std::cout << opt.get_algorithm_name() << std::endl;

    Bike bike(1, 4, 2);
    std::cout << bike.Nu << " " << bike.Nx << std::endl;

    ModelPredictiveControl mpc(bike, N);

    Eigen::VectorXd q(bike.Nx), r(bike.Nu);
    q <<   /*pos x*/ 200,  /*pos y*/ 200,  /*yaw*/ 1, /*velocity*/ 10;
    r <<   10,          0.1;
    mpc.create_G_matrix(q, r);

    std::cout << "G = \n" << mpc.get_G() << std::endl;

    // Set bounds
    Eigen::VectorXd lb(bike.Nx + bike.Nu), ub(bike.Nx + bike.Nu);
    // lb << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, 0, -0.7, -2;
    // ub <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL, 5,  0.7,  2;
    lb << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -0.7, -2;
    ub <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  0.7,  2;

    mpc.set_bounds(lb, ub);

    Eigen::VectorXd lb_states(bike.Nx), ub_states(bike.Nx);
    lb_states << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, 0;
    ub_states <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL, 5;

    bike.set_sim_state_bound(lb_states, ub_states);
    mpc.print_bounds();

    // Set reference:
    Eigen::VectorXd z_ref(bike.Nx);
    z_ref << 10, 10, 0, 1;

    mpc.set_Z_ref(z_ref);

    // Example compute dynamics
    Eigen::VectorXd state(4, 1);
    Eigen::VectorXd input_vec(2, 1);

    state(0, 0) = 0; // pos x
    state(1, 0) = 0; // pos y
    state(2, 0) = 0; // M_PI / 3; // yaw 
    state(3, 0) = 0; //1; // velocity

    input_vec(0, 0) = 0; // M_PI / 4; // steering wheel
    input_vec(1, 0) = 0; // 2; // acceleration

    double dt = 0.01, dt_mpc = 0.1;

    // Vectors for data.
    std::vector<std::vector<double>> plot_states;
    std::vector<std::vector<double>> plot_inputs;

    // For timing
    std::chrono::high_resolution_clock::time_point start, end;

    // Simulation loop.
    for (double t = 0; t < 50; t += dt)
    {
        std::cout << t << std::endl;

        // Set reference.
        z_ref << 5 * cos(0.1 * t), 5 * sin(0.1 * t), 0, 1;
        // std::cout << z_ref << std::endl;
        mpc.set_Z_ref(z_ref);

        // Get control output.
        start = std::chrono::high_resolution_clock::now();
        input_vec = mpc.update(state, input_vec, dt_mpc);
        end = std::chrono::high_resolution_clock::now();
        double time_elapsed = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
        // std::cout << "loop took " << time_elapsed << " miliseconds to execute\n";
        std::cout << "mpc took " << time_elapsed << " microseconds to execute\n";

        // Simulate the next step.
        // Apply input to dynamics.
        state = bike.compute_dynamics(state, input_vec, dt);

        // Save for plotting
        plot_states.push_back(std::vector<double> {state(0, 0), state(1, 0), state(2, 0), state(3, 0)});
        plot_inputs.push_back(std::vector<double> {input_vec(0, 0), input_vec(1, 0)});
    }

    std::ofstream state_file, actuator_file;
    state_file.open("../output/state_file.csv");
    actuator_file.open("../output/actuator_file.csv");

    state_file << "pos x, pos y, yaw, velocity," << std::endl;
    for (auto state : plot_states)
    {
        for (double x : state)
            state_file << x << ", ";
            // std::cout << x << ", ";
        
        // std::cout << std::endl;
        state_file << std::endl;
    }

    state_file.close();

    actuator_file << "steering wheel, acceleration," << std::endl;
    for (auto input : plot_inputs)
    {
        for (double x : input)
            actuator_file << x << ", ";
        
        actuator_file << std::endl;
    }

    actuator_file.close();

    return 0;
}