/*
 * Title: Experiment 1 - Fault Tolerance Analysis
 * This program is for running trials for the first experiment
 * in the second paper for my MSc thesis.
 * Procedure:
 *      Simulation for 25.
 *      Apply symmetric fault, i.e. the same fault on all motors,
 *      at 5s.
 *      Faults should go from 0-1 in steps of 0.01.
 *      For each fault; simulate 5 times.
 *      Make experiment for both AEKF and EKF.
 */

#include <iostream>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <random>
// #include <sstream>
#include <algorithm>
#include <sstream>

#include "../include/hexacopter.hpp"
#include "../include/mpc2.hpp"
#include "../include/aekf.hpp"
#include "../experiments/initialization.hpp"

#include "nlopt.hpp"

#include <eigen3/Eigen/Dense>

#define REFERENCE_STATIC 0

int main(int argc, char *argv[]) 
{
    if (argc < 4)
    {
        std::cout << "No argument passed to program.\n";
        std::cout << "Usage: " << argv[0] << " <sim_time> <trial_num> <fault_level> <AEKF/EKF>\n";
        std::cout << "fault level from 0 to 1, if not it will be clamped to this.\n";
        return 0;
    }

    double sim_time = 40;
    // bool sim_fault = true;
    std::string prefix, estimator;
    double fault_level;

    sim_time = atof(argv[1]);
    prefix = argv[2];
    fault_level = std::max(std::min(atof(argv[3]), 1.), 0.); // clamp fault in interval 0 to 1
    estimator = argv[4];

    SimObjects * sim_objects = new SimObjects();
    // init_simulation(sim_objects);

    // Set reference:
    Eigen::VectorXd z_ref(sim_objects->hexa->get_H_control().rows()); // tracks yaw, x, y, z
    // z_ref << 0, 0, 0, 0, M_PI_2, 0, 3, 0, -2, 0, 10, 0;
    z_ref << 0, 0, 0, 1;
    // z_ref << 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 10, 0;
    
    sim_objects->mpc->set_Z_ref(z_ref);

    // Initialize state and input vector.
    Eigen::VectorXd state(sim_objects->hexa->Nx);
    state.setZero();
    Eigen::VectorXd input_vec(sim_objects->hexa->Nu);
    input_vec.setZero();
    // input_vec.setConstant(4);

    // Gain loss vector.
    Eigen::VectorXd gain_loss(sim_objects->hexa->Nu);
    gain_loss.setZero();
    
    Eigen::MatrixXd C = sim_objects->hexa->get_C();

    // Example simulation with non-complete measurement.
    Eigen::VectorXd y(C.rows()); 
    y.setZero();

    std::random_device rd;
    // std::mt19937 gen(rd());  //here you could also set a seed
    std::default_random_engine gen{rd()};
    // std::uniform_real_distribution<double> dis(LO, HI);
    std::normal_distribution<double> dis(0., 1.);
    // https://stackoverflow.com/questions/35827926/eigen-matrix-library-filling-a-matrix-with-random-float-values-in-a-given-range
    
    Eigen::VectorXd sensor_noise(C.rows());

    std::ofstream outfile;
    // outfile.open("../output/experiments/fault_analysis/" + prefix + "_trial.csv");
    std::stringstream filename;
    filename << "../output/experiments/fault_analysis/" << estimator << 
        "/fault_level-" << std::setprecision(2) << std::fixed << fault_level << "_trial_" << prefix  << ".csv";
    std::cout << filename.str() << std::endl;
    outfile.open(filename.str());
    // outfile << "time, state_actual1, state_actual2, state_actual3, <, state_estimated, fault_actual, fault_estimated" << std::endl;
    outfile << "time, ";

    for (size_t i = 0; i < sim_objects->hexa->Nx; i++)
        outfile << "state_actual" << i << ", ";
    for (size_t i = 0; i < y.rows(); i++)
        outfile << "state_measured" << i << ", ";
    for (size_t i = 0; i < sim_objects->hexa->Nx; i++)
        outfile << "state_estimated" << i << ", ";
    for (size_t i = 0; i < sim_objects->hexa->get_H_control().rows(); i++)
        outfile << "state_reference" << i << ", ";
    for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
        outfile << "fault_actual" << i << ", ";
    for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
        outfile << "fault_estimated" << i << ", ";
    for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
        outfile << "input" << i << ", ";
    
    outfile << std::endl;

    // y = C * state + dt * RRf * randNoiseVec.Random(C.rows());
    // aekf.update(y, input_vec, dt);
    
    outfile << 0 << ", ";

    for (size_t i = 0; i < sim_objects->hexa->Nx; i++)
        outfile << std::fixed << std::setprecision(4) << state(i) << ", ";
    for (size_t i = 0; i < y.rows(); i++)
        outfile << std::fixed << std::setprecision(4) << y(i) << ", ";
    for (size_t i = 0; i < sim_objects->hexa->Nx; i++)
    {
        if (estimator == "AEKF")
            outfile << std::fixed << std::setprecision(4) << sim_objects->aekf->get_state_estimate()(i) << ", ";
        else if (estimator == "EKF")
            outfile << std::fixed << std::setprecision(4) << sim_objects->ekf->get_state_estimate()(i) << ", ";
    }
    for (size_t i = 0; i < sim_objects->hexa->get_H_control().rows(); i++)
        outfile << std::fixed << std::setprecision(4) << z_ref(i) << ", ";
    for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
        outfile << std::fixed << std::setprecision(4) << gain_loss(i) << ", ";
    for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
    {
        if (estimator == "AEKF")
            outfile << std::fixed << std::setprecision(4) << sim_objects->aekf->get_fault_estimate()(i) << ", ";
        else if (estimator == "EKF")
            outfile << std::fixed << std::setprecision(4) << 0 << ", ";
    }
    for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
        outfile << std::fixed << std::setprecision(4) << input_vec(i) << ", ";
    
    outfile << std::endl;

    std::chrono::high_resolution_clock::time_point start, end;

    for (double t = 0; t < sim_time; t += sim_objects->dt)
    {
        gain_loss << 0., 0., 0., 0., 0., 0.;
        if (t > 5)
            gain_loss.setConstant(fault_level);

        // Simulate process and sensor noise
        // https://www.mathworks.com/help/control/ug/kalman-filtering.html
        // process_noise = QQf.cwiseSqrt() * Eigen::MatrixXd::Zero(C.cols(), 1).unaryExpr([&](float){return dis(gen);});
        sensor_noise = sim_objects->aekf->get_R() * Eigen::MatrixXd::Zero(C.rows(), 1).unaryExpr([&](float){return dis(gen);});

        state = sim_objects->hexa->compute_dynamics(state, input_vec, gain_loss, sim_objects->dt); // + dt * process_noise; // The "real thing".
        // Eigen::MatrixXd randn = randNoiseVec;
        // y = C * state + dt * RRf * randNoiseVec.Random(C.rows());
        
        // y = C * state + dt * RRf * randNoiseVec;
        // std::cout << "sensor noise: " << sensor_noise.transpose() << std::endl;
        y = C * state + sim_objects->dt * sensor_noise;
        // y = C * state + sensor_noise;
        
        // std::cout << "y = \n" << y.transpose() << "\nC*X" << (C * state).transpose() << std::endl;

        if (estimator == "AEKF")
            sim_objects->aekf->update(y, input_vec, sim_objects->dt);
        else if (estimator == "EKF")
            sim_objects->ekf->update(y, input_vec, sim_objects->dt);
       
        // Gerono lemniscate
        z_ref(0) = 0;
        z_ref(1) = 5. * sin(t/3.); // x
        z_ref(2) = 5. * sin(t/3.) * cos(t/3.); // y
        z_ref(3) = 1 + 0.1 * t;

        sim_objects->mpc->set_Z_ref(z_ref);
        
        // std::cout << sim_objects->aekf->get_state_estimate().transpose() << std::endl;
        // std::cout << sim_objects->aekf->get_fault_estimate().transpose() << std::endl;

        // std::cout << t << " " << z_ref.transpose() << std::flush;

        start = std::chrono::high_resolution_clock::now();
        if (estimator == "AEKF")
            input_vec = sim_objects->mpc->update(sim_objects->aekf->get_state_estimate(), input_vec, sim_objects->aekf->get_fault_estimate(), sim_objects->dt);
        else if (estimator == "EKF")
            input_vec = sim_objects->mpc->update(sim_objects->ekf->get_state_estimate(), input_vec, Eigen::VectorXd::Zero(sim_objects->hexa->Nu), sim_objects->dt);
        end = std::chrono::high_resolution_clock::now();
        double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();

        // std::cout << ", elapsed: " << time_elapsed << " ms" << std::endl;

        // printProgress(t / sim_time);
        printProgress(t, sim_time);

        outfile << t + sim_objects->dt << ", ";

        for (size_t i = 0; i < sim_objects->hexa->Nx; i++)
            outfile << std::fixed << std::setprecision(4) << state(i) << ", ";
        for (size_t i = 0; i < y.rows(); i++)
            outfile << std::fixed << std::setprecision(4) << y(i) << ", ";
        for (size_t i = 0; i < sim_objects->hexa->Nx; i++)
        {
            if (estimator == "AEKF")
                outfile << std::fixed << std::setprecision(4) << sim_objects->aekf->get_state_estimate()(i) << ", ";
            else if (estimator == "EKF")
                outfile << std::fixed << std::setprecision(4) << sim_objects->ekf->get_state_estimate()(i) << ", ";
        }
        for (size_t i = 0; i < sim_objects->hexa->get_H_control().rows(); i++)
            outfile << std::fixed << std::setprecision(4) << z_ref(i) << ", ";
        for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
            outfile << std::fixed << std::setprecision(4) << gain_loss(i) << ", ";
        for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
        {
            if (estimator == "AEKF")
                outfile << std::fixed << std::setprecision(4) << sim_objects->aekf->get_fault_estimate()(i) << ", ";
            else if (estimator == "EKF")
                outfile << std::fixed << std::setprecision(4) << 0 << ", ";
        }
        for (size_t i = 0; i < sim_objects->hexa->Nu; i++)
            outfile << std::fixed << std::setprecision(4) << input_vec(i) << ", ";
        
        outfile << std::endl;
    }

    return 0;
}