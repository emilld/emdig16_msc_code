#include <iostream>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <random>
// #include <sstream>
#include <algorithm>

#include "../include/hexacopter.hpp"
#include "../include/mpc2.hpp"
#include "../include/aekf.hpp"
#include "../include/ekf.hpp"

#include "nlopt.hpp"

#include <eigen3/Eigen/Dense>

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

void printProgress(double current, double all) {
    double percentage = current / all;
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf("\r%3d%% [%.*s%*s] %1.2f / %1.2f", val, lpad, PBSTR, rpad, "", current, all);
    fflush(stdout);
}

struct SimObjects
{
    SimObjects()
    {
        double m = 1.5,         // mass in kg
            L = 0.268,        // arm length in m
            Jxx = 0.027465, // moment of inertia around 'x' axis in kg�m^2
            Jyy = 0.027465, // moment of inertia around 'y' axis in kg�m^2
            Jzz = 0.053868,    // moment of inertia around 'z' axis in kg�m^2
            g = 9.81,        // gravity
            b = 0.01,
            alpha = 1; /* for lpf on inputs in MPC*/;
        dt = 0.01;

        hexa = new Hexacopter(m, L, Jxx, Jyy, Jzz, g, b, dt);

        // Making the state and fault estimator.
        double ss = 0.001;
        // double lambdaa = 0.995;
        double lambdaa = 0.98;
        // double lambdaa = 0.995;
        // double lambdaa = 0.993;
        // double lambdaa = 0.995;
        // AEKF aekf(out.hexa, ss, lambdaa);
        aekf = new AEKF(*hexa, ss, lambdaa);
        ekf = new EKF(*hexa);

        Eigen::VectorXd Qf(hexa->Nx), Rf(hexa->get_C().rows());
        Qf << 0.1, 0.01, 0.1, 0.01, 0.1, 0.01, 0.5, 0.1, 0.5, 0.1, 0.1, 0.01; // System noise, [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]
        Rf << 0.5, 0.1, 0.5, 0.1, 0.2, 0.1, 1, 1, 0.3; // Measurement noise, [phi, phidot, theta, thetadot, psi, psidot, x, y, z]'
        // // Rf *= 1000;
        // Qf /= 100;
        // Rf /= 10;
        // Qf.setConstant(1e-6);
        // Rf.setConstant(1e-2);
        // Qf << 1e-8, 5e-7,
        //       1e-8, 5e-7,
        //       1e-8, 5e-7,
        //       1e-6, 1e-6, 
        //       1e-6, 1e-6,
        //       1e-6, 1e-6;

        // Rf << 1e-3, 5e-2, 
        //       1e-3, 5e-2, 
        //       1e-3, 5e-2, 
        //       0.1, 0.1, 0.1;
    

        Eigen::MatrixXd QQf = Qf.asDiagonal();
        Eigen::MatrixXd RRf = Rf.asDiagonal();

        aekf->set_Q(QQf);
        aekf->set_R(RRf);
        ekf->set_Q(QQf);
        ekf->set_R(RRf);

        std::cout << "Q = \n" << aekf->get_Q() << std::endl;
        std::cout << "R = \n" << aekf->get_R() << std::endl;

        // Making the MPC.
        // int N = 20; // Horizon
        // int N = ceil(1 / dt);
        int N_control = 1;
        // int N_prediction = 100;
        int N_prediction = 100;

        // ModelPredictiveControl2 mpc(hexa, N_prediction, N_control);
        mpc = new ModelPredictiveControl2(*hexa, N_prediction, N_control);
        Eigen::VectorXd q(hexa->get_H_control().rows()), r(hexa->Nu);
        // q <<   /*pos x*/ 1, /*pos y*/ 1,  /*yaw*/ 0.05, /*velocity*/ 100;
        // r <<   10,          10;
        // q <<   100, 100, 100, 100, 200, 100, 200, 100, 200, 100, 200, 100;
        // q <<   200, 200, 200, 200;
        q.setConstant(200);
        // // q *= 10;
        r <<   0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
        // r <<   10, 10, 10, 10, 10, 10;
        // r <<   0.01, 0.01, 0.01, 0.01, 0.01, 0.01;
        // q << 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1;
        // r <<   0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
        mpc->create_G_matrix(q, r);

        std::cout << "Setting bounds." << std::endl;

        // Set bounds
        Eigen::VectorXd lb(hexa->Nu), ub(hexa->Nu);
        lb <<  0, 0, 0, 0, 0, 0;
        // lb <<  1, 1, 1, 1, 1, 1;
        // lb *= 2.5;
        ub <<  8, 8, 8, 8, 8, 8;
        // lb << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -0.7, -2;
        // ub <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  0.7,  2;

        mpc->set_bounds(lb, ub);

        // change in input bound
        Eigen::VectorXd dinput_bound(hexa->Nu);
        dinput_bound.setConstant(4 * dt); // Soft limit the rotors to 4 N/s
        // dinput_bound.setConstant(0.5 * dt); // Soft limit the rotors to 0.5 N/s
        mpc->set_dinput_bound(dinput_bound);

        // Eigen::VectorXd lb_states(hexa.Nx), ub_states(hexa.Nx);
        // lb_states << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, 0.1;
        // ub_states <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL, 4.9;

        // bike.set_sim_state_bound(lb_states, ub_states);
        mpc->print_bounds();

        // Set reference:
        Eigen::VectorXd z_ref(hexa->get_H_control().rows()); // tracks yaw, x, y, z
        // z_ref << 0, 0, 0, 0, M_PI_2, 0, 3, 0, -2, 0, 10, 0;
        z_ref << 0, 0, 0, 1;
        // z_ref << 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 10, 0;
        
        mpc->set_Z_ref(z_ref);

        mpc->use_state_constraint();

        // dt = dt;

        return;
    }

    Hexacopter * hexa;
    ModelPredictiveControl2 * mpc;
    AEKF * aekf;
    EKF * ekf;
    double dt;
};
