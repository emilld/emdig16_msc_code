#include "../include/aekf.hpp"
#include <vector>
#include <iostream>

AEKF::AEKF(
    Plant & p,
    double _ss, 
    double _lambdaa)
{
    plant = &p;
    ss = _ss;
    lambdaa = _lambdaa;

    int N_states = plant->Nx;
    int N_inputs = plant->Nu;
    int N_measurements = plant->get_C().rows();
    
    Q = Eigen::MatrixXd(N_states, N_states);
    Q.setIdentity();

    R = Eigen::MatrixXd(N_measurements, N_measurements);
    R.setIdentity();

    X_hat = Eigen::VectorXd(N_states);
    theta_hat = Eigen::VectorXd(N_inputs);
    theta_hat_last = Eigen::VectorXd(N_inputs);

    X_hat.setZero();
    theta_hat.setZero();
    theta_hat_last.setZero();

    Pplus.setIdentity(N_states, N_states);
    Pplus *= 10; 
    S.setIdentity(N_inputs, N_inputs);
    S *= ss;

    upsilon      = Eigen::MatrixXd(N_states, N_inputs); 
    upsilon.setZero();
    upsilon_last = Eigen::MatrixXd(N_states, N_inputs); 
    upsilon_last.setZero();

    first_measurement = true;
}

void AEKF::init(Eigen::VectorXd Xhat_init, Eigen::VectorXd thetahat_init)
{
    X_hat = Xhat_init;
    theta_hat = thetahat_init;
}

void AEKF::set_Q(Eigen::MatrixXd & aQ) 
{
    Q =  aQ;
}

void AEKF::set_R(Eigen::MatrixXd & aR) 
{
    R = aR;
}

Eigen::MatrixXd AEKF::get_Q() 
{
    return Q;
}

Eigen::MatrixXd AEKF::get_R() 
{
    return R;
}

void AEKF::update(
    Eigen::VectorXd & y,         
    Eigen::VectorXd & input_vec, 
    double & dt) 
{
    // Get linear model.
    Eigen::MatrixXd C = plant->get_C();
    if (first_measurement)
    {
        first_measurement = false;
        
        X_hat = C.transpose() * y;
    }

    Eigen::MatrixXd Fxhat = plant->get_Fxhat(X_hat, input_vec, theta_hat, dt);
    Eigen::MatrixXd Fu = plant->get_Fu(X_hat, input_vec, theta_hat, dt);
    Eigen::MatrixXd Psi_system = -Fu * input_vec.asDiagonal();

    // std::cout << "Fu = \n" << Fu << std::endl;
    // std::cout << input_vec.asDiagonal().toDenseMatrix() << std::endl;
    // std::cout << Psi_system << std::endl;

    Eigen::MatrixXd I(Fxhat.rows(), Fxhat.rows());
    I.setIdentity();

    // Calculate Kalman gain and error covariance matrix
    Eigen::MatrixXd Pmin = Fxhat * Pplus * Fxhat.transpose() + Q;

    // std::cout << C << std::endl;
    // std::cout << Pmin << std::endl;
    // std::cout << R << std::endl;

    Eigen::MatrixXd Sigma = C * Pmin * C.transpose() + R;

    // std::cout << "Sigma = \n" << Sigma << std::endl;

    // std::cout << "Pmin = \n" << Pmin << std::endl;
    // std::cout << "Ct = \n" << C.transpose() << std::endl;
    // std::cout << "Sigmainv = \n" << Sigma.inverse() << std::endl;
    Eigen::MatrixXd KF = Pmin * C.transpose() * Sigma.inverse();
    // std::cout << KF << std::endl;

    Pplus = (I - KF * C) * Pmin;

    // std::cout << "Pplus = \n" << Pplus << std::endl;

    // Calculate the fault estimation Sigma(k + 1)
    upsilon = (I - KF * C) * Fxhat * upsilon_last + 
              (I - KF * C) * Psi_system;

    // std::cout << "upsilon = \n" << upsilon << std::endl;
    
    Eigen::MatrixXd omega = C * Fxhat * upsilon_last + C * Psi_system;
    // std::cout << "omega = \n" << omega << std::endl;

    Eigen::MatrixXd Lambdaa = lambdaa * Sigma + omega * S * omega.transpose();
    // std::cout << "Lambdaa = \n" << Lambdaa << std::endl;
    Lambdaa = Lambdaa.inverse();
    // std::cout << "Lambdaa = \n" << Lambdaa << std::endl;

    Eigen::MatrixXd Gamma = S * omega.transpose() * Lambdaa;
    // std::cout << "Gamma = \n" << Gamma << std::endl;

    S = (1 / lambdaa) * S - 
        (1 / lambdaa) * S * omega.transpose() * Lambdaa * omega * S;
    // std::cout << "S = \n" << S << std::endl;
    
    upsilon_last = upsilon;
    // std::cout << "upsilon_last = \n" << upsilon_last << std::endl;

    // Predict
    X_hat = plant->compute_dynamics(X_hat, input_vec, theta_hat, dt);
    // std::cout << "X_hat = \n" << X_hat << std::endl;

    // Error
    Eigen::VectorXd ytilde = y - C * X_hat;

    theta_hat = theta_hat_last + Gamma * ytilde;

    // for (size_t i = 0; i < theta_hat.rows(); i++)
    // {
    //     if (theta_hat(i) < 0)
    //     {
    //         theta_hat(i) = 0;
    //     }
    //     else if (theta_hat(i) > 1)
    //     {
    //         theta_hat(i) = 1;
    //     }
    // }
    // theta_hat.cwiseMin(1).cwiseMax(0);

    // Correction
    X_hat = X_hat + KF * ytilde + upsilon * (theta_hat - theta_hat_last);
    theta_hat_last = theta_hat;
}

Eigen::VectorXd AEKF::get_state_estimate() 
{
    return X_hat;   
}

Eigen::VectorXd AEKF::get_fault_estimate() 
{
    return theta_hat;
}

AEKF::~AEKF() 
{
    
}

