#include "../include/anomaly_detector.hpp"

AnomalyDetector::AnomalyDetector(
    std::string filename_w_res_res,
    std::string filename_w_in_res,
    std::string filename_w_res_out,
    Eigen::VectorXd rho1,
    Eigen::VectorXd rho2,
    Eigen::VectorXd scale,
    int numberOfHiddenNeurons,
    int numberOfInputNeurons,
    int numberOfOutputNeurons,
    double leakingRate)
        : rho1(rho1), rho2(rho2), scale(scale), 
        ESN(numberOfInputNeurons, 
            numberOfOutputNeurons, 
            numberOfHiddenNeurons, leakingRate, 0.001)
{
    for (size_t i = 0; i < numberOfInputNeurons; i++)
    {
        anomaly_detectors.push_back(
            PeakRecognition(rho1(i), rho2(i))
        );
    }

    last_prediction = Eigen::VectorXd(numberOfInputNeurons);
    last_prediction.setZero();    

    ESN::load_from_file(filename_w_res_res, filename_w_in_res, filename_w_res_out);
    // std::cout << "AnomalyDetector constructor successful" << std::endl;
}

AnomalyOutput AnomalyDetector::get_update(Eigen::VectorXd & u) 
{
    Eigen::VectorXd prediction_error = (u - last_prediction).cwiseAbs();
    // std::cout << "pred err " << prediction_error << std::endl;

    // std::cout << u.size() << " " << scale.size() << std::endl;
    // std::cout << u.cwiseQuotient(scale) << std::endl;

    Eigen::VectorXd esn_output = ESN::predict(u.cwiseQuotient(scale));
    // std::cout << "esn_output :" << esn_output.rows() << " " << esn_output.cols() 
    //     << esn_output.transpose() << std::endl;
    last_prediction = esn_output.cwiseProduct(scale);
    // last_prediction = ESN::predict(u.cwiseQuotient(scale)).cwiseProduct(scale);
    // std::cout << last_prediction << std::endl;

    std::vector<bool> anomaly;

    for (size_t i = 0; i < u.rows(); i++)
    {
        anomaly.push_back(anomaly_detectors.at(i).update(prediction_error(i)));
    }
    
    AnomalyOutput retval;
    retval.anomaly = anomaly;
    retval.input = u;
    retval.last_prediction = last_prediction;
    retval.prediction_error = prediction_error;

    return retval;
}
