#include "../include/peak_recognition.hpp"
#include <stdlib.h>     /* abs */
#include <numeric>

PeakRecognition::PeakRecognition(double rho1, double rho2) 
    : rho1(rho1), rho2(rho2)
{
    dataPoints = std::vector<double>(2, 0); // Vector of two zeros.
    dataPointsAvg = std::vector<double>(2, 0); // Vector of two zeros.
    anomalyHistory = std::vector<double>(5, 0); // Vector of five zeros.
}

bool PeakRecognition::update(double datapoint)
{
    bool retval;

    // Move the array for the avg forward.
    dataPoints[1] = dataPoints[0];
    dataPoints[0] = datapoint;
    
    stage1();
    retval = stage2();
    retval = stage3(retval);

    return retval;
}

void PeakRecognition::stage1()
{
    double new_avg = 0.5 * (abs(dataPoints[1]) + abs(dataPoints[0]));
    dataPointsAvg[1] = dataPointsAvg[0];
    dataPointsAvg[0] = new_avg;
}

bool PeakRecognition::stage2()
{
    return (dataPointsAvg[0] > rho1) && (dataPointsAvg[1] > rho2);
}

bool PeakRecognition::stage3(bool result)
{
    anomalyHistory.insert(anomalyHistory.begin(), result);
    anomalyHistory.pop_back();

    double sum_of_elements = std::accumulate(anomalyHistory.begin(), 
                                anomalyHistory.end(), 
                                decltype(anomalyHistory)::value_type(0));

    if (sum_of_elements >= anomalyHistory.size())
        std::fill(anomalyHistory.begin(), anomalyHistory.end(), 0);
    
    if (sum_of_elements > 1)
        result = false;

    return result;
}

PeakRecognition::~PeakRecognition()
{
}