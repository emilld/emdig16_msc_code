#include "../include/mpc2.hpp"
#include <nlopt.hpp>
#include <chrono>

ModelPredictiveControl2::ModelPredictiveControl2(Plant & p, int _prediction_horizon, int _control_horizon)
{
    // std::cout << "what" << std::endl
    plant = &p;
    horizon_prediction = _prediction_horizon;
    horizon_control = _control_horizon;

    // Optimizer init.
    //opt = nlopt::opt(nlopt::LN_SBPLX, N * plant->Nu);
    // opt = nlopt::opt(nlopt::LD_SLSQP, horizon_control * plant->Nu);
    opt = nlopt::opt(nlopt::LD_SLSQP, horizon_control * plant->Nu);

    // opt = nlopt::opt(nlopt::LD_AUGLAG, horizon_control * plant->Nu);
    // opt = nlopt::opt(nlopt::LN_BOBYQA, horizon_control * plant->Nu);
    // opt = nlopt::opt(nlopt::LN_NEWUOA_BOUND, horizon_control * plant->Nu);
    // opt = nlopt::opt(nlopt::LD_MMA, horizon_control * plant->Nu);
    // nlopt::opt local_opt(nlopt::LN_BOBYQA, horizon_control * plant->Nu);
    
    // opt = nlopt::opt(nlopt::LN_COBYLA, horizon_control * plant->Nu);
    // opt = nlopt::opt(nlopt::LN_AUGLAG, horizon_control * plant->Nu);

    // opt = nlopt::opt(nlopt::LD_MMA, horizon_control * plant->Nu);
    
    opt.set_min_objective(objective_function, &obj_data);

    opt.set_xtol_rel(1e-6); // Matlab: StepTolerance.
    // opt.set_xtol_abs(1e-4); // Matlab: StepTolerance.
    // local_opt.set_xtol_rel(1e-4); // Matlab: StepTolerance.
    // opt.set_xtol_rel(1e-6);
    // opt.set_ftol_rel(1e-12);
    // opt.set_ftol_rel(1e-15);
    opt.set_ftol_rel(1e-6);
    
    // opt.set_maxeval(100);
    // opt.set_maxeval(500);
    // opt.set_maxeval(250);
    // opt.set_maxeval(1e20);
    // opt.set_maxeval(10);
    // opt.set_maxeval(300);
    // local_opt.set_maxeval(200);

    // default matlab sqp iter
    // 10*max(numberOfVariables, numberOfInequalities + numberOfBounds)
    // opt.set_maxeval(10 * horizon_control * plant->Nu);

    // opt.set_local_optimizer(local_opt);

    input_vec_filtered = Eigen::VectorXd(plant->Nu);
    input_vec_filtered.setZero();

    // tol_constraint = std::vector<double>(plant->Nx, 1e-8);
    // tol_constraint = std::vector<double>(plant->Nx, 1e-1);
    // tol_constraint = std::vector<double>(plant->Nx, 5e-1);
    // tol_constraint_dU = std::vector<double>(plant->Nu, 1);


    // obj_data.G = Eigen::MatrixXd((plant->Nx + plant->Nu) * N, (plant->Nx + plant->Nu) * N);
    // obj_data.z_ref = Eigen::VectorXd((plant->Nx + plant->Nu) * N);
    obj_data.Q_weights = new Eigen::DiagonalMatrix<double, Eigen::Dynamic>();
    obj_data.R_weights = new Eigen::DiagonalMatrix<double, Eigen::Dynamic>();
    obj_data.z_ref = new Eigen::VectorXd;
    obj_data.init_state = new Eigen::VectorXd;
    obj_data.Fxhat = new Eigen::MatrixXd;
    obj_data.Fu = new Eigen::MatrixXd;
    obj_data.horizon_control = horizon_control;
    obj_data.horizon_prediction = horizon_prediction;
    obj_data.plant = plant;
    obj_data.dt = 0;

    constraint_data.state = new Eigen::VectorXd;
    constraint_data.gain_loss = new Eigen::VectorXd;
    constraint_data.plant = plant;
    constraint_data.horizon_control = horizon_control;
    constraint_data.horizon_prediction = horizon_prediction;
    constraint_data.dt = 0;

    last_opt_result = std::vector<double>(plant->Nu);
}

Eigen::VectorXd ModelPredictiveControl2::update(
    const Eigen::MatrixXd state,
    const Eigen::MatrixXd input_vec,
    const Eigen::MatrixXd gain_loss,
    const double & dt)
{
    Eigen::MatrixXd Fxhat = plant->get_Fxhat(state, input_vec, gain_loss, dt);
    Eigen::MatrixXd Fu = plant->get_Fu(state, input_vec, gain_loss, dt);
    Eigen::VectorXd init_state = state;
    Eigen::VectorXd gl = gain_loss;
    Eigen::VectorXd init_input_vec = input_vec;

    obj_data.Fxhat = &Fxhat;
    obj_data.Fu    = &Fu;
    obj_data.init_state = &init_state;
    obj_data.dt = dt;
    obj_data.gain_loss = &gl;
    obj_data.init_input = &init_input_vec;
    obj_data.dinput_bound = &dinput_bounds;

    if (state_constraint)
    {
        constraint_data.dt = dt;
        constraint_data.gain_loss = &gl;
        constraint_data.state = &init_state;
        constraint_data.init_input = &init_input_vec;
        constraint_data.dinput_bound = &dinput_bounds;
    }

    // std::cout << *obj_data.Fxhat << "\n" << 
    //              *obj_data.Fu << "\n" << std::endl;

    std::vector<double> z_init(horizon_control * plant->Nu, 0);
    
    // std::cout << "\n";
    // for (size_t j = 0; j < N; j++)
    // {
    //     for (size_t i = 0; i < plant->Nu; i++)
    //     {
    //         z_init.at(i + j * plant->Nu) = input_vec(i);
    //         // std::cout << z_init.at(i + j * plant->Nu) << ", ";
    //     }
    // }
    for (size_t i = 0; i < plant->Nu; i++)
    {
        z_init.at(i) = sqrt(input_vec(i));
        // z_init.at(i) = 4;
    }

    // std::cout << std::endl;
    // std::cout << "z_init \n" << input_vec << std::endl;

    double minf;
    // std::chrono::high_resolution_clock::time_point start, end;

    try
    {
        // std::cout << "Before optimizer" << std::endl;
        
        // for (double x : z_init)
        //     std::cout << x << ", ";

        // start = std::chrono::high_resolution_clock::now();
        opt.optimize(z_init, minf);
        // end = std::chrono::high_resolution_clock::now();

        // double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();
        // double time_elapsed = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
        // // std::cout << "loop took " << time_elapsed << " miliseconds to execute\n";
        // std::cout << "loop took " << time_elapsed << " microseconds to execute\n";

        // std::cout << "after" << std::endl;

        // Eigen::VectorXd tmp = Eigen::Map<Eigen::VectorXd>(z_init.data(), z_init.size());
        // std::cout << "result = \n" << tmp << std::endl;
        // std::cout << "minimum value = \n" << minf << std::endl;
    }
    catch (std::exception &e)
    {
        std::cout << "nlopt failed: " << e.what() << std::endl;
        // z_init = opt.last_optimum_value();
    
        for (size_t i = 0; i < plant->Nu; i++)
        {
            z_init[i] = last_opt_result[i];
            // std::cout << out(i) << " : " << z_init[i] << std::endl;
        }
        // std::cout << z_init.transpose() << std::endl;
        for (auto x : z_init)
            std::cout << x << ", ";
        std::cout << std::endl;
    }

    Eigen::VectorXd out(plant->Nu);

    // std::cout << "\n";
    for (size_t i = 0; i < plant->Nu; i++)
    {
        out(i) = z_init[i];
        last_opt_result[i] = z_init[i];
        // std::cout << out(i) << " : " << z_init[i] << std::endl;
    }

    return out;
}

Eigen::VectorXd ModelPredictiveControl2::update(
    const Eigen::MatrixXd state,
    const Eigen::MatrixXd input_vec,
    const Eigen::MatrixXd gain_loss,
    const double & dt,
    const double & alpha)
{
    Eigen::VectorXd raw_input_vec = update(state, input_vec, gain_loss, dt);

    // Append to our filtered data.   
    input_vec_filtered = alpha * raw_input_vec + (1 - alpha) * input_vec_filtered;
    return input_vec_filtered;
}

void ModelPredictiveControl2::use_state_constraint()
{
    state_constraint = true;
    // opt.add_inequality_mconstraint(model_constraint, &constraint_data, tol_constraint);
    // opt.add_inequality_mconstraint(dinput_constraint, &constraint_data, tol_constraint_dU);
}

void ModelPredictiveControl2::model_constraint(
    unsigned m, // size of result
    double * result, // Where output values should be stored.
    unsigned n, // Size of x
    const double * x, // The parameters to be optimized
    double * grad, // Output array for gradient.
    void * f_data // Extra data
)
{
    model_constraint_data * params = reinterpret_cast<model_constraint_data*>(f_data);
    Eigen::VectorXd input = Eigen::VectorXd::Map(&x[0], params->plant->Nu); // Copy the first Nu values from the input vector.
    // Eigen::VectorXd input = Eigen::VectorXd::Map(&x[0], n);

    Eigen::VectorXd cons(m);
    cons = params->plant->get_state_constraint(
        *params->state, 
        input, 
        *params->gain_loss, 
        params->dt
    );
    
    Eigen::Map<Eigen::VectorXd>(result, m) = cons;

    if (grad)
    {
        // std::cout << "Size of result: " << m << ", size of x: " << n << std::endl;
        // Eigen::MatrixXd jac(m, params->plant->Nu * params->N);
        Eigen::MatrixXd jac(m, n);
        jac.block(0, 0, m, params->plant->Nu) = params->plant->get_state_constraint_jac(
            *params->state, 
            input,
            *params->gain_loss, 
            params->dt
        );

        Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(grad, jac.rows(), jac.cols()) = jac;
    }
}

Eigen::VectorXd ModelPredictiveControl2::dinput_constraint_inner(
    Eigen::VectorXd & last_input,
    Eigen::VectorXd & input,
    Eigen::VectorXd & bounds,
    double dt
)
{
    Eigen::VectorXd deltaInput = input - last_input;
    return deltaInput.cwiseAbs() - bounds;
}

void ModelPredictiveControl2::dinput_constraint(
    unsigned m, // size of result
    double * result, // Where output values should be stored.
    unsigned n, // Size of x
    const double * x, // The parameters to be optimized
    double * grad, // Output array for gradient.
    void * f_data // Extra data
)
{
    model_constraint_data * params = reinterpret_cast<model_constraint_data*>(f_data);
    Eigen::VectorXd input = Eigen::VectorXd::Map(&x[0], params->plant->Nu); // Copy the first Nu values from the input vector.
    // Eigen::VectorXd input = Eigen::VectorXd::Map(&x[0], n);
    Eigen::VectorXd last_input = *params->init_input;
    // Eigen::VectorXd delta_input = input - *params->init_input;
    Eigen::VectorXd bound = *params->dinput_bound;

    Eigen::VectorXd cons(m);
    cons = dinput_constraint_inner(last_input, input, bound, params->dt);

    // std::cout << cons.transpose() << std::endl;
    
    Eigen::Map<Eigen::VectorXd>(result, m) = cons;

    if (grad)
    {
        // std::cout << "Size of result: " << m << ", size of x: " << n << std::endl;
        // Eigen::MatrixXd jac(m, params->plant->Nu * params->N);
        Eigen::MatrixXd jac(m, params->plant->Nu), jac_out(m, n);
        jac.setZero();

        // std::cout << jac << std::endl;

        double h = 1e-8,
            hinv = 1e8,
            tmp_h = h,
            tmp;

        // Eigen::VectorXd f0 = dinput_constraint_inner(last_input, input, bound, params->dt);
        Eigen::VectorXd f0 = cons;
        Eigen::VectorXd fd;

        Eigen::VectorXd input_vec_tmp;

        for (size_t i = 0; i < jac.cols(); i++)
        {
            input_vec_tmp = input;
            tmp = input_vec_tmp(i) + h;
            tmp_h = tmp - input_vec_tmp(i);
            input_vec_tmp(i) += tmp_h;
            fd = dinput_constraint_inner(last_input, input_vec_tmp, bound, params->dt);
            // std::cout << "fd\n" << fd << std::endl;
            jac(Eigen::all, i) = (fd - f0) / tmp_h;
            // std::cout << jac(Eigen::all, i).transpose() << std::endl;
        }

        jac_out.block(0, 0, m, params->plant->Nu) = jac;
        Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>(grad, jac_out.rows(), jac_out.cols()) = jac_out;

        // std::cout << "jac\n" << jac << std::endl;
    }
}

void ModelPredictiveControl2::create_G_matrix(
    Eigen::VectorXd & q, // penalties on states, length = Nx
    Eigen::VectorXd & r) // penalties on inputs, length = Nu
{
    assert(q.rows() == plant->get_H_control().rows());
    assert(r.rows() == plant->Nu);

    obj_data.Q_weights->diagonal() = q;
    obj_data.R_weights->diagonal() = r;

    // // Eigen::MatrixXd G((plant->Nx + plant->Nu) * N, (plant->Nx + plant->Nu) * N);
    // obj_data.G = Eigen::MatrixXd((plant->Nx + plant->Nu) * N, (plant->Nx + plant->Nu) * N);
    // for (size_t i = 0; i < N; i++)
    // {
    //     obj_data.G(Eigen::seqN(i * plant->Nx, plant->Nx),
    //                Eigen::seqN(i * plant->Nx, plant->Nx)) = q.asDiagonal();

    //     obj_data.G(Eigen::seqN(plant->Nx * N + i * plant->Nu, plant->Nu),
    //                Eigen::seqN(plant->Nx * N + i * plant->Nu, plant->Nu)) = r.asDiagonal();
    // }
    // obj_data.G_sparse = obj_data.G.sparseView();
}

void ModelPredictiveControl2::set_Z_ref(Eigen::VectorXd & _z_ref)
{
    // Set reference for the states
    assert(_z_ref.rows() == plant->get_H_control().rows());

    z_ref = _z_ref;
    obj_data.z_ref = &z_ref;
}

void ModelPredictiveControl2::set_bounds(
    const Eigen::VectorXd & lb,
    const Eigen::VectorXd & ub)
{
    assert(lb.rows() == ub.rows());
    assert(lb.rows() == plant->Nu);

    // std::cout << lb << std::endl;
    // Make lower and upper bounds
    std::vector<double> lb_vec(horizon_control * plant->Nu), ub_vec(horizon_control * plant->Nu);
    // Eigen::Map<Eigen::VectorXd>(lb_vec.data(), lb.rows(), lb.cols()) = lb;
    // Eigen::Map<Eigen::VectorXd>(ub_vec.data(), ub.rows(), ub.cols()) = ub;
    // De skal gentages N gange!
    for (size_t i = 0; i < horizon_control; i++)
    {
        for (size_t nnu = 0; nnu < plant->Nu; nnu++)
        {
            lb_vec.at(i * plant->Nu + nnu) = lb(nnu); // lower bound, input
            ub_vec.at(i * plant->Nu + nnu) = ub(nnu); // upper bound, input
        }
    }
    
    // std::cout << lb << std::endl;
    // for (auto x : lb_long)
    //     std::cout << x << " ";
    // std::cout << std::endl;

    std::cout << "before setting bounds" << std::endl;
    opt.set_lower_bounds(lb_vec);
    opt.set_upper_bounds(ub_vec);
    std::cout << "after setting bounds" << std::endl;
}

void ModelPredictiveControl2::print_bounds()
{
    std::vector<double> lb = opt.get_lower_bounds();
    for (auto x : lb)
        std::cout << x << " ";
    std::cout << std::endl;

    std::vector<double> ub = opt.get_upper_bounds();
    for (auto x : ub)
        std::cout << x << " ";
    std::cout << std::endl;
}

void ModelPredictiveControl2::set_dinput_bound(
    const Eigen::VectorXd & bound
)
{
    dinput_bounds = bound;
    constraint_data.dinput_bound = &dinput_bounds;
}

Eigen::VectorXd ModelPredictiveControl2::get_dinput_bound()
{
    return dinput_bounds;
}

double ModelPredictiveControl2::inner_objective_function(
    const std::vector<double> & x,
    void * my_func_data
)
{
    objective_params * params = reinterpret_cast<objective_params*>(my_func_data);

    Eigen::VectorXd last_input(params->plant->Nu, 1);
    Eigen::VectorXd delta_input(params->plant->Nu, 1);
    last_input = *params->init_input;
    Eigen::VectorXd state(params->plant->Nx, 1), state_err(params->plant->Nx, 1);
    state = *params->init_state;
    // std::cout << "\ninit state " << state.transpose() << std::endl;

    double sum_cost = 0;
    // Eigen::VectorXd sum_jac(params->plant->Nu, 1);
    // sum_jac.setZero();

    // std::cout << std::endl;
    // for (auto xxx : x)
    //     std::cout << xxx << ", ";
    // std::cout << std::endl;
    // std::cout << "\n" << std::endl;
    Eigen::MatrixXd gain_loss_diag = params->gain_loss->asDiagonal();
    Eigen::MatrixXd identity(params->plant->Nu, params->plant->Nu);
    identity.setIdentity();

    Eigen::MatrixXd Fxhat, Fu;

    Eigen::VectorXd tracked_variables;
    Eigen::VectorXd current_input(params->plant->Nu);

    // for(size_t i = 0; i < params->N; i++)
    for(size_t i = 0; i < params->horizon_prediction - 1; i++)
    {
        // input << x[i * params->N + params->plant->Nu];
        // std::cout << "input " << i << " : ";
        if (i < params->horizon_control)
        {
            for (size_t nnn = 0; nnn < params->plant->Nu; nnn++)
            {
                current_input(nnn) = x[i * params->plant->Nu + nnn];
                delta_input(nnn) = current_input(nnn) - last_input(nnn);
                // last_input(nnn) = x[i * params->plant->Nu + nnn];
                // std::cout << input(nnn) << ", ";
            }
            
            // std::cout << "soft constraint err: " << err.sum() << std::endl;
            // soft constraint on dinput
             // Eigen::VectorXd err = delta_input.cwiseAbs() - *params->dinput_bound;
            if (i == 0)
            {
                Eigen::VectorXd err = dinput_constraint_inner(last_input, current_input, *params->dinput_bound, params->dt);
                
                err = (
                        Eigen::VectorXd::Ones(err.rows()).array() + 
                        (20. * err).array().exp()
                    ).array().log().matrix() / 20.;
                // err = (
                //         Eigen::VectorXd::Ones(err.rows()).array() + 
                //         err.array().exp()
                //     ).array().log().matrix();
                // sum_cost += err.transpose() * params->R_weights->toDenseMatrix() * err;
                // sum_cost += 0.1 * err.sum();
                // err = err.cwiseMax(0);
                // sum_cost += params->R_weights->diagonal()[0] * err.lpNorm<Eigen::Infinity>();
                sum_cost += err.lpNorm<Eigen::Infinity>();
            }

            for (size_t nnn = 0; nnn < params->plant->Nu; nnn++)
            {
                last_input(nnn) = current_input(nnn);
                // std::cout << input(nnn) << ", ";
            }
        }
        // std::cout << std::endl;
        // std::cout << "\n" << delta_input.transpose() << std::endl;

        // // Simulate next state:
        // Fxhat = params->plant->get_Fxhat(state, last_input, *params->gain_loss, params->dt);
        // Fu = params->plant->get_Fu(state, last_input, *params->gain_loss, params->dt);
        
        // state = Fxhat * state + Fu * (identity - gain_loss_diag) * last_input;

        // state = *params->Fxhat * state + *params->Fu * (identity - gain_loss_diag) * last_input; // I know it say 'last' but now it's the current.
        state = params->plant->compute_dynamics(state, last_input, *params->gain_loss, params->dt);
        // std::cout << "\n" << params->plant->get_H_control() << std::endl;
        tracked_variables = params->plant->get_H_control() * state;
        // std::cout << "new state \n" << state.transpose() << std::endl;
        // std::cout << "\n" << tracked_variables.transpose() << std::endl;
        
        // Calculate cost and update the sum.
        // state_err = state - *params->z_ref;
        state_err = tracked_variables - *params->z_ref;
        // std::cout << "\n" << state_err.transpose() << std::endl;
        // std::cout << params->z_ref.transpose() << std::endl;
        
        sum_cost = sum_cost + state_err.transpose() * *params->Q_weights * state_err;

        if (i < params->horizon_control)
        {
        //     // sum_cost = sum_cost + 0.5 * delta_input.transpose() * *params->R_weights * last_input;  
            sum_cost = sum_cost + delta_input.transpose() * *params->R_weights * delta_input;  
        }

        // // soft constraint on state
        if (i == 0)
        {
            Eigen::VectorXd statecons = params->plant->get_state_constraint(state, last_input, *params->gain_loss, params->dt);
            statecons = (
                    Eigen::VectorXd::Ones(statecons.rows()).array() + 
                    (20. * statecons).array().exp()
                ).array().log().matrix() / 20.;

            // sum_cost += params->Q_weights->diagonal()[0] * statecons.lpNorm<Eigen::Infinity>();
            sum_cost += statecons.lpNorm<Eigen::Infinity>();
        }
        // sum_cost += statecons.lpNorm<Eigen::Infinity>();

        // sum_cost = sum_cost + 0.5 * ((identity - gain_loss_diag) * last_input).transpose() * *params->R_weights * (identity - gain_loss_diag) * last_input;  
                    // 0.5 * input.transpose() * *params->R_weights * input;  
        // std::cout << sum_cost << std::endl;

        // std::cout << params->Q_weights.toDenseMatrix() << std::endl;
        // std::cout << params->R_weights.toDenseMatrix() << std::endl;
        // std::cout << "sum_jac " << sum_jac.transpose() << std::endl;
    }

    // Fxhat = params->plant->get_Fxhat(state, last_input, *params->gain_loss, params->dt);
    // Fu = params->plant->get_Fu(state, last_input, *params->gain_loss, params->dt);
    // // // state = *params->Fxhat * state + *params->Fu * (identity - gain_loss_diag) * last_input; // I know it say 'last' but now it's the current.
    // state = Fxhat * state + Fu * (identity - gain_loss_diag) * last_input;
    // state = *params->Fxhat * state + *params->Fu * (identity - gain_loss_diag) * last_input;
    state = params->plant->compute_dynamics(state, last_input, *params->gain_loss, params->dt);

    tracked_variables = params->plant->get_H_control() * state;
    state_err = tracked_variables - *params->z_ref;
    sum_cost = sum_cost + state_err.transpose() * *params->Q_weights * state_err;
    // sum_cost *= 0.5;

    // std::cout << "sum_cost = " << sum_cost << std::endl;

    return sum_cost;
}

// void ModelPredictiveControl2::naive_jac_thread(
//     const std::vector<double> & x, 
//     std::vector<double> & grad, 
//     void * my_func_data, 
//     int i,
//     double f0,
//     double h,
//     double hinv)
// {   
//     double fd;
//     std::vector<double> xd = x;

//     xd[i] += h;
//     fd = inner_objective_function(xd, my_func_data);
//     grad[i] = hinv * (fd - f0);
// }

double ModelPredictiveControl2::objective_function(
    const std::vector<double> & x,
    std::vector<double> & grad,
    void * my_func_data)
{
    /*
     * Quadratic objective function.
     * f(z) = 0.5 * z.T * G * z
     */

    // std::cout << "sum_cost " << sum_cost << std::endl;
    // std::cout << "sum_jac " << sum_jac.transpose() << std::endl;
    // Eigen::VectorXd jac(x.size());
    double f0 = inner_objective_function(x, my_func_data);

    if (!grad.empty())
    {
        // Naive numerical jacobian.

        double h = 1e-8,
            hinv = 1e8,
            tmp_h = h,
            tmp;
        // std::vector<std::thread> threads;

        double fd;

        for (size_t i = 0; i < x.size(); i++)
        { // TODO: Make this multithreaded.
            std::vector<double> xd = x;
            tmp = xd[i] + h;
            tmp_h = tmp - xd[i];
            xd[i] += tmp_h;
            fd = inner_objective_function(xd, my_func_data);
            grad[i] = (fd - f0) / tmp_h;
            // grad[i] = ;
        }
    }

    return f0;
}

ModelPredictiveControl2::~ModelPredictiveControl2()
{

}