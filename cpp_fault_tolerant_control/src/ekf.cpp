#include "../include/ekf.hpp"
#include <vector>
#include <iostream>

EKF::EKF(
    Plant & p)
{
    plant = &p;

    int N_states = plant->Nx;
    int N_inputs = plant->Nu;
    int N_measurements = plant->get_C().rows();
    
    Q = Eigen::MatrixXd(N_states, N_states);
    Q.setIdentity();

    R = Eigen::MatrixXd(N_measurements, N_measurements);
    R.setIdentity();

    X_hat = Eigen::VectorXd(N_states);
    X_hat.setZero();

    Pplus.setIdentity(N_states, N_states);
    // Pplus *= 1000; 

    first_measurement = true;
}

void EKF::init(Eigen::VectorXd Xhat_init)
{
    X_hat = Xhat_init;
}

void EKF::set_Q(Eigen::MatrixXd & aQ) 
{
    Q =  aQ;
}

void EKF::set_R(Eigen::MatrixXd & aR) 
{
    R = aR;
}

Eigen::MatrixXd EKF::get_Q() 
{
    return Q;
}

Eigen::MatrixXd EKF::get_R() 
{
    return R;
}

void EKF::update(
    Eigen::VectorXd & y,         
    Eigen::VectorXd & input_vec, 
    double & dt) 
{
    // Get linear model.
    Eigen::MatrixXd C = plant->get_C();
    if (first_measurement)
    {
        first_measurement = false;
        
        X_hat = C.transpose() * y;
    }

    Eigen::MatrixXd Fxhat = plant->get_Fxhat(X_hat, input_vec, dt);
    Eigen::MatrixXd I(Fxhat.rows(), Fxhat.rows());
    I.setIdentity();

    // Predict
    X_hat = plant->compute_dynamics(X_hat, input_vec, dt);
    // Calculate Kalman gain and error covariance matrix
    Eigen::MatrixXd Pmin = Fxhat * Pplus * Fxhat.transpose() + Q;

    // Update
    // Innovation or measurement residual
    Eigen::VectorXd ytilde = y - C * X_hat;
    // Innovation (or residual) covariance
    Eigen::MatrixXd Sigma = C * Pmin * C.transpose() + R;
    // Near-optimal Kalman gain 
    Eigen::MatrixXd KF = Pmin * C.transpose() * Sigma.inverse();
    // Updated state estimate
    X_hat = X_hat + KF * ytilde;
    // Update covariance estimate
    Pplus = (I - KF * C) * Pmin;
}

Eigen::VectorXd EKF::get_state_estimate() 
{
    return X_hat;   
}

EKF::~EKF() 
{
    
}

