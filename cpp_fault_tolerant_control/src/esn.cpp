#include "../include/esn.hpp"
#include <fstream>

ESN::ESN(
    int numberOfInputs,
    int numberOfOutputs,
    int numberOfHiddenUnits,
    double leakingRate,
    double reservoirNeuronBias
) : numberOfInputs(numberOfInputs),
    numberOfHiddenUnits(numberOfHiddenUnits),
    numberOfOutputs(numberOfOutputs),
    leakingRate(leakingRate),
    reservoirNeuronBias(reservoirNeuronBias)
{
    W_in = Eigen::MatrixXd(numberOfHiddenUnits, numberOfInputs);
    W_res = Eigen::MatrixXd(numberOfHiddenUnits, numberOfHiddenUnits);
    W_out = Eigen::MatrixXd(numberOfOutputs, numberOfHiddenUnits);
    x = Eigen::VectorXd(numberOfHiddenUnits);
    x.setZero();
}

// https://stackoverflow.com/questions/34247057/how-to-read-csv-file-and-assign-to-eigen-matrix
template<typename M>
M ESN::load_csv (const std::string & path) {
    std::ifstream indata;
    indata.open(path);
    std::string line;
    std::vector<double> values;
    uint rows = 0;
    while (std::getline(indata, line)) {
        std::stringstream lineStream(line);
        std::string cell;
        while (std::getline(lineStream, cell, ' ')) {
            // std::cout << cell << ", ";
            values.push_back(std::stod(cell));
        }
        // std::cout << std::endl;
        ++rows;
    }
    return Eigen::Map<const Eigen::Matrix<typename M::Scalar, M::RowsAtCompileTime, M::ColsAtCompileTime, Eigen::RowMajor>>(values.data(), rows, values.size()/rows);
}

void ESN::load_from_file(
    std::string filename_w_res_res,
    std::string filename_w_in_res,
    std::string filename_w_res_out
)
{
    W_in  = load_csv<Eigen::MatrixXd>(filename_w_in_res);
    W_res = load_csv<Eigen::MatrixXd>(filename_w_res_res);
    W_out = load_csv<Eigen::MatrixXd>(filename_w_res_out);

    numberOfInputs = W_in.cols();
    numberOfHiddenUnits = W_res.rows();
    numberOfOutputs = W_out.rows();
    // std::cout << "W_in" << std::endl;
    // std::cout << W_in << std::endl;
    // std::cout << "W_res" << std::endl;
    // std::cout << W_res << std::endl;
    // std::cout << "W_out" << std::endl;
    // std::cout << W_out << std::endl;
}

void ESN::init_random()
{ // Just to see if the dimensions are correct. This has no proper use.
    W_in.setRandom();
    W_res.setRandom();
    W_out.setRandom();
}

void ESN::update(Eigen::VectorXd input)
{
    Eigen::VectorXd reservoirBiasVec(x.rows());
    reservoirBiasVec.setConstant(reservoirNeuronBias);

    Eigen::VectorXd tmp1 = W_in * input;
    Eigen::VectorXd tmp2 = W_res * x;
    Eigen::VectorXd tmp  = (tmp1 + tmp2 + reservoirBiasVec).array().tanh().matrix();
    x = (1 - leakingRate) * x + leakingRate * tmp;
}

Eigen::VectorXd ESN::predict(Eigen::VectorXd input) 
{
    update(input);
    return W_out * x;
}

ESN::~ESN()
{

};