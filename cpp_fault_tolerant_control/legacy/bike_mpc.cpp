#include <iomanip>
#include <iostream>
#include <vector>
#include <fstream>
#include <chrono>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>

#include <nlopt.hpp>

// constraint struct
typedef struct {
    double a, b;
} my_constraint_data;

typedef struct {
    Eigen::MatrixXd G; // Matrix of weights, q and r
    Eigen::VectorXd z_ref; // Vector of references
} objective_params;

typedef struct {
    Eigen::MatrixXd Aeq;
    Eigen::MatrixXd beq;
} model_constraint_data;

// dynamic function
Eigen::MatrixXd compute_dynamics(
    const Eigen::MatrixXd & state, 
    const Eigen::MatrixXd & input_vec, 
    const double & dt, 
    const double & L)
{
    double pos_x = state(0, 0), pos_y = state(1, 0), yaw = state(2, 0), vel = state(3, 0);
    double steering_angle = input_vec(0, 0), acc = input_vec(1, 0);

    Eigen::MatrixXd out(4, 1);

    out(0, 0) = pos_x + vel * cos(yaw) * dt;
    out(1, 0) = pos_y + vel * sin(yaw) * dt;
    out(2, 0) = yaw + vel / L * steering_angle * dt;
    out(3, 0) = vel + acc * dt;

    return out;
}

Eigen::MatrixXd get_Fxhat(
    const Eigen::MatrixXd & state, 
    const Eigen::MatrixXd & input_vec, 
    const double & dt, 
    const double & L)
{
    double pos_x = state(0, 0), pos_y = state(1, 0), yaw = state(2, 0), vel = state(3, 0);
    double steering_angle = input_vec(0, 0), acc = input_vec(1, 0);

    Eigen::MatrixXd out = Eigen::MatrixXd::Identity(4, 4);

    out(0, 2) = -vel * sin(yaw) * dt;
    out(0, 3) =        cos(yaw) * dt;
    out(1, 2) =  vel * cos(yaw) * dt;
    out(1, 3) =        sin(yaw) * dt;
    out(2, 3) = dt * steering_angle / L;

    return out;
}

Eigen::MatrixXd get_Fu(
    const Eigen::MatrixXd & state, 
    const Eigen::MatrixXd & input_vec, 
    const double & dt, 
    const double & L)
{
    double pos_x = state(0, 0), pos_y = state(1, 0), yaw = state(2, 0), vel = state(3, 0);
    double steering_angle = input_vec(0, 0), acc = input_vec(1, 0);

    Eigen::MatrixXd out(4, 2);

    out(2, 0) = dt * vel / L;
    out(3, 1) = dt;

    return out;
}

// Create linear problem
void get_Aeq_beq(
    Eigen::MatrixXd & Aeq, // Empty matrix for output
    Eigen::MatrixXd & beq, // Empty matrix for output
    const Eigen::MatrixXd & state, 
    const Eigen::MatrixXd & input_vec, 
    const int & N, 
    const double & dt, 
    const double & L)
{
    Eigen::MatrixXd A = get_Fxhat(state, input_vec, dt, L);
    Eigen::MatrixXd B = get_Fu(state, input_vec, dt, L);

    // Create the following matrix:
    // Aeq1 = 
    //   I 0 ...   0
    //  -A I ...   0
    //   . ...  I  0
    //   0 ... -A  I
    // It has size N*A.shape[0] x N*A.shape[0]

    Aeq = Eigen::MatrixXd::Identity(N * A.rows(), N * A.rows() + N * B.cols());
    
    for (size_t i = 0; i < N; i++)
    { // Go through all "blocks" of the matrix.
        // Aeq1(Eigen::seqN(i + N, N), Eigen::seqN(i + N, N)) = A;
        if (i > 0)
            Aeq(Eigen::seqN(i * A.rows(), A.rows()), 
                Eigen::seqN((i - 1) * A.rows(), A.rows())) = -A;
        // Eigen::MatrixXd tmp = Aeq1(Eigen::seqN(i * A.rows(), A.rows()), Eigen::seqN((i - 1) * A.rows(), A.rows()));
        // std::cout << "tmp \n" << tmp << std::endl;
        Aeq(Eigen::seqN(i * B.rows(), B.rows()), 
            Eigen::seqN(N * A.rows() + i * B.cols(), B.cols())) = -B;
    }

    beq = Eigen::MatrixXd::Zero(Aeq.rows(), 1);
    beq(Eigen::seqN(0, A.rows()), Eigen::all) = A * state;
}

// objective function
double myfunc(const std::vector<double> & x, std::vector<double> & grad, void * my_func_data)
{
    // Pass weight matrix G and z_ref in my_func_data
    // for (double i : x)
    //     std::cout << i << ", ";
    // std::cout << std::endl;

    Eigen::VectorXd z = Eigen::VectorXd::Map(x.data(), x.size());
    // objective_params & params = (objective_params &) my_func_data;
    objective_params * params = reinterpret_cast<objective_params*>(my_func_data);
    Eigen::MatrixXd G = params->G;
    Eigen::VectorXd z_ref = params->z_ref;

    // std::cout << G << std::endl;
    // // std::cout << z_ref << std::endl;
    // std::cout << z.rows() << " " << z.cols() << std::endl;
    // std::cout << z_ref.rows() << " " << z_ref.cols() << std::endl;
    Eigen::VectorXd zzz = z - z_ref;

    // std::cout << "z \n" << z << std::endl;
    // std::cout << "z_ref \n" << z_ref << std::endl;
    // std::cout << "zzz \n" << zzz << std::endl;

    if (!grad.empty()) 
    {
        // // grad[0] = 0.0;
        // // grad[1] = 0.5 / sqrt(x[1]);
        // double h = 1e-9, h_inv = 1e9;
        // double f_0 = zzz.transpose() * G * zzz;
        // Eigen::VectorXd x_d;
        // double f_d;
        // Eigen::VectorXd zzz_in_loop;

        // for (size_t i = 0; i < z.rows(); i++)
        // {
        //     x_d = z;
        //     x_d(i, 0) += h;

        //     zzz_in_loop = x_d - z_ref;
        //     f_d = zzz_in_loop.transpose() * G * zzz_in_loop;

        //     grad.at(i) = h_inv * (f_d - f_0);
        // }
        // Eigen::VectorXd jac = G * zzz - z_ref;
        Eigen::VectorXd jac = zzz.transpose() * G;

        for (size_t i = 0; i < x.size(); i++)
        {
            grad.at(i) = jac(i, 0);
            // std::cout << grad.at(i) << std::endl;
        }
    }

    // std::cout << "cost = " << zzz.transpose() * G * zzz << std::endl; 
    return 0.5 * zzz.transpose() * G * zzz;
}

// Constraint function
// double myconstraint(const std::vector<double> & x, std::vector<double> &  grad, void * data)
// {
//     my_constraint_data *d = reinterpret_cast<my_constraint_data*>(data);
//     double a = d->a, b = d->b;

//     if (!grad.empty()) 
//     {
//         grad[0] = 3 * a * (a * x[0] + b) * (a * x[0] + b);
//         grad[1] = -1.0;
//     }
//     return ((a * x[0] + b) * (a * x[0] + b) * (a * x[0] + b) - x[1]);
// }
// constraint function
void model_constraint(
    unsigned m, // size of result
    double * result, // Where output values should be stored.
    unsigned n, // Size of x
    const double * x, // The parameters to be optimized
    double * grad, // Output array for gradient.
    void * f_data // Extra data, in this case, our linear model Aeq and beq.
    )
{
    //n is the length of x, m is the length of result
    // The equality constraint is on the form h(x) = 0.
    // Ours look like this: Aeq * z = beq  =>  h(x) = Aeq * z - beq

    // Pass weight matrix G and z_ref in my_func_data
    Eigen::VectorXd z = Eigen::VectorXd::Map(&x[0], n);
    // objective_params & params = (objective_params &) my_func_data;
    model_constraint_data * params = reinterpret_cast<model_constraint_data*>(f_data);
    Eigen::MatrixXd Aeq = params->Aeq;
    Eigen::VectorXd beq = params->beq;

    Eigen::VectorXd h = Aeq * z - beq;
    
    // std::cout << "Aeq.rows() " << Aeq.rows() << std::endl;
    // std::cout << "z.rows() " << z.rows() << std::endl; 
    // std::cout << "m " << m << std::endl;
    // std::cout << "h.rows() " << h.rows() << std::endl; 

    // std::cout << h << std::endl;

    // std::cout << m << " " << h.rows() << std::endl;
    for (size_t i = 0; i < m; i++)
    {
        result[i] = h(i);
    }

    // for (size_t i = 0; i < m; i++)
    // {
    //     std::cout << result[i] << ", ";
    // }
    // std::cout << std::endl;    

    if (grad) 
    {
        for (size_t i = 0; i < m; i++)
        {
            for (size_t j = 0; j < n; j++)
            {
                grad[i * n + j] = Aeq(i, j);
            }
        }
        
        // Eigen::Map<Eigen::VectorXd> jac(Aeq.data(), Aeq.size());
        // for (size_t i = 0; i < jac.size(); i++)
        // {
        //     grad[i] = jac(i);
        //     std::cout << "Loop " << i << "\t\t" << grad[i] << std::endl;
        // }
        

        // // grad[0] = 0.0;
        // // grad[1] = 0.5 / sqrt(x[1]);
        // double hhh = 1e-9, hhh_inv = 1e9;
        // Eigen::VectorXd f_0 = Aeq * z - beq;
        // Eigen::VectorXd x_d;
        // Eigen::VectorXd f_d;
        // Eigen::VectorXd zzz_in_loop;

        // Eigen::VectorXd tmptmp;

        // for (size_t i = 0; i < z.rows(); i++)
        // {
        //     x_d = z;
        //     x_d(i, 0) += hhh;

        //     f_d = Aeq * x_d - beq;

        //     tmptmp = hhh_inv * (f_d - f_0);

        //     // std::cout << tmptmp.rows() << " " << n << std::endl;
        //     for (size_t j = 0; j < tmptmp.rows(); j++)
        //     {
        //         // std::cout << tmptmp(j, 0) << std::endl;
        //         grad[i * n + j] = tmptmp(j, 0);   
        //     }
        // }
    }
    
    return;
}

int main()
{
    std::chrono::high_resolution_clock::time_point start, end;

    // Example compute dynamics
    Eigen::MatrixXd state(4, 1);
    Eigen::MatrixXd input_vec(2, 1);

    state(0, 0) = 0; // pos x
    state(1, 0) = 5; // pos y
    state(2, 0) = 0; // M_PI / 3; // yaw 
    state(3, 0) = 1; //1; // velocity

    input_vec(0, 0) = M_PI/4; // M_PI / 4; // steering wheel
    input_vec(1, 0) = 0; // 2; // acceleration

    double dt = 0.01;
    double dt_mpc = 0.1;
    double L = 1; // distance from center of gravity and front wheel

    // std::cout << "pos x, pos y, yaw, velocity" << std::endl;

    // for (size_t i = 0; i < 10; i++)
    // {
    //     state = compute_dynamics(state, input_vec, dt, L);

    //     // std::cout << state(0, 0) << ", " << state(1, 0) << ", " << state(2, 0) << ", " << state(3, 0) << std::endl;
    //     std::cout << state.transpose() << std::endl;
    // }

    // Try to get Fxhat
    Eigen::MatrixXd Fxhat = get_Fxhat(state, input_vec, dt, L);
    std::cout << "Fxhat = \n" << Fxhat << std::endl;

    // Get Fu
    Eigen::MatrixXd Fu = get_Fu(state, input_vec, dt, L);
    std::cout << "Fu = \n" << Fu << std::endl;

    int N = 10;
    Eigen::MatrixXd Aeq, beq;
    get_Aeq_beq(Aeq, beq, state, input_vec, N, dt, L);

    // std::cout << "Aeq = \n" << Aeq 
    //     << "\nbeq = \n" << beq << std::endl;

    int nx = 4, nu = 2;

    // Create initial Z vector.
    // Consists of N * (states, input_vec) [N * (4 + 2)]
    std::vector<double> z_init(6 * N, 0);
    z_init[0] = 0; // pos x m
    z_init[1] = 10; // pos y m
    z_init[2] = M_PI; // # rad
    z_init[3] = 1; // # m/s

    // Reference vector:
    Eigen::VectorXd z_reference(6 * N);

    for (size_t i = 0; i < N; i++)
    {
        z_reference(nx * i)      = 0; // x pos
        z_reference(nx * i + 1)  = 0; // y pos
        z_reference(nx * i + 2)  = 0; // yaw rad
        z_reference(nx * i + 3)  = 1; // velocity 
        
        z_reference(N * nx + i * nu)  = 0; // steering angle
        z_reference(N * nx + i * nu + 1)  = 0; // acceleration
    }

    // for (double i : z_reference)
    //     std::cout << i << ", ";
    // std::cout << std::endl;
    // return 0;  
    
    // Try out optimization

    // nlopt::opt opt(nlopt::LN_COBYLA, N * (nx + nu)); // optimizer object
    nlopt::opt opt(nlopt::LD_SLSQP, N * (nx + nu)); // optimizer object
    // nlopt::opt opt(nlopt::LD_MMA, N * (nx + nu)); // optimizer object
    // nlopt::opt opt(nlopt::LD_LBFGS, N * (nx + nu)); // optimizer object
    
    // Make G-matrix of penalty values.
    Eigen::VectorXd q(nx), r(nu);
    //    posx, posy, yaw, velocity, steering, acceleration
    q <<   10,  10,   1,        10;
    r <<   0.1,          0.1;
    Eigen::MatrixXd G((nx + nu) * N, (nx + nu) * N);
    for (size_t i = 0; i < N; i++)
    {
        G(Eigen::seqN(i * nx, nx), 
          Eigen::seqN(i * nx, nx)) = q.asDiagonal();

        G(Eigen::seqN(nx * N + i * nu, nu), 
          Eigen::seqN(nx * N + i * nu, nu)) = r.asDiagonal();
    }

    objective_params f_data;
    f_data.G = G;
    f_data.z_ref = z_reference;

    std::cout << f_data.G << std::endl;
    std::cout << f_data.z_ref << std::endl;

    opt.set_min_objective(myfunc, &f_data);
    
    // Make lower and upper bounds
    std::vector<double> lb((nx + nu) * N), ub((nx + nu) * N);

    for (size_t i = 0; i < N; i++)
    { // State bounds
        lb.at(i * nx)     = -HUGE_VAL; // pos x
        ub.at(i * nx)     =  HUGE_VAL; // 
        lb.at(i * nx + 1) = -HUGE_VAL; // pos y
        ub.at(i * nx + 1) =  HUGE_VAL; // 
        lb.at(i * nx + 2) = -HUGE_VAL; // yaw
        ub.at(i * nx + 2) =  HUGE_VAL; // 
        lb.at(i * nx + 3) =  0; // vel
        ub.at(i * nx + 3) =  5; // 

        lb.at(N * nx + i * nu)     = -0.7; // Steering
        ub.at(N * nx + i * nu)     =  0.7; //
        lb.at(N * nx + i * nu + 1) =   -2; // acceleration
        ub.at(N * nx + i * nu + 1) =    2; //
    }

    // for (size_t i = 0; i < N; i++)
    // {
    //     lb.at(N * nx + i * nu)     = -0.7; // Steering
    //     ub.at(N * nx + i * nu)     =  0.7; //
    //     lb.at(N * nx + i * nu + 1) =   -2; // acceleration
    //     ub.at(N * nx + i * nu + 1) =    2; //
    // }

    std::cout << "Lower bound" << std::endl;
    for (auto x : lb)
        std::cout << x << ", ";
    std::cout << std::endl;

    std::cout << "Upper bound" << std::endl;
    for (auto x : ub)
        std::cout << x << ", ";
    std::cout << std::endl;

    opt.set_lower_bounds(lb);
    opt.set_upper_bounds(ub);    

    std::vector<std::vector<double>> plot_states;
    std::vector<std::vector<double>> plot_inputs;

    std::vector<double> input(2);

    // Model constraint
    // minimize subject to Aeq * z = beq.
    get_Aeq_beq(Aeq, beq, state, input_vec, N, dt_mpc, L);
    model_constraint_data c_model;
    c_model.Aeq = Aeq;
    c_model.beq = beq;

    std::vector<double> tol_constraint(Aeq.rows(), 1e-4);
    opt.add_equality_mconstraint(model_constraint, &c_model, tol_constraint);
    opt.set_xtol_abs(1e-4);
    opt.set_ftol_abs(1e-6);
    opt.set_maxeval(1e4);

    // now on to the mpc:
    for (double t = 0; t < 30; t += dt)
    {       
        std::cout << t << std::endl;

        // Update linear constraint
        get_Aeq_beq(c_model.Aeq, c_model.beq, state, input_vec, N, dt_mpc, L);

        // Insert state as the initial guess
        for (size_t i = 0; i < z_init.size(); i++)
        {
            if (i < 4)
                z_init.at(i) = state(i, 0);
            else if (i >= 4 && i < 6)
                z_init.at(i) = input_vec(i - 4);
            else
                z_init.at(i) = 0;
        }

        for (size_t i = 0; i < N; i++)
        {
            // f_data.z_ref(nx * i)      = t; // x pos
            // f_data.z_ref(nx * i + 1)  = 6 * cos(2 * M_PI * 0.05 * t) - 6; // y pos
            // f_data.z_ref(nx * i + 2)  =  - 2 * M_PI * 6 * 0.05 * sin(2 * M_PI * 0.05 * t); // yaw rad
            // f_data.z_ref(nx * i + 3)  = 1; // velocity 
            
            // f_data.z_ref(N * nx + i * nu)  = 0; // steering angle
            // f_data.z_ref(N * nx + i * nu + 1)  = 0; // acceleration
            f_data.z_ref(nx * i)      = 10; // x pos
            f_data.z_ref(nx * i + 1)  = 0; // y pos
            f_data.z_ref(nx * i + 2)  =  M_PI; // yaw rad
            f_data.z_ref(nx * i + 3)  =  2; // velocity 
            
            f_data.z_ref(N * nx + i * nu)  = 0; // steering angle
            f_data.z_ref(N * nx + i * nu + 1)  = 0; // acceleration
        }

        // for (auto x : f_data.z_ref)
        //     std::cout << x << ", ";
        // std::cout << std::endl;

        // return 0;
        double minf;

        
        try 
        {
            // std::cout << "Before optimizer" << std::endl;
            start = std::chrono::high_resolution_clock::now();
            opt.optimize(z_init, minf);
            end = std::chrono::high_resolution_clock::now();

            // double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();
            double time_elapsed = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();
            // std::cout << "loop took " << time_elapsed << " miliseconds to execute\n";
            std::cout << "loop took " << time_elapsed << " microseconds to execute\n";

            // std::cout << "after" << std::endl;

            // Eigen::VectorXd tmp = Eigen::Map<Eigen::VectorXd>(z_init.data(), z_init.size());
            // std::cout << "result = \n" << tmp << std::endl;
            // std::cout << "minimum value = \n" << minf << std::endl;
        }
        catch (std::exception &e)
        {
            std::cout << "nlopt failed: " << e.what() << std::endl;
        }

        // Eigen::VectorXd tmp = Eigen::Map<Eigen::VectorXd>(z_init.data(), z_init.size());
        // std::cout << "result = \n" << tmp << std::endl;
        // std::cout << "minimum value = \n" << minf << std::endl;

        input_vec(0, 0) = z_init[N * nx];
        input_vec(1, 0) = z_init[N * nx + 1];

        // Apply input to dynamics.
        state = compute_dynamics(state, input_vec, dt, L);

        // Save for plotting
        plot_states.push_back(std::vector<double> {state(0, 0), state(1, 0), state(2, 0), state(3, 0)});
        plot_inputs.push_back(std::vector<double> {input_vec(0, 0), input_vec(1, 0)});

        Eigen::VectorXd err_vec = f_data.z_ref(Eigen::seqN(0, 2)) - state(Eigen::seqN(0, 2), 0);
        
        if (err_vec.norm() < 1) // err smaller than 1 m, break.
            break;
    }

    std::ofstream state_file, actuator_file;
    state_file.open("output/state_file.csv");
    actuator_file.open("output/actuator_file.csv");

    state_file << "pos x, pos y, yaw, velocity," << std::endl;
    for (auto state : plot_states)
    {
        for (double x : state)
            state_file << x << ", ";
            // std::cout << x << ", ";
        
        // std::cout << std::endl;
        state_file << std::endl;
    }

    state_file.close();

    actuator_file << "steering wheel, acceleration," << std::endl;
    for (auto input : plot_inputs)
    {
        for (double x : input)
            actuator_file << x << ", ";
        
        actuator_file << std::endl;
    }

    actuator_file.close();

    return 0;
}