clc
clear
close all

%%
% Cost function
nx = 4;
nu = 2;
N = 2;

z = sym('z', [(nx + nu)*N 1], 'real');
z_ref = sym('z_ref', [(nx + nu)*N 1], 'real');
G = diag(sym('G', [1, (nx + nu)*N], 'real'));

f = 0.5 * (z - z_ref)' * G * (z - z_ref);

jac = jacobian(f, z);
jac = simplify(jac)

% jac_a = z' * G
jac_a = (z - z_ref)' * G

%% Linear constraint
Aeq = sym('Aeq', [nx * N, (nx + nu) * N], 'real');
beq = sym('beq', [nx * N, 1], 'real');

h = Aeq * z - beq;

h_jac = jacobian(h, z)

h_jac_a = Aeq