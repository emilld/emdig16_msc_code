#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

nx = 4
nu = 2

data_states = pd.read_csv("state_file.csv", nrows=1)
cols = data_states.columns.tolist()
cols_to_use = cols[:len(cols) - 1]
data_states = pd.read_csv("state_file.csv", usecols=cols_to_use)
data_states.columns = data_states.columns.str.strip()
print(data_states)

data_actuators = pd.read_csv("actuator_file.csv", nrows=1)
cols = data_actuators.columns.tolist()
cols_to_use = cols[:len(cols) - 1]
data_actuators = pd.read_csv("actuator_file.csv", usecols=cols_to_use)
data_actuators.columns = data_actuators.columns.str.strip()
print(data_actuators)

# states = np.asarray(data[:, 0:nx])
# print(states)
print(data_states.keys())

plt.figure()
plt.plot(data_states["pos x"], data_states["pos y"])

fig, ax = plt.subplots(2, 1)
ax[0].plot(data_states["pos x"], label="pos x")
ax[0].plot(data_states["pos y"], label="pos y")
ax[0].plot(data_states["yaw"], label="yaw")
ax[0].plot(data_states["velocity"], label="velocity")
ax[0].legend()
ax[0].grid()

ax[1].plot(data_actuators["steering wheel"], label="Steering angle [rad]")
ax[1].plot(data_actuators["acceleration"], label="Acceleration [m/s^2]")
ax[1].legend()
ax[1].grid()

plt.show()