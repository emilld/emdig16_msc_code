#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

nx = 4
nu = 2
N =  10

data = np.genfromtxt("open_loop_result_cpp.txt")
print(data)

states = data[:(N * nx)].reshape((N, nx))
print(states)

inputs = data[(N * nx):].reshape((N, nu))
print(inputs)


fig, ax = plt.subplots(2, 1)
ax[0].plot(states)
ax[0].legend(("pos x", "pos y", "yaw", "velocity"))

ax[1].plot(inputs)
ax[1].legend(("steering", "acceleration"))

plt.show()