clc
clear
close all

%% 
N = 4
Nx = 12 % 12
Nu = 6 % 6

A = sym("A", [Nx, Nx], 'real')
B = sym("B", [Nx, Nu], 'real')

input = sym("u", [Nu, N], 'real')
state = sym("x", [Nx, N], 'real')
state_ref = sym("x_ref", [Nx, 1], 'real')

Q = diag(sym("q", [Nx, 1], 'real'))
R = diag(sym("r", [Nu, 1], 'real'))

sum_cost = 0
% jac = zeros(N * Nu, 1)
jac = sym("j", [N * Nu, 1], 'real')

for i=1:N
%     state = A * state + B * input(:, i);
    
    state_err = state(:, 1) - state_ref;
    
    sum_cost = sum_cost + 0.5 * state_err' * Q * state_err + 0.5 * input(:, i)' * R * input(:, i)
%     sum_cost = sum_cost + state_err' * Q * state_err + input(:, i)' * R * input(:, i)
%     sum_jac = sum_jac + R * input;
%     sum_jac = sum_jac + (input(:, i)' * R)';
%     R * input(:, i)
%     jac(i * Nu : i * Nu + Nu - 1, :)
    jac((i - 1) * Nu + 1 : (i - 1) * Nu + 1 + Nu - 1, :) = R * input(:, i);
    
    analytical_jac = jacobian(sum_cost, input(:))'
%     analytical_jac = simplify(analytical_jac)
end

% state_err = state(:, 1) - state_ref;
% sum_cost = sum_cost + 2 * state_err' * Q * state_err;
% sum_cost = 0.5 * sum_cost

analytical_jac = jacobian(sum_cost, input(:))';
analytical_jac = simplify(analytical_jac)
jac

%% save analytic jacobian
fid = fopen("anal_jac.txt", "wt");
fprintf(fid, "%s\n", char(analytical_jac));
% fid = fopen("anal_jac.txt", "a");
% fprintf(fid, "%s\n", char(analytical_jac(2)));
% fclose(fid);