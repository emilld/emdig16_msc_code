#ifndef __HEXACOPTER_H__
#define __HEXACOPTER_H__

#include "plant.hpp"

class Hexacopter
    : public Plant
{
public:
    // Hexacopter(){};

    Hexacopter(
        double m,
        double L, 
        double Jxx, 
        double Jyy, 
        double Jzz, 
        double g, 
        double b,
        double dt
    );

    Eigen::VectorXd compute_dynamics(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    );

    Eigen::VectorXd compute_dynamics(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    );

    Eigen::MatrixXd get_Fxhat(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    );

    Eigen::MatrixXd get_Fxhat(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    );

    Eigen::MatrixXd get_Fu(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    );

    Eigen::MatrixXd get_Fu(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    );

    Eigen::MatrixXd get_C();

    Eigen::MatrixXd get_H_control();

    Eigen::VectorXd get_state_constraint(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    );

    Eigen::MatrixXd get_state_constraint_jac(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    );

    void set_sim_state_bound(
        const Eigen::VectorXd & lb,
        const Eigen::VectorXd & ub
    );

    ~Hexacopter();

private:
    double m, L, Jxx, Jyy, Jzz, g, b;
    double mInv, JxxInv, JyyInv, JzzInv;
    /*
        m       is the mass of the UAV
        L       is the arm length
        Jxx     is the moment of inertia orund the 'x' axis
        Jyy     is the moment of inertia orund the 'y' axis
        Jzz     is the moment of inertia orund the 'z' axis
        g       is gravity
        b       is the drag coefficient
    */

   double c60, s60; // Pre-computed sinus and cosinus values.


    Eigen::MatrixXd A, B, C;
    // A: Integrator matrix.
    // B: Motor Mixer Matrix.

    Eigen::VectorXd lb, ub;
};
#endif // __HEXACOPTER_H__