#ifndef __EKF_H__
#define __EKF_H__

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
 
#include "plant.hpp"

/*
 * Extended Kalman Filter for estimating the system state.
 */

class EKF
{
public:
    EKF(){};

    EKF(
        Plant & plant
    );

    void init(Eigen::VectorXd Xhat_init);

    void set_Q(Eigen::MatrixXd & Q);
    void set_R(Eigen::MatrixXd & R);

    Eigen::MatrixXd get_Q();
    Eigen::MatrixXd get_R();

    void update(
        Eigen::VectorXd & y,         // Measurements
        Eigen::VectorXd & input_vec, // System input
        double & dt
    );

    Eigen::VectorXd get_state_estimate();

    ~EKF();

private:
    Plant * plant;
    bool first_measurement;

    Eigen::MatrixXd Q, R;
    Eigen::VectorXd X_hat; // State etimate
    Eigen::MatrixXd Pplus; // Covariance for process. 
};
#endif // __EKF_H__