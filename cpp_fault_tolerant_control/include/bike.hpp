#ifndef __BIKE_H__
#define __BIKE_H__

#include "plant.hpp"

class Bike 
    : public Plant
{
public:
    Bike(double, int, int);

    Eigen::VectorXd compute_dynamics(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    );

    Eigen::VectorXd compute_dynamics(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const Eigen::VectorXd & gain_loss, // Fault on actuator
        const double & dt
    );

    Eigen::MatrixXd get_Fxhat(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    );

    Eigen::MatrixXd get_Fu(
        const Eigen::VectorXd & state, 
        const Eigen::VectorXd & input_vec,
        const double & dt
    );

    Eigen::MatrixXd get_C();

    void set_sim_state_bound(
        const Eigen::VectorXd & lb,
        const Eigen::VectorXd & ub
    );

    double * get_L()
    { return &L; }

    ~Bike();

private:
    double L, Linv;
    Eigen::VectorXd lb, ub;
};
#endif // __BIKE_H__