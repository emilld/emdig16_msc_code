#ifndef __ANOMALY_DETECTOR_H__
#define __ANOMALY_DETECTOR_H__

#include <iostream>
#include <vector>
#include <string>

#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include "../include/esn.hpp"
#include "../include/peak_recognition.hpp"

struct AnomalyOutput
{
    std::vector<bool> anomaly;
    Eigen::VectorXd input;
    Eigen::VectorXd last_prediction;
    Eigen::VectorXd prediction_error;
};

class AnomalyDetector
    : public ESN
{
public:
    AnomalyDetector(
        std::string filename_w_res_res,
        std::string filename_w_in_res,
        std::string filename_w_res_out,
        Eigen::VectorXd rho1,
        Eigen::VectorXd rho2,
        Eigen::VectorXd scale,
        int numberOfHiddenNeurons,
        int numberOfInputNeurons,
        int numberOfOutputNeurons,
        double leakingRate
    );

    AnomalyOutput get_update(Eigen::VectorXd & u);

private:
    Eigen::VectorXd rho1;
    Eigen::VectorXd rho2;
    Eigen::VectorXd scale;

    std::vector<PeakRecognition> anomaly_detectors;
    Eigen::VectorXd last_prediction;

    void update(
        Eigen::VectorXd input
    );
};

#endif // __ANOMALY_DETECTOR_H__