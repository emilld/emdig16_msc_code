#ifndef __AEKF_H__
#define __AEKF_H__

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
 
#include "plant.hpp"

/*
 * Adaptive Extended Kalman Filter for estimating the system
 * state and the fault value on the actuators (system inputs, u). 
 */

class AEKF
{
public:
    AEKF(){};

    AEKF(
        Plant & plant,
        double ss, // Something for fault estimation
        double lambdaa // Something for fault estimation
    );

    void init(Eigen::VectorXd Xhat_init, Eigen::VectorXd thetahat_init);

    void set_Q(Eigen::MatrixXd & Q);
    void set_R(Eigen::MatrixXd & R);

    Eigen::MatrixXd get_Q();
    Eigen::MatrixXd get_R();

    void update(
        Eigen::VectorXd & y,         // Measurements
        Eigen::VectorXd & input_vec, // System input
        double & dt
    );

    Eigen::VectorXd get_state_estimate();
    Eigen::VectorXd get_fault_estimate();

    ~AEKF();

private:
    Plant * plant;
    bool first_measurement;

    Eigen::MatrixXd Q, R;
    Eigen::VectorXd X_hat, theta_hat, theta_hat_last; // State etimate and fault estimate.
    Eigen::MatrixXd upsilon, upsilon_last;
    Eigen::MatrixXd S, Pplus; // Covariance for fault estimate and process. 
    double lambdaa, ss;
};
#endif // __AEKF_H__