#ifndef __MPC_H__
#define __MPC_H__

#include <iostream>
#include <vector>

#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>
#include <nlopt.hpp>

#include "plant.hpp"

// constraint struct
typedef struct {
    double a, b;
} my_constraint_data;

typedef struct {
    Eigen::MatrixXd G; // Matrix of weights, q and r
    Eigen::VectorXd z_ref; // Vector of references
    // sparse versions
    Eigen::SparseMatrix<double> G_sparse;
    Eigen::SparseMatrix<double> z_ref_sparse;
} objective_params;

typedef struct {
    Eigen::MatrixXd Aeq;
    Eigen::MatrixXd beq;
    // sparse versions
    Eigen::SparseMatrix<double> Aeq_sparse;
    Eigen::SparseMatrix<double> beq_sparse;
} model_constraint_data;

class ModelPredictiveControl
{
public:
    // ModelPredictiveControl() {};
    ModelPredictiveControl(Plant & p, int N);
    ~ModelPredictiveControl();

    Eigen::VectorXd update( // Returns the optimal next command for the system.
        const Eigen::MatrixXd & state,
        const Eigen::MatrixXd & input_vec,
        const double & dt // "Distance in time" between each N simulated points in controller.
    );

    Eigen::VectorXd update( // Returns the optimal next command for the system.
        const Eigen::MatrixXd & state,
        const Eigen::MatrixXd & input_vec,
        const Eigen::MatrixXd & gain_loss,
        const double & dt // "Distance in time" between each N simulated points in controller.
    );

    Eigen::VectorXd update( // Returns the optimal next command for the system.
        const Eigen::MatrixXd & state,
        const Eigen::MatrixXd & input_vec,
        const Eigen::MatrixXd & gain_loss,
        const double & dt, // "Distance in time" between each N simulated points in controller.
        const double & alpha // Filter coefficient.
    );

    void create_G_matrix(
        Eigen::VectorXd & q, // penalties on states, length = Nx
        Eigen::VectorXd & r  // penalties on inputs, length = Nu
    );

    Eigen::MatrixXd get_G()
    {
        return obj_data.G;
    }

    void set_Z_ref(
        const Eigen::VectorXd & z_ref
    );  // Set the reference vector. 
        // [x1, ..., xi, u1, ..., uj]

    void set_bounds(
        const Eigen::VectorXd & lb,
        const Eigen::VectorXd & ub
    ); // Set the upper and lower bounds of the variables.

    void print_bounds();

private:
    Plant * plant;
    int N;
    nlopt::opt opt;
    Eigen::VectorXd input_vec_filtered;
    
    objective_params obj_data;
    model_constraint_data constraint_data;

    // Create linear problem
    void get_Aeq_beq(
        Eigen::MatrixXd & Aeq, // Empty matrix for output
        Eigen::MatrixXd & beq, // Empty matrix for output
        const Eigen::MatrixXd & state, 
        const Eigen::MatrixXd & input_vec, 
        const double & dt
    );

    // Create linear problem
    void get_Aeq_beq(
        Eigen::MatrixXd & Aeq, // Empty matrix for output
        Eigen::MatrixXd & beq, // Empty matrix for output
        const Eigen::MatrixXd & state, 
        const Eigen::MatrixXd & input_vec, 
        const Eigen::MatrixXd & gain_loss, 
        const double & dt
    );

    static double objective_function(
        const std::vector<double> & x, 
        std::vector<double> & grad, 
        void * my_func_data
    );

    static void model_constraint(
        unsigned m, // size of result
        double * result, // Where output values should be stored.
        unsigned n, // Size of x
        const double * x, // The parameters to be optimized
        double * grad, // Output array for gradient.
        void * f_data // Extra data, in this case, our linear model Aeq and beq.
    );
};
#endif // __MPC_H__