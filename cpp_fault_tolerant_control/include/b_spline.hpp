#ifndef __B_SPLINE_H__
#define __B_SPLINE_H__

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>

class b_spline
{
public:
    b_spline(int p, Eigen::VectorXd knots);
    ~b_spline();

private:
    int p, n;
    Eigen::VectorXd knots; // Going from 0 to 1.
};

#endif // __B_SPLINE_H__