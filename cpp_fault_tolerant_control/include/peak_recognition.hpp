#ifndef __PEAK_RECOGNITION_H__
#define __PEAK_RECOGNITION_H__

#include <vector>

class PeakRecognition
{
public:
    PeakRecognition(double rho1, double rho2);
    ~PeakRecognition();

    bool update(double datapoint);

private:
    double rho1, rho2;

    std::vector<double> dataPoints,
                        dataPointsAvg,
                        anomalyHistory;


    // Three stages:
    //   1. Reduce the perturbations in the prediction error by applying a moving average on
    //       two consecutive data points. 
    //       d_avg(i) = 0.5 * (abs(d(i - 1)) + abs(d(i)))
    //   2.  Mark datapoints as anomalies by applying two different tresholds to two consecutive data points:
    //       d_avg(i) > rho1         # rho1; detect general location of anomaly
    //       d_avg(i - 1) > rho2     # only applied if the first is true. Ensure correct detection of starting point.
    //   3.  Removes extrked data points by assuming that single point anomalies will at most be marked
    //       as four data points.
    void stage1();
    bool stage2();
    bool stage3(bool result);
};



#endif // __PEAK_RECOGNITION_H__