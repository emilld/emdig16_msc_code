#ifndef __ESN_H__
#define __ESN_H__

#include <iostream>
#include <vector>
#include <string>

#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>

class ESN
{
public:
    ESN(
        int numberOfInputs,
        int numberOfOutputs,
        int numberOfHiddenUnits,
        double leakingRate,
        double reservoirNeuronBias
    );

    void load_from_file(
        std::string filename_w_res_res,
        std::string filename_w_in_res,
        std::string filename_w_res_out
    );

    void init_random();

    Eigen::VectorXd predict(
        Eigen::VectorXd input
    );

    ~ESN();
private:
    /* data */
    int numberOfInputs,
        numberOfOutputs,
        numberOfHiddenUnits;
    double leakingRate,
           reservoirNeuronBias;
    
    Eigen::MatrixXd W_in, W_res, W_out; // reservoir weights
    Eigen::VectorXd x; // reservoir state

    template<typename M>
    M load_csv(const std::string & path);

    void update(
        Eigen::VectorXd input
    );
};

#endif // __ESN_H__