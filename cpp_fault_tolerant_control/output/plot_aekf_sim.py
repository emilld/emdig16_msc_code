#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# filename = "aekf_sim_without_fault_estimation.csv"
# filename = "aekf_sim_with_fault_estimation.csv"
filename = "aekf_sim.csv"

data = pd.read_csv(filename, nrows=1)
cols = data.columns.tolist()
cols_to_use = cols[:len(cols) - 1]
data = pd.read_csv(filename, usecols=cols_to_use, dtype="float64")
data.columns = data.columns.str.strip()

state_names = ['Position, $x$',
               'Position, $y$',
               'Angle, $\\theta$',
               'Velocity, v',
               ]

fig, ax = plt.subplots(4, 1)
for i in range(4):
    ax[i].plot(data["time"], data[f"state_actual{i}"], label=state_names[i])
    ax[i].plot(data["time"], data[f"state_estimated{i}"], '--', label="Est." + state_names[i])
    ax[i].plot(data["time"], data[f"state_reference{i}"], '--', label="Ref." + state_names[i])

ax[0].plot(data["time"], data[f"state_measured{0}"], ':', label="Measured." + state_names[0])
ax[1].plot(data["time"], data[f"state_measured{1}"], ':', label="Measured." + state_names[1])
ax[3].plot(data["time"], data[f"state_measured{2}"], ':', label="Measured." + state_names[3])

for a in ax:
    a.legend()
    a.grid()

# Figure of position
plt.figure()
plt.plot(data[f"state_actual{0}"], data[f"state_actual{1}"], label="Actual position")
plt.plot(data[f"state_estimated{0}"], data[f"state_estimated{1}"], '--', label="Estimated position")
plt.plot(data[f"state_reference{0}"], data[f"state_reference{1}"], '--', label="Reference position")
plt.plot(data[f"state_measured{0}"], data[f"state_measured{1}"], '--', label="Measured position")
plt.legend()
plt.grid()
plt.axis("equal")

# fault_names = ["Steering fault", "Throttle fault"]
# plt.figure()
# for i in range(2):
#     plt.plot(data["time"], data[f"fault_actual{i}"], label=fault_names[i])
#     plt.plot(data["time"], data[f"fault_estimated{i}"], label= "Est." + fault_names[i])

# x = data[f"fault_estimated{0}"]
# print(x)
# print(np.round(x, 5))

# print(data[f"fault_estimated{0}"].to_numpy(dtype=float))

fault_names = ["Steering fault", "Throttle fault"]
plt.figure()
for i in range(2):
    plt.plot(data["time"], data[f"fault_actual{i}"], label=fault_names[i])
    plt.plot(data["time"], data[f"fault_estimated{i}"], '--', label= "Est." + fault_names[i])

plt.legend()
plt.grid()


input_names = ["Steering [rad]", "Acceleration [m/s^2]"]
plt.figure()
for i in range(2):
    plt.plot(data["time"], data[f"input{i}"], label=input_names[i])

plt.legend()
plt.grid()

plt.show()