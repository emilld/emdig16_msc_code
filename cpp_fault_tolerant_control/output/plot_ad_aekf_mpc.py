#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from matplotlib.legend import _get_legend_handles_labels

# plot options
plt.rc('pgf', texsystem='pdflatex')

plt.rc('font', family='serif', serif='Times')
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=10)
plt.rc('ytick', labelsize=10)
plt.rc('axes', labelsize=10)
plt.rc('legend', fontsize=10)

width = 3.4
height = width / 1.618

# filename = "aekf_sim_without_fault_estimation.csv"
# filename = "aekf_sim_with_fault_estimation.csv"
filename = "ad_aekf_mpc_test.csv"

data = pd.read_csv(filename, nrows=1)
cols = data.columns.tolist()
cols_to_use = cols[:len(cols) - 1]
data = pd.read_csv(filename, usecols=cols_to_use, dtype="float64")
data.columns = data.columns.str.strip()

orig_data = data
# data = data[data["time"] <= 40]

state_names = [ 'phi',
                'phid',
                'theta',
                'thetad',
                'psi',
                'psid',
                'x',
                'xd',
                'y',
                'yd',
                'z',
                'zd',
               ]

fig, ax = plt.subplots(12, 1)
for i in range(12):
    ax[i].plot(data["time"], data[f"state_actual{i}"], label=state_names[i])
    ax[i].plot(data["time"], data[f"state_estimated{i}"], '--', label="Est." + state_names[i])
    # ax[i].plot(data["time"], data[f"state_reference{i}"], '--', label="Ref." + state_names[i])

# ax[0].plot(data["time"], data[f"state_measured{0}"], ':', label="Measured." + state_names[0])
# ax[1].plot(data["time"], data[f"state_measured{1}"], ':', label="Measured." + state_names[1])
# ax[3].plot(data["time"], data[f"state_measured{2}"], ':', label="Measured." + state_names[3])

for a in ax:
    a.legend()
    a.grid()

plt.figure()
plt.plot(data["time"], data[f"state_actual11"], label=state_names[11])
plt.plot(data["time"], data[f"state_estimated11"], '--', label="Est." + state_names[11])
# plt.plot(data["time"], data[f"state_reference11"], '--', label="Ref." + state_names[11])

# Euler angles:
fig, ax = plt.subplots(3, 2)
fig.subplots_adjust(left=.10, bottom=.10, right=.97, top=.90, wspace=0.4, hspace=0.3)
ax = ax.flatten()
fig.set_size_inches(w=width * 1.5, h=height * 2)
ax[0].plot(data["time"], data[f"state_actual{0}"], '-', label="True")
ax[1].plot(data["time"], data[f"state_actual{1}"], '-')
ax[2].plot(data["time"], data[f"state_actual{2}"], '-')
ax[3].plot(data["time"], data[f"state_actual{3}"], '-')
ax[4].plot(data["time"], data[f"state_actual{4}"], '-')
ax[5].plot(data["time"], data[f"state_actual{5}"], '-')

ax[0].plot(data["time"], data[f"state_estimated{0}"], '--', label="Estimated")
ax[1].plot(data["time"], data[f"state_estimated{1}"], '--')
ax[2].plot(data["time"], data[f"state_estimated{2}"], '--')
ax[3].plot(data["time"], data[f"state_estimated{3}"], '--')
ax[4].plot(data["time"], data[f"state_estimated{4}"], '--')
ax[5].plot(data["time"], data[f"state_estimated{5}"], '--')

# ax[0].plot(data["time"], data[f"state_reference{0}"], '-.', label="Reference")
# ax[1].plot(data["time"], data[f"state_reference{1}"], '-.')
# ax[2].plot(data["time"], data[f"state_reference{2}"], '-.')
# ax[3].plot(data["time"], data[f"state_reference{3}"], '-.')
# ax[4].plot(data["time"], data[f"state_reference{0}"], '-.', label="Reference")
# ax[5].plot(data["time"], data[f"state_reference{5}"], '-.')

ax[0].plot(data["time"], data[f"state_measured{0}"], ':', label="Measured")
ax[1].plot(data["time"], data[f"state_measured{1}"], ':')
ax[2].plot(data["time"], data[f"state_measured{2}"], ':')
ax[3].plot(data["time"], data[f"state_measured{3}"], ':')
ax[4].plot(data["time"], data[f"state_measured{4}"], ':') 
ax[5].plot(data["time"], data[f"state_measured{5}"], ':')

ax[4].plot(data["time"], data[f"state_reference{0}"], '-.', label="Reference")

for a in ax:
    a.grid()
    a.get_yaxis().set_label_coords(-0.2, 0.5)

for i in [0, 2, 4]: 
    ax[i].set_ylim([-0.5, 0.5])

for i in [1, 3, 5]:
    ax[i].set_ylim([-2, 2])

ax[0].set_ylabel("Roll [rad]")
ax[2].set_ylabel("Pitch [rad]")
ax[4].set_ylabel("Yaw [rad]")

ax[1].set_ylabel("Roll rate [rad/s]")
ax[3].set_ylabel("Pitch rate [rad/s]")
ax[5].set_ylabel("Yaw rate [rad/s]")

fig.add_subplot(111, frameon=False)
# hide tick and tick label of the big axis
plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
plt.xlabel("Time [s]")
# plt.ylabel("Angle [rad] / Angle rate [rad/s]")

fig.legend(*_get_legend_handles_labels(fig.axes), loc="upper center", ncol=4)
plt.savefig(f'./plots/states_rpy_rpyrates.pdf')
# fig.tight_layout()

# Plot of the positions and the linear rates
fig, ax = plt.subplots(3, 2)
fig.subplots_adjust(left=.10, bottom=.10, right=.97, top=.90, wspace=0.4, hspace=0.3)
ax = ax.flatten()
fig.set_size_inches(w=width * 1.5, h=height * 2)
ax[0].plot(data["time"], data[f"state_actual{6}"], '-', label="True")
ax[1].plot(data["time"], data[f"state_actual{7}"], '-')
ax[2].plot(data["time"], data[f"state_actual{8}"], '-')
ax[3].plot(data["time"], data[f"state_actual{9}"], '-')
ax[4].plot(data["time"], data[f"state_actual{10}"], '-')
ax[5].plot(data["time"], data[f"state_actual{11}"], '-')

ax[0].plot(data["time"], data[f"state_estimated{6}"], '--', label="Estimated")
ax[1].plot(data["time"], data[f"state_estimated{7}"], '--')
ax[2].plot(data["time"], data[f"state_estimated{8}"], '--')
ax[3].plot(data["time"], data[f"state_estimated{9}"], '--')
ax[4].plot(data["time"], data[f"state_estimated{10}"], '--')
ax[5].plot(data["time"], data[f"state_estimated{11}"], '--')

ax[0].plot(data["time"], data[f"state_measured{6}"], ':', label="Measured")
ax[2].plot(data["time"], data[f"state_measured{7}"], ':') # Can only measure x, y, z, not the vels.
ax[4].plot(data["time"], data[f"state_measured{8}"], ':')

ax[0].plot(data["time"], data[f"state_reference{1}"], '-.', label="Reference")
# ax[1].plot(data["time"], data[f"state_reference{7}"], '-.')
ax[2].plot(data["time"], data[f"state_reference{2}"], '-.')
# ax[3].plot(data["time"], data[f"state_reference{9}"], '-.')
ax[4].plot(data["time"], data[f"state_reference{3}"], '-.')
# ax[5].plot(data["time"], data[f"state_reference{11}"], '-.')

for a in ax:
    a.grid()
    a.get_yaxis().set_label_coords(-0.2, 0.5)

for i in [0, 2]: 
    ax[i].set_ylim([-6, 6])
# ax[4].set_ylim([0, 12])

for i in [1, 3, 5]:
    ax[i].set_ylim([-4, 4])

ax[0].set_ylabel("$x$ [m]")
ax[2].set_ylabel("$y$ [m]")
ax[4].set_ylabel("$z$ [m]")

ax[1].set_ylabel("$\dot{x}$ [m/s]")
ax[3].set_ylabel("$\dot{y}$ [m/s]")
ax[5].set_ylabel("$\dot{z}$ [m/s]")

fig.add_subplot(111, frameon=False)
# hide tick and tick label of the big axis
plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
plt.xlabel("Time [s]")
# plt.ylabel("Angle [rad] / Angle rate [rad/s]")

fig.legend(*_get_legend_handles_labels(fig.axes), loc="upper center", ncol=4)
plt.savefig(f'./plots/states_pos_vel.pdf')
# fig.tight_layout()

# Figure of position
# from the top
plt.figure()
plt.subplot(311)
plt.plot(data[f"state_actual{6}"], data[f"state_actual{8}"], label="Actual position")
plt.plot(data[f"state_estimated{6}"], data[f"state_estimated{8}"], '--', label="Estimated position")
plt.plot(data[f"state_reference{1}"], data[f"state_reference{2}"], '*--', label="Reference position")
plt.plot(data[f"state_measured{6}"], data[f"state_measured{7}"], '--', label="Measured position")
plt.legend()
plt.grid()
plt.axis("equal")
plt.xlabel("x")
plt.ylabel("y")

# from the side, x-z
plt.subplot(312)
plt.plot(data[f"state_actual{6}"], data[f"state_actual{10}"], label="Actual position")
plt.plot(data[f"state_estimated{6}"], data[f"state_estimated{10}"], '--', label="Estimated position")
plt.plot(data[f"state_reference{1}"], data[f"state_reference{3}"], '*--', label="Reference position")
plt.plot(data[f"state_measured{6}"], data[f"state_measured{8}"], '--', label="Measured position")
plt.legend()
plt.grid()
plt.axis("equal")
plt.xlabel("x")
plt.ylabel("z")

# from the side, y-z
plt.subplot(313)
plt.plot(data[f"state_actual{8}"], data[f"state_actual{10}"], label="Actual position")
plt.plot(data[f"state_estimated{8}"], data[f"state_estimated{10}"], '--', label="Estimated position")
plt.plot(data[f"state_reference{2}"], data[f"state_reference{3}"], '*--', label="Reference position")
plt.plot(data[f"state_measured{7}"], data[f"state_measured{8}"], '--', label="Measured position")
plt.legend()
plt.grid()
plt.axis("equal")
plt.xlabel("y")
plt.ylabel("z")

# # 3d plot of position
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.plot(data[f"state_actual{6}"], data[f"state_actual{8}"], data[f"state_actual{10}"], label="Actual position")
# ax.plot(data[f"state_estimated{6}"], data[f"state_estimated{8}"], data[f"state_estimated{10}"], '--', label="Estimated position")
# ax.plot(data[f"state_reference{1}"], data[f"state_reference{2}"], data[f"state_reference{3}"], ':', label="Reference position")
# ax.plot(data[f"state_measured{6}"], data[f"state_measured{7}"], data[f"state_measured{8}"], '--', label="Measured position")
# # ax.set_box_aspect([1,1,1])
# # Make axes limits 
# xyzlim = np.array([ax.get_xlim3d(),ax.get_ylim3d(),ax.get_zlim3d()]).T
# XYZlim = np.asarray([min(xyzlim[0]),max(xyzlim[1])]) * 1.4
# ax.set_xlim3d(XYZlim) 
# ax.set_ylim3d(XYZlim)
# ax.set_zlim3d(XYZlim * 3 / 4)
# ax.legend()

# 3d plot of position
fig = plt.figure(figsize=(width, width))
# fig = plt.figure(figsize=plt.figaspect(0.5)*1.5)
# fig.subplots_adjust(left=.10, bottom=.10, right=.97, top=.90, wspace=0.4, hspace=0.3)
# fig.set_size_inches(w=width, h=height * 1.5)
ax = fig.add_subplot(111, projection='3d')
# ax = fig.gca(projection='3d')
# print(orig_data[f"state_actual{10}"])
ax.plot(orig_data[f"state_actual{6}"], orig_data[f"state_actual{8}"], orig_data[f"state_actual{10}"], 'r-', label="Actual")
ax.plot(orig_data[f"state_reference{1}"], orig_data[f"state_reference{2}"], orig_data[f"state_reference{3}"], 'k:', label="Reference")
# ax.set_box_aspect([1,1,1])
# Make axes limits 
# Option 1: aspect ratio is 1:1:1 in data space
ax.set_box_aspect((
    np.max(orig_data[f"state_reference{1}"]) - np.min(orig_data[f"state_reference{1}"]), 
    np.max(orig_data[f"state_reference{2}"]) - np.min(orig_data[f"state_reference{2}"]), 
    np.max(orig_data[f"state_reference{3}"])
))
# print(
#     "ptp",
#     np.ptp(orig_data[f"state_reference{1}"]), 
#     np.ptp(orig_data[f"state_reference{2}"]), 
#     np.ptp(orig_data[f"state_reference{3}"])
# )

# ax.set_xlim([-1.5, 1.5])
# ax.set_ylim([-1.5, 1.5])
# ax.set_zlim([0, 2.5])

ax.set_xlabel("$x$ [m]")
ax.set_ylabel("$y$ [m]")
ax.set_zlabel("$z$ [m]")
# ax.set_aspect('equal')
ax.legend()
plt.savefig(f'./plots/3d_pos.pdf')

fig, ax = plt.subplots(3, 1)
ax[0].plot(data[f"state_actual{6}"], label="$x$")
ax[0].plot(data[f"state_actual{7}"], label="$\dot\{x\}$")
ax[0].hlines(y=[-4, 4], xmin=0, xmax=len(data[f"state_actual{11}"]))
ax[1].plot(data[f"state_actual{8}"], label="$y$")
ax[1].plot(data[f"state_actual{9}"], label="$\dot\{y\}$")
ax[1].hlines(y=[-4, 4], xmin=0, xmax=len(data[f"state_actual{11}"]))
ax[2].plot(data[f"state_actual{10}"], label="$z$")
ax[2].plot(data[f"state_actual{11}"], label="$\dot\{z\}$")
ax[2].hlines(y=[-2, 2], xmin=0, xmax=len(data[f"state_actual{11}"]))

for a in ax:
    a.legend()
    a.grid()

# fault_names = ["Steering fault", "Throttle fault"]
# plt.figure()
# for i in range(2):
#     plt.plot(data["time"], data[f"fault_actual{i}"], label=fault_names[i])
#     plt.plot(data["time"], data[f"fault_estimated{i}"], label= "Est." + fault_names[i])

# x = data[f"fault_estimated{0}"]
# print(x)
# print(np.round(x, 5))

# print(data[f"fault_estimated{0}"].to_numpy(dtype=float))

# fault_names = [f'Motor {i + 1}' for i in range(6)]
fault_names = [f'Fault $\\vartheta_{i + 1}$' for i in range(6)]

# Estimated faults.
fig, ax = plt.subplots(3, 2)
fig.set_size_inches(w=width * 1.5, h=height * 2)
fig.subplots_adjust(left=.10, bottom=.10, right=.97, top=.90, wspace=0.4, hspace=0.3)
ax = ax.flatten()
for i in range(6):
    ax[i].plot(data["time"], data[f"fault_actual{i}"], '-')
    ax[i].plot(data["time"], data[f"fault_estimated{i}"], '--')
    # ax[i].legend()
    ax[i].set_ylim([-0.05, 0.25])
    ax[i].set_ylabel(fault_names[i])
    ax[i].grid()

fig.add_subplot(111, frameon=False)
# hide tick and tick label of the big axis
plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
plt.xlabel("Time [s]")

fig.legend(["True", "Estimated"], loc="upper center", ncol=2)
plt.savefig(f'./plots/fault_estimate.pdf')

# Plot showing all the actuator values.
# put lpf on the data so it's easier to see

alpha = 0.1
# alpha = 1.0
motor_forces_lpf = []
for i in range(6):
    motor_forces_lpf.append(
        [data[f"input{i}"][0]]
    )

    for j in range(len(data[f"input{i}"][1:])):
       motor_forces_lpf[-1].append(
           alpha * data[f"input{i}"][j + 1] + (1 - alpha) * motor_forces_lpf[-1][-1]
       )

print(motor_forces_lpf)
motor_forces_lpf = np.asarray(motor_forces_lpf)


fig = plt.figure()
fig.set_size_inches(w=width, h=height*2)

# for i in range(6):
#     # plt.plot(data["time"], data[f"input{i}"], label=f"Motor {i+1}")
#     plt.plot(data["time"], motor_forces_lpf[i, :], label=f"Motor {i+1}")
for i in range(2):
    plt.subplot(2, 1, i + 1)
    plt.plot(data["time"], motor_forces_lpf[3 * i + 0, :], '-', label=f"Motor {3 * i + 1}")
    plt.plot(data["time"], motor_forces_lpf[3 * i + 1, :], '--', label=f"Motor {3 * i + 2}")
    plt.plot(data["time"], motor_forces_lpf[3 * i + 2, :], '-.', label=f"Motor {3 * i + 3}")

    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1),
        ncol=3, fancybox=False, shadow=False)
    plt.ylabel("Force [N]")
    plt.ylim((0, 8))
    plt.grid()

plt.xlabel("Time [s]")

plt.savefig(f'./plots/motor_thrust.pdf')

# Plot with derivative of the motor thrusts.
motor_forces_derivative = []

for i in range(6):
    motor_forces_derivative.append(data[f"input{i}"])

motor_forces_derivative = np.asarray(motor_forces_derivative)
motor_forces_derivative = np.diff(motor_forces_derivative, axis=1) / 0.01
print(motor_forces_derivative)

plt.figure()
plt.plot(motor_forces_derivative.T, zorder=1)
plt.hlines(y=[-0.5, 0.5], xmin=0, xmax=4000, linewidth=2, color='k', zorder=2)
plt.grid(zorder=0)

plt.show()