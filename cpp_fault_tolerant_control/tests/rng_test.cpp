#include <random>
#include <iostream>
#include <eigen3/Eigen/Dense>

int main()
{
    std::random_device rd;
    std::mt19937 gen(rd());  //here you could also set a seed
    // std::default_random_engine gen{rd()};
    // std::uniform_real_distribution<double> dis(LO, HI);
    std::normal_distribution<double> dis(0., 1.);
    // https://stackoverflow.com/questions/35827926/eigen-matrix-library-filling-a-matrix-with-random-float-values-in-a-given-range

    for (size_t i = 0; i < 10; i++)
    {
        std::cout << dis(gen) << std::endl;
    }

    std::cout << "random vector" << std::endl;
    Eigen::MatrixXd tmp = Eigen::MatrixXd::Zero(10, 1).unaryExpr([&](float){return dis(gen);});

    std::cout << tmp << std::endl;

    return 0;
}