#include "../include/esn.hpp"
#include <string>
#include <vector>

int main(int argc, char const *argv[])
{
    std::vector<std::string> names = {"pitch_pitchrate_x_",
                                      "roll_rollrate_y_",
                                      "yaw_yawrate_z_"};
    std::vector<ESN> networks;

    for (size_t i = 0; i < names.size(); i++)
    {       
        std::cout << "iteration " << i << std::endl;

        std::string filename_w_res_res = "../save_files/" + names[i] + "weights_res_res.txt";
        std::string filename_w_in_res = "../save_files/" + names[i] + "weights_in_res.txt";
        std::string filename_w_res_out = "../save_files/" + names[i] + "weights_res_out.txt";

        ESN esn(3, 3, 50, 0.1, 0.001);
        // esn.init_random();
        esn.load_from_file(filename_w_res_res,
                           filename_w_in_res,
                           filename_w_res_out);

        networks.push_back(esn);
    }

    std::cout << "Predictions:" << std::endl;

    // for (size_t i = 0; i < networks.size(); i++)
    for (ESN network : networks)
    {
        // std::cout << "iteration " << i << std::endl;

        Eigen::VectorXd measurement(3);
        measurement << 0.1, 0.2, 0.3;
        // Eigen::VectorXd prediction = networks[i].predict(measurement);
        Eigen::VectorXd prediction = network.predict(measurement);

        std::cout << "Measurement: " << measurement << std::endl;
        std::cout << "Prediction: " << prediction << std::endl;
    }
    
    return 0;
}
