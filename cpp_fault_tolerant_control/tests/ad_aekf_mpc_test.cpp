#include <iostream>

#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <random>
// #include <sstream>
#include <algorithm>

#include "../include/hexacopter.hpp"
#include "../include/mpc2.hpp"
#include "../include/aekf.hpp"
#include "../include/anomaly_detector.hpp"

#include "nlopt.hpp"

#include <eigen3/Eigen/Dense>

Hexacopter hexa_init(double dt)
{
    // Initializing hexacopter
    double m = 1.5,         // mass in kg
        L = 0.268,        // arm length in m
        Jxx = 0.027465, // moment of inertia around 'x' axis in kg�m^2
        Jyy = 0.027465, // moment of inertia around 'y' axis in kg�m^2
        Jzz = 0.053868,    // moment of inertia around 'z' axis in kg�m^2
        g = 9.81,        // gravity
        b = 0.01,
        dt_mpc = 0.01, 
        // dt_mpc = 0.01,
        alpha = 1; /* for lpf on inputs in MPC*/;

    Hexacopter hexa(m, L, Jxx, Jyy, Jzz, g, b, dt);
    std::cout << hexa.Nu << " " << hexa.Nx << std::endl;
    return hexa;
}

AEKF aekf_init(Hexacopter & hexa)
{
    // Making the state and fault estimator.
    double ss = 0.001;
    double lambdaa = 0.992;
    // double lambdaa = 0.99;
    AEKF aekf(hexa, ss, lambdaa);

    Eigen::VectorXd Qf(hexa.Nx), Rf(hexa.get_C().rows());
    Qf << 0.1, 0.01, 0.1, 0.01, 0.1, 0.01, 0.5, 0.1, 0.5, 0.1, 0.1, 0.01; // System noise, [phi, phidot, theta, thetadot, psi, psidot, x, xdot, y, ydot, z, zdot]
    Rf << 0.5, 0.1, 0.5, 0.1, 0.2, 0.1, 1, 1, 0.3; // Measurement noise, [phi, phidot, theta, thetadot, psi, psidot, x, y, z]'
    // Rf *= 1000;

    Eigen::MatrixXd QQf = Qf.asDiagonal();
    Eigen::MatrixXd RRf = Rf.asDiagonal();

    aekf.set_Q(QQf);
    aekf.set_R(RRf);

    std::cout << "Q = \n" << aekf.get_Q() << std::endl;
    std::cout << "R = \n" << aekf.get_R() << std::endl;

    return aekf;
}

ModelPredictiveControl2 mpc_init(Hexacopter & hexa, double dt)
{
    // Making the MPC.
    // int N = 20; // Horizon
    // int N = ceil(1 / dt);
    int N_control = 1;
    // int N_prediction = 100;
    int N_prediction = 100;

    ModelPredictiveControl2 mpc(hexa, N_prediction, N_control);
    Eigen::VectorXd q(hexa.get_H_control().rows()), r(hexa.Nu);
    // q <<   /*pos x*/ 1, /*pos y*/ 1,  /*yaw*/ 0.05, /*velocity*/ 100;
    // r <<   10,          10;
    // q <<   100, 100, 100, 100, 200, 100, 200, 100, 200, 100, 200, 100;
    // q <<   200, 200, 200, 200;
    q.setConstant(200);
    // // q *= 10;
    r <<   0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
    // r <<   10, 10, 10, 10, 10, 10;
    // r <<   0.01, 0.01, 0.01, 0.01, 0.01, 0.01;
    // q << 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1;
    // r <<   0.1, 0.1, 0.1, 0.1, 0.1, 0.1;
    mpc.create_G_matrix(q, r);

    std::cout << "Setting bounds." << std::endl;

    // Set bounds
    Eigen::VectorXd lb(hexa.Nu), ub(hexa.Nu);
    lb <<  0, 0, 0, 0, 0, 0;
    // lb <<  1, 1, 1, 1, 1, 1;
    // lb *= 2.5;
    ub <<  8, 8, 8, 8, 8, 8;
    // lb << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, -0.7, -2;
    // ub <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  HUGE_VAL,  0.7,  2;

    mpc.set_bounds(lb, ub);

    // change in input bound
    Eigen::VectorXd dinput_bound(hexa.Nu);
    // dinput_bound.setConstant(0.5 * dt); // Soft limit the rotors to 4 N/s
    dinput_bound.setConstant(0.5 * dt); // Soft limit the rotors to 0.5 N/s
    mpc.set_dinput_bound(dinput_bound);

    // Eigen::VectorXd lb_states(hexa.Nx), ub_states(hexa.Nx);
    // lb_states << -HUGE_VAL, -HUGE_VAL, -HUGE_VAL, 0.1;
    // ub_states <<  HUGE_VAL,  HUGE_VAL,  HUGE_VAL, 4.9;

    // bike.set_sim_state_bound(lb_states, ub_states);
    mpc.print_bounds();

    return mpc;
}

int main(int argc, char *argv[])
{
    std::cout << "Beginning" << std::endl;

    double sim_time = 40,
           dt = 0.01;

    if (argc > 1) sim_time = atof(argv[1]);
    
    Hexacopter hexa = hexa_init(dt);
    AEKF aekf = aekf_init(hexa);
    ModelPredictiveControl2 mpc = mpc_init(hexa, dt);

    // Set reference:
    Eigen::VectorXd z_ref(hexa.get_H_control().rows()); // tracks yaw, x, y, z
    z_ref << 0, 0, 0, 2;
    
    mpc.set_Z_ref(z_ref);

    // Initialize state and input vector.
    Eigen::VectorXd state(hexa.Nx);
    state.setZero();
    Eigen::VectorXd input_vec(hexa.Nu);
    input_vec.setZero();
    // state(10) = 20;
    // input_vec << 4, 4.2, 4, 4, 4, 4.1;
    // input_vec << 0, 0, 0, 0, 0, 0;
    // input_vec = (ub - lb) / 2;

    // Gain loss vector.
    Eigen::VectorXd gain_loss(hexa.Nu);
    gain_loss.setZero();
    
    Eigen::MatrixXd C = hexa.get_C();

    // Example simulation with non-complete measurement.
    Eigen::VectorXd y(C.rows()); 
    y.setZero();

    // Measurement anomalies.
    Eigen::VectorXd y_anomalies(y.rows());
    y_anomalies << 5. * M_PI / 180., 
                  10. * M_PI / 180.,
                   5. * M_PI / 180., 
                  10. * M_PI / 180.,
                   5. * M_PI / 180., 
                  10. * M_PI / 180.,
                   1., 
                   1., 
                   1.;

    std::random_device rd;
    // std::mt19937 gen(rd());  //here you could also set a seed
    std::default_random_engine gen{rd()};
    // std::uniform_real_distribution<double> dis(LO, HI);
    std::normal_distribution<double> dis(0., 1.);
    // https://stackoverflow.com/questions/35827926/eigen-matrix-library-filling-a-matrix-with-random-float-values-in-a-given-range
    
    Eigen::VectorXd process_noise(C.cols()), sensor_noise(C.rows());

    std::ofstream outfile;
    outfile.open("../output/ad_aekf_mpc_test.csv");
    // outfile << "time, state_actual1, state_actual2, state_actual3, <, state_estimated, fault_actual, fault_estimated" << std::endl;
    outfile << "time, ";

    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << "state_actual" << i << ", ";
    for (size_t i = 0; i < y.rows(); i++)
        outfile << "state_measured" << i << ", ";
    for (size_t i = 0; i < hexa.Nx; i++)
        outfile << "state_estimated" << i << ", ";
    for (size_t i = 0; i < hexa.get_H_control().rows(); i++)
        outfile << "state_reference" << i << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << "fault_actual" << i << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << "fault_estimated" << i << ", ";
    for (size_t i = 0; i < hexa.Nu; i++)
        outfile << "input" << i << ", ";
    
    outfile << std::endl;

    std::chrono::high_resolution_clock::time_point start, end;
    mpc.use_state_constraint();

    double sin_freq = 2.0 * M_PI * 0.1; // 0.01 Hz

    for (double t = 0; t < sim_time; t += dt)
    {
        gain_loss << 0., 0., 0., 0., 0., 0.;
        if (t > 10)
            gain_loss << 0.1, 0.05, 0.2, 0.15, 0.1, 0.07;

        // Simulate system and take a measurement.
        sensor_noise = aekf.get_R() * Eigen::MatrixXd::Zero(C.rows(), 1).unaryExpr([&](float){return dis(gen);});
        state = hexa.compute_dynamics(state, input_vec, gain_loss, dt); // Simulate.
        // std::cout << "sensor noise: " << sensor_noise.transpose() << std::endl; 
        y = C * state + dt * sensor_noise; // Take measurement with sensor noise.

        // Add anomalies
        std::cout << "fmod(t, 2.) " <<  std::fmod(t, 2.) << std::endl;

        // if (std::fmod(t, 2.) - dt <= 0)
        // {
        //     std::cout << "Every 2s" << std::endl;
        //     y += y_anomalies;
        // }
        if (t > 30 && t < 35) // 5 s increase attack
        {
            y(0) *= 10; 
        }

        // Update the estimator
        aekf.update(y, input_vec, dt);

        // std::cout << "[" << t <<  "]: z est: " << 
        //     aekf.get_state_estimate()(10) << std::endl; //<< " " << 
            // aekf.get_fault_estimate().transpose() << std::endl;
        Eigen::VectorXd state_est = aekf.get_state_estimate();
        std::cout << "[" << t << "]: " <<
            "(x, y, z): (" << state_est(6) << ","
                           << state_est(8) << ","
                           << state_est(10) <<
            "), (r, p, y): (" << state_est(0) << ","
                           << state_est(2) << ","
                           << state_est(4) << ")" << std::endl;

        // Set mpc reference and update mpc
        z_ref(0) = 0.;
        z_ref(1) = 1. * cos(sin_freq * t);
        z_ref(2) = 1. * sin(sin_freq * t);
        z_ref(3) = 2.;

        mpc.set_Z_ref(z_ref);

        start = std::chrono::high_resolution_clock::now();
        input_vec = mpc.update(aekf.get_state_estimate(), input_vec, aekf.get_fault_estimate(), dt);
        end = std::chrono::high_resolution_clock::now();
        double time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count();

        std::cout << ", elapsed: " << time_elapsed << " ms" << std::endl;

        //
        outfile << t + dt << ", ";

        for (size_t i = 0; i < hexa.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << state(i) << ", ";
        for (size_t i = 0; i < y.rows(); i++)
            outfile << std::fixed << std::setprecision(4) << y(i) << ", ";
        for (size_t i = 0; i < hexa.Nx; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_state_estimate()(i) << ", ";
        for (size_t i = 0; i < hexa.get_H_control().rows(); i++)
            outfile << std::fixed << std::setprecision(4) << z_ref(i) << ", ";
        for (size_t i = 0; i < hexa.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << gain_loss(i) << ", ";
        for (size_t i = 0; i < hexa.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << aekf.get_fault_estimate()(i) << ", ";
        for (size_t i = 0; i < hexa.Nu; i++)
            outfile << std::fixed << std::setprecision(4) << input_vec(i) << ", ";
        
        outfile << std::endl;
    }

    std::cout << "Ending" << std::endl;
}