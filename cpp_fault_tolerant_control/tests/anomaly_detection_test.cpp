#include "../include/esn.hpp"
#include "../include/anomaly_detector.hpp"
#include <string>
#include <vector>

int main(int argc, char const *argv[])
{
    std::vector<std::string> names = {"pitch_pitchrate_x_",
                                      "roll_rollrate_y_",
                                      "yaw_yawrate_z_"};
    // std::vector<std::string> names = {"pitch_pitchrate_x_"};
                                     
    std::vector<AnomalyDetector> networks;

    for (size_t i = 0; i < names.size(); i++)
    {       
        std::cout << "iteration " << i << std::endl;

        std::string filename_w_res_res = "../save_files/" + names[i] + "weights_res_res.txt";
        std::string filename_w_in_res = "../save_files/" + names[i] + "weights_in_res.txt";
        std::string filename_w_res_out = "../save_files/" + names[i] + "weights_res_out.txt";

        std::cout << filename_w_res_res << std::endl;
        std::cout << filename_w_in_res << std::endl;
        std::cout << filename_w_res_out << std::endl;

        // ESN esn(3, 3, 50, 0.1, 0.001);
        // // esn.init_random();
        // esn.load_from_file(filename_w_res_res,
        //                    filename_w_in_res,
        //                    filename_w_res_out);
        Eigen::VectorXd rho1(3), rho2(3), scale(3);
        rho1 << 0.1, 0.2, 0.5;
        rho2 << 0.001, 0.001, 0.001;
        scale << M_PI_2, 2. * M_PI, 100.;

        AnomalyDetector ad(filename_w_res_res,
                           filename_w_in_res,
                           filename_w_res_out,
                           rho1, 
                           rho2,
                           scale,
                           50,
                           3,
                           3,
                           0.1
                           );

        networks.push_back(ad);
    }

    std::cout << "Predictions:" << std::endl;

    // for (size_t i = 0; i < networks.size(); i++)
    for (AnomalyDetector ad : networks)
    {
        // std::cout << "iteration " << i << std::endl;

        Eigen::VectorXd measurement(3);
        measurement << 0.1, 0.2, 0.3;
        // Eigen::VectorXd prediction = networks[i].predict(measurement);
        AnomalyOutput prediction = ad.get_update(measurement);

        std::cout << "Measurement: " << measurement << std::endl;
        // std::cout << "Prediction: " << prediction << std::endl;
        std::cout << "Anomaly: \n";
        for (bool b : prediction.anomaly)
            std::cout << b << ", ";
        std::cout << std::endl;

        std::cout << "Input passtrough: \n" << prediction.input << std::endl;
        std::cout << "Last Prediction: \n" << prediction.last_prediction << std::endl;
        std::cout << "Prediction Error: \n" << prediction.prediction_error << std::endl;
    }
    
    return 0;
}
