#include <iostream>
#include <Eigen/Geometry> 


struct EulerAngles {
    double roll, pitch, yaw;
};

EulerAngles ToEulerAngles(Eigen::Quaterniond q) {
    EulerAngles angles;

    // roll (x-axis rotation)
    double sinr_cosp = 2 * (q.w() * q.x() + q.y() * q.z());
    double cosr_cosp = 1 - 2 * (q.x() * q.x() + q.y() * q.y());
    angles.roll = std::atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = 2 * (q.w() * q.y() - q.z() * q.x());
    if (std::abs(sinp) >= 1)
        angles.pitch = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        angles.pitch = std::asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = 2 * (q.w() * q.z() + q.x() * q.y());
    double cosy_cosp = 1 - 2 * (q.y() * q.y() + q.z() * q.z());
    angles.yaw = std::atan2(siny_cosp, cosy_cosp);

    return angles;
}

int main()
{
	Eigen::Quaterniond q1(-0.9995, 0.0097, -0.0096, 0.0299);
	auto euler_angles = q1.normalized().toRotationMatrix().eulerAngles(0, 1, 2);

	std::cout << euler_angles.transpose() << std::endl;
	
	// new

	Eigen::Quaterniond q2(0.9995, -0.0097, 0.0096, -0.0299);
	euler_angles = q2.normalized().matrix().eulerAngles(0, 1, 2);

	std::cout << q2.matrix() << std::endl;
	std::cout << euler_angles.transpose() << std::endl;
	
	std::cout << "Custom implementaiton" << std::endl;
	EulerAngles rpy1 = ToEulerAngles(q1);
	EulerAngles rpy2 = ToEulerAngles(q2);
	std::cout << rpy1.roll << ", " << rpy1.pitch << ", " << rpy1.yaw << std::endl;
	std::cout << rpy2.roll << ", " << rpy2.pitch << ", " << rpy2.yaw << std::endl;

	
	Eigen::Quaterniond q3(0.4889, 1.0347, 0.7269, -0.3034);
	std::cout << q3.normalized().toRotationMatrix().eulerAngles(0, 1, 2).transpose() << std::endl;
	EulerAngles rpy3 = ToEulerAngles(q3.normalized());
	
	std::cout << rpy3.roll << ", " << rpy3.pitch << ", " << rpy3.yaw << std::endl;

	std::cout << q3.normalized().matrix() << std::endl;

	return 0;
}
